import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:swim_teacher/screens/splash_screen/splash_screen.dart';

void main() {
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp])
      .then((_) {
    runApp(MaterialApp(home: SplashScreen()));
  });
}
