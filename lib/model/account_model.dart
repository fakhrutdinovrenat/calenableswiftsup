import 'package:firebase_database/firebase_database.dart';

class AccountModel {
  String status, sex, birthDate, key;

  AccountModel(this.status, this.sex, this.birthDate);

  AccountModel.fromSnapshot(DataSnapshot snapshot)
      : status = snapshot.value["status"],
        sex = snapshot.value["sex"],
        birthDate = snapshot.value['birthDate'],
        key = snapshot.key;

  toJson() {
    return {"status": status, "sex": sex, "birthDate": birthDate};
  }
}
