import 'package:firebase_database/firebase_database.dart';

class DistanceModel {
  String /*length,*/ style, inventory, key;

  int rest, repetitions;

  DistanceModel(
      /*this.length,*/ this.style, this.inventory, this.rest, this.repetitions);

  DistanceModel.fromSnapshot(DataSnapshot snapshot)
      : //length = snapshot.value["length"],
        style = snapshot.value["style"],
        inventory = snapshot.value["inventory"],
        rest = snapshot.value["rest"],
        repetitions = snapshot.value["repetitions"],
        key = snapshot.key;

  toJson() {
    return {
      //"length": length,
      "style": style,
      "inventory": inventory,
      "rest": rest,
      "repetitions": repetitions
    };
  }
}
