import 'package:firebase_database/firebase_database.dart';

class ResultsModel {
  String swimSex,
      swimTime,
      swimDist,
      key,
      swimStyle,
      swimAge,
      swimName,
      swimDate,
      swimDay,
      swimMonth;

  ResultsModel(
      this.swimSex,
      this.swimTime,
      this.swimDist,
      this.key,
      this.swimStyle,
      this.swimAge,
      this.swimName,
      this.swimDate,
      this.swimDay,
      this.swimMonth);

  ResultsModel.fromSnapshot(DataSnapshot snapshot)
      : swimSex = snapshot.value['swimSex'],
        swimTime = snapshot.value['swimTime'],
        swimDist = snapshot.value['swimDist'],
        key = snapshot.key,
        swimStyle = snapshot.value['swimStyle'],
        swimAge = snapshot.value['swimAge'],
        swimName = snapshot.value['swimName'],
        swimDate = snapshot.value['swimDate'],
        swimDay = snapshot.value['swimDay'],
        swimMonth = snapshot.value['swimMonth'];

  toJson() {
    return {
      "swimSex": swimSex,
      "swimTime": swimTime,
      "swimDist": swimDist,
      "key": key,
      "swimStyle": swimStyle,
      "swimAge": swimAge,
      "swimName": swimName,
      "swimDate": swimDate,
      "swimDay": swimDay,
      "swimMonth": swimMonth,
    };
  }
}