import 'package:flutter/material.dart';
import 'package:swim_teacher/model/strings.dart';

class FirstYscData {
  static List firstYscList = [
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.sportDiscipStarter,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.man,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.women,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(Strings.pool25m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 0.75))),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.freestyle50m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s527,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s528,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.freestyle100m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s529,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s530,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.freestyle200m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s531,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s532,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.freestyle400m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s533,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s534,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.freestyle800m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s535,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s536,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.freestyle1500m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s537,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s538,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.onBack50m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s539,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s540,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.onBack100m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s541,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s542,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.onBack200m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s543,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s544,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.breaststroke50m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s545,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s546,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.breaststroke100m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s547,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s548,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.breaststroke200m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s549,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s550,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.butterfly50m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s551,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s552,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.butterfly100m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s553,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s554,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.butterfly200m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s555,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s556,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.complexSwim100m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s557,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s558,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.complexSwim200m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s559,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s560,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.complexSwim400m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s561,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s562,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.relay4x50mFree,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s563,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s564,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.relay4x100mFree,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s565,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s566,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.relay4x200mFree,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s567,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s568,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.relay4x50mComb,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s569,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s570,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.relay4x100mComb,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s571,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s572,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(Strings.pool50m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 0.75))),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.freestyle50m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s573,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s574,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.freestyle100m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s575,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s576,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.freestyle200m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s577,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s578,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.freestyle400m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s579,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s580,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.freestyle800m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s581,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s582,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.freestyle1500m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s583,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s584,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.onBack50m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s585,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s586,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.onBack100m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s587,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s588,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.onBack200m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s589,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s590,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.breaststroke50m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s591,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s592,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.breaststroke100m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s593,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s594,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.breaststroke200m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s595,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s596,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.butterfly50m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s597,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s598,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.butterfly100m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s599,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s600,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.butterfly200m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s601,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s602,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.complexSwim200m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s603,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s604,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.complexSwim400m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s605,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s606,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.relay4x100mFree,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s607,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s608,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.relay4x200mFree,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s609,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s610,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.relay4x100mComb,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s611,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s612,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
  ];
}
