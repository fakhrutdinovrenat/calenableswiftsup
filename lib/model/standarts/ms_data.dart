import 'package:flutter/material.dart';
import 'package:swim_teacher/model/strings.dart';

class MsData {
  static List msList = [
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.sportDiscipStarter,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.man,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.women,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(Strings.pool25m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 0.75))),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.freestyle50m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s087,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s088,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.freestyle100m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s089,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s090,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.freestyle200m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s091,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s092,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.freestyle400m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s093,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s094,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.freestyle800m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s095,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s096,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.freestyle1500m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s097,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s098,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.onBack50m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s099,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s100,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.onBack100m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s101,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s102,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.onBack200m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s103,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s104,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.breaststroke50m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s105,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s106,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.breaststroke100m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s107,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s108,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.breaststroke200m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s109,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s110,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.butterfly50m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s111,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s112,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.butterfly100m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s113,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s114,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.butterfly200m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s115,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s116,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.complexSwim100m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s117,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s118,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.complexSwim200m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s119,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s120,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.complexSwim400m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s121,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s122,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.relay4x50mFree,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s123,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s124,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.relay4x100mFree,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s125,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s126,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.relay4x200mFree,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s127,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s128,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.relay4x50mComb,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s129,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s130,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.relay4x100mComb,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s131,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s132,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(Strings.pool50m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 0.75))),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.freestyle50m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s133,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s134,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.freestyle100m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s135,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s136,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.freestyle200m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s137,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s138,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.freestyle400m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s139,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s140,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.freestyle800m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s141,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s142,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.freestyle1500m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s143,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s144,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.onBack50m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s145,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s146,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.onBack100m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s147,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s148,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.onBack200m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s149,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s150,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.breaststroke50m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s151,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s152,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.breaststroke100m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s153,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s154,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.breaststroke200m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s155,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s156,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.butterfly50m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s157,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s158,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.butterfly100m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s159,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s160,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.butterfly200m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s161,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s162,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.complexSwim200m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s163,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s164,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.complexSwim400m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s165,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s166,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.relay4x100mFree,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s167,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s168,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.relay4x200mFree,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s169,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s170,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.relay4x100mComb,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s171,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s172,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
  ];
}
