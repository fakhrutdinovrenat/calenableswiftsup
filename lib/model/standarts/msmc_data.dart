import 'package:flutter/material.dart';
import 'package:swim_teacher/model/strings.dart';

class MsmcData {
  static List msmcList = [
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.sportDiscipStarter,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.man,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.women,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(Strings.pool25m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 0.75))),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.freestyle50m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s001,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s002,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.freestyle100m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s003,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s004,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.freestyle200m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s005,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s006,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.freestyle400m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s007,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s008,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.freestyle800m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s009,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s010,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.freestyle1500m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s011,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s012,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.onBack50m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s013,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s014,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.onBack100m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s015,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s016,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.onBack200m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s017,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s018,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.breaststroke50m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s019,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s020,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.breaststroke100m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s021,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s022,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.breaststroke200m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s023,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s024,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.butterfly50m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s025,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s026,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.butterfly100m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s027,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s028,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.butterfly200m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s029,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s030,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.complexSwim100m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s031,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s032,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.complexSwim200m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s033,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s034,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.complexSwim400m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s035,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s036,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.relay4x50mFree,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s037,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s038,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.relay4x100mFree,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s039,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s040,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.relay4x200mFree,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s041,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s042,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.relay4x50mComb,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s043,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s044,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.relay4x100mComb,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s045,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s046,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(Strings.pool50m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 0.75))),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.freestyle50m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s047,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s048,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.freestyle100m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s049,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s050,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.freestyle200m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s051,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s052,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.freestyle400m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s053,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s054,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.freestyle800m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s055,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s056,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.freestyle1500m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s057,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s058,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.onBack50m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s059,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s060,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.onBack100m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s061,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s062,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.onBack200m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s063,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s064,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.breaststroke50m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s065,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s066,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.breaststroke100m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s067,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s068,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.breaststroke200m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s069,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s070,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.butterfly50m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s071,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s072,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.butterfly100m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s073,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s074,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.butterfly200m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s075,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s076,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.complexSwim200m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s077,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s078,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.complexSwim400m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s079,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s080,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.relay4x100mFree,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s081,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s082,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.relay4x200mFree,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s083,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s084,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.relay4x100mComb,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s085,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s086,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
  ];
}
