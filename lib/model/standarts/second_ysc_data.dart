import 'package:flutter/material.dart';
import 'package:swim_teacher/model/strings.dart';

class SecondYscData {
  static List secondYscList = [
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.sportDiscipStarter,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.man,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.women,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(Strings.pool25m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 0.75))),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.freestyle50m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s613,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s614,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.freestyle100m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s615,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s616,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.freestyle200m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s617,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s618,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.freestyle400m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s619,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s620,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.freestyle800m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s621,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s622,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.freestyle1500m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s623,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s624,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.onBack50m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s625,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s626,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.onBack100m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s627,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s628,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.onBack200m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s629,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s630,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.breaststroke50m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s631,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s632,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.breaststroke100m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s633,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s634,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.breaststroke200m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s635,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s636,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.butterfly50m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s637,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s638,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.butterfly100m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s639,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s640,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.butterfly200m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s641,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s642,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.complexSwim100m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s643,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s644,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.complexSwim200m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s645,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s646,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.complexSwim400m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s647,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s648,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.relay4x50mFree,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s649,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s650,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.relay4x100mFree,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s651,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s652,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.relay4x200mFree,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s653,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s654,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.relay4x50mComb,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s655,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s656,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.relay4x100mComb,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s657,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s658,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(Strings.pool50m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 0.75))),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.freestyle50m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s659,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s660,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.freestyle100m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s661,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s662,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.freestyle200m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s663,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s664,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.freestyle400m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s665,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s666,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.freestyle800m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s667,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s668,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.freestyle1500m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s669,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s670,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.onBack50m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s671,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s672,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.onBack100m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s673,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s674,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.onBack200m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s675,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s676,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.breaststroke50m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s677,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s678,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.breaststroke100m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s679,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s680,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.breaststroke200m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s681,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s682,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.butterfly50m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s683,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s684,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.butterfly100m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s685,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s686,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.butterfly200m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s687,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s688,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.complexSwim200m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s689,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s690,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.complexSwim400m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s691,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s692,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.relay4x100mFree,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s693,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s694,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.relay4x200mFree,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s695,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s696,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.relay4x100mComb,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s697,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s698,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
  ];
}
