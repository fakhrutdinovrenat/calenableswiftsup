import 'package:flutter/material.dart';
import 'package:swim_teacher/model/strings.dart';

class ThirdYscData {
  static List thirdYscList = [
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.sportDiscipStarter,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.man,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.women,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(Strings.pool25m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 0.75))),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.freestyle50m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s699,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s700,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.freestyle100m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s701,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s702,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.freestyle200m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s703,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s704,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.freestyle400m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s705,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s706,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.freestyle800m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s707,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s708,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.freestyle1500m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s709,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s710,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.onBack50m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s711,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s712,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.onBack100m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s713,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s714,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.onBack200m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s715,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s716,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.breaststroke50m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s717,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s718,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.breaststroke100m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s719,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s720,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.breaststroke200m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s721,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s722,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.butterfly50m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s723,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s724,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.butterfly100m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s725,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s726,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.butterfly200m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s727,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s728,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.complexSwim100m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s729,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s730,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.complexSwim200m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s731,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s732,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.complexSwim400m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s733,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s734,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.relay4x50mFree,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s735,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s736,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.relay4x100mFree,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s737,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s738,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.relay4x200mFree,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s739,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s740,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.relay4x50mComb,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s741,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s742,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.relay4x100mComb,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s743,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s744,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(Strings.pool50m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 0.75))),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.freestyle50m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s745,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s746,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.freestyle100m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s747,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s748,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.freestyle200m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s749,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s750,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.freestyle400m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s751,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s752,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.freestyle800m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s753,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s754,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.freestyle1500m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s755,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s756,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.onBack50m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s757,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s758,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.onBack100m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s759,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s760,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.onBack200m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s761,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s762,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.breaststroke50m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s763,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s764,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.breaststroke100m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s765,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s766,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.breaststroke200m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s767,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s768,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.butterfly50m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s769,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s770,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.butterfly100m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s771,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s772,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.butterfly200m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s773,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s774,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.complexSwim200m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s775,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s776,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.complexSwim400m,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s777,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s778,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.relay4x100mFree,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s779,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s780,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.relay4x200mFree,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s781,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s782,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
    Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Text(
              Strings.relay4x100mComb,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 12.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(27, 39, 45, 1)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s783,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              Strings.s784,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 12.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
          ),
        ],
      ),
    ),
  ];
}
