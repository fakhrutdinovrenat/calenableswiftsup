class Strings {
  static String appTitle = "SwimCoach";
  static String mainScreenTitle = "Ваш личный помощник по плаванию";
  static String learnRules = "Правила";
  static String rulesTitle = "Правила вида спорта «Плавание»";
  static String swimming = "Плавание";
  static String standarts = "Нормативы";
  static String generalities = "Общие положения";
  static String participants = "Участники соревнований";
  static String management = "Руководство соревнованиями";
  static String judges = "Судьи";
  static String formation = "Формирование заплывов";
  static String startDistTiming = "Старт, дистанция, хронометраж";
  static String styles = "Стили плавания";
  static String protest = "Протест";
  static String requirements = "Требования к бассейнам";
  static String openWater = "Плавание на открытой воде";
  static String practice = "Практика";
  static String timer = "Секундомер";
  static String timerLap = "Круг";
  static String results = "Результаты";
  static String auth = "Авторизация";
  static String about = "О приложении";
  static String man = "М";
  static String women = "Ж";
  static String manResults = "муж";
  static String womenResults = "жен";
  static String welcome = "Авторизация прошла успешно :-)";
  static String hello = "Здравствуйте, ";
  static String signIn = "Sign in";
  static String signOut = "Sign out";
  static String signedOut = "Signed out";
  static String notSignInMessage =
      "Зарегистрируйтесь или войдите, чтобы начать отслеживать Ваши тренировки";
  static String goToAuth = "Перейти к авторизации";
  static String notToAuth = "Продолжить без авторизации";
  static String search = "Поиск";
  static String account = "Аккаунт";
  static String privacyPolicy = "Политика конфиденциальности";
  static String downloadPDF = "Скачать pdf";
  static String download = "Скачать";
  static String scrollUp = "Наверх";
  static String turnOnStopwatch = "Запустите, пожалуйста, секундомер";
  static String nothingToSave = "Нет результата для сохранения";
  static String turnOffStopwatch = "Остановите, пожалуйста, секундомер";
  static String writeName = "Укажите имя пловца";
  static String shortName = "Слишком короткое имя";
  static String hintName = "Имя пловца";
  static String noData = "Нет записей";
  static String ageResults = "Возраст: ";
  static String filterDist = "Дистанция";
  static String filterClear = "Очистить";
  static String filterUse = "Применить";
  static String filterStyle = "Стиль";
  static String filterSex = "Пол";
  static String filterBothSex = "М и Ж";
  static String somethingWrong = "Что-то пошло не так";
  static String accSettings = "Настройки аккаунта";
  static String birthDay = "Дата рождения";
  static String noDate = "Дата не указана";
  static String coach = "тренер";
  static String swimmer = "ученик";
  static String coaching = "Тренировки";
  static String createTraining = "Создать тренировку";
  static String resultsReference = "results";
  static String resultSaved = "Результат сохранён";
  static String accountReference = "account";
  static String noBday = "Укажите дату рождения";
  static String age = "Возраст: ";
  static String cancelCreateTraining =
      "Вы действительно хотите отменить действие?";
  static String cancelButton = "Отменить";
  static String nextStep = "Далее";
  static String trainingName = "Название";
  static String p = "П";
  static String r = "Р";
  static String n = "Н";
  static String dolphin = "Дельфин";
  static String kroll = "Кроль";
  static String fullTraining = "Полная координация";
  static String armTraining = "Руки";
  static String legTraining = "Ноги";
  static String board = "Доска";
  static String kolobashka = "Колобашка";
  static String flippers = "Ласты";
  static String brake = "Тормоз";
  static String pipe = "Трубка";
  static String paddle = "Лопатки";
  static String pipes = "Трубы";
  static String inventory = "Инвентарь";
  static String addRestOrExercise = "Добавьте дистанцию";
  static String addRestExercise = "Добавьте дистанцию";
  static String close = "Завершить";
  static String exercise = "Дистанция";
  static String restSec = "Отдых, сек";
  static String checkDist = "Дистанция не может быть равно нулю";
  static String repetitionCount = "Количество повторений";
  static String back = "Назад";
  static String trainings = "trainings";
  static String exercises = "exercises";
  static String kolobaskaAndFlippers = "Вы уверены, что хотите использовать колобашку и ласты одновременно?\n\nO_o";
  static String noTrainings = "У вас ещё нет ни одной тренировки";
  static String edit = "Редактировать";
  static String share = "Поделиться";
  static String rest = "Отдых ";
  static String sec = " сек";
  static String trainingTitle = "Тренировка";
  static String styleValidation = "Необходимо указать стиль";
  static String skip = "Пропустить";

  //Table data
  static String sPage =
      "Нормы, требования и условия их выполнения по виду спорта «плавание»";
  static String freestyle = "Вольный стиль";
  static String onBack = "На спине";
  static String breaststroke = "Брасс";
  static String butterfly = "Баттерфляй";
  static String complexSwim = "Комплексное плавание";
  static String relays = "Эстафеты";
  static String relayFreestyle = "Эстафета, вольный стиль";
  static String relayCombined = "Эстафета, комбинированная";
  static String combined = "Комбинированная";
  static String mixedFreestyle = "Смешанная вольный стиль";
  static String mixedCombined = "Смешанная комбинированная";
  static String msmcHeadOfTable = "МСМК, МС, КМС выполняются с 14 лет";
  static String status = "Статус спортивных соревнований";
  static String sportsDiscipline = "Спортивная дисциплина";
  static String requirementPlace = "Требование: занять место";
  static String olympic = "Олимпийские игры";
  static String worldCham = "Чемпионат мира";
  static String euroCham = "Чемпионат Европы";
  static String otherCham =
      "Другие международные спортивные  соревнования, включенные в ЕКП";
  static String relay = "Эстафета";
  static String openWat = "Открытая вода";
  static String relayOpenWater = "Эстафета, открытая вода";
  static String condition =
      "*Условие: если спортивное соревнование проводится по регламенту  Кубка мира\n";
  //static String mscmsHeadOfTable = "МС, КМС выполняются с 14 лет";
  static String rusCham = "Чемпионат России*";
  static String rusComp = "Первенство России**";
  static String otherRusCham =
      "Другие всероссийские спортивные соревнования, включенные в ЕКП***";
  static String openWaterMsCms =
      "Открытая вода 5 км, открытая вода 10 км, открытая вода 16 км, открытая вода 25 км и более";
  static String openWater10km = "Открытая вода 10 км (18-19 лет)";
  static String openWater7km = "Открытая вода 7,5 км (16-17 лет)";
  static String openWater5kmY = "Открытая вода 5 км (14-15 лет)";
  static String openWater5km = "Открытая вода 5 км";
  static String conditions =
      "\n*Условие: пройти дистанцию, отстав от победителя не более, чем: 5 км - 10 мин, 10 км - 20 мин, 16 км - 32 мин, 25 км - 50 мин\n"
      "**Условие: пройти дистанцию, отстав от победителя не более  чем: 5 км - 10 мин, 7,5 км - 15 мин, 10 км - 20 мин\n"
      "***Условие: пройти дистанцию, отстав от победителя не более чем на 10 мин\n\n"
      "Иные условия:\n"
      "1. Требование выполняется в водоемах и в  реках с течением. В этом случае дистанция должна быть проложена так, чтобы одна половина ее преодолевалась по течению, а другая - против.\n"
      "2. Для участия в спортивных соревнованиях  спортсмен должен достичь установленного возраста в календарный год проведения спортивных соревнований\n";
  static String sportDiscipStarter = "Дисциплина";
  static String pool25m = "Бассейн 25 метров";
  static String pool50m = "Бассейн 50 метров";
  static String freestyle50m = "Вольный стиль \n50 м";
  static String freestyle100m = "Вольный стиль \n100 м";
  static String freestyle200m = "Вольный стиль \n200 м";
  static String freestyle400m = "Вольный стиль \n400 м";
  static String freestyle800m = "Вольный стиль \n800 м";
  static String freestyle1500m = "Вольный стиль \n1500 м";
  static String onBack50m = "На спине \n50 м";
  static String onBack100m = "На спине \n100 м";
  static String onBack200m = "На спине \n200 м";
  static String breaststroke50m = "Брасс \n50 м";
  static String breaststroke100m = "Брасс \n100 м";
  static String breaststroke200m = "Брасс \n200 м";
  static String butterfly50m = "Баттерфляй \n50 м";
  static String butterfly100m = "Баттерфляй \n100 м";
  static String butterfly200m = "Баттерфляй \n200 м";
  static String complexSwim100m = "Комплексное плавание \n100 м";
  static String complexSwim200m = "Комплексное плавание \n200 м";
  static String complexSwim400m = "Комплексное плавание \n400 м";
  static String relay4x50mFree = "Эстафета \n4х50 м \nвольный стиль";
  static String relay4x100mFree = "Эстафета \n4х100 м \nвольный стиль";
  static String relay4x200mFree = "Эстафета \n4х200 м \nвольный стиль";
  static String relay4x50mComb = "Эстафета \n4х50 м \nкомбинированная";
  static String relay4x100mComb = "Эстафета \n4х100 м \nкомбинированная";
  static String justForFirst = "\n*только для спортсмена стартующего первым\n";
  static String justForOnBack =
      "**только для спортсмена стартующего первым - на спине\n";
  static String description = "Описание";
  static String writeNameOfTraining = "Укажите название тренировки";
  static String distCount = "Дистанция, м";
  static String withoutInventary = "Без инвентаря";
  static String withoutRest = "Без отдыха";

  static String distCount1 = "12,5";
  static String distCount2 = "25";
  static String distCount3 = "50";
  static String distCount4 = "100";
  static String distCount5 = "200";
  static String distCount6 = "400";
  static String distCount7 = "15";
  static String distCount8 = "20";
  static String distCount9 = "30";
  static String distCount10 = "60";
  static String distCount11 = "120";
  static String distCount12 = "5";
  static String distCount13 = "10";

  static String repetition1 = "1";
  static String repetition2 = "2";
  static String repetition3 = "3";
  static String repetition4 = "4";
  static String repetition5 = "5";
  static String repetition6 = "6";

  static String tableData01 = "50м, 100м, 200м";
  static String tableData02 = "400м, 800 м, 1500м";
  static String tableData03 = "100м, 200м, 400м";
  static String tableData04 = "4х50м, 4х100м, 4х200м";
  static String tableData05 = "4х50м, 4х100м";
  static String tableData06 = "4х50м";
  static String tableData07 = "200м, 400м";
  static String tableData08 = "4х100м, 4х200м";
  static String tableData09 = "4х100м";
  static String tableData11 = "1-8";
  static String tableData12 = "1-6";
  static String tableData13 = "1-7";
  static String tableData14 = "1-3";
  static String tableData15 = "1-3*";
  static String tableData16 = ", 1";
  static String tableData17 = "1-8(МС)";
  static String tableData18 = "1-6(МС), 7-10(КМС)";
  static String tableData19 = "1-10(КМС)";
  static String tableData20 = "1-8(КМС)";
  static String tableData21 = "1(МС), 2-3(КМС)";

  //data msmc
  static String s001 = "21,29";
  static String s002 = "24,19";
  static String s003 = "47,05";
  static String s004 = "52,66";
  static String s005 = "1:44,25";
  static String s006 = "1:54,74";
  static String s007 = "3:42,57";
  static String s008 = "4:01,47";
  static String s009 = "7:45,64";
  static String s010 = "8:16,54";
  static String s011 = "14:42,19";
  static String s012 = "16:02,75";
  static String s013 = "24,45";
  static String s014 = "27,56";
  static String s015 = "52,48";
  static String s016 = "58,91";
  static String s017 = "1:54,41";
  static String s018 = "2:06,59";
  static String s019 = "26,87";
  static String s020 = "30,62";
  static String s021 = "58,98";
  static String s022 = "1:06,06";
  static String s023 = "2:08,35";
  static String s024 = "2:22,76";
  static String s025 = "22,87";
  static String s026 = "25,64";
  static String s027 = "50,66";
  static String s028 = "56,81";
  static String s029 = "1:53,47";
  static String s030 = "2:06,17";
  static String s031 = "52,74";
  static String s032 = "59,90";
  static String s033 = "1:56,37";
  static String s034 = "2:09,31";
  static String s035 = "4:09,38";
  static String s036 = "4:33,76";
  static String s037 = "21,29";
  static String s038 = "24,19";
  static String s039 = "47,05";
  static String s040 = "52,66";
  static String s041 = "1:44,25";
  static String s042 = "1:54,74";
  static String s043 = "24,45";
  static String s044 = "27,56";
  static String s045 = "52,48";
  static String s046 = "58,91";
  static String s047 = "21,99";
  static String s048 = "24,78";
  static String s049 = "48,35";
  static String s050 = "53,90";
  static String s051 = "1:46,72";
  static String s052 = "1:57,28";
  static String s053 = "3:47,43";
  static String s054 = "4:07,26";
  static String s055 = "7:58,29";
  static String s056 = "8:28,12";
  static String s057 = "15:02,33";
  static String s058 = "16:26,08";
  static String s059 = "25,19";
  static String s060 = "28,20";
  static String s061 = "53,77";
  static String s062 = "59,96";
  static String s063 = "1:57,19";
  static String s064 = "2:09,31";
  static String s065 = "27,61";
  static String s066 = "31,26";
  static String s067 = "59,94";
  static String s068 = "1:07,07";
  static String s069 = "2:10,10";
  static String s070 = "2:24,69";
  static String s071 = "23,70";
  static String s072 = "26,20";
  static String s073 = "51,91";
  static String s074 = "58,03";
  static String s075 = "1:56,45";
  static String s076 = "2:08,58";
  static String s077 = "1:59,43";
  static String s078 = "2:11,88";
  static String s079 = "4:14,98";
  static String s080 = "4:38,66";
  static String s081 = "48,35";
  static String s082 = "53,90";
  static String s083 = "1:46,72";
  static String s084 = "1:57,28";
  static String s085 = "53,77";
  static String s086 = "59,96";

  //data ms
  static String s087 = "22,65";
  static String s088 = "25,95";
  static String s089 = "50,40";
  static String s090 = "56,40";
  static String s091 = "1:51,75";
  static String s092 = "2:04,25";
  static String s093 = "3:59,00";
  static String s094 = "4:23,00";
  static String s095 = "8:17,00";
  static String s096 = "9:00,00";
  static String s097 = "15:38,50";
  static String s098 = "17:22,50";
  static String s099 = "26,00";
  static String s100 = "28,85";
  static String s101 = "57,40";
  static String s102 = "1:04,00";
  static String s103 = "2:05,55";
  static String s104 = "2:18,75";
  static String s105 = "28,45";
  static String s106 = "32,65";
  static String s107 = "1:03,40";
  static String s108 = "1:12,40";
  static String s109 = "2:19,25";
  static String s110 = "2:35,25";
  static String s111 = "24,15";
  static String s112 = "27,50";
  static String s113 = "54,40";
  static String s114 = "1:01,90";
  static String s115 = "2:03,75";
  static String s116 = "2:17,75";
  static String s117 = "56,90";
  static String s118 = "1:04,90";
  static String s119 = "2:06,75";
  static String s120 = "2:21,75";
  static String s121 = "4:31,00";
  static String s122 = "5:01,00";
  static String s123 = "22,65";
  static String s124 = "25,95";
  static String s125 = "50,40";
  static String s126 = "56,40";
  static String s127 = "1:51,75";
  static String s128 = "2:04,25";
  static String s129 = "26,00";
  static String s130 = "28,85";
  static String s131 = "57,40";
  static String s132 = "1:04,00";
  static String s133 = "23,40";
  static String s134 = "26,70";
  static String s135 = "51,90";
  static String s136 = "57,90";
  static String s137 = "1:54,75";
  static String s138 = "2:07,25";
  static String s139 = "4:05,00";
  static String s140 = "4:29,00";
  static String s141 = "8:29,00";
  static String s142 = "9:12,00";
  static String s143 = "16:01,00";
  static String s144 = "17:45,00";
  static String s145 = "25,40";
  static String s146 = "29,20";
  static String s147 = "58,90";
  static String s148 = "1:06,40";
  static String s149 = "2:08,55";
  static String s150 = "2:21,75";
  static String s151 = "29,20";
  static String s152 = "33,40";
  static String s153 = "1:04,90";
  static String s154 = "1:13,90";
  static String s155 = "2:22,25";
  static String s156 = "2:38,25";
  static String s157 = "24,90";
  static String s158 = "28,25";
  static String s159 = "55,90";
  static String s160 = "1:03,40";
  static String s161 = "2:06,75";
  static String s162 = "2:20,75";
  static String s163 = "2:09,75";
  static String s164 = "2:24,75";
  static String s165 = "4:37,00";
  static String s166 = "5:07,00";
  static String s167 = "51,90";
  static String s168 = "57,90";
  static String s169 = "1:54,75";
  static String s170 = "2:07,25";
  static String s171 = "58,90";
  static String s172 = "1:06,40";

  //data cms
  static String s173 = "23,40";
  static String s174 = "26,75";
  static String s175 = "53,70";
  static String s176 = "1:00,40";
  static String s177 = "1:58,25";
  static String s178 = "2:12,55";
  static String s179 = "4:11,50";
  static String s180 = "4:38,00";
  static String s181 = "8:50,00";
  static String s182 = "9:34,00";
  static String s183 = "17:16,50";
  static String s184 = "18:31,50";
  static String s185 = "27,55";
  static String s186 = "30,05";
  static String s187 = "1:00,80";
  static String s188 = "1:08,90";
  static String s189 = "2:12,25";
  static String s190 = "2:26,75";
  static String s191 = "30,00";
  static String s192 = "34,45";
  static String s193 = "1:07,30";
  static String s194 = "1:16,40";
  static String s195 = "2:27,25";
  static String s196 = "2:44,25";
  static String s197 = "25,15";
  static String s198 = "28,65";
  static String s199 = "58,40";
  static String s200 = "1:05,40";
  static String s201 = "2:10,75";
  static String s202 = "2:25,25";
  static String s203 = "1:01,90";
  static String s204 = "1:09,90";
  static String s205 = "2:14,25";
  static String s206 = "2:30,25";
  static String s207 = "4:46,00";
  static String s208 = "5:18,50";
  static String s209 = "23,40";
  static String s210 = "26,75";
  static String s211 = "53,70";
  static String s212 = "1:00,40";
  static String s213 = "1:58,25";
  static String s214 = "2:12,55";
  static String s215 = "27.55";
  static String s216 = "30,05";
  static String s217 = "1:00,80";
  static String s218 = "1:08,90";
  static String s219 = "24,15";
  static String s220 = "27,50";
  static String s221 = "55,30";
  static String s222 = "1:01,90";
  static String s223 = "2:01,45";
  static String s224 = "2:15,55";
  static String s225 = "4:17.50";
  static String s226 = "4:44,00";
  static String s227 = "9:02,00";
  static String s228 = "9:46,00";
  static String s229 = "17:39,00";
  static String s230 = "18:54,00";
  static String s231 = "26,90";
  static String s232 = "30,90";
  static String s233 = "1:02,40";
  static String s234 = "1:10,40";
  static String s235 = "2:15,25";
  static String s236 = "2:29,75";
  static String s237 = "30,70";
  static String s238 = "35,20";
  static String s239 = "1:08,90";
  static String s240 = "1:17,90";
  static String s241 = "2:30,25";
  static String s242 = "2:47,25";
  static String s243 = "25,90";
  static String s244 = "29,40";
  static String s245 = "59,90";
  static String s246 = "1:06,90";
  static String s247 = "2:13,75";
  static String s248 = "2:28.25";
  static String s249 = "2:17,25";
  static String s250 = "2:33,25";
  static String s251 = "4:52,00";
  static String s252 = "5:24.50";
  static String s253 = "55,30";
  static String s254 = "1:01,90";
  static String s255 = "2:01,45";
  static String s256 = "2:15,55";
  static String s257 = "1:02,40";
  static String s258 = "1:10,40";

  //data I sc
  static String s259 = "24,65";
  static String s260 = "28,05";
  static String s261 = "57,10";
  static String s262 = "1:04,24";
  static String s263 = "2:06,50";
  static String s264 = "2:21,25";
  static String s265 = "4:28,00";
  static String s266 = "4:56,00";
  static String s267 = "9:28,00";
  static String s268 = "10:15,00";
  static String s269 = "18:15,00";
  static String s270 = "20:14,50";
  static String s271 = "29,35";
  static String s272 = "31,75";
  static String s273 = "1:04,80";
  static String s274 = "1:13,40";
  static String s275 = "2:20,00";
  static String s276 = "2:35,75";
  static String s277 = "31,85";
  static String s278 = "36,15";
  static String s279 = "1:11,80";
  static String s280 = "1:21,40";
  static String s281 = "2:37,25";
  static String s282 = "2:54,75";
  static String s283 = "27,15";
  static String s284 = "31,15";
  static String s285 = "1:01,90";
  static String s286 = "1:09,90";
  static String s287 = "2:18,75";
  static String s288 = "2:35,25";
  static String s289 = "1:05,90";
  static String s290 = "1:14,90";
  static String s291 = "2:22,75";
  static String s292 = "2:39,75";
  static String s293 = "5:05,00";
  static String s294 = "5:40,00";
  static String s295 = "24,65";
  static String s296 = "28,05";
  static String s297 = "57,10";
  static String s298 = "1:04,24";
  static String s299 = "2:06,50";
  static String s300 = "2:21,25";
  static String s301 = "29,35";
  static String s302 = "31,75";
  static String s303 = "1:04,80";
  static String s304 = "1:13,40";
  static String s305 = "25,40";
  static String s306 = "28,80";
  static String s307 = "58,70";
  static String s308 = "1:05,74";
  static String s309 = "2:09,75";
  static String s310 = "2:24,25";
  static String s311 = "4:34,00";
  static String s312 = "5:02,00";
  static String s313 = "9:41,00";
  static String s314 = "10:27,00";
  static String s315 = "18:39,00";
  static String s316 = "20:37,00";
  static String s317 = "28,70";
  static String s318 = "32,50";
  static String s319 = "1:06,40";
  static String s320 = "1:14,90";
  static String s321 = "2:23,25";
  static String s322 = "2:38,75";
  static String s323 = "32,60";
  static String s324 = "36,90";
  static String s325 = "1:13,40";
  static String s326 = "1:22,90";
  static String s327 = "2:40,25";
  static String s328 = "2:57,75";
  static String s329 = "27,90";
  static String s330 = "31,90";
  static String s331 = "1:03,40";
  static String s332 = "1:11,40";
  static String s333 = "2:21,75";
  static String s334 = "2:38,25";
  static String s335 = "2:25,75";
  static String s336 = "2:42,75";
  static String s337 = "5:11,00";
  static String s338 = "5:46,00";
  static String s339 = "58,70";
  static String s340 = "1:05,74";
  static String s341 = "2:09,75";
  static String s342 = "2:24,25";
  static String s343 = "1:06,40";
  static String s344 = "1:14,90";

  //data II sc
  static String s345 = "27,05";
  static String s346 = "30,75";
  static String s347 = "1:03,50";
  static String s348 = "1:11,80";
  static String s349 = "2:21,00";
  static String s350 = "2:37,00";
  static String s351 = "5:03,00";
  static String s352 = "5:37,00";
  static String s353 = "11:06,00";
  static String s354 = "11:46,00";
  static String s355 = "20:37.50";
  static String s356 = "22:44,50";
  static String s357 = "32,25";
  static String s358 = "36,75";
  static String s359 = "1:13,00";
  static String s360 = "1:21,50";
  static String s361 = "2:37,00";
  static String s362 = "2:55,00";
  static String s363 = "35,25";
  static String s364 = "40,25";
  static String s365 = "1:20,50";
  static String s366 = "1:30,00";
  static String s367 = "2:56,50";
  static String s368 = "3:15,00";
  static String s369 = "30,25";
  static String s370 = "33,75";
  static String s371 = "1:10,50";
  static String s372 = "1:19,50";
  static String s373 = "2:37,50";
  static String s374 = "2:56,00";
  static String s375 = "1:14,00";
  static String s376 = "1:24,00";
  static String s377 = "2:41,00";
  static String s378 = "3:00,00";
  static String s379 = "5:46,00";
  static String s380 = "6:24,00";
  static String s381 = "27,05";
  static String s382 = "30,75";
  static String s383 = "1:03,50";
  static String s384 = "1:11,80";
  static String s385 = "2:21,00";
  static String s386 = "2:37,00";
  static String s387 = "32,25";
  static String s388 = "36,75";
  static String s389 = "1:13,00";
  static String s390 = "1:21,50";
  static String s391 = "27,80";
  static String s392 = "31,50";
  static String s393 = "1:05,00";
  static String s394 = "1:13,30";
  static String s395 = "2:24,00";
  static String s396 = "2:40,00";
  static String s397 = "5:09,00";
  static String s398 = "5:43,00";
  static String s399 = "11:18,00";
  static String s400 = "11:58,00";
  static String s401 = "21:00,00";
  static String s402 = "23:07,00";
  static String s403 = "33,00";
  static String s404 = "37,50";
  static String s405 = "1:14,50";
  static String s406 = "1:23,00";
  static String s407 = "2:40,00";
  static String s408 = "2:58,00";
  static String s409 = "36,00";
  static String s410 = "41,00";
  static String s411 = "1:22,00";
  static String s412 = "1:31,50";
  static String s413 = "2:59,50";
  static String s414 = "3:18,00";
  static String s415 = "31,00";
  static String s416 = "34,50";
  static String s417 = "1:12,00";
  static String s418 = "1:21,00";
  static String s419 = "2:40,50";
  static String s420 = "2:59,00";
  static String s421 = "2:44,00";
  static String s422 = "3:03,00";
  static String s423 = "5:52,00";
  static String s424 = "6:30,00";
  static String s425 = "1:05,00";
  static String s426 = "1:13,30";
  static String s427 = "2:24,00";
  static String s428 = "2:40,00";
  static String s429 = "1:14,50";
  static String s430 = "1:23,00";

  //data III sc
  static String s431 = "29,25";
  static String s432 = "32,75";
  static String s433 = "1:11,00";
  static String s434 = "1:19,50";
  static String s435 = "2:39,50";
  static String s436 = "2:55,00";
  static String s437 = "5:44,00";
  static String s438 = "6:21,00";
  static String s439 = "12:28,00";
  static String s440 = "13:19,00";
  static String s441 = "23:37,50";
  static String s442 = "26:07,50";
  static String s443 = "35,75";
  static String s444 = "40,75";
  static String s445 = "1:21,50";
  static String s446 = "1:31,50";
  static String s447 = "2:57,00";
  static String s448 = "3:17,00";
  static String s449 = "38,75";
  static String s450 = "44,25";
  static String s451 = "1:28,50";
  static String s452 = "1:42,00";
  static String s453 = "3:19.50";
  static String s454 = "3:40,00";
  static String s455 = "33,25";
  static String s456 = "36,75";
  static String s457 = "1:20,50";
  static String s458 = "1:30,50";
  static String s459 = "2:58,00";
  static String s460 = "3:19,00";
  static String s461 = "1:24,00";
  static String s462 = "1:35,00";
  static String s463 = "3:05,00";
  static String s464 = "3:26,00";
  static String s465 = "6:34,00";
  static String s466 = "7:17,00";
  static String s467 = "29,25";
  static String s468 = "32,75";
  static String s469 = "1:11,00";
  static String s470 = "1:19,50";
  static String s471 = "2:39,50";
  static String s472 = "2:55,00";
  static String s473 = "35,75";
  static String s474 = "40,75";
  static String s475 = "1:21,50";
  static String s476 = "1:31,50";
  static String s477 = "30,00";
  static String s478 = "33,50";
  static String s479 = "1:12,50";
  static String s480 = "1:21,00";
  static String s481 = "2:42,50";
  static String s482 = "2:58,00";
  static String s483 = "5:50,00";
  static String s484 = "6:27,00";
  static String s485 = "12:40,00";
  static String s486 = "13:31,00";
  static String s487 = "24:00,00";
  static String s488 = "26:30,00";
  static String s489 = "36,50";
  static String s490 = "41,50";
  static String s491 = "1:23,00";
  static String s492 = "1:33,00";
  static String s493 = "3:00,00";
  static String s494 = "3:20,00";
  static String s495 = "39,50";
  static String s496 = "45,00";
  static String s497 = "1:30,00";
  static String s498 = "1:43,50";
  static String s499 = "3:22,50";
  static String s500 = "3:43.00";
  static String s501 = "34,00";
  static String s502 = "37,50";
  static String s503 = "1:22,00";
  static String s504 = "1:32,00";
  static String s505 = "3:01,00";
  static String s506 = "3:22,00";
  static String s507 = "3:08,00";
  static String s508 = "3:29,00";
  static String s509 = "6:40,00";
  static String s510 = "7:23,00";
  static String s511 = "1:12,50";
  static String s512 = "1:21,00";
  static String s513 = "2:42,50";
  static String s514 = "2:58,00";
  static String s515 = "1:23,00";
  static String s516 = "1:33,00";

  //data I ysc
  static String s527 = "35,25";
  static String s528 = "39,75";
  static String s529 = "1:23,50";
  static String s530 = "1:33,50";
  static String s531 = "3:05,00";
  static String s532 = "3:26,00";
  static String s533 = "6:40,00";
  static String s534 = "7:32,00";
  static String s535 = "14:30,00";
  static String s536 = "16:04,00";
  static String s537 = "27:40,00";
  static String s538 = "30:15,00";
  static String s539 = "41,75";
  static String s540 = "47,25";
  static String s541 = "1:34,00";
  static String s542 = "1:45,50";
  static String s543 = "3:25,00";
  static String s544 = "3:51,00";
  static String s545 = "45,25";
  static String s546 = "51,75";
  static String s547 = "1:44,50";
  static String s548 = "2:06,50";
  static String s549 = "3:52,00";
  static String s550 = "4:17,00";
  static String s551 = "38,25";
  static String s552 = "43,75";
  static String s553 = "1:30,50";
  static String s554 = "1:42,50";
  static String s555 = "3:22,00";
  static String s556 = "3:46,00";
  static String s557 = "1:35,00";
  static String s558 = "1:47,00";
  static String s559 = "3:30,00";
  static String s560 = "3:55,00";
  static String s561 = "7:29,00";
  static String s562 = "8:18,00";
  static String s563 = "35,25";
  static String s564 = "39,75";
  static String s565 = "1:23,50";
  static String s566 = "1:33,50";
  static String s567 = "3:05,00";
  static String s568 = "3:26,00";
  static String s569 = "41,75";
  static String s570 = "47,25";
  static String s571 = "1:34,00";
  static String s572 = "1:45,50";
  static String s573 = "36,00";
  static String s574 = "40,50";
  static String s575 = "1:25,00";
  static String s576 = "1:35,00";
  static String s577 = "3:08,00";
  static String s578 = "3:29,00";
  static String s579 = "6:46,00";
  static String s580 = "7:38,00";
  static String s581 = "14:42,00";
  static String s582 = "16:16,00";
  static String s583 = "28:02,50";
  static String s584 = "30:37,50";
  static String s585 = "42,50";
  static String s586 = "48,00";
  static String s587 = "1:35,50";
  static String s588 = "1:47,00";
  static String s589 = "3:28,00";
  static String s590 = "3:54,00";
  static String s591 = "46,00";
  static String s592 = "52,50";
  static String s593 = "1:46,00";
  static String s594 = "2:08,00";
  static String s595 = "3:55,00";
  static String s596 = "4:20,00";
  static String s597 = "39,00";
  static String s598 = "44,50";
  static String s599 = "1:32,00";
  static String s600 = "1:44,00";
  static String s601 = "3:25,00";
  static String s602 = "3:49,00";
  static String s603 = "3:33,00";
  static String s604 = "3:58,00";
  static String s605 = "7:35,00";
  static String s606 = "8:24,00";
  static String s607 = "1:25,00";
  static String s608 = "1:35,00";
  static String s609 = "3:08,00";
  static String s610 = "3:29,00";
  static String s611 = "1:35,50";
  static String s612 = "1:47,00";

  //data II ysc
  static String s613 = "45,25";
  static String s614 = "49,75";
  static String s615 = "1:43,50";
  static String s616 = "1:53,50";
  static String s617 = "3:15,00";
  static String s618 = "4:06,00";
  static String s619 = "7:36,00";
  static String s620 = "8:43,00";
  static String s621 = "16:30,00";
  static String s622 = "18:34,00";
  static String s623 = "31:40,00";
  static String s624 = "34:20,00";
  static String s625 = "51,75";
  static String s626 = "57,25";
  static String s627 = "1:56,50";
  static String s628 = "2:08,50";
  static String s629 = "4:11,00";
  static String s630 = "4:36,00";
  static String s631 = "55,25";
  static String s632 = "1:01,75";
  static String s633 = "2:03,50";
  static String s634 = "2:16,50";
  static String s635 = "4:25,00";
  static String s636 = "4:52,00";
  static String s637 = "48,25";
  static String s638 = "53,75";
  static String s639 = "1:49,50";
  static String s640 = "2:01,50";
  static String s641 = "3:57,00";
  static String s642 = "4:22,00";
  static String s643 = "1:54,00";
  static String s644 = "2:06,00";
  static String s645 = "4:05,00";
  static String s646 = "4:31,00";
  static String s647 = "8:25,00";
  static String s648 = "9:29,00";
  static String s649 = "45,25";
  static String s650 = "49,75";
  static String s651 = "1:43,50";
  static String s652 = "1:53,50";
  static String s653 = "3:15,00";
  static String s654 = "4:06,00";
  static String s655 = "51,75";
  static String s656 = "57,25";
  static String s657 = "1:56,50";
  static String s658 = "2:08,50";
  static String s659 = "46,00";
  static String s660 = "50,50";
  static String s661 = "1:45,00";
  static String s662 = "1:55,00";
  static String s663 = "3:48,00";
  static String s664 = "4:09,00";
  static String s665 = "7:42,00";
  static String s666 = "8:49,00";
  static String s667 = "16:42,00";
  static String s668 = "18:46,00";
  static String s669 = "32:02,50";
  static String s670 = "34:42,50";
  static String s671 = "52,50";
  static String s672 = "58,00";
  static String s673 = "1:58,00";
  static String s674 = "2:10,00";
  static String s675 = "4:14,00";
  static String s676 = "4:39:00";
  static String s677 = "56,00";
  static String s678 = "1.02,50";
  static String s679 = "2:05,00";
  static String s680 = "2:18,00";
  static String s681 = "4:28,00";
  static String s682 = "4:55,00";
  static String s683 = "49,00";
  static String s684 = "54.50";
  static String s685 = "1:51,00";
  static String s686 = "2:03,00";
  static String s687 = "4:00,00";
  static String s688 = "4:25,00";
  static String s689 = "4:08,00";
  static String s690 = "4:34,00";
  static String s691 = "8:31,00";
  static String s692 = "9:35,00";
  static String s693 = "1:45,00";
  static String s694 = "1:55,00";
  static String s695 = "3:48,00";
  static String s696 = "4:09,00";
  static String s697 = "1:58,00";
  static String s698 = "2:10,00";

  //data III ysc
  static String s699 = "55,25";
  static String s700 = "59,25";
  static String s701 = "2:03,50";
  static String s702 = "2:12,50";
  static String s703 = "4:25,00";
  static String s704 = "4:44,00";
  static String s705 = "8:32,00";
  static String s706 = "9:54,00";
  static String s707 = "18:30,00";
  static String s708 = "21:04,00";
  static String s709 = "35:40,00";
  static String s710 = "38:30,00";
  static String s711 = "1:01,75";
  static String s712 = "1:07,25";
  static String s713 = "2:16,50";
  static String s714 = "2:28,50";
  static String s715 = "4:51,00";
  static String s716 = "5:16,00";
  static String s717 = "1:05,25";
  static String s718 = "1:11,75";
  static String s719 = "2:23,50";
  static String s720 = "2:37,50";
  static String s721 = "5:05,00";
  static String s722 = "5:34,00";
  static String s723 = "58,25";
  static String s724 = "1:03,75";
  static String s725 = "2:09,50";
  static String s726 = "2:21,50";
  static String s727 = "4:37,00";
  static String s728 = "5:02,00";
  static String s729 = "2:14,00";
  static String s730 = "2:46,00";
  static String s731 = "4:45,00";
  static String s732 = "5:11,00";
  static String s733 = "9:21,00";
  static String s734 = "10:40,00";
  static String s735 = "55,25";
  static String s736 = "59,25";
  static String s737 = "2:03,50";
  static String s738 = "2:12,50";
  static String s739 = "4:25,00";
  static String s740 = "4:44,00";
  static String s741 = "1:01,75";
  static String s742 = "1:07,25";
  static String s743 = "2:16,50";
  static String s744 = "2:28,50";
  static String s745 = "56,00";
  static String s746 = "1:00,00";
  static String s747 = "2:05,00";
  static String s748 = "2:14,00";
  static String s749 = "4:28,00";
  static String s750 = "4:47,00";
  static String s751 = "8:38,00";
  static String s752 = "10:00,00";
  static String s753 = "18:42,00";
  static String s754 = "21:16,00";
  static String s755 = "36:02,50";
  static String s756 = "38:52,50";
  static String s757 = "1:02,50";
  static String s758 = "1:08,00";
  static String s759 = "2:18,00";
  static String s760 = "2:30,00";
  static String s761 = "4:54,00";
  static String s762 = "5:19,00";
  static String s763 = "1:06,00";
  static String s764 = "1:12,50";
  static String s765 = "2:25,00";
  static String s766 = "2:39,00";
  static String s767 = "5:08,00";
  static String s768 = "5:37,00";
  static String s769 = "59,00";
  static String s770 = "1:04,50";
  static String s771 = "2:11,00";
  static String s772 = "2:23,00";
  static String s773 = "4:40,00";
  static String s774 = "5:05,00";
  static String s775 = "4:48,00";
  static String s776 = "5:14,00";
  static String s777 = "9:27,00";
  static String s778 = "10:46,00";
  static String s779 = "2:05,00";
  static String s780 = "2:14,00";
  static String s781 = "4:28,00";
  static String s782 = "4:47,00";
  static String s783 = "2:18,00";
  static String s784 = "2:30,00";

  //standart_list
  static String msmc = "МСМК";
  static String msCms = "МС-КМС";
  static String standartsMsmc = "Нормы МСМК";
  static String standartsMs = "МС";
  static String standartsCms = "КМС";
  static String firstSc = "I спортивный разряд";
  static String secondSc = "II спортивный разряд";
  static String thirdSc = "III спортивный разряд";
  static String firstYsc = "I юношеский спортивный разряд";
  static String secondYsc = "II юношеский спортивный разряд";
  static String thirdYsc = "III юношеский спортивный разряд";

  //Generalities
  static String generalities001 = "1. ОБЩИЕ ПОЛОЖЕНИЯ";
  static String generalities002 = "Дистанция (м)";
  static String generalities003 = "50, 100, 200, 400, 800, 1500";
  static String generalities004 = "50, 100, 200";
  static String generalities005 = "100, 200, 400";
  static String generalities006 = "4х50 ,4х100, 4х200";
  static String generalities007 = "4х50, 4х100";
  static String generalities008 = "4х50";
  static String generalities009 = "200, 400";
  static String generalities010 = "4х100, 4х200";
  static String generalities011 = "4х100";
  static String generalities1 =
      "\nПравила вида спорта «плавание» (далее – Правила) разработаны в полном соответствии с Правилами Международной федерации плавания (FINA).\n\n"
      "Настоящие Правила являются обязательными при проведении всех соревнований по плаванию на территории Российской Федерации.\n\n"
      "При возникновении спорных или неописанных ситуаций, не предусмотренными настоящими Правилами во время проведения соревнований, руководствоваться Правилами Международной федерации плавания (FINA).\n\n";
  static String generalities2 =
      "1.1. СПОСОБЫ ПЛАВАНИЯ, ДИСТАНЦИИ И ПРОГРАММА СОРЕВНОВАНИЙ\n";
  static String generalities2_1 =
      "Способ плавания, дистанция и программа соревнований";
  static String generalities3 =
      "Соревнования по плаванию проводятся в бассейнах с длиной дорожек 25 и 50 метров.\n\n"
      "1.1.1. В 25-метровых бассейнах на следующих дистанциях (для мужчин и женщин):\n";
  static String generalities4 = "\nЭстафеты:";
  static String generalities5 =
      "\nЗаявочным временем могут быть результаты как установленные в 25-метровом бассейне, так и 50-метровом бассейне. Предварительные и полуфинальные заплывы могут проводиться на 10 дорожках. Финальные заплывы проводятся только на 8 дорожках.\n\n"
      "1.1.2. В 50-метровых бассейнах на следующих дистанциях (для мужчин и женщин):\n";
  static String generalities6 =
      "\nЗаявочным временем могут быть результаты, установленные только в 50-метровом бассейне. Предварительные и полуфинальные заплывы могут проводиться на 10 дорожках. Финальные заплывы проводятся только на 8 дорожках.\n";
  static String generalities7 = "Примечания:";
  static String generalities8 =
      "1. В зависимости от возраста участников, целей и задач соревнований в них могут быть включены и другие дистанции.\n"
      "2. Для отбора спортсменов на международные и всероссийские соревнования, ВФП может устанавливать нормативы и определять соревнования, на которых эти нормативы могут быть выполнены.\n\n";
  static String generalities9 =
      "1.2. РЕГИСТРАЦИЯ РЕКОРДОВ И ВЫСШИХ ДОСТИЖЕНИЙ\n";
  static String generalities9_1 = "Регистрация рекордов и высших достижений";
  static String generalities10 =
      "Всероссийская федерация плавания (далее ВФП) регистрирует рекорды России (мужчины, женщины), юношеские рекорды России (юноши – 18 лет и моложе, девушки – 16 лет и моложе).\n\n"
      "1.2.1. Рекорды регистрируются отдельно в бассейнах с длиной дорожек 25 и 50 метров.\n\n"
      "1.2.2. Рекорды России и Юношеские рекорды России регистрируются на дистанциях, указанных в п.п.1.1.1., 1.1.2. только при проведении финальных всероссийских соревнований (согласно Единому календарному плану Министерства спорта Российской Федерации (далее Минспорта России) и Календарю ВФП) и использовании на них системы автоматической регистрации времени. Кроме того, они могут быть зарегистрированы при участии российских спортсменов в международных соревнованиях в составе спортивной сборной команды России. Для чемпионатов, кубков и первенств России могут регистрироваться высшие достижения этих соревнований.\n\n"
      "1.2.3. Регистрируются только те рекорды и высшие достижения, которые установлены в бассейнах со спокойной водой (без течения), отвечающие требованиям, изложенным в разделе «Требования к спортивным сооружениям» настоящих Правил.\n\n"
      "1.2.4. Длина каждой дорожки бассейна должна быть удостоверена свидетельством метрологической службы.\n\n"
      "1.2.5. Всероссийские рекорды должны быть зарегистрированы, с точностью до 0,01сек с использованием системы автоматической регистрации времени. К бланку регистрации рекорда обязательно должны быть приложены: чек системы регистрации времени и свидетельство (свидетельства – для всех членов команды эстафеты) об отрицательном результате допинг-контроля, проведенном не позднее 24 часов после установления рекорда.\n\n"
      "1.2.6. Время, равное рекордному с точностью до 0,01с, признается как эквивалентное рекорду, и пловцы, показавшие это время, называются «обладатели рекорда». Только время победителя заплыва может быть представлено для регистрации рекорда. В случае если два или несколько спортсменов показывают одинаковое рекордное время, все они считаются рекордсменами.\n\n"
      "1.2.7. В эстафетном плавании (за исключением смешанных эстафет) участнику, плывущему на первом этапе, разрешается сделать попытку установления рекорда. Если он закончит свой этап в рекордное время в соответствии с правилами прохождения этой дистанции, то его результат не может быть аннулирован из-за последующей дисквалификации его эстафетной команды за нарушение, совершенное после окончания пловцом своего этапа.\n\n"
      "1.2.8. Пловец в индивидуальном заплыве может сделать попытку установления рекорда на промежуточной дистанции, если его тренер или представитель подадут заявку рефери о том, чтобы время пловца на промежуточной дистанции было официально зарегистрировано. Время на промежуточной дистанции регистрируется системой автоматической регистрации времени. Такой пловец должен полностью закончить запланированную дистанцию соревнований, с тем, чтобы рекорд на промежуточной дистанции был зарегистрирован.\n\n"
      "1.2.9. Все рекорды, установленные на чемпионатах, первенствах и в финале Кубка России регистрируются автоматически. Заявку на регистрацию рекордов мира и Европы, установленных спортсменами на всероссийских соревнованиях, оформляет ВФП и отправляет в офисы FINA, LEN в течение 14 дней со дня установления рекорда, предварительно передав заявление на регистрацию мирового рекорда по электронной почте, телеграфу, телексу или факсу почетному секретарю FINA, LEN в течение 7 дней со дня установления рекорда.\n\n";
  static String generalities11 = "1.3. ПРОГРАММА СОРЕВНОВАНИЙ\n";
  static String generalities11_1 = "Программа соревнований";
  static String generalities12 =
      "1.3.1. Программа соревнований и расписание по дням определяется в Положении и Регламенте о соревнованиях, утверждаемом соответствующим Органом исполнительной власти в области физической культуры и спорта.\n\n";
  static String generalities13 = "1.4. КЛАССИФИКАЦИЯ СОРЕВНОВАНИЙ\n";
  static String generalities13_1 = "Классификация соревнований";
  static String generalities14 =
      "1.4.1. Соревнования классифицируются по виду, статусу и способу проведения.\n\n"
      "1.4.2. Вид соревнований: личные, лично-командные и командные. В личных соревнованиях результаты (очки) и места засчитываются каждому отдельному участнику. В лично-командных соревнованиях результаты и места засчитываются одновременно отдельным участникам и команде в целом, в командных соревнованиях – только командам.\n\n"
      "1.4.3. Статус соревнований:\n"
      "1.4.3.1. Всероссийские соревнования: чемпионат, кубок (этап кубка), Спартакиада учащихся, Спартакиада молодежи, первенство среди юниоров и юниорок, первенство среди юношей и девушек, другие официальные соревнования\n"
      "1.4.3.2. Соревнования Федерального округа: чемпионат, первенство, другие официальные соревнования.\n"
      "1.4.3.3. Соревнования субъекта Российской Федерации: чемпионат, кубок (этап кубка), первенство, другие официальные соревнования.\n"
      "1.4.3.4. Соревнования муниципального образования: чемпионат, первенство, квалификационные соревнования.\n"
      "1.4.3.5. Соревнования спортивных организаций, клубов: чемпионат, кубок (этап кубка), первенство, квалификационные соревнования.\n\n"
      "1.4.4. Способ проведения соревнований:\n"
      "1.4.4.1. Очные\n"
      "1.4.4.2. Заочные\n\n"
      "1.4.5. Вид, статус, способ проведения соревнований и программа соревнований определяются Положением о соревнованиях, которое разрабатывает организация, проводящая соревнование. Положение должно быть опубликовано не менее чем за 1 месяц до дня проведения соревнований. \n";

  //Participants
  static String participants01 = "2.1. ВОЗРАСТ УЧАСТНИКОВ\n";
  static String participants01_1 = "Возраст участников";
  static String participants02 =
      "2.1.1. Участники соревнований делятся на следующие возрастные группы:\n"
      "    • младшая группа: девушки 9-10 лет и юноши 9-10 лет, 11-12 лет;\n"
      "    • средняя группа: девушки 11-12 лет и юноши 13-14 лет;\n"
      "    • старшая группа: девушки 13-14 лет и юноши 15-16 лет;\n"
      "    • юниоры, юниорки: девушки 15-17 лет, юноши 17-18 лет;\n"
      "    • женщины и мужчины без ограничения возраста.\n"
      "    • Возраст участников определяется по состоянию на 31 декабря года проведения соревнований.\n\n"
      "2.1.2 Границы возрастных групп указываются в Положении о соревнованиях\n\n";
  static String participants03 = "2.2. ДОПУСК УЧАСТНИКОВ К СОРЕВНОВАНИЯМ\n";
  static String participants03_1 = "Допуск участников к соревнованиям";
  static String participants04 =
      "2.2.1. К участию в соревнованиях допускаются лица, прошедшие медицинский осмотр и получившие разрешение врача.\n\n"
      "2.2.2. Допуск к участию в соревнованиях регламентируется Положением о соревнованиях. В зависимости от требований Положения о соревнованиях, допуск участников может быть ограничен возрастом, уровнем спортивной подготовленности и т.д. К участию во всероссийских соревнованиях по плаванию участники допускаются решением комиссии, которую возглавляет представитель ВФП.\n\n"
      "2.2.3. При соответствующем уровне подготовки участники младшей возрастной группы, с разрешения врача, тренера и организации, проводящей соревнования (далее ОПС), могут допускаться к соревнованиям в более старшей группе. Спортсмены старших возрастных групп не допускаются к участию в соревнованиях младших групп.\n\n";
  static String participants05 = "2.3. ОБЯЗАННОСТИ И ПРАВА УЧАСТНИКОВ\n";
  static String participants05_1 = "Обязанности и права участников";
  static String participants06 = "2.3.1. Участники соревнований обязаны:\n"
      "2.3.1.1. Знать правила соревнований и четко выполнять их.\n"
      "2.3.1.2. Строго соблюдать нормы поведения в бассейне, организованно выходить на старт, при представлении участника судьейинформатором, встать, сделать шаг вперед, повернуться лицом к зрителям\n"
      "2.3.1.3 Выполнять все распоряжения судей во время соревнований.\n\n"
      "2.3.2. Участник имеет право обращаться к судьям только через представителя своей команды.\n\n"
      "2.3.3. На соревнованиях, где организован допинг-контроль, участник, получивший карточку-извещение, обязан незамедлительно явиться в службу допинг-контроля. В случае если участник, получивший данную карточку-извещение, принимает участие в предстоящих заплывах или церемонии награждения, он обязан уведомить об этом представителя допинг-службы.\n\n"
      "2.3.4. Если участник (команда в эстафете) установил (а) рекорд мира, Европы или России, то он в обязательном порядке должен пройти допинг-контроль, без результатов которого его рекорд не будет утвержден.\n\n"
      "2.3.5. Участники соревнований не могут быть судьями на тех же соревнованиях.\n\n";
  static String participants07 = "2.4. ФОРМА И РЕКЛАМА\n";
  static String participants07_1 = "Форма и реклама";
  static String participants08 =
      "2.4.1. Форма всех участников соревнований должна отличаться хорошим вкусом, быть пригодной для выступления в данной дисциплине и не носить никаких обозначений, которые могли бы показаться оскорбительными. Вся форма должна быть непрозрачной. Разрешается носить 2 шапочки. Рефери соревнований имеет право снять с соревнований любого участника, на плавательном костюме или на теле которого нанесены изображения, не отвечающие требованиям этого правила.\n"
      "2.4.1.1. На Всероссийских соревнованиях не разрешается использование формы (спортивный костюм, плавательный костюм, шапочка, полотенце) с символами, флагами, буквенными кодами других государств.\n\n"
      "2.4.2. На парад и церемонию награждения участники каждой организации должны выходить в единой форме, на которой должны быть:\n"
      "    • государственные символы - на международных соревнованиях;\n"
      "    • герб и название субъекта РФ или эмблема общества (ведомства) – на всероссийских соревнованиях;\n"
      "    • эмблема общества, ведомства, клуба или другой спортивной организации - на других соревнованиях.\n\n"
      "2.4.3. Плавательная экипировка (плавательные костюмы, шапочки и очки) должны соответствовать требованиям FINA, согласно утвержденным спискам – производителей. Спортсмен, участвующий в соревнованиях в не сертифицированном FINA плавательном костюме может быть не допущен к заплыву или дисквалифицирован.\n\n"
      "2.4.4. На каждом костюме участника, т.е. купальном костюме и шапочке, разрешается иметь два логотипа: производителя и другого спонсора. Две части костюма должны рассматриваться как один костюм. Другие виды рекламы не разрешаются. Название и флаг страны участника или буквенный код страны рекламой не считаются. Каждый из логотипов не должен превышать: плавательный костюм 40 кв. см.; плавательная шапочка 20 кв. см.\n\n"
      "2.4.5. На полотенцах и сумках могут быть нанесены два рекламных изображения. На тренировочных костюмах и форме официальных лиц могут быть нанесены два рекламных изображения на верхней части одежды и два на брюках или юбке. Логотип производителя или спонсора может повторяться, но одно и, тоже название может использоваться только один раз на каждом виде одежды или экипировки, не превышающие 40 кв. см.\n\n"
      "2.4.6. Нательная реклама не разрешается, ни под каким видом.\n\n"
      "2.4.7. Реклама табачных изделий и алкоголя запрещена.\n\n";
  static String participants09 =
      "2.5. ПРЕДСТАВИТЕЛИ, ТРЕНЕРЫ И КАПИТАНЫ КОМАНД\n";
  static String participants09_1 = "Представители, тренеры и капитаны команд";
  static String participants10 =
      "2.5.1. Каждая организация, участвующая в соревнованиях, должна иметь уполномоченного представителя.\n\n"
      "2.5.2. Представитель является руководителем команды. Он отвечает за дисциплину, здоровье участников и должен обеспечить их своевременную явку на старт, награждение, допинг-контроль и т.д.\n\n"
      "2.5.3. Представитель должен присутствовать на технических совещаниях, проводимых Оргкомитетом соревнований.\n\n"
      "2.5.4. Представителю запрещается вмешиваться в распоряжения судей и лиц, проводящих соревнование. Со всеми вопросами он должен обращаться к рефери (главному судье) или лицу, его заменяющему.\n\n"
      "2.5.5. Если команда не имеет специального представителя, то его обязанности должен выполнять тренер или капитан команды.\n\n"
      "2.5.6. Представитель имеет право получать в судейской коллегии справки по всем вопросам проведения соревнований, а в секретариате - материалы соревнований.\n\n"
      "2.5.7. Представитель должен проверить заявки на участие спортсменов в индивидуальных видах программы и команд в эстафетном плавании на комиссии по допуску участников. Именной состав эстафетной команды, должен быть подан за один час до начала утренней (вечерней) части соревнований, в которой разыгрывается данная эстафета, имена пловцов указываются в том порядке, в котором спортсмены будут плыть. Имена пловцов в комбинированной эстафете должны быть указаны в соответствии с тем стилем, которым плывет данный спортсмен.\n\n"
      "2.5.8. Если по каким-либо причинам пловец или команда эстафетного плавания на Всероссийских финальных соревнованиях не может выступать на заявленной дистанции, то представитель команды должен в письменной форме заполнить бланк отказа (замены) установленного образца на пловца или эстафетную команду при прохождении комиссии по допуску участников соревнований или до окончания совещания представителей команд. После этого все отказы (замены) запрещены. Заполненный бланк передаётся главному секретарю соревнований.\n\n"
      "2.5.9. Если по каким-либо причинам пловец или команда эстафетного плавания на Всероссийских соревнованиях отказывается от участия в финальном (полуфинальном) заплыве, то представитель команды должен в письменной форме заполнить бланк отказа установленного образца на пловца или эстафетную команду не позднее чем через 30 минут после окончания вида программы. Заполненный бланк передаётся главному секретарю соревнований.\n\n"
      "2.5.10. Представитель команды не может быть судьёй на тех же соревнованиях.\n";

  //Management
  static String management01 =
      "3.1. ПРЕДСТАВИТЕЛИ (ДЕЛЕГАТЫ) ВСЕРОССИЙСКОЙ ФЕДЕРАЦИИ ПЛАВАНИЯ\n";
  static String management01_1 =
      "Представители (делегаты) Всероссийской Федерации Плавания";
  static String management02 =
      "3.1.1. На всех всероссийских соревнованиях, включенных в Единый календарный план Минспорта России и Календарь ВФП, должен присутствовать, по меньшей мере, один представитель (технический делегат) ВФП.\n\n"
      "3.1.2. Представитель ВФП осуществляет контроль организации соревнований и соблюдения настоящих Правил, Положения и/или Регламента проводимых соревнований, а также любых других решений, принятых Президиумом (Исполкомом) ВФП или предусмотренных соглашениями между организаторами соревнований.\n\n"
      "3.1.3. На всех всероссийских соревнованиях Представитель ВФП возглавляет Комиссию по допуску участников соревнований.\n\n"
      "3.1.4. Представитель ВФП не выступает в качестве судьи или представителя команды в тех же соревнованиях. \n\n";
  static String management03 =
      "3.2. ОРГАНИЗАЦИОННЫЙ КОМИТЕТ И СУДЕЙСКАЯ КОЛЛЕГИЯ\n";
  static String management03_1 = "Организационный комитет и судейская коллегия";
  static String management04 =
      "3.2.1. Организация, проводящая соревнования назначает Организационный комитет, который наделяется правом решать все вопросы проведения соревнований, не входящие в компетенцию рефери, судей или других официальных лиц, и может откладывать соревнования и давать указания, применительно к любым случаям, согласуясь с правилами проведения соревнований.\n\n"
      "3.2.2. Проведение Всероссийских соревнований осуществляется судейской коллегией, утверждённой исполкомом ВФП.\n\n"
      "3.2.3. Для проведения официальных соревнований должно быть назначено следующее минимальное количество судей:\n"
      "    • рефери (2)\n"
      "    • главный судья (1)\n"
      "    • заместители главного судьи (1)\n"
      "    • судьи по технике плавания (1)\n"
      "    • стартер (1)\n"
      "    • старшие судьи на поворотах (2, по одному на каждом конце бассейна)\n"
      "    • судьи на поворотах (по одному на каждом конце каждой дорожки)\n"
      "    • судья на финише (3)\n"
      "    • главный секретарь(1)\n"
      "    • секретарь (1)\n"
      "    • судьи при участниках (1)\n"
      "    • информатор (1)\n"
      "    • судьи по награждению (1)\n\n"
      "3.2.4. Численный состав судейской коллегии определяется организацией, проводящей соревнования. Он зависит от масштаба соревнований и имеет то же самое или большее количество судей.\n\n"
      "3.2.5. Рефери мужских видов, рефери женских видов, главный судья, главный секретарь, заместители главного судьи, заместитель главного секретаря – это состав Главной судейской коллегии.\n\n"
      "3.2.6. На все Всероссийские соревнования Главная судейская коллегия утверждается Президентом (Исполкомом) ВФП.\n\n"
      "3.2.7. Когда не применяется система автоматической регистрации, в состав судей должны быть включены старший хронометрист, по 3 хронометриста на дорожку и дополнительно – два хронометриста.\n\n"
      "3.2.8. В том случае, когда не применяется система автоматической регистрации и/или нет 3-х цифровых секундомеров на одну дорожку, могут быть назначены старший судья на финише и судьи на финише.\n\n"
      "3.2.9. Соревнования по плаванию проводятся в плавательных бассейнах, включенных во Всероссийский реестр объектов спорта.\n\n"
      "3.2.10. Плавательный бассейн и техническое оборудование всероссийских соревнований должны быть тщательно осмотрены и одобрены до начала соревнований делегатом ВФП совместно с членом Всероссийской коллегии судей. После осмотра и одобрения места проведения соревнований и оборудования составляется «Акт о приёмки спортсооружения», который подписывают руководитель спортсооружения, Представитель ВФП и главный судья соревнований.\n\n"
      "3.2.11. Когда телевидение использует подводное видео оборудование оно должно иметь дистанционное управление и не затруднять обзор или путь пловцам, а также не должно изменять конфигурацию бассейна или заслонять обязательную разметку.\n\n"
      "3.2.12 На чемпионатах, первенствах и финалах розыгрыша Кубка России разрешается проводить видео, телевизионную съёмку, фотографировать и брать интервью, только аккредитованным журналистам, операторам, фотографам и только в строго отведённых для этой цели местах.\n";

  //Judges
  static String judges01 = "4.1. РЕФЕРИ\n";
  static String judges01_1 = "Рефери";
  static String judges02 =
      "4.1.1. Рефери должен полностью контролировать работу всех судей и руководить ими, утверждать их назначение, расставлять по местам работы, при необходимости проводить ежедневную «ротацию» судей и инструктировать по всем специальным вопросам или особенностям правил, относящихся к соревнованиям. Он исполняет все инструкции и решения ВФП и решает все вопросы, возникающие при проведении соревнований, окончательное урегулирование которых так или иначе не оговорено в Правилах.\n\n"
      "4.1.2. Рефери может вмешиваться в ход соревнований на любой стадии с целью соблюдения настоящих Правил и выносить решения по всем протестам в ходе соревнований.\n\n"
      "4.1.3. Когда используются судьи на финише без трех цифровых секундомеров, рефери должен определить их местонахождение. При наличии работающей системы автоматической регистрации, ее показания должны быть учтены, как указано в п. 14 Правил.\n\n"
      "4.1.4. Рефери проверяет наличие на своих местах всех необходимых для проведения соревнований судей. Он может заменить любого судью, если тот отсутствует, не в состоянии работать или не справляется со своими обязанностями. Он может назначить дополнительных судей, если сочтет это необходимым.\n\n"
      "4.1.5. Перед началом каждого заплыва рефери дает сигнал пловцам серией коротких свистков, приглашая их снять всю одежду, за исключением плавательного костюма, затем длинным свистком показывает, что они должны встать на стартовую тумбочку (или при плавании на спине и в комбинированной эстафете немедленно войти в воду). По второму длинному свистку спортсмены, плывущие на спине и в комбинированной эстафете должны немедленно принять стартовое положение. Когда пловцы и судьи готовы к старту, рефери жестом вытянутой к стартеру руки показывает, что пловцы теперь находятся под контролем стартера. Вытянутая рука должна оставаться в этом положении до стартового сигнала.\n\n"
      "4.1.6. Спортсмен, совершивший старт до стартового сигнала, который зафиксирован и подтвержден как стартером, так и рефери, подлежит дисквалификации.\n\n"
      "4.1.7. Рефери должен дисквалифицировать любого пловца за любое нарушение правил, которое рефери видел лично. Рефери может также дисквалифицировать спортсмена за нарушение правил, на которое рефери указали другие уполномоченные на это судьи. Все дисквалификации являются прерогативой рефери.\n\n";
  static String judges03 = "4.2. ГЛАВНЫЙ СУДЬЯ\n";
  static String judges03_1 = "Главный судья";
  static String judges04 =
      "4.2.1. Главный судья должен заблаговременно проверить готовность места проведения соревнований, соответствующего оборудования и инвентаря. Принять меры по обеспечению безопасности участников и зрителей соревнований. Совместно с врачом, комендантом и администрацией спортивной базы он несет ответственность за здоровье участников соревнований (травмы в результате неисправности или непригодности оборудования; нарушения установленного порядка и дисциплины; нарушения температурного и химического режимов воды и т.п.).\n\n"
      "4.2.2. Главный судья должен помогать рефери контролировать выполнение своих обязанностей судьями.\n\n"
      "4.2.3. Главный судья по требованию рефери должен организовать проведение собраний с судьями перед началом и после соревнований, а также заседания судейской коллегии перед началом и после каждой части соревнований и в тех случаях, когда этого требует ход соревнований.\n\n"
      "4.2.4. На тех соревнованиях, где не назначен рефери, главный судья должен выполнять и его обязанности.\n\n"
      "4.2.5. После окончания соревнований главный судья обязан в однодневный срок написать отчет и представить его и результаты соревнований в ОПС.\n\n";
  static String judges05 = "4.3. ЗАМЕСТИТЕЛИ ГЛАВНОГО СУДЬИ\n";
  static String judges05_1 = "Заместители главного судьи";
  static String judges06 =
      "4.3.1. Заместители главного судьи работают по его указаниям; в отсутствие главного судьи один из них заменяет его.\n\n";
  static String judges07 = "4.4. СТАРТЁР\n";
  static String judges07_1 = "Стартёр";
  static String judges08 =
      "4.4.1. Стартер должен полностью контролировать пловцов от момента передачи их ему рефери (п.4.1.5) до начала заплыва. Старт должен даваться в соответствии с п. 6 Правил.\n\n"
      "4.4.2. Стартер должен сообщать рефери обо всех случаях задержки спортсменом старта, преднамеренном непослушании или о любом другом нарушении дисциплины на старте, но только рефери может дисквалифицировать пловца за эту задержку или неправильное поведение.\n\n"
      "4.4.3. Стартер имеет право решать, был ли старт правильным, причем это решение может отменить, только рефери.\n\n"
      "4.4.4. Во время подачи сигнала стартер должен находиться на боковой стороне бассейна примерно в пяти метрах от стартовой линии так, чтобы хронометристы могли видеть и /или слышать стартовый сигнал, а пловцы – слышать его.\n\n";
  static String judges09 = "4.5. СУДЬЯ ПРИ УЧАСТНИКАХ\n";
  static String judges09_1 = "Судья при участниках";
  static String judges10 =
      "4.5.1. Судья при участниках должен собирать пловцов перед каждым заплывом.\n\n"
      "4.5.2. Судья при участниках должен сообщать рефери о любых нарушениях в отношении рекламы (п. 2.4.), а также, если пловец отсутствует при формировании заплыва перед выходом на старт.\n\n";
  static String judges11 = "ИНЫЕ КАТЕГОРИИ СУДЕЙ\n";
  static String judges11_1 = "Иные категории судей";
  static String judges12 = "- Старший судья на повороте;\n"
      "- Судья на повороте;\n"
      "- Судья по технике плавания;\n"
      "- Старший судья-хронометрист;\n"
      "- Судья-хронометрист;\n"
      "- Старший судья на финише (при необходимости);\n"
      "- Судья на финише (при необходимости);\n"
      "- Секретариат;\n"
      "- Ответственный за правильность результатов (секретарь);\n";

  //Formation of swims
  static String formation01 =
      "На всех дистанциях соревнований по плаванию чемпионатов, первенств, Кубков, Спартакиад и других соревнований ВФП, участники распределяются следующим образом.\n\n";
  static String formation02 = "5.1. ПРЕДВАРИТЕЛЬНЫЕ ЗАПЛЫВЫ\n";
  static String formation02_1 = "Предварительные заплывы";
  static String formation03 =
      "5.1.1. У всех участников должны быть проставлены в заявке лучшие результаты, показанные в соревнованиях за одобренный квалификационный период до последнего срока подачи заявки на соревнования. Заявки должны быть написаны на заявочной форме или присланы on-line по требованию организаторов. Эти результаты по нарастающему времени заносятся руководящим комитетом в список. Пловцы, не имеющие в заявках официальных результатов, помещаются в конце списка с нулевым временем. Места в списке у пловцов с одинаковым временем или у пловцов, не имеющих заявочных результатов, определяются жеребьевкой. Номера дорожек в заплыве пловцы получают согласно методике, описанной ниже – в п. 5.1.2. Пловцы распределяются по предварительным заплывам в соответствии с заявочными результатами следующим образом:\n\n"
      "5.1.1.1. при одном предварительном заплыве дорожки в нем распределяются, как в финальных заплывах, и этот заплыв должен быть перенесен в финальную часть соревнований;\n\n"
      "5.1.1.2. при двух предварительных заплывах быстрейший спортсмен распределяется во второй заплыв, следующий - в первый, следующий за ним – во второй, далее следующий - в первый и т.д.;\n\n"
      "5.1.1.3. при трех заплывах, кроме дистанций 400 м, 800 м и 1500 м, сильнейший пловец распределяется в третий заплыв, следующий за ним – во второй, далее следующий - в первый. Четвертый по скорости пловец распределяется в третий заплыв, пятый – во второй, шестой – в первый, седьмой – в третий и т.д.;\n\n"
      "5.1.1.4. при четырех и более заплывах, кроме дистанций 400 м, 800 м и 1500 м, последние три заплыва дистанции распределяются в соответствии п. 5.1.1.3. Заплыв, предшествующий последним трем, включает в себя пловцов, имеющих более низкие результаты; заплыв, предшествующий последним четырем заплывам, состоит из участников с еще более слабыми результатами и т.д. Дорожки внутри каждого заплыва определяются по заявочным результатам в соответствии с п. 5.1.2.\n\n"
      "5.1.1.5. На дистанциях 400 м, 800 м и 1500 м спортсмены для двух последних заплывов отбираются в соответствии с п.5.1.1.2\n\n"
      "5.1.1.6. Исключение: если на одной дистанции оказываются два или более заплыва, то в каждом предварительном заплыве должно быть не менее трех пловцов, однако при последующих отказах участников от старта число пловцов в таком заплыве может стать и менее трех.\n\n"
      "5.1.1.7. Если в бассейнах с 10 дорожками в предварительных заплывах на дистанциях 800м и 1500м вольным стилем на 8 месте устанавливается равное время, может быть использована дорожка № 9. Распределение спортсменов на дорожки № 8 и № 9 осуществляется по жребию. В случае если на 8 месте показаны 3 равных результата, могут использоваться дорожки № 9 и № 0. Распределение спортсменов на дорожки №№ 8, 9 и № 0 осуществляется по жребию.\n\n"
      "5.1.1.8. Если в бассейне нет 10 дорожек, применяется правило 5.2.3.\n\n"
      "5.1.2. Исключая дистанции 50 м в пятидесятиметровом бассейне, распределение дорожек (дорожка № 1 (дорожка № 0 при 10 дорожках) расположена на правой стороне бассейна, если смотреть от стартового конца) производится назначением сильнейшего пловца или команды на центральную дорожку в бассейне с нечетным числом дорожек или на третью или четвертую дорожку соответственно в бассейнах имеющих 6 или 8 дорожек. В бассейнах с 10 дорожками сильнейший спортсмен ставится на дорожку № 4. Пловец, имеющий следующее время, помещается слева от него, затем попеременно справа и слева в соответствии с заявочным временем размещаются остальные пловцы. Пловцы с одинаковыми результатами получают дорожки по жеребьевке, как было сказано выше.\n\n"
      "5.1.3. Когда проводятся соревнования на дистанциях 50 метров в пятидесятиметровом бассейне по усмотрению организационного комитета, дистанция может проходить либо от стартового конца бассейна к поворотному концу, либо от поворотного конца к стартовому, в зависимости от таких факторов, как наличие с обоих концов бассейна автоматического оборудования, места стартера и т. п. Организационный комитет должен известить пловцов о своем решении до начала стартов. Вне зависимости от того, в каком направлении будут стартовать пловцы, они получают те же дорожки, как и при старте от стартового конца бассейна.\n\n";
  static String formation04 = "5.2. ПОЛУФИНАЛЫ И ФИНАЛЫ\n";
  static String formation04_1 = "Полуфиналы и финалы";
  static String formation05 =
      "5.2.1. В полуфиналах формирование заплывов должно осуществляться в соответствии с п. 5.1.1.2.\n\n"
      "5.2.2. Когда нет необходимости в предварительных заплывах, дорожки распределяются согласно п. 5.1.2. Когда предварительные заплывы или полуфиналы проводятся, дорожки распределяются в соответствии с п. 5.1.2, однако с учетом результатов, показанных в этих заплывах.\n\n"
      "5.2.3. Если в каком-либо номере программы один или несколько пловцов из одного или разных заплывов имеют равные результаты с точностью до 0/100 сек. и претендуют на 8/10 или 16/20 место (в зависимости от того, 8 или 10 дорожек используются), им должен быть назначен переплыв. Переплыв, назначается для определения того, кто попадет в соответствующий финал. Такой, переплыв, проводится после завершения пловцами этой дистанции, по договоренности между рефери соревнований и заинтересованными сторонами. Если будет снова зарегистрировано одинаковое время, назначается, еще один переплыв. При необходимости, может быть назначен переплыв для 1 и 2 номеров резерва, если ими было показано одинаковое время.\n\n"
      "5.2.4. Если один или несколько участников снимаются с полуфинальных или финальных заплывов, вместо них вызываются спортсмены в порядке их классификации в предварительных или полуфинальных заплывах. Заплыв или заплывы должны быть переформированы, как предписано п. 5.1.2; подробная информация обо всех изменениях и заменах должна быть представлена в дополнительных листах стартовых протоколов.\n\n"
      "5.2.5. При участии в предварительных заплывах, полуфиналах и финалах спортсмены должны явиться в комнату предварительного формирования заплывов не позднее, чем за 20 минут до старта. После проверки спортсмены отправляются в комнату финального формирования заплывов.\n\n"
      "5.2.6. На других соревнованиях возможно распределение дорожек жеребьевкой.\n";

  //StartDistanceTiming
  static String startDistTiming01 = "6. СТАРТ\n";
  static String startDistTiming02 =
      "6.1. Старт в заплывах вольным стилем, брассом, баттерфляем и комплексным плаванием осуществляется прыжком со стартовой тумбочки. После длинного свистка рефери (п. 4.1.5) пловцы должны встать на стартовую тумбочку и остаться там. По команде стартера «На старт» они немедленно принимают стартовое положение, поставив хотя бы одну ногу на переднюю часть стартовой тумбочки. Положение рук не регламентируется. Когда пловцы примут неподвижное положение, стартер должен дать стартовый сигнал.\n\n"
      "6.2. Старт в заплывах на спине и комбинированной эстафете производится из воды. По длинному свистку рефери (п. 4.1.5) пловцы должны немедленно войти в воду. По второму длинному свистку рефери пловцы должны без задержки вернуться к стартовой позиции (п. 8.1). Убедившись, что все спортсмены приняли стартовое положение, стартер дает команду «На старт». Когда все участники примут неподвижное положение, стартер должен дать стартовый сигнал.\n\n"
      "6.3. На Олимпийских играх, чемпионатах мира и других международных соревнованиях команда «На старт» должна подаваться на английском языке («Take your marks») и транслироваться через громкоговорители, установленные в каждой стартовой тумбочке.\n\n"
      "6.4. Любой пловец, стартующий до подачи стартового сигнала, должен быть дисквалифицирован. Если стартовый сигнал звучит до объявления дисквалификации, заплыв должен быть продолжен, и пловец или пловцы должны быть дисквалифицированы после окончания заплыва. Если дисквалификация объявлена перед стартовым сигналом, сигнал не дается, оставшиеся пловцы отзываются со стартовых тумбочек для повторного старта. Рефери повторяет стартовую процедуру, начиная с длинного свитка (второй свисток для плавания на спине) в соответствии с правилом 4.1.5.\n\n";
  static String startDistTiming03 = "12. ПРОХОЖДЕНИЕ ДИСТАНЦИИ\n";
  static String startDistTiming031 = "Прохождение дистанции";
  static String startDistTiming04 =
      "12.1. Все индивидуальные виды программы должны проводиться отдельно в мужских и женских дисциплинах.\n\n"
      "12.2. Спортсмен, выступающий в индивидуальных видах программы, должен пройти всю дистанцию, чтобы его результат был зачтен.\n\n"
      "12.3. Пловец должен закончить дистанцию на той же дорожке, на которой он стартовал.\n\n"
      "12.4. Во всех заплывах при выполнении поворотов пловец должен коснуться стенки или поворотного щита на конце бассейна. Поворот необходимо сделать от стенки, отталкиваться ногами от дна бассейна не разрешается.\n\n"
      "12.5. Пловец, вставший на дно бассейна во время заплывов вольным стилем или на этапе вольного стиля в комбинированной эстафете, не дисквалифицируется, если он не идет по дну.\n\n"
      "12.6. Подтягивание за разделительный шнур не разрешается.\n\n"
      "12.7. Пловец, оказавшийся на пути другого пловца и помешавший ему пройти дистанцию, дисквалифицируется. Если это нарушение совершено намеренно, то рефери должен доложить о нем представителю организации, проводящей соревнование, и представителю команды, членом которой является нарушитель.\n\n"
      "12.8. Во время соревнований пловцам не разрешается использовать или надевать какие-либо приспособления, увеличивающие скорость, плавучесть или выносливость (такие, как перчатки с перепонками, ласты, ручные повязки, липкие субстанции и пр.). Защитные очки надевать можно. Не допускается применение повязок на теле, если только это не разрешено врачом соревнований.\n\n"
      "12.9. Если спортсмен, не участвующий в проходящем заплыве, оказывается в воде до завершения заплыва всеми участниками, он отстраняется от участия в своем следующем заплыве.\n\n"
      "12.10. В каждой команде эстафетного плавания должно быть четыре пловца. Возможно проведение смешанных эстафет. В состав смешанных эстафет должны входить два (2) мужчины и две (2) женщины. Время, показанное на отрезках такой эстафеты, не может быть засчитано как рекорд и не может быть заявочным временем.\n\n"
      "12.11. В эстафетном плавании команда, пловец, который оторвет ноги от стартовой тумбочки раньше, чем коснулся стенки участник предыдущего этапа, должна быть дисквалифицирована.\n\n"
      "12.12. Команда эстафетного плавания дисквалифицируется, если ее участник, кроме пловца, назначенного плыть данный этап, окажется в воде до окончания дистанции участниками всех команд.\n\n"
      "12.13. Участники эстафетной команды и их очередность должны быть заявлены до заплыва. Участник эстафетной команды может стартовать только на одном этапе. Замена участников эстафетной команды производится из числа пловцов, заявленных на эти соревнования в соответствии с технической заявкой. Нарушение порядка поименной очередности проплыва этапов ведет к дисквалификации. Имена пловцов в эстафете комплексным плаванием должны быть указаны в соответствии с тем стилем, которым плывет данный спортсмен. Замена может быть сделана только по медицинским показаниям, подтвержденным документально.\n\n"
      "12.14. Пловец, закончивший свою дистанцию или этап в эстафетном плавании, должен как можно быстрее покинуть бассейн, не мешая при этом другим пловцам, которые еще не закончили свою дистанцию. В противном случае пловец, совершивший нарушение, или эстафетная команда должны быть дисквалифицированы.\n\n"
      "12.15. Если нарушение повлияло на результат пловца, рефери имеет право предоставить ему новую попытку в следующих заплывах, если нарушение произошло в финале или в последнем заплыве – назначить его переплыв.\n\n"
      "12.16. Не разрешается лидирование (гонка за лидером), также как не может быть использовано оборудование или другие средства для достижения такого эффекта.\n\n";
  static String startDistTiming05 = "13. ХРОНОМЕТРАЖ\n";
  static String startDistTiming051 = "Хронометраж";
  static String startDistTiming06 =
      "13.1. Работа автоматической системы регистрации времени должна проходить под контролем назначенных для этого судей. Данные, полученные автоматической системой регистрации времени, используются для определения победителя, всех последующих мест участников, а также всех результатов, показанных на каждой дорожке. Места и результаты, определенные таким образом, должны иметь преимущество перед показаниями хронометристов. В заплывах, где автоматическая система полностью отказала или очевидно, что она выдала ошибочные результаты, а также в тех случаях, когда пловцу не удалось вызвать срабатывания системы, официальными должны считаться результаты хронометристов (см. п.14.3).\n\n"
      "13.2. При использовании автоматической системы регистрации времени результаты должны учитываться только с точностью до 0,01 сек. При наличии результатов с точностью до 0,001 сек. третий знак не должен учитываться или использоваться при распределении мест. В заплыве с одинаковыми зарегистрированными результатами с точностью до 0,01 сек. все пловцы, имеющие эти результаты, должны занимать одно и то же место. Время, показываемое на демонстрационном табло, должно иметь точность только до 0,01 сек.\n\n"
      "13.3. Любое устройство для измерения времени, которое выключается судьей, считается секундомером. Время, установленное при помощи секундомера (ручной результат), должно быть зарегистрировано тремя хронометристами, назначенными или утвержденными представителем федерации страны, в которой проходит соревнование. Все секундомеры должны иметь свидетельства о поверке, выданные соответствующей контрольной организацией, гарантирующие точность секундомеров. Ручной результат должен регистрироваться с точностью до 0,01 сек. Когда не применяется автоматическая система регистрации времени, официальный ручной результат должен определяться следующим образом:\n\n"
      "13.3.1. Если два из трех секундомеров зарегистрировали одно и то же время, а третий результат не совпадает, два одинаковых времени должны стать официальным результатом.\n\n"
      "13.3.2. Если результаты всех трех секундомеров не совпадают, время секундомера со средним результатом считается официальным результатом.\n\n"
      "13.3.3. Когда сработали только два из трех секундомеров, официальным результатом должно быть их среднее время.\n\n"
      "13.4. Если участник дисквалифицирован во время дистанции или после ее окончания, то эта дисквалификация отмечается в официальных результатах, но ни результат, ни место участника не указывается и не объявляется.\n\n"
      "13.5. В случае дисквалификации эстафеты, зафиксированные этапы до момента дисквалификации должны быть зарегистрированы в официальных результатах.\n\n"
      "13.6. Во время проведения эстафет все отрезки по 50 и 100 м у пловцов первого этапа должны быть зарегистрированы и опубликованы в официальных результатах.\n";

  //Styles
  static String styles01 = "7. ПЛАВАНИЕ ВОЛЬНЫМ СТИЛЕМ\n";
  static String styles011 = "Плавание вольным стилем";
  static String styles02 =
      "7.1. Плавание вольным стилем означает, что участнику на дистанции разрешается плыть любым способом, исключением являются комплексное плавание и комбинированная эстафета, где вольный стиль – это любой другой способ, кроме плавания на спине, брасса и баттерфляя.\n\n"
      "7.2. Пловец должен коснуться стенки какой-либо частью своего тела при завершении каждого отрезка дистанции и на финише.\n\n"
      "7.3. Любая часть тела пловца должна разрывать поверхность воды во время заплыва, за исключением разрешения пловцу быть полностью погруженным под водой во время поворота и на расстоянии не более 15 м после старта и каждого поворота. В этой точке голова спортсмена должна разорвать поверхность воды.\n\n";
  static String styles03 = "8. ПЛАВАНИЕ НА СПИНЕ\n";
  static String styles031 = "Плавание на спине";
  static String styles04 =
      "8.1. Перед стартовым сигналом пловцы занимают исходное положение в воде лицом к стартовой тумбочке, держась обеими руками за стартовые поручни. Стоять на краю сливного желоба или упираться в него пальцами запрещено. При использовании устройства для старта на спине пальцы обеих ног должны упираться в середину или нижний край контактного щита. Цепляться пальцами ног за верхний край контактного щита запрещено.\n\n"
      "8.2. Когда используется устройство для старта на спине, каждый судья на повороте на стартовом конце бассейна должен установить устройство перед стартом и убрать его после старта.\n\n"
      "8.3. После стартового сигнала и после поворота пловец должен оттолкнуться и плыть на спине в течение всей дистанции, исключая момент выполнения поворота, как указано далее в п. 8.4. Нормальное положение на спине может включать вращательное движение тела в горизонтальной плоскости, но не более чем на 90о относительно горизонтальной поверхности. Положение головы не регламентируется.\n\n"
      "8.4. Какая-либо часть тела должна разрывать поверхность воды на всем протяжении дистанции, исключая разрешенное пловцу полное погружение во время поворота и на отрезке не более чем 15 м после старта и после каждого поворота. В этой точки голова спортсмена должна разорвать поверхность воды.\n\n"
      "8.5. При выполнении поворота пловец должен коснуться стенки бассейна на своей дорожке какой-либо частью тела. Во время поворота плечи могут быть опрокинуты по вертикали к положению на груди, после чего сразу же следует либо один непрерывный гребок одной рукой, либо один непрерывный гребок одновременно двумя руками для начала поворота. После отталкивания от стенки бассейна спортсмен должен вернуться к положению на спине.\n\n"
      "8.6. На финише дистанции пловец должен коснуться стенки, находясь в положении на спине.\n\n";
  static String styles05 = "9. ПЛАВАНИЕ БРАССОМ\n";
  static String styles051 = "Плавание брассом";
  static String styles06 =
      "9.1. После старта и после каждого поворота спортсмен может сделать один длинный гребок руками, находясь при этом полностью погруженным в воду. Разрешается один дельфинообразный удар ногами в любое время после старта и каждого поворота перед первым циклом движений.\n\n"
      "9.2. С начала первого гребка руками после старта и после каждого поворота пловец должен лежать на груди. Поворот на спину запрещен в любое время, кроме как при повороте после касания стенки бассейна, когда тело спортсмена может повернуться в любой плоскости и выйти в положение «на груди» после отрыва от стенки бассейна. От старта и на всем протяжении дистанции должен соблюдаться цикл «один гребок – один толчок ногами». Все движения рук должны быть одновременны и в одной горизонтальной плоскости без чередующихся движений.\n\n"
      "9.3. Обе руки вытягиваются вперед от груди по поверхности, выше или ниже поверхности воды. Локти должны находиться под водой, за исключением финального гребка до поворота, во время поворота и финального гребка на финише. Руки возвращаются назад по поверхности воды или под водой. Они не должны заходить за линию бедер, исключая первый гребок после старта и каждого поворота.\n\n"
      "9.4. В течение каждого полного цикла, какая-либо часть головы пловца должна разорвать водную поверхность. Голова должна разорвать поверхность воды прежде, чем руки начнут движение внутрь из наиширочайшей части второго гребка. Все движения ног должны быть одновременны и выполняться в одной горизонтальной плоскости без чередующихся движений.\n\n"
      "9.5. Во время активной части толчка стопы должны быть развернуты в стороны. Чередующиеся движения или дельфинообразные удары книзу не допускаются, кроме случаев, указанных в п. 9.1. Нарушение поверхности воды стопами ног разрешается, если только вслед за этим не следует дельфинообразный удар книзу.\n\n"
      "9.6. На каждом повороте и на финише дистанции касание должно быть сделано обеими раздвинутыми руками одновременно выше, ниже или по поверхности воды. При последнем гребке при повороте или на финише необязателен толчок ногами после гребка. Голова может погружаться в воду после последнего гребка руками перед касанием. Однако голова должна разорвать поверхность воды в какой-либо точке во время последнего полного или неполного цикла, предшествующего касанию.\n\n";
  static String styles07 = "10. ПЛАВАНИЕ БАТТЕРФЛЯЕМ\n";
  static String styles071 = "Плавание баттерфляем";
  static String styles08 =
      "10.1. От начала первого гребка руками после старта и после каждого поворота тело должно находиться на груди. Разрешены подводные удары ногами в стороны. Поворот на спину не разрешен в любое время, кроме как при повороте после касания стенки бассейна, когда тело спортсмена может повернуться в любой плоскости и выйти в положение «на груди» после отрыва от стенки бассейна.\n\n"
      "10.2. Обе руки должны одновременно проноситься вперед над водой и одновременно возвращаться обратно во время всей дистанции в соответствии в п. 10.5.\n\n"
      "10.3. Все движения вверх и вниз должны выполняться одновременно двумя ногами. Ноги или ступни могут быть не на одном уровне, но чередующиеся движения не разрешаются. Не разрешаются движения ногами как при плавании брассом.\n\n"
      "10.4. На каждом повороте и на финише касание должно быть одновременно обеими разведенными руками по поверхности, выше или ниже поверхности воды.\n\n"
      "10.5. При старте и на поворотах пловцу разрешается сделать под водой одно или несколько движений ногами и один гребок руками, которые должны вынести его на поверхность. Пловцу разрешается полное погружение на отрезке не более 15 м после старта и каждого поворота. В этой точке голова спортсмена должна разорвать поверхность воды. Пловец должен оставаться на поверхности до следующего поворота или до финиша.\n\n";
  static String styles09 =
      "11. КОМПЛЕКСНОЕ ПЛАВАНИЕ И КОМБИНИРОВАННАЯ ЭСТАФЕТА\n";
  static String styles091 = "Комплексное плавание и комбинированная эстафета";
  static String styles10 =
      "11.1. В индивидуальном комплексном плавании пловец проходит дистанцию четырьмя стилями в следующем порядке: баттерфляй, на спине, брасс и вольный стиль. Каждый отрезок должен составлять ¼ от общей дистанции.\n\n"
      "11.2. В комбинированной эстафете пловцы проходят дистанцию четырьмя стилями в следующем порядке: на спине, брасс, баттерфляй и вольный стиль.\n\n"
      "11.3. На каждом этапе спортсмен должен финишировать в соответствии с правилами данного способа плавания.\n";

  //Protest
  static String protest01 = "15. ПРОТЕСТ\n";
  static String protest02 = "15.1. Протесты возможны:\n"
      "    а) если правила и инструкции по проведению соревнований не соблюдаются;\n"
      "    б) если какие-либо условия представляют опасность для проведения соревнований и/или участников соревнований;\n"
      "    в) против решения рефери, при этом, протест против решения рефери по явному факту нарушения не принимается, так как решение рефери является окончательным;\n\n"
      "15.2. Протест должен быть направлен:\n"
      "    а) рефери (при отсутствии рефери – главному судье);\n"
      "    б) в письменном виде;\n"
      "    в) уполномоченным представителем команды;\n"
      "    г) в течение 30 минут по окончании соответствующего заплыва;\n"
      "Если условия, являющиеся причиной для подачи протеста замечены до начала проведения какого-либо вида программы (заплыва), протест следует подать до того момента, пока не прозвучал свисток к началу заплыва.\n\n"
      "15.3. В протесте должен быть указан вид программы, номер заплыва, номер дорожки, причина дисквалификации. Протест должен быть подписан уполномоченным представителем команды с расшифровкой фамилии и указанием даты и времени подачи протеста.\n\n"
      "15.4. Все протесты должны быть рассмотрены рефери в течение 60 минут по окончании сессии соревнований. Если им протест отклонён, рефери должен изложить причину, по которой принял такое решение.\n\n"
      "15.5. Представитель команды, чей протест был отклонён, имеет право в течение 60 минут после окончания сессии соревнований обратиться в Апелляционное жюри, чьё решение будет окончательным.\n\n"
      "15.6. Апелляционное жюри состоит из представителя ВФП, который является Председателем апелляционного жюри, Председателя Всероссийской коллегии судей, или в его отсутствие члена коллегии судей и 3-х членов жюри, состоящих из числа представителей команд, назначенных на совещании представителей команд.\n\n"
      "15.7. Решение апелляционного жюри принимается открытым голосованием до окончания самих соревнований. Член жюри, который является заинтересованным лицом в принятии какого-либо решения, участия в голосовании не принимает. При равенстве голосов, голос Председателя жюри является решающим.\n\n"
      "15.8. Состав апелляционного жюри объявляется на совещании представителей команд перед началом соревнований. Если на этом совещании не было объявлено о составе апелляционного жюри, то оно не может быть назначено после начала соревнований. В этом случае решения рефери являются окончательными\n\n";
  static String protest04 = "Форма протеста";

  //Requirements
  static String requirements01 = "16.1.1. ДЛИНА\n";
  static String requirements01_1 = "Длина";
  static String requirements02 =
      "16.1.1.1. 50,000 метров. Когда используются контактные щиты автоматической системы регистрации времени на стартовом или дополнительно на поворотном конце, бассейн должен иметь такую длину, чтобы между двумя щитами обеспечивались размеры дистанции 50,000 метров.\n\n"
      "16.1.1.2. 25,000 метров. Когда используются контактные щиты автоматической системы регистрации времени на стартовом или дополнительно на поворотном конце, бассейн должен иметь такую длину, чтобы между двумя щитами обеспечивались размеры дистанции 25,000 метров.\n\n";
  static String requirements03 =
      "16.1.2. ДОПУСТИМЫЕ ОТКЛОНЕНИЯ В РАЗМЕРАХ БАССЕЙНА\n";
  static String requirements03_1 = "Допустимые отклонения в размерах бассейна";
  static String requirements04 =
      "16.1.2.1. Разрешаются отклонения от номинальной длины 50,000 метров – плюс 0,030 м, на каждой дорожке, минус 0.000 м - между торцевыми стенками бассейна во всех точках от 0,300 м выше и 0,800 м ниже поверхности воды. Эти измерения должны быть сделаны инспектором или другим официальным квалифицированным лицом, назначенным или одобренным представителем федерации страны, в которой находится бассейн. Допустимые отклонения не должны быть превышены и в том случае, когда установлены контактные щиты.\n\n"
      "16.1.2.2. Разрешаются отклонения от номинальной длины 25,000 метров – плюс 0.030 м, на каждой дорожке, минус 0,000 м - между торцевыми стенками бассейна во всех точках от 0,300 м выше и 0,800 м ниже поверхности воды. Эти измерения должны быть сделаны инспектором или другим официальным квалифицированным лицом, назначенным или одобренным представителем федерации страны, в которой находится бассейн. Допустимые отклонения не должны быть превышены и в том случае, когда установлены контактные щиты.\n\n"
      "16.1.2.3 Минимальная требуемая глубина – 1,0 м. Минимальная глубина в 1,35 м должна быть обеспечена на отрезке от 1 м до 6 м на концах бассейна, оборудованных стартовыми тумбочками. На остальном протяжении бассейна минимальная требуемая глубина – 1,0 м.\n\n";
  static String requirements05 = "16.1.3. СТЕНКИ\n";
  static String requirements05_1 = "Стенки";
  static String requirements06 =
      "16.1.3.1. Торцевые стенки должны быть вертикальными, параллельными и образовывать прямой угол 90о с направлением плавания и с поверхностью воды. Стенки должны быть сделаны из прочного материала, быть нескользкими до глубины 0,8 м от поверхности воды так, чтобы обеспечить пловцу возможность безопасного касания и толчка на поворотах.\n\n"
      "16.1.3.2. Вдоль стенок бассейна разрешается делать выступы для отдыха. Они должны находиться на глубине не менее 1,2 м от поверхности воды и могут иметь ширину от 01, до 0,15 м. Возможно применение как внутренних, так и внешних выступов, однако предпочтительнее внутренние выступы.\n\n"
      "16.1.3.3. Все четыре стороны бассейна могут иметь сливные желоба. При наличии желобов следует учитывать возможность установки контактных щитов на торцевых стенках бассейна, выступающих на высоту 0,3 м над поверхностью воды. Желоба должны быть закрыты решеткой или сеткой.\n\n"
      "16.1.3.4. Дорожки должны иметь ширину 2,5 м. С внешней стороны первой и последней дорожек должны быть свободные пространства шириной 0,2 м.\n";

  //OpenWater
  static String openWater01 = "19. ОПРЕДЕЛЕНИЯ\n";
  static String openWater011 = "Определения";
  static String openWater02 =
      "19.1. ПЛАВАНИЕ НА ОТКРЫТОЙ ВОДЕ означает соревнования, которые проводятся в реках, озерах, океанах или водных каналах, за исключением плавания на дистанции 10 км.\n\n"
      "19.1.1. МАРАФОНСКОЕ ПЛАВАНИЕ означает плавание на дистанцию 10 км и длиннее в соревнованиях по плаванию на открытой воде.\n\n"
      "19.2. Возраст участников соревнований по плаванию на открытой воде должен быть не менее 14 лет. Возраст участников определяется на 31 декабря года проведения соревнований.\n\n"
      "19.2.1. В плавании на открытой воде определяются следующие возрастные категории:\n"
      "    • юноши и девушки 14-15 лет;\n"
      "    • юноши и девушки 16-17 лет;\n"
      "    • юниоры и юниорки 18-19 лет;\n"
      "    • мужчины и женщины.\n\n";
  static String openWater03 = "20. СУДЬИ\n";
  static String openWater04 =
      "20.1 Следующие судьи должны быть назначены для судейства соревнований по плаванию на открытой воде:\n"
      "Главный рефери (один на дистанцию)\n"
      "Рефери (минимум 2, дополнительные рефери назначаются в зависимости от количества заявившихся участников)\n"
      "    • Главный судья\n"
      "    • Старший хронометрист плюс 2 хронометриста\n"
      "    • Старший судья на финише плюс 2 судьи на финише\n"
      "    • Судья по безопасности\n"
      "    • Судья по медицине\n"
      "    • Судья сопровождения\n"
      "    • Судья при участниках\n"
      "    • Старший судья на дистанции и судьи на дистанции (по одному на каждого участника) кроме дистанций 10 км и менее.\n"
      "    • Судья на повороте (один на каждое изменение трассы)\n"
      "    • Судья на плоту для питания (когда используется плот для питания)\n"
      "    • Стартер\n"
      "    • Судья-информатор\n"
      "    • Главный секретарь плюс 5 секретарей\n\n"
      "Примечание: один судья не может выступать одновременно на разных позициях. Он может выступать на другой позиции только после того, как выполнил все свои обязанности на предыдущей позиции.\n\n";
  static String openWater05 = "22. СТАРТ\n";
  static String openWater051 = "Старт";
  static String openWater06 =
      "22.1. Все соревнования по плаванию на открытой воде должны начинаться, когда все спортсмены стоят на стартовой платформе или в воде, на такой глубине, с которой можно начать плавание по звуку стартового сигнала.\n"
      "22.1.1. При старте со стартовой платформы, спортсмен должен занять позицию, доставшуюся ему по жребию.\n\n"
      "22.2. Судья при участниках должен с равными промежутками информировать спортсменов и судей о времени, оставшемся до старта вплоть до последних 5 минут, когда оповещение о начале старта дается каждую минуту.\n\n"
      "22.3. В соответствии с количеством поданных заявок, соревнований должны разделяться на мужские и женские заплывы. Мужские заплывы должны всегда предшествовать женским.\n\n"
      "22.4. Линия старта должна быть четко обозначена либо обозначением, приподнятым над поверхностью воды, либо временным приспособлением на уровне поверхности воды, которое затем убирается.\n\n"
      "22.5. Рефери, поднятым над головой флажком и короткими свистками, дает знать о приближении старта и жестом вытянутой к стартеру руки с флажком показывает, что пловцы теперь находятся под контролем стартера.\n\n"
      "22.6. Стартер должен располагаться на такой позиции, чтобы его было хорошо видно всем участникам соревнований.\n"
      "22.6.1. По команде стартера «На старт» участники соревнований должны принять стартовое положение, когда, используется стартовая платформа, хотя бы одна нога должна находиться на передней части платформы. Если стартовая платформа не используется, все спортсмены должны встать вдоль стартовой линии.\n"
      "22.6.2. Стартер дает стартовый сигнал, когда он считает, что все спортсмены готовы.\n\n"
      "22.7. Стартовый сигнал должен быть как звуковым, так и визуальным.\n\n"
      "22.8. Если, по мнению рефери один из спортсменов получил нечестное преимущество на старте, провинившийся спортсмен получает желтый или красный флажок в соответствии с правилом 25.\n\n"
      "22.9. Все лодки сопровождения должны находиться за линией старта и не мешать старту спортсменов, а в случае, если команда лодки сопровождения забирает спортсмена из воды, она должна маневрировать так, чтобы не мешать основной массе плывущих спортсменов.\n\n"
      "22.10. Независимо от того, стартуют ли мужчины и женщины одновременно или в разных заплывах, эти заплывы в любом случае должны считаться отдельными видами соревнований.\n\n";
  static String openWater07 = "23. МЕСТО ПРОВЕДЕНИЯ СОРЕВНОВАНИЙ\n";
  static String openWater071 = "Место проведения соревнований";
  static String openWater08 =
      "23.1. Чемпионаты России могут иметь дистанции 25 км, 16 км, 10 км и 5 км, и проводиться в водных бассейнах по трассам, одобренными ВФП.\n"
      "23.1.1. Первенства России могут иметь дистанции 7,5 км, 5 км, 3 км и проводиться в водных бассейнах по трассам, одобренными ВФП.\n\n"
      "23.2. Трасса должна быть проложена в местах со слабым течением или небольшими приливами, вода может быть как пресной, так и соленой.\n\n"
      "23.3. Место проведения соревнований должно иметь сертификат соответствия, выданный уполномоченными органами. В целом, сертификат должен подтверждать чистоту воды и отсутствие в ней вредных примесей.\n\n"
      "23.4. Минимальная глубина воды в любой точке трассы должна быть 1,40 м.\n\n"
      "23.5. Минимальная температура воды должна быть 16 С, а максимальная температура – 31С. Температура воды должна быть измерена в день соревнований, за 2 часа до старта, в середине трассы на глубине 40 см. Контроль температуры воды производиться комиссией, в состав которой входят рефери, представитель оргкомитета и один тренер от присутствующих команд, назначенный на техническом совещании.\n"
      "23.5.1. Судья по безопасности должен проводить периодический мониторинг температуры воды в течение прохождения трассы спортсменами.\n\n"
      "23.6. Все места поворотов/изменения трассы должны быть четко обозначены. Поворотные буи, которые отмечают изменения трассы, должны отличаться по цвету от направляющих буев.\n\n"
      "23.7. Лодка или платформа с обозначениями, на которой располагается судья на повороте, должна находиться во всех точках изменения трассы и таким образом, чтобы не препятствовать спортсменам, видеть точку поворота.\n\n"
      "23.8. Все платформы для питания спортсменов, поворотные буйки и лодки судей на повороте должны быть надежно закреплены и не совершать движений под воздействием приливов, ветра или других факторов.\n\n"
      "23.9. Финишный створ должен быть обозначен ограничительными дорожками яркого цвета.\n\n"
      "23.10. Линия финиша должна быть четко обозначена и иметь вертикальную створку финиша.\n\n";
  static String openWater09 = "24. ТРАССА (ДИСТАНЦИЯ)\n";
  static String openWater091 = "Трасса (дистанция)";
  static String openWater10 =
      "24.1. Все соревнования по плаванию на открытой воде проводятся вольным стилем. Спортсмены должны проплыть всю дистанцию, пройти все повороты и придерживаться общего направления трассы.\n\n"
      "24.2. Судья на дистанции должен дать указание спортсмену, который, по его мнению, приобретает преимущество, следуя в створе лодки сопровождения, выйти из створа.\n";

  //about app
  static String about01 =
      "Записывайте свои результаты по плаванию с помощью SwimCoach, отслеживайте свой прогресс и улучшенную статистику плавания.\n"
      "Идеально подходит для каждого – начинающих, продвинутых спортсменов, любителей фитнеса, триатлетов и тренеров.";

  //privacy policy
  static String privacyPolicy01 = "\nДата вступления в силу: 1 марта 2019 г.\n";
  static String privacyPolicy02 =
      "В настоящей Политике конфиденциальности для SwimCoach объясняются методы защиты конфиденциальности, которые компания (“Brave Developers”, “мы”, “нас”, “наш” и производные) применяют в отношении информации, собираемой через приложение SwimCoach.\n\n";
  static String privacyPolicy03 = "1. СОБИРАЕМЫЕ СВЕДЕНИЯ\n";
  static String privacyPolicy04 =
      "Мы можем собирать и сохранять информацию, которую вы предоставляете напрямую или создаете посредством использования SwimCoach, а именно:\n\n • контактная информация (например, адрес электронной почты);\n\n • информация, связанная с физической активностью (например, стилях плавания, длительности упражнений);\n\n • данные профиля SwimCoach (например, имя, дата рождения, пол, фото профиля);\n\n • информация об устройстве (например, IP-адрес, версия встроенного ПО, операционная система, а также идентификаторы устройства, включая MAC-адрес, идентификатор Android, серийный номер и идентификаторы рекламодателей на мобильном устройстве);\n\n • содержимое, которое вы публикуете (например, свои результаты)";
  static String privacyPolicy05 =
      "Оставляя свои данные, Вы соглашаетесь с условиями и ";
  static String privacyPolicy06 = "Политикой конфиденциальности";

  //saveDialog in Timer Screen
  static String saveDialog01 = "Сохранить";
  static String saveDialog02 = "Отмена";
  static String saveDialog08 = "Бассейн";
  static String saveDialog09 = "25 м";
  static String saveDialog10 = "50 м";
  static String saveDialog11 = "бассейн 25 м";
  static String saveDialog12 = "бассейн 50 м";
  static String saveDialog13 = "Дата";

  //distances
  static String distances01 = "50 м";
  static String distances02 = "100 м";
  static String distances03 = "200 м";
  static String distances04 = "400 м";
  static String distances05 = "800 м";
  static String distances06 = "1500 м";
  static String distances07 = "4х50 м";
  static String distances08 = "4х100 м";
  static String distances09 = "4х200 м";

  static String resultCard01 = "Пол    ";
  static String resultCard02 = "Возраст";
  static String resultCard03 = "Тип плавания: ";
  static String resultCard04 = "Дистанция: ";
  static String resultCard05 = "Результат: ";
  static String resultsRemove = "Результат удален";
  static String resultsRemoveIt = "Удалить";
}
