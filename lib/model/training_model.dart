import 'package:firebase_database/firebase_database.dart';

class TrainingModel {
  String title, date, description, key;

  TrainingModel(this.title, this.date, this.description, this.key);

  TrainingModel.fromSnapshot(DataSnapshot snapshot)
      : title = snapshot.value["title"],
        date = snapshot.value["date"],
        description = snapshot.value['description'],
        key = snapshot.key;

  toJson() {
    return {
      "title": title,
      "date": date,
      "description": description,
      "key": key
    };
  }
}
