import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';

abstract class BaseAuth {
  Future<String> currentName();
  Future<String> signIn();
  Future<void> signOut();
  Future<void> signOutGoogle();
  Future<String> getUid();
  Future<String> getPhotoUrl();
}

class Authentication implements BaseAuth {
  final FirebaseAuth _auth = FirebaseAuth.instance;
  final GoogleSignIn _googleSignIn = GoogleSignIn();

  @override
  Future<String> signIn() async {
    GoogleSignInAccount googleSignInAccount = await _googleSignIn.signIn();
    GoogleSignInAuthentication authentication =
    await googleSignInAccount.authentication;
    final AuthCredential credential = GoogleAuthProvider.getCredential(
      accessToken: authentication.accessToken,
      idToken: authentication.idToken,
    );
    final FirebaseUser user = await _auth.signInWithCredential(credential);
    return user.displayName;
  }

  @override
  Future<void> signOut() async {
    return _auth.signOut();
  }

  @override
  Future<void> signOutGoogle() async {
    return _googleSignIn.signOut();
  }

  @override
  Future<String> currentName() async {
    FirebaseUser user = await _auth.currentUser();
    return user != null ? user.displayName : null;
  }

  @override
  Future<String> getUid() async {
    FirebaseUser user = await _auth.currentUser();
    return user != null ? user.uid : null;
  }

  @override
  Future<String> getPhotoUrl() async {
    FirebaseUser user = await _auth.currentUser();
    return user != null ? user.photoUrl : null;
  }
}
