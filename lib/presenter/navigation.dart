import 'package:flutter/material.dart';
import 'package:swim_teacher/screens/main_screen/main_drawer.dart';

import 'custom_route.dart';

class AppNavigation {
  static void toMainPage(BuildContext context) {
    Navigator.push(
        context,
        CustomRoute(
            builder: (context) => MainDrawer()));
  }
}
