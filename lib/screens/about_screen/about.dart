import 'package:flutter/material.dart';
import 'package:swim_teacher/model/strings.dart';
import 'package:swim_teacher/presenter/navigation.dart';
import 'package:swim_teacher/screens/main_screen/navigation_drawer.dart';
import 'package:flutter_svg/flutter_svg.dart';

class About extends StatefulWidget {
  State createState() => _AboutState();
}

class _AboutState extends State<About> {
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey();

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        AppNavigation.toMainPage(context);
      },
      child: Scaffold(
          backgroundColor: Colors.white,
          drawer: NavigationDrawer(),
          key: _scaffoldKey, // New Line
          appBar: AppBar(
              backgroundColor: Colors.white,
              elevation: 0.0,
              centerTitle: true,
              title: Text(
                Strings.about,
                style: TextStyle(
                    fontFamily: 'RoundedMplusMedium',
                    fontWeight: FontWeight.w500,
                    fontSize: 16,
                    color: Color.fromRGBO(27, 39, 45, 0.75)),
              ),
              leading: FlatButton(
                  onPressed: () {
                    _scaffoldKey.currentState.openDrawer();
                  },
                  child: SvgPicture.asset('images/hamburger.svg'))),
          body: Padding(
            padding: EdgeInsets.only(left: 24.0, right: 24.0),
            child: ListView(
              //mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.only(top: 8.0, bottom: 16.0),
                  child: SvgPicture.asset('images/logo.svg'),
                ),
                Padding(
                  padding: EdgeInsets.only(top: 16, bottom: 16),
                  child: Image.asset('images/about_pict.png'),
                ),
                Text(
                  Strings.about01,
                  style: TextStyle(fontFamily: 'RoundedMplus', fontSize: 16.0),
                ),
              ],
            ),
          )),
    );
  }
}
