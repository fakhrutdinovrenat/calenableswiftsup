import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:intl/intl.dart';
import 'package:swim_teacher/model/account_model.dart';
import 'package:swim_teacher/model/strings.dart';
import 'package:swim_teacher/presenter/authentication.dart';
import 'package:swim_teacher/presenter/custom_route.dart';
import 'package:swim_teacher/presenter/navigation.dart';
import 'package:swim_teacher/presenter/toastview.dart';
import 'package:swim_teacher/screens/auth_screen/auth_screen.dart';
import 'package:swim_teacher/screens/main_screen/navigation_drawer.dart';
import 'package:swim_teacher/screens/privacy_policy/privacy_screen.dart';

class AccountSettings extends StatefulWidget {
  AccountSettings({Key key, this.auth});

  final BaseAuth auth;

  @override
  _AccountSettingsState createState() => _AccountSettingsState();
}

class _AccountSettingsState extends State<AccountSettings> {
  AccountModel _accountModel;
  DatabaseReference _reference;

  bool _sex;
  String _gender;
  String _birthDay = "";
  String _bDayToDatabase = "";
  String typeOfUser;

  var userTypes = <String>[Strings.swimmer, Strings.coach];

  @override
  void initState() {
    super.initState();
    _accountModel = AccountModel("", "", "");
    _getReference();
  }

  void _getReference() async {
    try {
      String _userUid = await widget.auth.getUid();
      final FirebaseDatabase database = FirebaseDatabase.instance;
      _reference =
          database.reference().child(_userUid).child(Strings.accountReference);
      _reference.once().then((ds) {
        print(ds.value);
        if (ds.value != null) {
          setState(() {
            initializeDateFormatting("ky", null);
            DateTime date = DateTime.parse(ds.value['birthDate']);
            _gender = ds.value['sex'];
            ds.value['sex'] == Strings.manResults ? _sex = true : _sex = false;
            _bDayToDatabase = ds.value['birthDate'];
            _birthDay = DateFormat.d().format(date) +
                " " +
                DateFormat.MMMM("ky").format(date) +
                " " +
                DateFormat.y().format(date);
            typeOfUser = ds.value['status'];
          });
        } else {
          setState(() {
            _sex = true;
            _gender = Strings.manResults;
            _birthDay = Strings.noDate;
            typeOfUser = Strings.swimmer;
          });
        }
      });
    } catch (e) {
      print(e);
    }
  }

  void _handleSubmit() {
    _accountModel.status = typeOfUser;
    _accountModel.sex = _gender;
    _accountModel.birthDate = _bDayToDatabase;
    _reference.set(_accountModel.toJson()).then((_) {
      Toast.show(Strings.resultSaved, context);
    });
  }

  _selectSex(bool val) {
    setState(() {
      _sex = val;
      _sex ? _gender = Strings.manResults : _gender = Strings.womenResults;
    });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        AppNavigation.toMainPage(context);
      },
      child: Scaffold(
          backgroundColor: Colors.white,
          drawer: NavigationDrawer(),
          appBar: AppBar(
            backgroundColor: Colors.white,
            elevation: 0.0,
            centerTitle: true,
            title: Text(Strings.accSettings,
                style: TextStyle(
                    fontFamily: 'RoundedMplusMedium',
                    fontWeight: FontWeight.w500,
                    fontSize: 16,
                    color: Color.fromRGBO(27, 39, 45, 0.75))),
            leading: Builder(
                builder: (context) => FlatButton(
                    onPressed: () {
                      Scaffold.of(context).openDrawer();
                    },
                    child: SvgPicture.asset('images/hamburger.svg'))),
          ),
          body: Padding(
            padding: EdgeInsets.all(24.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                Column(
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.only(top: 16, bottom: 16),
                      child: FutureBuilder(
                          future: widget.auth.getPhotoUrl(),
                          //initialData: "Loading...",
                          builder: (BuildContext context,
                              AsyncSnapshot<String> text) {
                            if (text.hasData) {
                              if (text.data != null) {
                                return ClipOval(
                                  child: Image.network(text.data),
                                );
                              }
                            } else {
                              return CircularProgressIndicator();
                            }
                          }),
                    ),
                    FutureBuilder(
                        future: widget.auth.currentName(),
                        //initialData: "Loading...",
                        builder:
                            (BuildContext context, AsyncSnapshot<String> text) {
                          if (text.hasData) {
                            if (text.data != null) {
                              return Text(text.data,
                                  style: TextStyle(
                                      fontFamily: 'RoundedMplus',
                                      fontSize: 16,
                                      color: Color.fromRGBO(27, 39, 45, 0.75)));
                            }
                          } else {
                            return CircularProgressIndicator();
                          }
                        }),
                  ],
                ),
                Column(
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.only(left: 16),
                          child: Text(
                            Strings.resultCard01,
                            style: TextStyle(
                                fontFamily: 'RoundedMplus',
                                fontSize: 14.0,
                                color: Color.fromRGBO(27, 39, 45, 0.56)),
                          ),
                        ),
                        Row(
                          children: <Widget>[
                            Radio(
                              value: true,
                              groupValue: _sex,
                              onChanged: (val) {
                                _selectSex(val);
                              },
                            ),
                            Text(
                              Strings.manResults,
                              style: TextStyle(
                                  fontFamily: 'RoundedMplus',
                                  fontSize: 14.0,
                                  color: Color.fromRGBO(27, 39, 45, 0.75)),
                            ),
                          ],
                        ),
                        Row(
                          children: <Widget>[
                            Radio(
                              value: false,
                              groupValue: _sex,
                              onChanged: (val) {
                                _selectSex(val);
                              },
                            ),
                            Text(
                              Strings.womenResults,
                              style: TextStyle(
                                  fontFamily: 'RoundedMplus',
                                  fontSize: 14.0,
                                  color: Color.fromRGBO(27, 39, 45, 0.75)),
                            ),
                          ],
                        ),
                      ],
                    ),
                    Divider(color: Color.fromRGBO(27, 39, 45, 0.30)),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Expanded(
                          child: FlatButton(
                            child: Row(
                              children: <Widget>[
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Text(Strings.birthDay,
                                        style: TextStyle(
                                            fontFamily: 'RoundedMplus',
                                            fontSize: 10.0,
                                            color: Color.fromRGBO(
                                                27, 39, 45, 0.56))),
                                    Text(_birthDay,
                                        style: TextStyle(
                                            fontFamily: 'RoundedMplus',
                                            fontSize: 14.0,
                                            color: Color.fromRGBO(
                                                27, 39, 45, 0.75))),
                                  ],
                                ),
                              ],
                            ),
                            onPressed: () {
                              _showDatePicker();
                            },
                          ),
                        ),
                        IconButton(
                          icon: Icon(Icons.clear,
                              color: Color.fromRGBO(27, 39, 45, 0.3)),
                          onPressed: () {
                            setState(() {
                              _birthDay = Strings.noDate;
                              _bDayToDatabase = "";
                            });
                          },
                        )
                      ],
                    ),
                    Divider(color: Color.fromRGBO(27, 39, 45, 0.30)),
                    DropdownButton(
                      isExpanded: true,
                      style: TextStyle(
                          fontFamily: 'RoundedMplus',
                          fontSize: 14.0,
                          color: Color.fromRGBO(27, 39, 45, 0.56)),
                      value: typeOfUser,
                      items: userTypes.map((String value) {
                        return DropdownMenuItem<String>(
                          value: value,
                          child: Padding(
                            padding: EdgeInsets.only(left: 16),
                            child: Text(
                              value,
                              style: TextStyle(
                                  fontFamily: 'RoundedMplus',
                                  fontSize: 14.0,
                                  color: Color.fromRGBO(27, 39, 45, 0.75)),
                            ),
                          ),
                        );
                      }).toList(),
                      onChanged: (value) {
                        setState(() {
                          typeOfUser = value;
                        });
                      },
                    )
                  ],
                ),
                Column(
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Expanded(
                          child: Padding(
                            padding: EdgeInsets.symmetric(vertical: 4),
                            child: Container(
                              decoration: BoxDecoration(
                                  gradient: LinearGradient(
                                    colors: [
                                      Color.fromRGBO(86, 155, 232, 1),
                                      Color.fromRGBO(64, 114, 226, 1)
                                    ],
                                    begin: Alignment.bottomRight,
                                    end: Alignment.topLeft,
                                  ),
                                  borderRadius: BorderRadius.circular(22.0)),
                              child: FlatButton(
                                shape: StadiumBorder(),
                                onPressed: () {
                                  if (_bDayToDatabase.isNotEmpty) {
                                    _handleSubmit();
                                    Navigator.push(
                                        context,
                                        CustomRoute(
                                            builder: (context) =>
                                                Auth(auth: Authentication())));
                                  } else {
                                    Toast.show(Strings.noBday, context);
                                  }
                                },
                                child: Text(
                                  Strings.saveDialog01,
                                  style: TextStyle(
                                      fontFamily: 'RoundedMplusMedium',
                                      fontSize: 14.0,
                                      fontWeight: FontWeight.w500,
                                      color: Colors.white),
                                  textAlign: TextAlign.center,
                                ),
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                    RichText(
                      text: TextSpan(children: [
                        TextSpan(
                          text: Strings.privacyPolicy05,
                          style: TextStyle(
                              fontFamily: 'RoundedMplus',
                              fontSize: 12.0,
                              color: Color.fromRGBO(27, 39, 45, 0.56)),
                        ),
                        TextSpan(
                            text: Strings.privacyPolicy06,
                            style: TextStyle(
                                fontFamily: 'RoundedMplus',
                                fontSize: 12.0,
                                decoration: TextDecoration.underline,
                                color: Color.fromRGBO(63, 113, 226, 1)),
                            recognizer: TapGestureRecognizer()
                              ..onTap = () {
                                Navigator.push(
                                    context,
                                    CustomRoute(
                                        builder: (context) => PrivacyPolicy()));
                              })
                      ]),
                      textAlign: TextAlign.center,
                    )
                  ],
                )
              ],
            ),
          )),
    );
  }

  _showDatePicker() {
    DatePicker.showDatePicker(context,
        showTitleActions: true,
        minTime: DateTime(1900, 1, 1),
        maxTime: DateTime.now().subtract(Duration(days: 3650)),
        theme: DatePickerTheme(
            backgroundColor: Color.fromRGBO(63, 113, 226, 1),
            itemStyle: TextStyle(
                fontFamily: 'RoundedMplus', fontSize: 16, color: Colors.white),
            doneStyle: TextStyle(
                fontFamily: 'RoundedMplus',
                color: Colors.white,
                fontSize: 16)), onChanged: (date) {
      print('change $date');
    }, onConfirm: (date) {
      print('confirm $date');
      initializeDateFormatting("ky", null);
      print('${(DateTime.now().difference(date).inDays ~/ 365.25)} years');
      setState(() {
        _bDayToDatabase = date.toString();
        _birthDay = DateFormat.d().format(date) +
            " " +
            DateFormat.MMMM("ky").format(date) +
            " " +
            DateFormat.y().format(date);
      });
    }, currentTime: DateTime.now(), locale: LocaleType.ru);
  }
}
