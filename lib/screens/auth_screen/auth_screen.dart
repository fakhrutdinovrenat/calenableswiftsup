import 'package:flutter/material.dart';
import 'package:swim_teacher/model/auth_status.dart';

import 'package:swim_teacher/presenter/authentication.dart';
import 'package:swim_teacher/screens/auth_screen/sign_in_screen.dart';
import 'package:swim_teacher/screens/auth_screen/sign_out_screen.dart';

class Auth extends StatefulWidget {
  Auth({Key key, this.auth}) : super(key: key);
  final BaseAuth auth;

  @override
  _AuthState createState() => _AuthState();
}

class _AuthState extends State<Auth> {
  AuthStatus authStatus = AuthStatus.notSignedIn;

  @override
  void initState() {
    super.initState();
    widget.auth.currentName().then((userId) {
      setState(() {
        authStatus =
        userId != null ? AuthStatus.signedIn : AuthStatus.notSignedIn;
      });
    });
  }

  void _updateAuthStatus(AuthStatus status) {
    setState(() {
      authStatus = status;
    });
  }

  @override
  Widget build(BuildContext context) {
    switch (authStatus) {
      case AuthStatus.notSignedIn:
        return SignInScreen(
          auth: widget.auth,
          onSignIn: () => _updateAuthStatus(AuthStatus.signedIn),
        );
      case AuthStatus.signedIn:
        return SignOutScreen(
            auth: widget.auth,
            onSignOut: () => _updateAuthStatus(AuthStatus.notSignedIn));
    }
  }
}
