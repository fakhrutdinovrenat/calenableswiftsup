import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:swim_teacher/model/strings.dart';
import 'package:swim_teacher/presenter/authentication.dart';
import 'package:swim_teacher/presenter/navigation.dart';
import 'package:swim_teacher/presenter/toastview.dart';
import 'package:swim_teacher/screens/main_screen/navigation_drawer.dart';

class SignInScreen extends StatefulWidget {
  SignInScreen({Key key, this.auth, this.onSignIn}) : super(key: key);

  final BaseAuth auth;
  final VoidCallback onSignIn;

  @override
  _SignInScreenState createState() => _SignInScreenState();
}

class _SignInScreenState extends State<SignInScreen> {
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey();

  void _signIn() async {
    try {
      String userId = await widget.auth.signIn();
      Toast.show('${Strings.hello} $userId', context, duration: 4);
      setState(() {});
      widget.onSignIn();
    } catch (e) {
      Toast.show(Strings.somethingWrong, context);
      print(e);
    }
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        AppNavigation.toMainPage(context);
      },
      child: Scaffold(
        backgroundColor: Colors.white,
        drawer: NavigationDrawer(),
        key: _scaffoldKey, // New Line
        appBar: AppBar(
            backgroundColor: Colors.white,
            elevation: 0.0,
            centerTitle: true,
            title: Text(
              Strings.auth,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontWeight: FontWeight.w500,
                  fontSize: 16,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
            leading: FlatButton(
                onPressed: () {
                  _scaffoldKey.currentState.openDrawer();
                },
                child: SvgPicture.asset('images/hamburger.svg'))),
        body: Center(
          child: Row(
            children: <Widget>[
              Expanded(
                  child: Padding(
                    padding: EdgeInsets.only(left: 24, right: 24),
                    child: Container(
                      decoration: BoxDecoration(
                          border: Border.all(color: Color.fromRGBO(27, 39, 45, 0.1)),
                          borderRadius: BorderRadius.circular(22.0)),
                      child: FlatButton(
                        shape: StadiumBorder(),
                        onPressed: () {
                          _signIn();
                        },
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            SvgPicture.asset('images/google.svg'),
                            Padding(
                                padding: EdgeInsets.only(left: 8),
                                child: Text(
                                  Strings.signIn,
                                  style: TextStyle(
                                      fontFamily: 'RoundedMplusMedium',
                                      fontSize: 14.0,
                                      fontWeight: FontWeight.w500,
                                      color: Color.fromRGBO(27, 39, 45, 0.75)),
                                  textAlign: TextAlign.center,
                                )),
                          ],
                        ),
                      ),
                    ),
                  ))
            ],
          ),
        ),
      ),
    );
  }
}
