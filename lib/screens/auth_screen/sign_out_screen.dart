import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:flutter_svg/svg.dart';
import 'package:swim_teacher/model/strings.dart';
import 'package:swim_teacher/presenter/authentication.dart';
import 'package:swim_teacher/presenter/custom_route.dart';
import 'package:swim_teacher/presenter/navigation.dart';
import 'package:swim_teacher/screens/account_settings/account_settings.dart';
import 'package:swim_teacher/screens/main_screen/navigation_drawer.dart';
import 'package:swim_teacher/screens/results_screen/results.dart';
import 'package:swim_teacher/screens/training_screen/trainings.dart';

class SignOutScreen extends StatefulWidget {
  SignOutScreen({Key key, this.auth, this.onSignOut});

  final BaseAuth auth;
  final VoidCallback onSignOut;

  @override
  _SignOutScreenState createState() => _SignOutScreenState();
}

class _SignOutScreenState extends State<SignOutScreen> {
  DatabaseReference _reference;

  String userStatus = "";
  String userSex = "";
  String userBday = "";

  @override
  void initState() {
    super.initState();
    _getReference();
  }

  void _getReference() async {
    try {
      String _userUid = await widget.auth.getUid();
      final FirebaseDatabase database = FirebaseDatabase.instance;
      _reference =
          database.reference().child(_userUid).child(Strings.accountReference);
      _reference.once().then((ds) {
        print(ds.value);

        if (ds.value == null) {
          Navigator.push(
              context,
              CustomRoute(
                  builder: (context) =>
                      AccountSettings(auth: Authentication())));
        } else {
          setState(() {
            userStatus = ds.value['status'];
            userSex = ds.value['sex'];
            userBday = ds.value['birthDate'];
          });
        }
      });
    } catch (e) {
      print(e);
    }
  }

  void _signOut() async {
    try {
      await widget.auth.signOut();
      await widget.auth.signOutGoogle();
      widget.onSignOut();
    } catch (e) {
      print(e);
    }
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        AppNavigation.toMainPage(context);
      },
      child: Scaffold(
        backgroundColor: Colors.white,
        drawer: NavigationDrawer(),
        appBar: AppBar(
          backgroundColor: Colors.white,
          elevation: 0.0,
          centerTitle: true,
          title: Text(Strings.auth,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontWeight: FontWeight.w500,
                  fontSize: 16,
                  color: Color.fromRGBO(27, 39, 45, 0.75))),
          leading: Builder(
              builder: (context) => FlatButton(
                  onPressed: () {
                    Scaffold.of(context).openDrawer();
                  },
                  child: SvgPicture.asset('images/hamburger.svg'))),
          actions: <Widget>[
            IconButton(
              icon: SvgPicture.asset('images/sign_out.svg'),
              onPressed: () {
                _signOut();
              },
            )
          ],
        ),
        body: Padding(
          padding: EdgeInsets.all(24.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Column(
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.only(top: 16, bottom: 16),
                    child: FutureBuilder(
                        future: widget.auth.getPhotoUrl(),
                        //initialData: "Loading...",
                        builder:
                            (BuildContext context, AsyncSnapshot<String> text) {
                          if (text.hasData) {
                            if (text.data != null) {
                              return ClipOval(
                                child: Image.network(text.data),
                              );
                            }
                          } else {
                            return CircularProgressIndicator();
                          }
                        }),
                  ),
                  FutureBuilder(
                      future: widget.auth.currentName(),
                      //initialData: "Loading...",
                      builder:
                          (BuildContext context, AsyncSnapshot<String> text) {
                        if (text.hasData) {
                          if (text.data != null) {
                            return Text(text.data,
                                style: TextStyle(
                                    fontFamily: 'RoundedMplus',
                                    fontSize: 16,
                                    color: Color.fromRGBO(27, 39, 45, 0.75)));
                          }
                        } else {
                          return CircularProgressIndicator();
                        }
                      }),
                  userBday.isEmpty
                      ? SpinKitChasingDots(color: Color.fromRGBO(63, 113, 226, 1))
                      : Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Text(
                        Strings.age +
                            "${DateTime.now().difference(DateTime.parse(userBday)).inDays ~/ 365.25}, " +
                            userStatus,
                        style: TextStyle(
                            fontFamily: 'RoundedMplus',
                            fontSize: 14,
                            color: Color.fromRGBO(27, 39, 45, 0.56)),
                      ),
                      IconButton(
                        icon: SvgPicture.asset("images/edit_settings.svg"),
                        onPressed: () {
                          Navigator.push(
                              context,
                              CustomRoute(
                                  builder: (context) => AccountSettings(
                                      auth: Authentication())));
                        },
                      )
                    ],
                  ),
                ],
              ),
              IntrinsicHeight(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    FlatButton(
                      shape: StadiumBorder(),
                      onPressed: () {
                        Navigator.push(
                            context,
                            CustomRoute(
                                builder: (context) =>
                                    Trainings(auth: Authentication())));
                      },
                      child: Text(
                        Strings.coaching,
                        style: TextStyle(
                          fontFamily: 'RoundedMplus',
                          fontSize: 16,
                          color: Color.fromRGBO(63, 113, 226, 1),
                        ),
                      ),
                    ),
                    VerticalDivider(color: Color.fromRGBO(27, 39, 45, 0.3)),
                    FlatButton(
                      shape: StadiumBorder(),
                      onPressed: () {
                        Navigator.push(
                            context,
                            CustomRoute(
                                builder: (context) =>
                                    Results(auth: Authentication())));
                      },
                      child: Text(
                        Strings.results,
                        style: TextStyle(
                          fontFamily: 'RoundedMplus',
                          fontSize: 16,
                          color: Color.fromRGBO(63, 113, 226, 1),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
