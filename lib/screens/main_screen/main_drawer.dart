import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:swim_teacher/model/strings.dart';
import 'package:swim_teacher/presenter/authentication.dart';
import 'package:swim_teacher/presenter/custom_route.dart';
import 'package:swim_teacher/screens/main_screen/navigation_drawer.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:swim_teacher/screens/results_screen/results.dart';
import 'package:swim_teacher/screens/standarts_screen/standarts.dart';
import 'package:swim_teacher/screens/timer_screen/timer.dart';
import 'package:swim_teacher/screens/training_screen/trainings.dart';

class MainDrawer extends StatefulWidget {
  @override
  State createState() => _MainDrawerState();
}

class _MainDrawerState extends State<MainDrawer> {
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey();

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        SystemNavigator.pop();
      },
      child: Scaffold(
        backgroundColor: Colors.white,
        drawer: NavigationDrawer(),
        key: _scaffoldKey,
        appBar: AppBar(
            backgroundColor: Colors.white,
            elevation: 0.0,
            title: SvgPicture.asset('images/logo.svg'),
            leading: FlatButton(
                onPressed: () {
                  _scaffoldKey.currentState.openDrawer();
                },
                child: SvgPicture.asset('images/hamburger.svg'))),
        body: Container(
          padding: EdgeInsets.only(top: 8, left: 24, right: 24, bottom: 24),
          decoration: BoxDecoration(
            gradient: LinearGradient(
              colors: [Colors.white, Color.fromRGBO(63, 113, 226, 0.2)],
              begin: Alignment(0.0, -0.8),
              end: Alignment.bottomCenter,
            ),
          ),
          child: Center(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(Strings.mainScreenTitle,
                    style: TextStyle(
                        fontFamily: 'RoundedMplus',
                        fontSize: 24.0,
                        color: Color.fromRGBO(27, 39, 45, 0.5))),
                Column(
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.only(right: 12.0, bottom: 12.0),
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(22.0)),
                          child: FlatButton(
                              onPressed: () {
                                Navigator.push(
                                    context,
                                    CustomRoute(
                                        builder: (context) => Trainings(auth: Authentication())));
                              },
                              padding: EdgeInsets.all(0.0),
                              color: Colors.white,
                              shape: StadiumBorder(),
                              child: SizedBox(
                                width: 128,
                                height: 172,
                                child: Column(
                                  mainAxisSize: MainAxisSize.max,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Padding(
                                      padding: EdgeInsets.only(
                                          top: 16,
                                          left: 16,
                                          right: 16,
                                          bottom: 16),
                                      child: Text(Strings.coaching,
                                          style: TextStyle(
                                              fontFamily: 'RoundedMplus',
                                              fontWeight: FontWeight.w500,
                                              fontSize: 16.0,
                                              color: Color.fromRGBO(
                                                  27, 39, 45, 0.75))),
                                    ),
                                    Padding(
                                      padding: EdgeInsets.only(bottom: 10),
                                      child: SvgPicture.asset('images/coaching_logo.svg'),
                                    ),
                                  ],
                                ),
                              )),
                        ),
                        Container(
                          margin: EdgeInsets.only(left: 12.0, bottom: 12.0),
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(22.0)),
                          child: FlatButton(
                              onPressed: () {
                                Navigator.push(
                                    context,
                                    CustomRoute(
                                        builder: (context) =>
                                            Results(auth: Authentication())));
                              },
                              padding: EdgeInsets.all(0.0),
                              color: Colors.white,
                              shape: StadiumBorder(),
                              child: SizedBox(
                                width: 128,
                                height: 172,
                                child: Column(
                                  mainAxisSize: MainAxisSize.max,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Padding(
                                      padding: EdgeInsets.only(
                                          top: 16,
                                          left: 16,
                                          right: 16,
                                          bottom: 16),
                                      child: Text(Strings.results,
                                          style: TextStyle(
                                              fontFamily: 'RoundedMplus',
                                              fontWeight: FontWeight.w500,
                                              fontSize: 16.0,
                                              color: Color.fromRGBO(
                                                  27, 39, 45, 0.75))),
                                    ),
                                    SvgPicture.asset('images/results_logo.svg')
                                  ],
                                ),
                              )),
                        ),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.only(right: 12.0, top: 12.0),
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(22.0)),
                          child: FlatButton(
                              onPressed: () {
                                Navigator.push(
                                    context,
                                    CustomRoute(
                                        builder: (context) => TimerScr()));
                              },
                              padding: EdgeInsets.all(0.0),
                              color: Colors.white,
                              shape: StadiumBorder(),
                              child: SizedBox(
                                width: 128,
                                height: 172,
                                child: Column(
                                  mainAxisSize: MainAxisSize.max,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Padding(
                                      padding: EdgeInsets.only(
                                          top: 16,
                                          left: 16,
                                          right: 16,
                                          bottom: 16),
                                      child: Text(Strings.timer,
                                          style: TextStyle(
                                              fontFamily: 'RoundedMplus',
                                              fontWeight: FontWeight.w500,
                                              fontSize: 16.0,
                                              color: Color.fromRGBO(
                                                  27, 39, 45, 0.75))),
                                    ),
                                    SvgPicture.asset(
                                        'images/stopwatch_logo.svg')
                                  ],
                                ),
                              )),
                        ),
                        Container(
                          margin: EdgeInsets.only(left: 12.0, top: 12.0),
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(22.0)),
                          child: FlatButton(
                              onPressed: () {
                                Navigator.push(
                                    context,
                                    CustomRoute(
                                        builder: (context) => Standarts()));
                              },
                              padding: EdgeInsets.all(0.0),
                              color: Colors.white,
                              shape: StadiumBorder(),
                              child: SizedBox(
                                width: 128,
                                height: 172,
                                child: Column(
                                  mainAxisSize: MainAxisSize.max,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Padding(
                                      padding: EdgeInsets.only(
                                          top: 16,
                                          left: 16,
                                          right: 16,
                                          bottom: 16),
                                      child: Text(Strings.standarts,
                                          style: TextStyle(
                                              fontFamily: 'RoundedMplus',
                                              fontWeight: FontWeight.w500,
                                              fontSize: 16.0,
                                              color: Color.fromRGBO(
                                                  27, 39, 45, 0.75))),
                                    ),
                                    SvgPicture.asset(
                                        'images/standarts_logo.svg')
                                  ],
                                ),
                              )),
                        ),
                      ],
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
