import 'package:flutter/material.dart';
import 'package:swim_teacher/presenter/authentication.dart';
import 'package:flutter_svg/flutter_svg.dart';

import 'package:swim_teacher/model/strings.dart';
import 'package:swim_teacher/presenter/custom_route.dart';
import 'package:swim_teacher/screens/about_screen/about.dart';
import 'package:swim_teacher/screens/auth_screen/auth_screen.dart';
import 'package:swim_teacher/screens/main_screen/user_logo/user_logo.dart';
import 'package:swim_teacher/screens/privacy_policy/privacy_screen.dart';
import 'package:swim_teacher/screens/protest_screen/protest.dart';
import 'package:swim_teacher/screens/results_screen/results.dart';
import 'package:swim_teacher/screens/rules_screen/rules.dart';
import 'package:swim_teacher/screens/standarts_screen/standarts.dart';
import 'package:swim_teacher/screens/timer_screen/timer.dart';
import 'package:swim_teacher/screens/training_screen/trainings.dart';

class NavigationDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          DrawerHeader(
            padding: EdgeInsets.only(left: 24),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                SvgPicture.asset('images/logo.svg'),
                UserLogo(
                    auth:
                        Authentication()), //здесь мы показываем пустое место, если не прошла авторизация, либо показываем иконку и имя пользователя гугла
              ],
            ),
          ),
          ListTile(
              leading: Padding(
                  padding: EdgeInsets.only(left: 8),
                  child: SvgPicture.asset('images/auth_icon.svg')),
              title: Text(Strings.account,
                  style: TextStyle(
                      fontFamily: 'RoundedMplus',
                      fontWeight: FontWeight.w500,
                      fontSize: 16.0,
                      color: Color.fromRGBO(27, 39, 45, 0.75))),
              onTap: () {
                Navigator.pop(context);
                Navigator.push(
                    context,
                    CustomRoute(
                        builder: (context) => Auth(auth: Authentication())));
              }),
          ListTile(
              leading: Padding(
                  padding: EdgeInsets.only(left: 8),
                  child: SvgPicture.asset('images/coaching.svg')),
              title: Text(Strings.coaching,
                  style: TextStyle(
                      fontFamily: 'RoundedMplus',
                      fontWeight: FontWeight.w500,
                      fontSize: 16.0,
                      color: Color.fromRGBO(27, 39, 45, 0.75))),
              onTap: () {
                Navigator.pop(context);
                Navigator.push(
                    context,
                    CustomRoute(
                        builder: (context) =>
                            Trainings(auth: Authentication())));
              }),
          ListTile(
              leading: Padding(
                  padding: EdgeInsets.only(left: 8),
                  child: SvgPicture.asset('images/results_icon.svg')),
              title: Text(Strings.results,
                  style: TextStyle(
                      fontFamily: 'RoundedMplus',
                      fontWeight: FontWeight.w500,
                      fontSize: 16.0,
                      color: Color.fromRGBO(27, 39, 45, 0.75))),
              onTap: () {
                Navigator.pop(context);
                Navigator.push(
                    context,
                    CustomRoute(
                        builder: (context) => Results(auth: Authentication())));
              }),
          ListTile(
              leading: Padding(
                  padding: EdgeInsets.only(left: 8),
                  child: SvgPicture.asset('images/stopwatch_icon.svg')),
              title: Text(Strings.timer,
                  style: TextStyle(
                      fontFamily: 'RoundedMplus',
                      fontWeight: FontWeight.w500,
                      fontSize: 16.0,
                      color: Color.fromRGBO(27, 39, 45, 0.75))),
              onTap: () {
                Navigator.pop(context);
                Navigator.push(
                    context, CustomRoute(builder: (context) => TimerScr()));
              }),
          ListTile(
              leading: Padding(
                  padding: EdgeInsets.only(left: 8),
                  child: SvgPicture.asset('images/standarts_icon.svg')),
              title: Text(Strings.standarts,
                  style: TextStyle(
                      fontFamily: 'RoundedMplus',
                      fontWeight: FontWeight.w500,
                      fontSize: 16.0,
                      color: Color.fromRGBO(27, 39, 45, 0.75))),
              onTap: () {
                Navigator.pop(context);
                Navigator.push(
                    context, CustomRoute(builder: (context) => Standarts()));
              }),
          ListTile(
            leading: Padding(
                padding: EdgeInsets.only(left: 8),
                child: SvgPicture.asset('images/rules_icon.svg')),
            title: Text(Strings.learnRules,
                style: TextStyle(
                    fontFamily: 'RoundedMplus',
                    fontWeight: FontWeight.w500,
                    fontSize: 16.0,
                    color: Color.fromRGBO(27, 39, 45, 0.75))),
            onTap: () {
              Navigator.pop(context);
              Navigator.push(
                  context, CustomRoute(builder: (context) => Rules()));
            },
          ),
          ListTile(
              leading: Padding(
                  padding: EdgeInsets.only(left: 8),
                  child: SvgPicture.asset('images/protest_icon.svg')),
              title: Text(Strings.protest,
                  style: TextStyle(
                      fontFamily: 'RoundedMplus',
                      fontWeight: FontWeight.w500,
                      fontSize: 16.0,
                      color: Color.fromRGBO(27, 39, 45, 0.75))),
              onTap: () {
                Navigator.pop(context);
                Navigator.push(
                    context, CustomRoute(builder: (context) => Protest()));
              }),
          ListTile(
              leading: Padding(
                  padding: EdgeInsets.only(left: 8),
                  child: SvgPicture.asset('images/about_icon.svg')),
              title: Text(Strings.about,
                  style: TextStyle(
                      fontFamily: 'RoundedMplus',
                      fontWeight: FontWeight.w500,
                      fontSize: 16.0,
                      color: Color.fromRGBO(27, 39, 45, 0.75))),
              onTap: () {
                Navigator.pop(context);
                Navigator.push(
                    context, CustomRoute(builder: (context) => About()));
              }),
          Divider(),
          FlatButton(
              onPressed: () {
                Navigator.pop(context);
                Navigator.push(context,
                    CustomRoute(builder: (context) => PrivacyPolicy()));
              },
              child: Padding(
                padding: EdgeInsets.only(bottom: 8),
                child: Text(Strings.privacyPolicy,
                    style: TextStyle(
                        fontFamily: 'RoundedMplus',
                        fontSize: 12.0,
                        color: Color.fromRGBO(27, 39, 45, 0.75))),
              ))
        ],
      ),
    );
  }
}
