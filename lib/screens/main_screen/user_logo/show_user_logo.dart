import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:swim_teacher/model/strings.dart';
import 'package:swim_teacher/presenter/authentication.dart';

class ShowUserLogo extends StatefulWidget {
  ShowUserLogo({Key key, this.auth});

  final BaseAuth auth;

  _ShowUserLogoState createState() => _ShowUserLogoState();
}

class _ShowUserLogoState extends State<ShowUserLogo> {
  DatabaseReference _reference;

  String userStatus = "";
  String userSex = "";
  String userBday = "";

  @override
  void initState() {
    super.initState();
    _getReference();
  }

  void _getReference() async {
    try {
      String _userUid = await widget.auth.getUid();
      final FirebaseDatabase database = FirebaseDatabase.instance;
      _reference =
          database.reference().child(_userUid).child(Strings.accountReference);
      _reference.once().then((ds) {
        //print(ds.value);
        if (ds.value != null) {
          setState(() {
            userStatus = ds.value['status'];
            userSex = ds.value['sex'];
            userBday = ds.value['birthDate'];
          });
        }
      });
    } catch (e) {
      print(e);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        FutureBuilder(
            future: widget.auth.getPhotoUrl(),
            //initialData: "Loading...",
            builder: (BuildContext context, AsyncSnapshot<String> text) {
              if (text.hasData) {
                if (text.data != null) {
                  return ClipOval(
                    child: Image.network(
                      text.data,
                      height: 70,
                      width: 70,
                    ),
                  );
                }
              } else {
                return CircularProgressIndicator();
              }
            }),
        Padding(
          padding: EdgeInsets.only(left: 16),
          child: Column(
            children: <Widget>[
              FutureBuilder(
                  future: widget.auth.currentName(),
                  //initialData: "Loading...",
                  builder: (BuildContext context, AsyncSnapshot<String> text) {
                    if (text.hasData) {
                      if (text.data != null) {
                        return Text(text.data,
                            style: TextStyle(
                                fontFamily: 'RoundedMplus',
                                fontSize: 14,
                                color: Color.fromRGBO(27, 39, 45, 0.75)));
                      }
                    } else {
                      return CircularProgressIndicator();
                    }
                  }),
              userBday.isEmpty
                  ? Text("")//SpinKitChasingDots(color: Color.fromRGBO(63, 113, 226, 1))
                  : Text(
                      Strings.age +
                          "${DateTime.now().difference(DateTime.parse(userBday)).inDays ~/ 365.25}, " +
                          userStatus,
                      style: TextStyle(
                          fontFamily: 'RoundedMplus',
                          fontSize: 14,
                          color: Color.fromRGBO(27, 39, 45, 0.56)),
                    ),
            ],
          ),
        ),
      ],
    );
  }
}
