import 'package:flutter/material.dart';
import 'package:swim_teacher/presenter/authentication.dart';
import 'package:swim_teacher/screens/main_screen/user_logo/show_user_logo.dart';

class UserLogo extends StatefulWidget {
  UserLogo({Key key, this.auth}) : super(key: key);
  final BaseAuth auth;

  _UserLogoState createState() => _UserLogoState();
}

enum CheckSignIn { notSigned, signed }

class _UserLogoState extends State<UserLogo> {
  CheckSignIn checkSignIn = CheckSignIn.notSigned;

  @override
  void initState() {
    super.initState();
    widget.auth.currentName().then((userId) {
      setState(() {
        checkSignIn =
            userId != null ? CheckSignIn.signed : CheckSignIn.notSigned;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    switch (checkSignIn) {
      case CheckSignIn.notSigned:
        return SizedBox(
          height: 60,
        );
      case CheckSignIn.signed:
        return ShowUserLogo(
            auth:
                Authentication()); //ResultsScreen(auth: Authentication());///пропишем виджет с отображением лого
    }
  }
}