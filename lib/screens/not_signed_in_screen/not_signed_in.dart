import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:swim_teacher/model/strings.dart';
import 'package:swim_teacher/presenter/authentication.dart';
import 'package:swim_teacher/presenter/custom_route.dart';
import 'package:swim_teacher/screens/auth_screen/auth_screen.dart';
import 'package:swim_teacher/screens/main_screen/navigation_drawer.dart';

class NotSignedIn extends StatefulWidget {
  @override
  State createState() => _NotSignedInState();
}

class _NotSignedInState extends State<NotSignedIn> {
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      drawer: NavigationDrawer(),
      key: _scaffoldKey,
      appBar: AppBar(
          backgroundColor: Colors.white,
          elevation: 0.0,
          title: SvgPicture.asset('images/logo.svg'),
          leading: FlatButton(
              onPressed: () {
                _scaffoldKey.currentState.openDrawer();
              },
              child: SvgPicture.asset('images/hamburger.svg'))),
      floatingActionButton: FloatingActionButton(
        child: ClipOval(
            child: Container(
                decoration: BoxDecoration(
                    gradient: LinearGradient(
                      colors: [
                        Color.fromRGBO(86, 155, 232, 1),
                        Color.fromRGBO(64, 114, 226, 1)
                      ],
                      begin: Alignment.bottomRight,
                      end: Alignment.topLeft,
                    ),
                    borderRadius: BorderRadius.circular(22.0)),
                child: Center(child: SvgPicture.asset('images/to_auth.svg')))),
        elevation: 0.0,
        backgroundColor: Colors.transparent,
        onPressed: () {
          Navigator.push(context,
              CustomRoute(builder: (context) => Auth(auth: Authentication())));
        },
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      body: Padding(
        padding: EdgeInsets.all(24),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Spacer(flex: 1),
            Padding(
              padding: EdgeInsets.only(bottom: 36),
              child: SvgPicture.asset('images/swimmer.svg'),
            ),
            Text(
              Strings.notSignInMessage,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 16.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
              textAlign: TextAlign.center,
            ),
            Spacer(flex: 2)
          ],
        ),
      ),
    );
  }
}