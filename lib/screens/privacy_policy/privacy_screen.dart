import 'package:flutter/material.dart';
import 'package:swim_teacher/model/strings.dart';
import 'package:swim_teacher/presenter/navigation.dart';
import 'package:swim_teacher/screens/main_screen/navigation_drawer.dart';
import 'package:flutter_svg/flutter_svg.dart';

class PrivacyPolicy extends StatefulWidget {
  State createState() => _PrivacyPolicyState();
}

class _PrivacyPolicyState extends State<PrivacyPolicy> {
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey();

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        AppNavigation.toMainPage(context);
      },
      child: Scaffold(
          backgroundColor: Colors.white,
          drawer: NavigationDrawer(),
          key: _scaffoldKey, // New Line
          appBar: AppBar(
              backgroundColor: Colors.white,
              elevation: 0.0,
              centerTitle: true,
              title: Text(
                Strings.privacyPolicy,
                style: TextStyle(
                    fontFamily: 'RoundedMplusMedium',
                    fontWeight: FontWeight.w500,
                    fontSize: 16,
                    color: Color.fromRGBO(27, 39, 45, 0.75)),
              ),
              leading: FlatButton(
                  onPressed: () {
                    _scaffoldKey.currentState.openDrawer();
                  },
                  child: SvgPicture.asset('images/hamburger.svg'))),
          body: Padding(
            padding: EdgeInsets.only(left: 16, right: 16),
            child: ListView(
              children: <Widget>[
                Text(
                  Strings.privacyPolicy01,
                  style: TextStyle(
                      fontFamily: 'RoundedMplusMedium',
                      fontSize: 14.0,
                      fontWeight: FontWeight.w500,
                      color: Color.fromRGBO(27, 39, 45, 0.75)),
                  textAlign: TextAlign.center,
                ),
                Padding(
                  padding: EdgeInsets.only(left: 8, right: 8),
                  child: Text(
                    Strings.privacyPolicy02,
                    style: TextStyle(
                        fontFamily: 'RoundedMplus',
                        fontSize: 14.0,
                        color: Color.fromRGBO(27, 39, 45, 0.75)),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(left: 8, right: 8),
                  child: Text(
                    Strings.privacyPolicy03,
                    style: TextStyle(
                        fontFamily: 'RoundedMplusMedium',
                        fontSize: 16.0,
                        fontWeight: FontWeight.w500,
                        color: Color.fromRGBO(27, 39, 45, 0.75)),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(left: 8, right: 8),
                  child: Text(
                    Strings.privacyPolicy04,
                    style: TextStyle(
                        fontFamily: 'RoundedMplus',
                        fontSize: 14.0,
                        color: Color.fromRGBO(27, 39, 45, 0.75)),
                  ),
                ),
              ],
            ),
          )),
    );
  }
}
