import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:swim_teacher/presenter/navigation.dart';
import 'package:url_launcher/url_launcher.dart';

import 'package:swim_teacher/model/strings.dart';
import 'package:swim_teacher/screens/main_screen/navigation_drawer.dart';

class Protest extends StatefulWidget {
  _ProtestState createState() => _ProtestState();
}

class _ProtestState extends State<Protest> {
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey();

  _launchURL() async {
    const url =
        'https://firebasestorage.googleapis.com/v0/b/swimteacher-34d6b.appspot.com/o/protest_form.pdf?alt=media&token=25cd14e0-d32c-4f26-9619-5d7eb96fc61d';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        AppNavigation.toMainPage(context);
      },
      child: Scaffold(
        backgroundColor: Colors.white,
        drawer: NavigationDrawer(),
        key: _scaffoldKey, // New Line
        appBar: AppBar(
            backgroundColor: Colors.white,
            elevation: 0.0,
            centerTitle: true,
            title: Text(
              Strings.protest,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontWeight: FontWeight.w500,
                  fontSize: 16,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
            leading: FlatButton(
                onPressed: () {
                  _scaffoldKey.currentState.openDrawer();
                },
                child: SvgPicture.asset('images/hamburger.svg'))),
        body: Column(
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Row(),

            FlatButton(
              onPressed: () {
                _launchURL();
              },
              child: Column(
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.only(bottom: 8),
                    child: Text(
                      Strings.protest04,
                      style: TextStyle(
                          fontFamily: 'RoundedMplusMedium',
                          fontSize: 14.0,
                          fontWeight: FontWeight.w500,
                          color: Color.fromRGBO(63, 113, 226, 1)),
                    ),
                  ),
                  Row(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      SvgPicture.asset('images/save.svg'),
                      Padding(
                          padding: EdgeInsets.only(left: 8),
                          child: Text(Strings.downloadPDF,
                              style: TextStyle(
                                  fontFamily: 'RoundedMplusMedium',
                                  fontSize: 14.0,
                                  fontWeight: FontWeight.w500,
                                  color: Color.fromRGBO(63, 113, 226, 1)))),
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
