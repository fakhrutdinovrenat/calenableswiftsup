import 'package:flutter/material.dart';
import 'package:swim_teacher/model/auth_status.dart';
import 'package:swim_teacher/presenter/authentication.dart';
import 'package:swim_teacher/screens/not_signed_in_screen/not_signed_in.dart';
import 'package:swim_teacher/screens/results_screen/results_screen.dart';

class Results extends StatefulWidget {
  Results({Key key, this.auth}) : super(key: key);
  final BaseAuth auth;

  @override
  _ResultsState createState() => _ResultsState();
}

class _ResultsState extends State<Results> {
  AuthStatus authStatus = AuthStatus.notSignedIn;

  @override
  void initState() {
    super.initState();
    widget.auth.currentName().then((userId) {
      setState(() {
        authStatus =
            userId != null ? AuthStatus.signedIn : AuthStatus.notSignedIn;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    switch (authStatus) {
      case AuthStatus.notSignedIn:
        return NotSignedIn();
      case AuthStatus.signedIn:
        return ResultsScreen(auth: Authentication());
    }
  }
}