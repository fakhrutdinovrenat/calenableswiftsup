import 'dart:async';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:intl/intl.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:swim_teacher/model/results_model.dart';
import 'package:swim_teacher/model/strings.dart';
import 'package:swim_teacher/presenter/authentication.dart';
import 'package:swim_teacher/presenter/custom_route.dart';
import 'package:swim_teacher/presenter/navigation.dart';
import 'package:swim_teacher/screens/main_screen/navigation_drawer.dart';
import 'package:swim_teacher/screens/timer_screen/timer.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class ResultsScreen extends StatefulWidget {
  ResultsScreen({Key key, this.auth}) : super(key: key);
  final BaseAuth auth;

  @override
  _ResultsScreenState createState() => _ResultsScreenState();
}

class _ResultsScreenState extends State<ResultsScreen> {
  int _radioValue = 0;
  bool _visible = true;
  bool _showFilter = false;
  bool _showBtn = true;
  bool _showStyleMenu = false;
  bool _dateSelected = false;
  bool _styleSelected = false;
  bool _distSelected = false;
  bool _showDistMenu = false;

  bool _b50mdistCheck = false;
  bool _b100mdistCheck = false;
  bool _b200mdistCheck = false;
  bool _b400mdistCheck = false;
  bool _b800mdistCheck = false;
  bool _b1500mdistCheck = false;
  bool _b4x50mdistCheck = false;
  bool _b4x100mdistCheck = false;
  bool _b4x200mdistCheck = false;

  bool _freestyleCheck = false;
  bool _onBackCheck = false;
  bool _breaststrokeCheck = false;
  bool _butterflyCheck = false;
  bool _complexSwimCheck = false;
  bool _relayFreestyleCheck = false;
  bool _relayCombinedCheck = false;

  bool _preload = true;

  SlidableController slidableController;
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey();
  List<ResultsModel> items = List();
  List<ResultsModel> filteredItems;
  List<ResultsModel> _filteredByParametrsItems;
  List<String> _styleList = List();
  List<String> _distList = List();
  DatabaseReference _reference;

  final TextEditingController _controller = TextEditingController();
  Icon actionIcon = Icon(Icons.search, color: Color.fromRGBO(63, 113, 226, 1));
  var filterIcon = SvgPicture.asset('images/filter.svg');
  Widget appBarTitle = Text(Strings.results,
      style: TextStyle(
          fontFamily: 'RoundedMplusMedium',
          fontSize: 16.0,
          fontWeight: FontWeight.w500,
          color: Color.fromRGBO(27, 39, 45, 0.75)));
  bool _firstSearch = true;
  String _searchText = "";
  String _day = "";
  String _month = "";
  String _sex = "";

  _ResultsScreenState() {
    _controller.addListener(() {
      if (_controller.text.isEmpty &&
          _month.isEmpty &&
          _sex.isEmpty &&
          _styleList.length == 0 &&
          _distList.length == 0) {
        setState(() {
          _firstSearch = true;
          _searchText = "";
        });
      } else {
        setState(() {
          _firstSearch = false;
          _searchText = _controller.text;
        });
      }
    });
  }

  Future<void> _getReference() async {
    try {
      String _userUid = await widget.auth.getUid();
      final FirebaseDatabase database = FirebaseDatabase.instance;
      _reference =
          database.reference().child(_userUid).child(Strings.resultsReference);
      _reference.onChildAdded.listen(_onEntryAdded);
    } catch (e) {
      print(e);
    }
  }

  @override
  void initState() {
    slidableController = new SlidableController();
    super.initState();
    //_item = ResultsModel("", "", "", "", "", "", "", "", "", "");
    _getReference();

    Timer(Duration(milliseconds: 2900), () {
      setState(() {
        _preload = false;
      });
    });
  }

  _onEntryAdded(Event event) {
    setState(() {
      items.add(ResultsModel.fromSnapshot(event.snapshot));
    });
  }

  void _deleteNote(
      BuildContext context, ResultsModel result, int position) async {
    await _reference.child(result.key).remove().then((_) {
      if (_firstSearch) {
        setState(() {
          items.removeAt(position);
        });
      } else {
        setState(() {
          filteredItems.removeAt(position);
          _getReference();
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        AppNavigation.toMainPage(context);
      },
      child: Scaffold(
        backgroundColor: Colors.white,
        drawer: NavigationDrawer(),
        key: _scaffoldKey,
        appBar: AppBar(
          backgroundColor: Colors.white,
          title: appBarTitle,
          leading: FlatButton(
              onPressed: () {
                _scaffoldKey.currentState.openDrawer();
              },
              child: SvgPicture.asset('images/hamburger.svg')),
          elevation: 0.0,
          centerTitle: true,
          actions: <Widget>[
            Visibility(
              //maintainSize: true,
              maintainAnimation: true,
              maintainState: true,
              visible: _visible,
              child: IconButton(
                icon: filterIcon,
                onPressed: () {
                  if (_showFilter) {
                    setState(() {
                      _showFilter = false;
                      _showBtn = true;
                    });
                  } else
                    setState(() {
                      _showFilter = true;
                      _showBtn = false;
                    });
                },
              ),
            ),
            IconButton(
              icon: actionIcon,
              onPressed: () {
                setState(() {
                  if (this.actionIcon.icon == Icons.search) {
                    this.actionIcon = Icon(Icons.close,
                        color: Color.fromRGBO(63, 113, 226, 0.5));
                    this.appBarTitle = TextField(
                      autofocus: true,
                      controller: _controller,
                      //style: TextStyle(color: Colors.white),
                      decoration: InputDecoration(hintText: Strings.search),
                    );
                    _visible = false;
                  } else {
                    setState(() {
                      this.actionIcon = Icon(Icons.search,
                          color: Color.fromRGBO(63, 113, 226, 1));
                      this.appBarTitle = Text(Strings.results,
                          style: TextStyle(
                              fontFamily: 'RoundedMplusMedium',
                              fontSize: 16.0,
                              fontWeight: FontWeight.w500,
                              color: Color.fromRGBO(27, 39, 45, 0.75)));
                      _controller.clear();
                      _visible = true;
                    });
                  }
                });
              },
            )
          ],
        ),
        floatingActionButton: Visibility(
          visible: _showBtn,
          child: FloatingActionButton(
              shape: CircleBorder(
                  side: BorderSide(
                      color: Color.fromRGBO(27, 39, 45, 0.1), width: 1.0)),
              child: SvgPicture.asset('images/plus.svg'),
              elevation: 0.0,
              backgroundColor: Colors.white,
              onPressed: () {
                Navigator.push(
                    context, CustomRoute(builder: (context) => TimerScr()));
              }),
        ),
        resizeToAvoidBottomPadding: false,
        floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
        body: Stack(
          children: <Widget>[
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 8.0),
              child: _firstSearch ? _createListView() : _filteredSearch(),
            ),
            Container(
                color: Colors.white,
                child: Visibility(
                  visible: _showFilter,
                  child: Stack(
                    children: <Widget>[
                      Padding(
                          padding:
                              EdgeInsets.only(left: 24, right: 24, bottom: 24),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            mainAxisSize: MainAxisSize.max,
                            children: <Widget>[
                              Column(
                                children: <Widget>[
                                  _distSelected
                                      ? _distSelectedWidget()
                                      : _distNotSelected(),
                                  Divider(
                                      color: Color.fromRGBO(27, 39, 45, 0.30)),
                                  _dateSelected
                                      ? _dateSelectedWidget()
                                      : _dateNotSelected(),
                                  Divider(
                                      color: Color.fromRGBO(27, 39, 45, 0.30)),
                                  _styleSelected
                                      ? _styleSelectedWidget()
                                      : _styleNotSelected(),
                                  Divider(
                                      color: Color.fromRGBO(27, 39, 45, 0.30)),
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      Padding(
                                        padding: EdgeInsets.only(left: 16),
                                        child: Text(
                                          Strings.filterSex,
                                          style: TextStyle(
                                              fontFamily: 'RoundedMplus',
                                              fontSize: 14.0,
                                              color: Color.fromRGBO(
                                                  27, 39, 45, 0.75)),
                                        ),
                                      ),
                                      Row(
                                        children: <Widget>[
                                          Radio(
                                            value: 0,
                                            groupValue: _radioValue,
                                            onChanged: (val) {
                                              setState(() {
                                                _radioValue = 0;
                                                _sex = "";
                                              });
                                            },
                                          ),
                                          Text(
                                            Strings.filterBothSex,
                                            style: TextStyle(
                                                fontFamily: 'RoundedMplus',
                                                fontSize: 14.0,
                                                color: Color.fromRGBO(
                                                    27, 39, 45, 0.75)),
                                          ),
                                        ],
                                      ),
                                      Row(
                                        children: <Widget>[
                                          Radio(
                                            value: 1,
                                            groupValue: _radioValue,
                                            onChanged: (val) {
                                              setState(() {
                                                _radioValue = 1;
                                                _sex = Strings.manResults;
                                              });
                                            },
                                          ),
                                          Text(
                                            Strings.man,
                                            style: TextStyle(
                                                fontFamily: 'RoundedMplus',
                                                fontSize: 14.0,
                                                color: Color.fromRGBO(
                                                    27, 39, 45, 0.75)),
                                          ),
                                        ],
                                      ),
                                      Row(
                                        children: <Widget>[
                                          Radio(
                                            value: 2,
                                            groupValue:
                                                _radioValue, //_changePool,
                                            onChanged: (val) {
                                              setState(() {
                                                _radioValue = 2;
                                                _sex = Strings.womenResults;
                                              });
                                              //_setSelectedRadioPool(val);
                                            },
                                          ),
                                          Text(
                                            Strings.women,
                                            style: TextStyle(
                                                fontFamily: 'RoundedMplus',
                                                fontSize: 14.0,
                                                color: Color.fromRGBO(
                                                    27, 39, 45, 0.75)),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                  Divider(
                                      color: Color.fromRGBO(27, 39, 45, 0.30)),
                                ],
                              ),
                              Row(
                                children: <Widget>[
                                  Expanded(
                                    child: FlatButton(
                                        onPressed: () {
                                          setState(() {
                                            //день и месяц
                                            _day = "";
                                            _month = "";
                                            _dateSelected = false;

                                            //пол
                                            _sex = "";
                                            _radioValue = 0;

                                            //дистанции
                                            _distSelected = false;
                                            _b50mdistCheck = false;
                                            _b100mdistCheck = false;
                                            _b200mdistCheck = false;
                                            _b400mdistCheck = false;
                                            _b800mdistCheck = false;
                                            _b1500mdistCheck = false;
                                            _b4x50mdistCheck = false;
                                            _b4x100mdistCheck = false;
                                            _b4x200mdistCheck = false;
                                            _distList.clear();

                                            //стили
                                            _styleSelected = false;
                                            _freestyleCheck = false;
                                            _onBackCheck = false;
                                            _breaststrokeCheck = false;
                                            _butterflyCheck = false;
                                            _complexSwimCheck = false;
                                            _relayFreestyleCheck = false;
                                            _relayCombinedCheck = false;
                                            _styleList.clear();

                                            //_showFilter = false;
                                            //_showBtn = true;

                                            _firstSearch = true;
                                            _searchText = "";

                                            filterIcon = SvgPicture.asset(
                                                'images/filter.svg');
                                          });
                                        },
                                        child: Text(Strings.filterClear,
                                            style: TextStyle(
                                                fontFamily:
                                                    'RoundedMplusMedium',
                                                fontSize: 14.0,
                                                fontWeight: FontWeight.w500,
                                                color: Color.fromRGBO(
                                                    27, 39, 45, 0.56)))),
                                  ),
                                  Expanded(
                                    child: FlatButton(
                                        onPressed: () {
                                          setState(() {
                                            _showFilter = false;
                                            _showBtn = true;

                                            _searchText = _controller.text;

                                            if (_distList.isEmpty &&
                                                _day.trim().isEmpty &&
                                                _month.trim().isEmpty &&
                                                _styleList.isEmpty &&
                                                _sex.trim().isEmpty &&
                                                _searchText.trim().isEmpty) {
                                              _firstSearch = true;
                                              filterIcon = SvgPicture.asset(
                                                  'images/filter.svg');
                                            } else {
                                              _firstSearch = false;
                                              filterIcon = SvgPicture.asset(
                                                  'images/filter_redline.svg');
                                            }
                                          });
                                        },
                                        child: Text(Strings.filterUse,
                                            style: TextStyle(
                                                fontFamily:
                                                    'RoundedMplusMedium',
                                                fontSize: 14.0,
                                                fontWeight: FontWeight.w500,
                                                color: Color.fromRGBO(
                                                    63, 113, 226, 1)))),
                                  )
                                ],
                              ),
                            ],
                          )),
                      _stylesWidget(),
                      _distWidget(),
                    ],
                  ),
                ))
          ],
        ),
      ),
    );
  }

  Widget _createListView() {
    if (items.isNotEmpty) {
      return ListView.separated(
          shrinkWrap: true,
          reverse: true,
          itemCount: items.length,
          separatorBuilder: (BuildContext context, int index) => Divider(),
          itemBuilder: (context, index) {
            items.sort((a, b) => a.swimDate.compareTo(b.swimDate));
            if (index == 0) {
              return Column(
                children: <Widget>[
                  Slidable(
                    closeOnScroll: true,
                    controller: slidableController,
                    delegate: SlidableDrawerDelegate(),
                    secondaryActions: <Widget>[
                      Container(
                        color: Color.fromRGBO(238, 104, 104, 1),
                        child: FlatButton(
                            onPressed: () async {
                              _deleteNote(context, items[index], index);
                              ResultsModel deletedItem = items[index];

                              Scaffold.of(context).showSnackBar(SnackBar(
                                content: Text(
                                  Strings.resultsRemove,
                                  style: TextStyle(
                                      fontFamily: 'RoundedMplus',
                                      fontSize: 14.0,
                                      color: Colors.white),
                                ),
                                action: SnackBarAction(
                                    label: Strings.saveDialog02,
                                    textColor: Color.fromRGBO(223, 239, 252, 1),
                                    onPressed: () {
                                      _reference
                                          .child(deletedItem.key)
                                          .set(deletedItem.toJson());
                                    }),
                              ));
                              slidableController.activeState.close();
                            },
                            child: Container(
                              height: 80,
                              color: Color.fromRGBO(238, 104, 104, 1),
                              child: OverflowBox(
                                minHeight: 0,
                                minWidth: 0,
                                child: SvgPicture.asset("images/basket.svg"),
                              ),
                            )),
                      ),
                    ],
                    child: Padding(
                      padding: EdgeInsets.symmetric(horizontal: 24),
                      child: Row(
                        children: <Widget>[
                          ClipOval(
                            child: Container(
                              width: 60,
                              height: 60,
                              decoration: BoxDecoration(
                                  color: Color.fromRGBO(63, 113, 226, 1),
                                  shape: BoxShape.circle),
                              child: Center(
                                child: Column(
                                  mainAxisSize: MainAxisSize.min,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Text(
                                      '${items[index].swimDay}',
                                      style: TextStyle(
                                          fontFamily: 'RoundedMplusMedium',
                                          fontSize: 20.0,
                                          fontWeight: FontWeight.w500,
                                          color: Colors.white,
                                          height: 0.8),
                                    ),
                                    Text(
                                      '${items[index].swimMonth}',
                                      style: TextStyle(
                                          fontFamily: 'RoundedMplus',
                                          fontSize: 14.0,
                                          color: Colors.white,
                                          height: 0.45),
                                    )
                                  ],
                                ),
                              ),
                            ),
                          ),
                          Expanded(
                            child: Padding(
                              padding: EdgeInsets.only(left: 16),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisSize: MainAxisSize.max,
                                children: <Widget>[
                                  Row(
                                    children: <Widget>[
                                      Text(
                                        '${items[index].swimDist}',
                                        style: TextStyle(
                                            fontFamily: 'RoundedMplus',
                                            fontSize: 14.0,
                                            color: Color.fromRGBO(
                                                27, 39, 45, 0.75)),
                                      ),
                                      Padding(
                                        padding:
                                            EdgeInsets.symmetric(horizontal: 8),
                                        child: Text('•',
                                            style: TextStyle(
                                                color: Color.fromRGBO(
                                                    63, 113, 226, 1))),
                                      ),
                                      Text(
                                        '${items[index].swimTime}',
                                        style: TextStyle(
                                            fontFamily: 'RoundedMplus',
                                            fontSize: 14.0,
                                            color: Color.fromRGBO(
                                                27, 39, 45, 0.75)),
                                      ),
                                    ],
                                  ),
                                  Text(
                                    '${items[index].swimStyle}',
                                    style: TextStyle(
                                        fontFamily: 'RoundedMplus',
                                        fontSize: 14.0,
                                        color:
                                            Color.fromRGBO(27, 39, 45, 0.75)),
                                  ),
                                  Row(
                                    children: <Widget>[
                                      Text(
                                        '${Strings.ageResults}${items[index].swimAge}',
                                        style: TextStyle(
                                            fontFamily: 'RoundedMplus',
                                            fontSize: 14.0,
                                            color: Color.fromRGBO(
                                                27, 39, 45, 0.75)),
                                      ),
                                      Padding(
                                        padding:
                                            EdgeInsets.symmetric(horizontal: 8),
                                        child: Text('•',
                                            style: TextStyle(
                                                color: Color.fromRGBO(
                                                    63, 113, 226, 1))),
                                      ),
                                      Text(
                                        '${items[index].swimSex}',
                                        style: TextStyle(
                                            fontFamily: 'RoundedMplus',
                                            fontSize: 14.0,
                                            color: Color.fromRGBO(
                                                27, 39, 45, 0.75)),
                                      ),
                                    ],
                                  ),
                                  Text(
                                    '${items[index].swimName}',
                                    style: TextStyle(
                                        fontFamily: 'RoundedMplusMedium',
                                        fontSize: 16.0,
                                        color: Color.fromRGBO(63, 113, 226, 1)),
                                  ),
                                ],
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 80,
                  )
                ],
              );
            } else
              return Slidable(
                closeOnScroll: true,
                controller: slidableController,
                delegate: SlidableDrawerDelegate(),
                secondaryActions: <Widget>[
                  Container(
                    color: Color.fromRGBO(238, 104, 104, 1),
                    child: FlatButton(
                        onPressed: () async {
                          _deleteNote(context, items[index], index);
                          ResultsModel deletedItem = items[index];

                          Scaffold.of(context).showSnackBar(SnackBar(
                            content: Text(
                              Strings.resultsRemove,
                              style: TextStyle(
                                  fontFamily: 'RoundedMplus',
                                  fontSize: 14.0,
                                  color: Colors.white),
                            ),
                            action: SnackBarAction(
                                label: Strings.saveDialog02,
                                textColor: Color.fromRGBO(223, 239, 252, 1),
                                onPressed: () {
                                  _reference
                                      .child(deletedItem.key)
                                      .set(deletedItem.toJson());
                                }),
                          ));
                          slidableController.activeState.close();
                        },
                        child: Container(
                          height: 80,
                          color: Color.fromRGBO(238, 104, 104, 1),
                          child: OverflowBox(
                            minHeight: 0,
                            minWidth: 0,
                            child: SvgPicture.asset("images/basket.svg"),
                          ),
                        )),
                  ),
                ],
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 24),
                  child: Row(
                    children: <Widget>[
                      ClipOval(
                        child: Container(
                          width: 60,
                          height: 60,
                          decoration: BoxDecoration(
                              color: Color.fromRGBO(63, 113, 226, 1),
                              shape: BoxShape.circle),
                          child: Center(
                            child: Column(
                              mainAxisSize: MainAxisSize.min,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Text(
                                  '${items[index].swimDay}',
                                  style: TextStyle(
                                      fontFamily: 'RoundedMplusMedium',
                                      fontSize: 20.0,
                                      fontWeight: FontWeight.w500,
                                      color: Colors.white,
                                      height: 0.8),
                                ),
                                Text(
                                  '${items[index].swimMonth}',
                                  style: TextStyle(
                                      fontFamily: 'RoundedMplus',
                                      fontSize: 14.0,
                                      color: Colors.white,
                                      height: 0.45),
                                )
                              ],
                            ),
                          ),
                        ),
                      ),
                      Expanded(
                        child: Padding(
                          padding: EdgeInsets.only(left: 16),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisSize: MainAxisSize.max,
                            children: <Widget>[
                              Row(
                                children: <Widget>[
                                  Text(
                                    '${items[index].swimDist}',
                                    style: TextStyle(
                                        fontFamily: 'RoundedMplus',
                                        fontSize: 14.0,
                                        color:
                                            Color.fromRGBO(27, 39, 45, 0.75)),
                                  ),
                                  Padding(
                                    padding:
                                        EdgeInsets.symmetric(horizontal: 8),
                                    child: Text('•',
                                        style: TextStyle(
                                            color: Color.fromRGBO(
                                                63, 113, 226, 1))),
                                  ),
                                  Text(
                                    '${items[index].swimTime}',
                                    style: TextStyle(
                                        fontFamily: 'RoundedMplus',
                                        fontSize: 14.0,
                                        color:
                                            Color.fromRGBO(27, 39, 45, 0.75)),
                                  ),
                                ],
                              ),
                              Text(
                                '${items[index].swimStyle}',
                                style: TextStyle(
                                    fontFamily: 'RoundedMplus',
                                    fontSize: 14.0,
                                    color: Color.fromRGBO(27, 39, 45, 0.75)),
                              ),
                              Row(
                                children: <Widget>[
                                  Text(
                                    '${Strings.ageResults}${items[index].swimAge}',
                                    style: TextStyle(
                                        fontFamily: 'RoundedMplus',
                                        fontSize: 14.0,
                                        color:
                                            Color.fromRGBO(27, 39, 45, 0.75)),
                                  ),
                                  Padding(
                                    padding:
                                        EdgeInsets.symmetric(horizontal: 8),
                                    child: Text('•',
                                        style: TextStyle(
                                            color: Color.fromRGBO(
                                                63, 113, 226, 1))),
                                  ),
                                  Text(
                                    '${items[index].swimSex}',
                                    style: TextStyle(
                                        fontFamily: 'RoundedMplus',
                                        fontSize: 14.0,
                                        color:
                                            Color.fromRGBO(27, 39, 45, 0.75)),
                                  ),
                                ],
                              ),
                              Text(
                                '${items[index].swimName}',
                                style: TextStyle(
                                    fontFamily: 'RoundedMplusMedium',
                                    fontSize: 16.0,
                                    color: Color.fromRGBO(63, 113, 226, 1)),
                              ),
                            ],
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              );
          });
    } else if (items.isEmpty && _preload) {
      return Container(
        color: Colors.white,
        child: Visibility(
          visible: _preload,
          child: Center(
            child: SpinKitChasingDots(
                color: Color.fromRGBO(63, 113, 226, 1), size: 100),
          ),
        ),
      );
    } else if (items.isEmpty && !_preload) return _noData();
  }

  Widget _filteredSearch() {
    _filteredByParametrsItems = List<ResultsModel>();
    filteredItems = List<ResultsModel>();

    for (ResultsModel resultsModel in items) {
      if (resultsModel.swimMonth.toLowerCase().contains(_month.toLowerCase()) &&
          resultsModel.swimDay.toLowerCase().contains(_day.toLowerCase()) &&
          resultsModel.swimSex.toLowerCase().contains(_sex.toLowerCase())) {
        _filteredByParametrsItems.add(resultsModel);
      }
    }

    if (_styleSelected) {
      List<ResultsModel> _test = List<ResultsModel>();
      List<ResultsModel> _testInLoop = List<ResultsModel>();
      for (String s in _styleList) {
        _testInLoop = _filteredByParametrsItems
            .where((resultModel) =>
                resultModel.swimStyle.toLowerCase().contains(s.toLowerCase()))
            .toList();
        _test.addAll(_testInLoop);
      }
      _filteredByParametrsItems.clear();
      _filteredByParametrsItems.addAll(_test);
    }

    if (_distSelected) {
      List<ResultsModel> _test = List<ResultsModel>();
      List<ResultsModel> _testInLoop = List<ResultsModel>();
      for (String s in _distList) {
        _testInLoop = _filteredByParametrsItems
            .where((resultModel) =>
                resultModel.swimDist.toLowerCase().contains(s.toLowerCase()))
            .toList();
        _test.addAll(_testInLoop);
      }
      _filteredByParametrsItems.clear();
      _filteredByParametrsItems.addAll(_test);
    }

    for (ResultsModel resultsModel in _filteredByParametrsItems) {
      if (resultsModel.swimSex
              .toLowerCase()
              .contains(_searchText.toLowerCase()) ||
          resultsModel.swimAge
              .toLowerCase()
              .contains(_searchText.toLowerCase()) ||
          resultsModel.swimStyle
              .toLowerCase()
              .contains(_searchText.toLowerCase()) ||
          resultsModel.swimDist
              .toLowerCase()
              .contains(_searchText.toLowerCase()) ||
          resultsModel.swimTime
              .toLowerCase()
              .contains(_searchText.toLowerCase()) ||
          resultsModel.swimName
              .toLowerCase()
              .contains(_searchText.toLowerCase()) ||
          resultsModel.swimDay
              .toLowerCase()
              .contains(_searchText.toLowerCase()) ||
          resultsModel.swimMonth
              .toLowerCase()
              .contains(_searchText.toLowerCase())) {
        filteredItems.add(resultsModel);
      }
    }

    return _createFilteredListView();
  }

  Widget _createFilteredListView() {
    if (filteredItems.isEmpty) {
      return _noData();
    }
    return ListView.separated(
        shrinkWrap: true,
        reverse: true,
        itemCount: filteredItems.length,
        separatorBuilder: (BuildContext context, int index) => Divider(),
        itemBuilder: (context, index) {
          filteredItems.sort((a, b) => a.swimDate.compareTo(b.swimDate));
          if (index == 0) {
            return Column(
              children: <Widget>[
                Slidable(
                  controller: slidableController,
                  delegate: SlidableDrawerDelegate(),
                  secondaryActions: <Widget>[
                    Container(
                      color: Color.fromRGBO(238, 104, 104, 1),
                      child: FlatButton(
                        onPressed: () async {
                          _deleteNote(context, filteredItems[index], index);
                          ResultsModel deletedFilteredItem =
                              filteredItems[index];
                          items.clear();

                          Scaffold.of(context).showSnackBar(SnackBar(
                            content: Text(
                              Strings.resultsRemove,
                              style: TextStyle(
                                  fontFamily: 'RoundedMplus',
                                  fontSize: 14.0,
                                  color: Colors.white),
                            ),
                            action: SnackBarAction(
                                label: Strings.saveDialog02,
                                textColor: Color.fromRGBO(223, 239, 252, 1),
                                onPressed: () {
                                  _reference
                                      .child(deletedFilteredItem.key)
                                      .set(deletedFilteredItem.toJson())
                                      .then((s) {
                                    setState(() {
                                      items.remove(filteredItems[index]);
                                    });
                                  });
                                }),
                          ));
                          slidableController.activeState.close();
                        },
                        child: Container(
                          height: 80,
                          color: Color.fromRGBO(238, 104, 104, 1),
                          child: OverflowBox(
                            minHeight: 0,
                            minWidth: 0,
                            child: SvgPicture.asset("images/basket.svg"),
                          ),
                        ),
                      ),
                    ),
                  ],
                  child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Row(
                      children: <Widget>[
                        ClipOval(
                          child: Container(
                            width: 60,
                            height: 60,
                            decoration: BoxDecoration(
                                color: Color.fromRGBO(63, 113, 226, 1),
                                shape: BoxShape.circle),
                            child: Center(
                              child: Column(
                                mainAxisSize: MainAxisSize.min,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Text(
                                    '${filteredItems[index].swimDay}',
                                    style: TextStyle(
                                        fontFamily: 'RoundedMplusMedium',
                                        fontSize: 20.0,
                                        fontWeight: FontWeight.w500,
                                        color: Colors.white,
                                        height: 0.8),
                                  ),
                                  Text(
                                    '${filteredItems[index].swimMonth}',
                                    style: TextStyle(
                                        fontFamily: 'RoundedMplus',
                                        fontSize: 14.0,
                                        color: Colors.white,
                                        height: 0.45),
                                  )
                                ],
                              ),
                            ),
                          ),
                        ),
                        Expanded(
                          child: Padding(
                            padding: EdgeInsets.only(left: 16),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisSize: MainAxisSize.max,
                              children: <Widget>[
                                Row(
                                  children: <Widget>[
                                    Text(
                                      '${filteredItems[index].swimDist}',
                                      style: TextStyle(
                                          fontFamily: 'RoundedMplus',
                                          fontSize: 14.0,
                                          color:
                                              Color.fromRGBO(27, 39, 45, 0.75)),
                                    ),
                                    Padding(
                                      padding:
                                          EdgeInsets.symmetric(horizontal: 8),
                                      child: Text('•',
                                          style: TextStyle(
                                              color: Color.fromRGBO(
                                                  63, 113, 226, 1))),
                                    ),
                                    Text(
                                      '${filteredItems[index].swimTime}',
                                      style: TextStyle(
                                          fontFamily: 'RoundedMplus',
                                          fontSize: 14.0,
                                          color:
                                              Color.fromRGBO(27, 39, 45, 0.75)),
                                    ),
                                  ],
                                ),
                                Text(
                                  '${filteredItems[index].swimStyle}',
                                  style: TextStyle(
                                      fontFamily: 'RoundedMplus',
                                      fontSize: 14.0,
                                      color: Color.fromRGBO(27, 39, 45, 0.75)),
                                ),
                                Row(
                                  children: <Widget>[
                                    Text(
                                      '${Strings.ageResults}${filteredItems[index].swimAge}',
                                      style: TextStyle(
                                          fontFamily: 'RoundedMplus',
                                          fontSize: 14.0,
                                          color:
                                              Color.fromRGBO(27, 39, 45, 0.75)),
                                    ),
                                    Padding(
                                      padding:
                                          EdgeInsets.symmetric(horizontal: 8),
                                      child: Text('•',
                                          style: TextStyle(
                                              color: Color.fromRGBO(
                                                  63, 113, 226, 1))),
                                    ),
                                    Text(
                                      '${filteredItems[index].swimSex}',
                                      style: TextStyle(
                                          fontFamily: 'RoundedMplus',
                                          fontSize: 14.0,
                                          color:
                                              Color.fromRGBO(27, 39, 45, 0.75)),
                                    ),
                                  ],
                                ),
                                Text(
                                  '${filteredItems[index].swimName}',
                                  style: TextStyle(
                                      fontFamily: 'RoundedMplusMedium',
                                      fontSize: 16.0,
                                      color: Color.fromRGBO(63, 113, 226, 1)),
                                ),
                              ],
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
                SizedBox(
                  height: 80,
                )
              ],
            );
          } else
            return Slidable(
              controller: slidableController,
              delegate: SlidableDrawerDelegate(),
              secondaryActions: <Widget>[
                Container(
                    color: Color.fromRGBO(238, 104, 104, 1),
                    child: FlatButton(
                        onPressed: () async {
                          _deleteNote(context, filteredItems[index], index);
                          ResultsModel deletedFilteredItem =
                              filteredItems[index];
                          items.clear();

                          Scaffold.of(context).showSnackBar(SnackBar(
                            content: Text(
                              Strings.resultsRemove,
                              style: TextStyle(
                                  fontFamily: 'RoundedMplus',
                                  fontSize: 14.0,
                                  color: Colors.white),
                            ),
                            action: SnackBarAction(
                                label: Strings.saveDialog02,
                                textColor: Color.fromRGBO(223, 239, 252, 1),
                                onPressed: () {
                                  _reference
                                      .child(deletedFilteredItem.key)
                                      .set(deletedFilteredItem.toJson())
                                      .then((s) {
                                    setState(() {
                                      items.remove(filteredItems[index]);
                                    });
                                  });
                                }),
                          ));
                          slidableController.activeState.close();
                        },
                        child: Container(
                          height: 80,
                          color: Color.fromRGBO(238, 104, 104, 1),
                          child: OverflowBox(
                            minHeight: 0,
                            minWidth: 0,
                            child: SvgPicture.asset("images/basket.svg"),
                          ),
                        ))),
              ],
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 24),
                child: Row(
                  children: <Widget>[
                    ClipOval(
                      child: Container(
                        width: 60,
                        height: 60,
                        decoration: BoxDecoration(
                            color: Color.fromRGBO(63, 113, 226, 1),
                            shape: BoxShape.circle),
                        child: Center(
                          child: Column(
                            mainAxisSize: MainAxisSize.min,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text(
                                '${filteredItems[index].swimDay}',
                                style: TextStyle(
                                    fontFamily: 'RoundedMplusMedium',
                                    fontSize: 20.0,
                                    fontWeight: FontWeight.w500,
                                    color: Colors.white,
                                    height: 0.8),
                              ),
                              Text(
                                '${filteredItems[index].swimMonth}',
                                style: TextStyle(
                                    fontFamily: 'RoundedMplus',
                                    fontSize: 14.0,
                                    color: Colors.white,
                                    height: 0.45),
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                    Expanded(
                      child: Padding(
                        padding: EdgeInsets.only(left: 16),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisSize: MainAxisSize.max,
                          children: <Widget>[
                            Row(
                              children: <Widget>[
                                Text(
                                  '${filteredItems[index].swimDist}',
                                  style: TextStyle(
                                      fontFamily: 'RoundedMplus',
                                      fontSize: 14.0,
                                      color: Color.fromRGBO(27, 39, 45, 0.75)),
                                ),
                                Padding(
                                  padding: EdgeInsets.symmetric(horizontal: 8),
                                  child: Text('•',
                                      style: TextStyle(
                                          color:
                                              Color.fromRGBO(63, 113, 226, 1))),
                                ),
                                Text(
                                  '${filteredItems[index].swimTime}',
                                  style: TextStyle(
                                      fontFamily: 'RoundedMplus',
                                      fontSize: 14.0,
                                      color: Color.fromRGBO(27, 39, 45, 0.75)),
                                ),
                              ],
                            ),
                            Text(
                              '${filteredItems[index].swimStyle}',
                              style: TextStyle(
                                  fontFamily: 'RoundedMplus',
                                  fontSize: 14.0,
                                  color: Color.fromRGBO(27, 39, 45, 0.75)),
                            ),
                            Row(
                              children: <Widget>[
                                Text(
                                  '${Strings.ageResults}${filteredItems[index].swimAge}',
                                  style: TextStyle(
                                      fontFamily: 'RoundedMplus',
                                      fontSize: 14.0,
                                      color: Color.fromRGBO(27, 39, 45, 0.75)),
                                ),
                                Padding(
                                  padding: EdgeInsets.symmetric(horizontal: 8),
                                  child: Text('•',
                                      style: TextStyle(
                                          color:
                                              Color.fromRGBO(63, 113, 226, 1))),
                                ),
                                Text(
                                  '${filteredItems[index].swimSex}',
                                  style: TextStyle(
                                      fontFamily: 'RoundedMplus',
                                      fontSize: 14.0,
                                      color: Color.fromRGBO(27, 39, 45, 0.75)),
                                ),
                              ],
                            ),
                            Text(
                              '${filteredItems[index].swimName}',
                              style: TextStyle(
                                  fontFamily: 'RoundedMplusMedium',
                                  fontSize: 16.0,
                                  color: Color.fromRGBO(63, 113, 226, 1)),
                            ),
                          ],
                        ),
                      ),
                    )
                  ],
                ),
              ),
            );
        });
  }

  Widget _noData() {
    return Padding(
      padding: EdgeInsets.all(24),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Spacer(flex: 1),
          Padding(
            padding: EdgeInsets.only(bottom: 36),
            child: SvgPicture.asset('images/swimmer.svg'),
          ),
          Text(
            Strings.noData,
            style: TextStyle(
                fontFamily: 'RoundedMplus',
                fontSize: 16.0,
                color: Color.fromRGBO(27, 39, 45, 0.75)),
            textAlign: TextAlign.center,
          ),
          Spacer(flex: 2)
        ],
      ),
    );
  }

  Widget _distNotSelected() {
    return FlatButton(
      onPressed: () {
        setState(() {
          _showDistMenu = true;
        });
      },
      child: Row(
        children: <Widget>[
          Text(Strings.filterDist,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 14.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75))),
        ],
      ),
    );
  }

  Widget _distSelectedWidget() {
    return Row(
      //mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Expanded(
          child: FlatButton(
            child: Row(
              children: <Widget>[
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(Strings.filterDist,
                          style: TextStyle(
                              fontFamily: 'RoundedMplus',
                              fontSize: 10.0,
                              color: Color.fromRGBO(27, 39, 45, 0.56))),
                      Text(
                        _distList.join(", "),
                        style: TextStyle(
                            fontFamily: 'RoundedMplus',
                            fontSize: 14.0,
                            color: Color.fromRGBO(27, 39, 45, 0.75)),
                        overflow: TextOverflow.fade,
                        softWrap: false,
                      ),
                    ],
                  ),
                )
              ],
            ),
            onPressed: () {
              setState(() {
                _showDistMenu = true;
              });
            },
          ),
        ),
        IconButton(
          icon: Icon(Icons.clear, color: Color.fromRGBO(27, 39, 45, 0.3)),
          onPressed: () {
            setState(() {
              _distSelected = false;
              _b50mdistCheck = false;
              _b100mdistCheck = false;
              _b200mdistCheck = false;
              _b400mdistCheck = false;
              _b800mdistCheck = false;
              _b1500mdistCheck = false;
              _b4x50mdistCheck = false;
              _b4x100mdistCheck = false;
              _b4x200mdistCheck = false;
              _distList.clear();
            });
          },
        )
      ],
    );
  }

  Widget _distWidget() {
    return Container(
      color: Colors.white,
      child: Visibility(
          visible: _showDistMenu,
          child: Padding(
            padding: EdgeInsets.only(left: 24, right: 24, bottom: 24),
            child: Stack(
              children: <Widget>[
                ListView(
                  children: <Widget>[
                    FlatButton(
                      onPressed: () {
                        setState(() {
                          if (_b50mdistCheck) {
                            _b50mdistCheck = false;
                            if (_distList.contains(Strings.distances01)) {
                              _distList.removeWhere(
                                  (item) => item == Strings.distances01);
                            }
                          } else {
                            _b50mdistCheck = true;
                            _distList.add(Strings.distances01);
                          }
                        });
                      },
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text(Strings.distances01,
                              style: TextStyle(
                                  fontFamily: 'RoundedMplus',
                                  fontSize: 14.0,
                                  color: Color.fromRGBO(27, 39, 45, 0.75))),
                          Visibility(
                            visible: _b50mdistCheck,
                            child: Icon(Icons.check,
                                color: Color.fromRGBO(63, 113, 226, 1)),
                          )
                        ],
                      ),
                    ),
                    Divider(color: Color.fromRGBO(27, 39, 45, 0.30)),
                    FlatButton(
                      onPressed: () {
                        setState(() {
                          if (_b100mdistCheck) {
                            _b100mdistCheck = false;
                            if (_distList.contains(Strings.distances02)) {
                              _distList.removeWhere(
                                  (item) => item == Strings.distances02);
                            }
                          } else {
                            _b100mdistCheck = true;
                            _distList.add(Strings.distances02);
                          }
                        });
                      },
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text(Strings.distances02,
                              style: TextStyle(
                                  fontFamily: 'RoundedMplus',
                                  fontSize: 14.0,
                                  color: Color.fromRGBO(27, 39, 45, 0.75))),
                          Visibility(
                            visible: _b100mdistCheck,
                            child: Icon(Icons.check,
                                color: Color.fromRGBO(63, 113, 226, 1)),
                          )
                        ],
                      ),
                    ),
                    Divider(color: Color.fromRGBO(27, 39, 45, 0.30)),
                    FlatButton(
                      onPressed: () {
                        setState(() {
                          if (_b200mdistCheck) {
                            _b200mdistCheck = false;
                            if (_distList.contains(Strings.distances03)) {
                              _distList.removeWhere(
                                  (item) => item == Strings.distances03);
                            }
                          } else {
                            _b200mdistCheck = true;
                            _distList.add(Strings.distances03);
                          }
                        });
                      },
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text(Strings.distances03,
                              style: TextStyle(
                                  fontFamily: 'RoundedMplus',
                                  fontSize: 14.0,
                                  color: Color.fromRGBO(27, 39, 45, 0.75))),
                          Visibility(
                            visible: _b200mdistCheck,
                            child: Icon(Icons.check,
                                color: Color.fromRGBO(63, 113, 226, 1)),
                          )
                        ],
                      ),
                    ),
                    Divider(color: Color.fromRGBO(27, 39, 45, 0.30)),
                    FlatButton(
                      onPressed: () {
                        setState(() {
                          if (_b400mdistCheck) {
                            _b400mdistCheck = false;
                            if (_distList.contains(Strings.distances04)) {
                              _distList.removeWhere(
                                  (item) => item == Strings.distances04);
                            }
                          } else {
                            _b400mdistCheck = true;
                            _distList.add(Strings.distances04);
                          }
                        });
                      },
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text(Strings.distances04,
                              style: TextStyle(
                                  fontFamily: 'RoundedMplus',
                                  fontSize: 14.0,
                                  color: Color.fromRGBO(27, 39, 45, 0.75))),
                          Visibility(
                            visible: _b400mdistCheck,
                            child: Icon(Icons.check,
                                color: Color.fromRGBO(63, 113, 226, 1)),
                          )
                        ],
                      ),
                    ),
                    Divider(color: Color.fromRGBO(27, 39, 45, 0.30)),
                    FlatButton(
                      onPressed: () {
                        setState(() {
                          if (_b800mdistCheck) {
                            _b800mdistCheck = false;
                            if (_distList.contains(Strings.distances05)) {
                              _distList.removeWhere(
                                  (item) => item == Strings.distances05);
                            }
                          } else {
                            _b800mdistCheck = true;
                            _distList.add(Strings.distances05);
                          }
                        });
                      },
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text(Strings.distances05,
                              style: TextStyle(
                                  fontFamily: 'RoundedMplus',
                                  fontSize: 14.0,
                                  color: Color.fromRGBO(27, 39, 45, 0.75))),
                          Visibility(
                            visible: _b800mdistCheck,
                            child: Icon(Icons.check,
                                color: Color.fromRGBO(63, 113, 226, 1)),
                          )
                        ],
                      ),
                    ),
                    Divider(color: Color.fromRGBO(27, 39, 45, 0.30)),
                    FlatButton(
                      onPressed: () {
                        setState(() {
                          if (_b1500mdistCheck) {
                            _b1500mdistCheck = false;
                            if (_distList.contains(Strings.distances06)) {
                              _distList.removeWhere(
                                  (item) => item == Strings.distances06);
                            }
                          } else {
                            _b1500mdistCheck = true;
                            _distList.add(Strings.distances06);
                          }
                        });
                      },
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text(Strings.distances06,
                              style: TextStyle(
                                  fontFamily: 'RoundedMplus',
                                  fontSize: 14.0,
                                  color: Color.fromRGBO(27, 39, 45, 0.75))),
                          Visibility(
                            visible: _b1500mdistCheck,
                            child: Icon(Icons.check,
                                color: Color.fromRGBO(63, 113, 226, 1)),
                          )
                        ],
                      ),
                    ),
                    Divider(color: Color.fromRGBO(27, 39, 45, 0.30)),
                    FlatButton(
                      onPressed: () {
                        setState(() {
                          if (_b4x50mdistCheck) {
                            _b4x50mdistCheck = false;
                            if (_distList.contains(Strings.distances07)) {
                              _distList.removeWhere(
                                  (item) => item == Strings.distances07);
                            }
                          } else {
                            _b4x50mdistCheck = true;
                            _distList.add(Strings.distances07);
                          }
                        });
                      },
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text(Strings.distances07,
                              style: TextStyle(
                                  fontFamily: 'RoundedMplus',
                                  fontSize: 14.0,
                                  color: Color.fromRGBO(27, 39, 45, 0.75))),
                          Visibility(
                            visible: _b4x50mdistCheck,
                            child: Icon(Icons.check,
                                color: Color.fromRGBO(63, 113, 226, 1)),
                          )
                        ],
                      ),
                    ),
                    Divider(color: Color.fromRGBO(27, 39, 45, 0.30)),
                    FlatButton(
                      onPressed: () {
                        setState(() {
                          if (_b4x100mdistCheck) {
                            _b4x100mdistCheck = false;
                            if (_distList.contains(Strings.distances08)) {
                              _distList.removeWhere(
                                  (item) => item == Strings.distances08);
                            }
                          } else {
                            _b4x100mdistCheck = true;
                            _distList.add(Strings.distances08);
                          }
                        });
                      },
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text(Strings.distances08,
                              style: TextStyle(
                                  fontFamily: 'RoundedMplus',
                                  fontSize: 14.0,
                                  color: Color.fromRGBO(27, 39, 45, 0.75))),
                          Visibility(
                            visible: _b4x100mdistCheck,
                            child: Icon(Icons.check,
                                color: Color.fromRGBO(63, 113, 226, 1)),
                          )
                        ],
                      ),
                    ),
                    Divider(color: Color.fromRGBO(27, 39, 45, 0.30)),
                    FlatButton(
                      onPressed: () {
                        setState(() {
                          if (_b4x200mdistCheck) {
                            _b4x200mdistCheck = false;
                            if (_distList.contains(Strings.distances09)) {
                              _distList.removeWhere(
                                  (item) => item == Strings.distances09);
                            }
                          } else {
                            _b4x200mdistCheck = true;
                            _distList.add(Strings.distances09);
                          }
                        });
                      },
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text(Strings.distances09,
                              style: TextStyle(
                                  fontFamily: 'RoundedMplus',
                                  fontSize: 14.0,
                                  color: Color.fromRGBO(27, 39, 45, 0.75))),
                          Visibility(
                            visible: _b4x200mdistCheck,
                            child: Icon(Icons.check,
                                color: Color.fromRGBO(63, 113, 226, 1)),
                          )
                        ],
                      ),
                    ),
                    Divider(color: Color.fromRGBO(27, 39, 45, 0.30)),
                    SizedBox(height: 40)
                  ],
                ),
                Container(
                  alignment: Alignment.bottomCenter,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      FlatButton(
                          color: Color.fromRGBO(255, 255, 255, 0.8),
                          onPressed: () {
                            setState(() {
                              _b50mdistCheck = false;
                              _b100mdistCheck = false;
                              _b200mdistCheck = false;
                              _b400mdistCheck = false;
                              _b800mdistCheck = false;
                              _b1500mdistCheck = false;
                              _b4x50mdistCheck = false;
                              _b4x100mdistCheck = false;
                              _b4x200mdistCheck = false;
                              _distSelected = false;
                              _distList.clear();
                              //_showDistMenu = false;
                            });
                          },
                          child: Text(Strings.filterClear,
                              style: TextStyle(
                                  fontFamily: 'RoundedMplusMedium',
                                  fontSize: 14.0,
                                  fontWeight: FontWeight.w500,
                                  color: Color.fromRGBO(27, 39, 45, 0.56)))),
                      FlatButton(
                          color: Color.fromRGBO(255, 255, 255, 0.8),
                          onPressed: () {
                            setState(() {
                              _showDistMenu = false;
                              if (!_b50mdistCheck &&
                                  !_b100mdistCheck &&
                                  !_b200mdistCheck &&
                                  !_b400mdistCheck &&
                                  !_b800mdistCheck &&
                                  !_b1500mdistCheck &&
                                  !_b4x50mdistCheck &&
                                  !_b4x100mdistCheck &&
                                  !_b4x200mdistCheck) {
                                _distSelected = false;
                              } else
                                _distSelected = true;
                            });
                          },
                          child: Text(Strings.filterUse,
                              style: TextStyle(
                                  fontFamily: 'RoundedMplusMedium',
                                  fontSize: 14.0,
                                  fontWeight: FontWeight.w500,
                                  color: Color.fromRGBO(63, 113, 226, 1)))),
                    ],
                  ),
                )
              ],
            ),
          )),
    );
  }

  Widget _styleNotSelected() {
    return FlatButton(
      onPressed: () {
        setState(() {
          _showStyleMenu = true;
        });
      },
      child: Row(
        children: <Widget>[
          Text(Strings.styles,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 14.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75))),
        ],
      ),
    );
  }

  Widget _styleSelectedWidget() {
    return Row(
      //mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Expanded(
          child: FlatButton(
            child: Row(
              children: <Widget>[
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(Strings.styles,
                          style: TextStyle(
                              fontFamily: 'RoundedMplus',
                              fontSize: 10.0,
                              color: Color.fromRGBO(27, 39, 45, 0.56))),
                      Text(
                        _styleList.join(", "),
                        style: TextStyle(
                            fontFamily: 'RoundedMplus',
                            fontSize: 14.0,
                            color: Color.fromRGBO(27, 39, 45, 0.75)),
                        overflow: TextOverflow.fade,
                        softWrap: false,
                      ),
                    ],
                  ),
                )
              ],
            ),
            onPressed: () {
              setState(() {
                _showStyleMenu = true;
              });
            },
          ),
        ),
        IconButton(
          icon: Icon(Icons.clear, color: Color.fromRGBO(27, 39, 45, 0.3)),
          onPressed: () {
            setState(() {
              _styleSelected = false;
              _freestyleCheck = false;
              _onBackCheck = false;
              _breaststrokeCheck = false;
              _butterflyCheck = false;
              _complexSwimCheck = false;
              _relayFreestyleCheck = false;
              _relayCombinedCheck = false;
              _styleList.clear();
            });
          },
        )
      ],
    );
  }

  Widget _stylesWidget() {
    return Container(
      color: Colors.white,
      child: Visibility(
          visible: _showStyleMenu,
          child: Padding(
            padding: EdgeInsets.only(left: 24, right: 24, bottom: 24),
            child: Stack(
              children: <Widget>[
                ListView(
                  children: <Widget>[
                    FlatButton(
                      onPressed: () {
                        setState(() {
                          if (_freestyleCheck) {
                            _freestyleCheck = false;
                            if (_styleList.contains(Strings.freestyle)) {
                              _styleList.removeWhere(
                                  (item) => item == Strings.freestyle);
                            }
                          } else {
                            _freestyleCheck = true;
                            _styleList.add(Strings.freestyle);
                          }
                        });
                      },
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text(Strings.freestyle,
                              style: TextStyle(
                                  fontFamily: 'RoundedMplus',
                                  fontSize: 14.0,
                                  color: Color.fromRGBO(27, 39, 45, 0.75))),
                          Visibility(
                            visible: _freestyleCheck,
                            child: Icon(Icons.check,
                                color: Color.fromRGBO(63, 113, 226, 1)),
                          )
                        ],
                      ),
                    ),
                    Divider(color: Color.fromRGBO(27, 39, 45, 0.30)),
                    FlatButton(
                      onPressed: () {
                        setState(() {
                          if (_onBackCheck) {
                            _onBackCheck = false;
                            if (_styleList.contains(Strings.onBack)) {
                              _styleList.removeWhere(
                                  (item) => item == Strings.onBack);
                            }
                          } else {
                            _onBackCheck = true;
                            _styleList.add(Strings.onBack);
                          }
                        });
                      },
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text(Strings.onBack,
                              style: TextStyle(
                                  fontFamily: 'RoundedMplus',
                                  fontSize: 14.0,
                                  color: Color.fromRGBO(27, 39, 45, 0.75))),
                          Visibility(
                            visible: _onBackCheck,
                            child: Icon(Icons.check,
                                color: Color.fromRGBO(63, 113, 226, 1)),
                          )
                        ],
                      ),
                    ),
                    Divider(color: Color.fromRGBO(27, 39, 45, 0.30)),
                    FlatButton(
                      onPressed: () {
                        setState(() {
                          if (_breaststrokeCheck) {
                            _breaststrokeCheck = false;
                            if (_styleList.contains(Strings.breaststroke)) {
                              _styleList.removeWhere(
                                  (item) => item == Strings.breaststroke);
                            }
                          } else {
                            _breaststrokeCheck = true;
                            _styleList.add(Strings.breaststroke);
                          }
                        });
                      },
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text(Strings.breaststroke,
                              style: TextStyle(
                                  fontFamily: 'RoundedMplus',
                                  fontSize: 14.0,
                                  color: Color.fromRGBO(27, 39, 45, 0.75))),
                          Visibility(
                            visible: _breaststrokeCheck,
                            child: Icon(Icons.check,
                                color: Color.fromRGBO(63, 113, 226, 1)),
                          )
                        ],
                      ),
                    ),
                    Divider(color: Color.fromRGBO(27, 39, 45, 0.30)),
                    FlatButton(
                      onPressed: () {
                        setState(() {
                          if (_butterflyCheck) {
                            _butterflyCheck = false;
                            if (_styleList.contains(Strings.butterfly)) {
                              _styleList.removeWhere(
                                  (item) => item == Strings.butterfly);
                            }
                          } else {
                            _butterflyCheck = true;
                            _styleList.add(Strings.butterfly);
                          }
                        });
                      },
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text(Strings.butterfly,
                              style: TextStyle(
                                  fontFamily: 'RoundedMplus',
                                  fontSize: 14.0,
                                  color: Color.fromRGBO(27, 39, 45, 0.75))),
                          Visibility(
                            visible: _butterflyCheck,
                            child: Icon(Icons.check,
                                color: Color.fromRGBO(63, 113, 226, 1)),
                          )
                        ],
                      ),
                    ),
                    Divider(color: Color.fromRGBO(27, 39, 45, 0.30)),
                    FlatButton(
                      onPressed: () {
                        setState(() {
                          if (_complexSwimCheck) {
                            _complexSwimCheck = false;
                            if (_styleList.contains(Strings.complexSwim)) {
                              _styleList.removeWhere(
                                  (item) => item == Strings.complexSwim);
                            }
                          } else {
                            _complexSwimCheck = true;
                            _styleList.add(Strings.complexSwim);
                          }
                        });
                      },
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text(Strings.complexSwim,
                              style: TextStyle(
                                  fontFamily: 'RoundedMplus',
                                  fontSize: 14.0,
                                  color: Color.fromRGBO(27, 39, 45, 0.75))),
                          Visibility(
                            visible: _complexSwimCheck,
                            child: Icon(Icons.check,
                                color: Color.fromRGBO(63, 113, 226, 1)),
                          )
                        ],
                      ),
                    ),
                    Divider(color: Color.fromRGBO(27, 39, 45, 0.30)),
                    FlatButton(
                      onPressed: () {
                        setState(() {
                          if (_relayFreestyleCheck) {
                            _relayFreestyleCheck = false;
                            if (_styleList.contains(Strings.relayFreestyle)) {
                              _styleList.removeWhere(
                                  (item) => item == Strings.relayFreestyle);
                            }
                          } else {
                            _relayFreestyleCheck = true;
                            _styleList.add(Strings.relayFreestyle);
                          }
                        });
                      },
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text(Strings.relayFreestyle,
                              style: TextStyle(
                                  fontFamily: 'RoundedMplus',
                                  fontSize: 14.0,
                                  color: Color.fromRGBO(27, 39, 45, 0.75))),
                          Visibility(
                            visible: _relayFreestyleCheck,
                            child: Icon(Icons.check,
                                color: Color.fromRGBO(63, 113, 226, 1)),
                          )
                        ],
                      ),
                    ),
                    Divider(color: Color.fromRGBO(27, 39, 45, 0.30)),
                    FlatButton(
                      onPressed: () {
                        setState(() {
                          if (_relayCombinedCheck) {
                            _relayCombinedCheck = false;
                            if (_styleList.contains(Strings.relayCombined)) {
                              _styleList.removeWhere(
                                  (item) => item == Strings.relayCombined);
                            }
                          } else {
                            _relayCombinedCheck = true;
                            //_relayComb = Strings.relayCombined;
                            _styleList.add(Strings.relayCombined);
                          }
                        });
                      },
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text(Strings.relayCombined,
                              style: TextStyle(
                                  fontFamily: 'RoundedMplus',
                                  fontSize: 14.0,
                                  color: Color.fromRGBO(27, 39, 45, 0.75))),
                          Visibility(
                            visible: _relayCombinedCheck,
                            child: Icon(Icons.check,
                                color: Color.fromRGBO(63, 113, 226, 1)),
                          )
                        ],
                      ),
                    ),
                    Divider(color: Color.fromRGBO(27, 39, 45, 0.30)),
                    SizedBox(height: 40)
                  ],
                ),
                Container(
                  alignment: Alignment.bottomCenter,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      FlatButton(
                          color: Color.fromRGBO(255, 255, 255, 0.8),
                          onPressed: () {
                            setState(() {
                              _freestyleCheck = false;
                              _onBackCheck = false;
                              _breaststrokeCheck = false;
                              _butterflyCheck = false;
                              _complexSwimCheck = false;
                              _relayFreestyleCheck = false;
                              _relayCombinedCheck = false;
                              _styleSelected = false;
                              _styleList.clear();
                              //_showStyleMenu = false;
                            });
                          },
                          child: Text(Strings.filterClear,
                              style: TextStyle(
                                  fontFamily: 'RoundedMplusMedium',
                                  fontSize: 14.0,
                                  fontWeight: FontWeight.w500,
                                  color: Color.fromRGBO(27, 39, 45, 0.56)))),
                      FlatButton(
                          color: Color.fromRGBO(255, 255, 255, 0.8),
                          onPressed: () {
                            setState(() {
                              if (!_freestyleCheck &&
                                  !_onBackCheck &&
                                  !_breaststrokeCheck &&
                                  !_butterflyCheck &&
                                  !_complexSwimCheck &&
                                  !_relayFreestyleCheck &&
                                  !_relayCombinedCheck) {
                                _styleSelected = false;
                              } else
                                _styleSelected = true;

                              _showStyleMenu = false;
                            });
                          },
                          child: Text(Strings.filterUse,
                              style: TextStyle(
                                  fontFamily: 'RoundedMplusMedium',
                                  fontSize: 14.0,
                                  fontWeight: FontWeight.w500,
                                  color: Color.fromRGBO(63, 113, 226, 1)))),
                    ],
                  ),
                )
              ],
            ),
          )),
    );
  }

  Widget _dateNotSelected() {
    return FlatButton(
        onPressed: () {
          if (!_dateSelected) {
            _datePickerShow();
          } else {}
        },
        child: Row(
          children: <Widget>[
            Text(Strings.saveDialog13,
                style: TextStyle(
                    fontFamily: 'RoundedMplus',
                    fontSize: 14.0,
                    color: Color.fromRGBO(27, 39, 45, 0.75)))
          ],
        ));
  }

  Widget _dateSelectedWidget() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Expanded(
          child: FlatButton(
            child: Row(
              children: <Widget>[
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(Strings.saveDialog13,
                        style: TextStyle(
                            fontFamily: 'RoundedMplus',
                            fontSize: 10.0,
                            color: Color.fromRGBO(27, 39, 45, 0.56))),
                    Text(_day + " " + _month,
                        style: TextStyle(
                            fontFamily: 'RoundedMplus',
                            fontSize: 14.0,
                            color: Color.fromRGBO(27, 39, 45, 0.75))),
                  ],
                ),
              ],
            ),
            onPressed: () {
              _datePickerShow();
            },
          ),
        ),
        IconButton(
          icon: Icon(Icons.clear, color: Color.fromRGBO(27, 39, 45, 0.3)),
          onPressed: () {
            setState(() {
              _month = "";
              _day = "";
              _dateSelected = false;
            });
          },
        )
      ],
    );
  }

  _datePickerShow() {
    DatePicker.showDatePicker(context,
        showTitleActions: true,
        minTime: DateTime(2019, 1, 1),
        maxTime: DateTime.now(),
        theme: DatePickerTheme(
            backgroundColor: Color.fromRGBO(63, 113, 226, 1),
            itemStyle: TextStyle(
                fontFamily: 'RoundedMplus', fontSize: 16, color: Colors.white),
            doneStyle: TextStyle(
                fontFamily: 'RoundedMplus',
                color: Colors.white,
                fontSize: 16)), onChanged: (date) {
      print('change $date');
    }, onConfirm: (date) {
      print('confirm $date');
      initializeDateFormatting("ky", null);
      setState(() {
        _day = DateFormat.d().format(date);
        _month = DateFormat.MMM("ky").format(date).toLowerCase();
        _dateSelected = true;
      });
    }, currentTime: DateTime.now(), locale: LocaleType.ru);
  }
}
