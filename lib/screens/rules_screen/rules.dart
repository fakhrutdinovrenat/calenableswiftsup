import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:swim_teacher/presenter/draggable_scrollbar.dart';
import 'package:swim_teacher/presenter/navigation.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:swim_teacher/model/strings.dart';
import 'package:swim_teacher/screens/main_screen/navigation_drawer.dart';
import 'package:rect_getter/rect_getter.dart';

class Rules extends StatefulWidget {
  State createState() => RulesState();
}

class RulesState extends State<Rules> {
  var _keys = {};

  _launchURL() async {
    const url =
        'http://www.russwimming.ru/sites/default/files/documents/2019/Pravila_plavanie2019.pdf';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  @override
  Widget build(BuildContext context) {
    var listViewKey = RectGetter.createGlobalKey();
    var _ctl = new ScrollController();

    List<int> getVisible() {
      var rect = RectGetter.getRectFromKey(listViewKey);
      var _items = <int>[];
      _keys.forEach((index, key) {
        var itemRect = RectGetter.getRectFromKey(key);
        if (itemRect != null &&
            !(itemRect.top > rect.bottom || itemRect.bottom < rect.top))
          _items.add(index);
      });

      return _items;
    }

    void scrollLoop(int target, Rect listRect) {
      var first = getVisible().first;
      bool direction = first < target;
      Rect _rect;
      if (_keys.containsKey(target))
        _rect = RectGetter.getRectFromKey(_keys[target]);
      if (_rect == null ||
          (direction
              ? _rect.bottom < listRect.top
              : _rect.top > listRect.bottom)) {
        var offset = _ctl.offset +
            (direction ? listRect.height / 2 : -listRect.height / 2);
        offset = offset < 0.0 ? 0.0 : offset;
        offset = offset > _ctl.position.maxScrollExtent
            ? _ctl.position.maxScrollExtent
            : offset;
        _ctl.jumpTo(offset);
        Timer(Duration(milliseconds: 15), () {
          scrollLoop(target, listRect);
        });
        return;
      }

      _ctl.jumpTo(_ctl.offset + _rect.top - listRect.top);
    }

    void jumpTo(int target) {
      var visible = getVisible();
      if (visible.contains(target)) return;

      var listRect = RectGetter.getRectFromKey(listViewKey);
      scrollLoop(target, listRect);
    }

    List listRulesScreen = [
      SizedBox.shrink(),
      FlatButton(
        onPressed: () {
          _launchURL();
        },
        child: Column(
          children: <Widget>[
            Text(
              Strings.rulesTitle,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 14.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(63, 113, 226, 1)),
              textAlign: TextAlign.center,
            ),
            Row(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                SvgPicture.asset('images/save.svg'),
                Padding(
                    padding: EdgeInsets.only(left: 8),
                    child: Text(Strings.downloadPDF,
                        style: TextStyle(
                            fontFamily: 'RoundedMplusMedium',
                            fontSize: 14.0,
                            fontWeight: FontWeight.w500,
                            color: Color.fromRGBO(63, 113, 226, 1)))),
              ],
            ),
          ],
        ),
      ),
      GestureDetector(
        child: Text.rich(TextSpan(
            text: '1. ',
            style: TextStyle(
                fontFamily: 'RoundedMplusMedium',
                fontSize: 16.0,
                fontWeight: FontWeight.w500,
                color: Color.fromRGBO(27, 39, 45, 0.75)),
            children: <TextSpan>[
              TextSpan(
                  text: Strings.generalities + "\n",
                  style: TextStyle(
                      decoration: TextDecoration.underline,
                      fontFamily: 'RoundedMplusMedium',
                      fontSize: 16.0,
                      fontWeight: FontWeight.w500,
                      color: Color.fromRGBO(27, 39, 45, 0.75)))
            ])),
        onTap: () {
          jumpTo(46);
        },
      ),
      GestureDetector(
        child: Text.rich(TextSpan(
            text: '2. ',
            style: TextStyle(
                fontFamily: 'RoundedMplusMedium',
                fontSize: 16.0,
                fontWeight: FontWeight.w500,
                color: Color.fromRGBO(27, 39, 45, 0.75)),
            children: <TextSpan>[
              TextSpan(
                  text: Strings.generalities2_1 + "\n",
                  style: TextStyle(
                      decoration: TextDecoration.underline,
                      fontFamily: 'RoundedMplusMedium',
                      fontSize: 16.0,
                      fontWeight: FontWeight.w500,
                      color: Color.fromRGBO(27, 39, 45, 0.75)))
            ])),
        onTap: () {
          jumpTo(48);
        },
      ),
      GestureDetector(
        child: Text.rich(TextSpan(
            text: '3. ',
            style: TextStyle(
                fontFamily: 'RoundedMplusMedium',
                fontSize: 16.0,
                fontWeight: FontWeight.w500,
                color: Color.fromRGBO(27, 39, 45, 0.75)),
            children: <TextSpan>[
              TextSpan(
                  text: Strings.generalities9_1 + "\n",
                  style: TextStyle(
                      decoration: TextDecoration.underline,
                      fontFamily: 'RoundedMplusMedium',
                      fontSize: 16.0,
                      fontWeight: FontWeight.w500,
                      color: Color.fromRGBO(27, 39, 45, 0.75)))
            ])),
        onTap: () {
          jumpTo(100);
        },
      ),
      GestureDetector(
        child: Text.rich(TextSpan(
            text: '4. ',
            style: TextStyle(
                fontFamily: 'RoundedMplusMedium',
                fontSize: 16.0,
                fontWeight: FontWeight.w500,
                color: Color.fromRGBO(27, 39, 45, 0.75)),
            children: <TextSpan>[
              TextSpan(
                  text: Strings.generalities11_1 + "\n",
                  style: TextStyle(
                      decoration: TextDecoration.underline,
                      fontFamily: 'RoundedMplusMedium',
                      fontSize: 16.0,
                      fontWeight: FontWeight.w500,
                      color: Color.fromRGBO(27, 39, 45, 0.75)))
            ])),
        onTap: () {
          jumpTo(102);
        },
      ),
      GestureDetector(
        child: Text.rich(TextSpan(
            text: '5. ',
            style: TextStyle(
                fontFamily: 'RoundedMplusMedium',
                fontSize: 16.0,
                fontWeight: FontWeight.w500,
                color: Color.fromRGBO(27, 39, 45, 0.75)),
            children: <TextSpan>[
              TextSpan(
                  text: Strings.generalities13_1 + "\n",
                  style: TextStyle(
                      decoration: TextDecoration.underline,
                      fontFamily: 'RoundedMplusMedium',
                      fontSize: 16.0,
                      fontWeight: FontWeight.w500,
                      color: Color.fromRGBO(27, 39, 45, 0.75)))
            ])),
        onTap: () {
          jumpTo(104);
        },
      ),
      Text(Strings.participants + "\n",
          style: TextStyle(
              fontFamily: 'RoundedMplusMedium',
              fontSize: 16.0,
              fontWeight: FontWeight.w500,
              color: Color.fromRGBO(63, 113, 226, 1))),
      GestureDetector(
        child: Text.rich(TextSpan(
            text: '6. ',
            style: TextStyle(
                fontFamily: 'RoundedMplusMedium',
                fontSize: 16.0,
                fontWeight: FontWeight.w500,
                color: Color.fromRGBO(27, 39, 45, 0.75)),
            children: <TextSpan>[
              TextSpan(
                  text: Strings.participants01_1 + "\n",
                  style: TextStyle(
                      decoration: TextDecoration.underline,
                      fontFamily: 'RoundedMplusMedium',
                      fontSize: 16.0,
                      fontWeight: FontWeight.w500,
                      color: Color.fromRGBO(27, 39, 45, 0.75)))
            ])),
        onTap: () {
          jumpTo(108);
        },
      ),
      GestureDetector(
        child: Text.rich(TextSpan(
            text: '7. ',
            style: TextStyle(
                fontFamily: 'RoundedMplusMedium',
                fontSize: 16.0,
                fontWeight: FontWeight.w500,
                color: Color.fromRGBO(27, 39, 45, 0.75)),
            children: <TextSpan>[
              TextSpan(
                  text: Strings.participants03_1 + "\n",
                  style: TextStyle(
                      decoration: TextDecoration.underline,
                      fontFamily: 'RoundedMplusMedium',
                      fontSize: 16.0,
                      fontWeight: FontWeight.w500,
                      color: Color.fromRGBO(27, 39, 45, 0.75)))
            ])),
        onTap: () {
          jumpTo(110);
        },
      ),
      GestureDetector(
        child: Text.rich(TextSpan(
            text: '8. ',
            style: TextStyle(
                fontFamily: 'RoundedMplusMedium',
                fontSize: 16.0,
                fontWeight: FontWeight.w500,
                color: Color.fromRGBO(27, 39, 45, 0.75)),
            children: <TextSpan>[
              TextSpan(
                  text: Strings.participants05_1 + "\n",
                  style: TextStyle(
                      decoration: TextDecoration.underline,
                      fontFamily: 'RoundedMplusMedium',
                      fontSize: 16.0,
                      fontWeight: FontWeight.w500,
                      color: Color.fromRGBO(27, 39, 45, 0.75)))
            ])),
        onTap: () {
          jumpTo(112);
        },
      ),
      GestureDetector(
        child: Text.rich(TextSpan(
            text: '9. ',
            style: TextStyle(
                fontFamily: 'RoundedMplusMedium',
                fontSize: 16.0,
                fontWeight: FontWeight.w500,
                color: Color.fromRGBO(27, 39, 45, 0.75)),
            children: <TextSpan>[
              TextSpan(
                  text: Strings.participants07_1 + "\n",
                  style: TextStyle(
                      decoration: TextDecoration.underline,
                      fontFamily: 'RoundedMplusMedium',
                      fontSize: 16.0,
                      fontWeight: FontWeight.w500,
                      color: Color.fromRGBO(27, 39, 45, 0.75)))
            ])),
        onTap: () {
          jumpTo(114);
        },
      ),
      GestureDetector(
        child: Text.rich(TextSpan(
            text: '10. ',
            style: TextStyle(
                fontFamily: 'RoundedMplusMedium',
                fontSize: 16.0,
                fontWeight: FontWeight.w500,
                color: Color.fromRGBO(27, 39, 45, 0.75)),
            children: <TextSpan>[
              TextSpan(
                  text: Strings.participants09_1 + "\n",
                  style: TextStyle(
                      decoration: TextDecoration.underline,
                      fontFamily: 'RoundedMplusMedium',
                      fontSize: 16.0,
                      fontWeight: FontWeight.w500,
                      color: Color.fromRGBO(27, 39, 45, 0.75)))
            ])),
        onTap: () {
          jumpTo(116);
        },
      ),
      Text(Strings.management + "\n",
          style: TextStyle(
              fontFamily: 'RoundedMplusMedium',
              fontSize: 16.0,
              fontWeight: FontWeight.w500,
              color: Color.fromRGBO(63, 113, 226, 1))),
      GestureDetector(
        child: Text.rich(TextSpan(
            text: '11. ',
            style: TextStyle(
                fontFamily: 'RoundedMplusMedium',
                fontSize: 16.0,
                fontWeight: FontWeight.w500,
                color: Color.fromRGBO(27, 39, 45, 0.75)),
            children: <TextSpan>[
              TextSpan(
                  text: Strings.management01_1 + "\n",
                  style: TextStyle(
                      decoration: TextDecoration.underline,
                      fontFamily: 'RoundedMplusMedium',
                      fontSize: 16.0,
                      fontWeight: FontWeight.w500,
                      color: Color.fromRGBO(27, 39, 45, 0.75)))
            ])),
        onTap: () {
          jumpTo(120);
        },
      ),
      GestureDetector(
        child: Text.rich(TextSpan(
            text: '12. ',
            style: TextStyle(
                fontFamily: 'RoundedMplusMedium',
                fontSize: 16.0,
                fontWeight: FontWeight.w500,
                color: Color.fromRGBO(27, 39, 45, 0.75)),
            children: <TextSpan>[
              TextSpan(
                  text: Strings.management03_1 + "\n",
                  style: TextStyle(
                      decoration: TextDecoration.underline,
                      fontFamily: 'RoundedMplusMedium',
                      fontSize: 16.0,
                      fontWeight: FontWeight.w500,
                      color: Color.fromRGBO(27, 39, 45, 0.75)))
            ])),
        onTap: () {
          jumpTo(122);
        },
      ),
      Text(Strings.judges + "\n",
          style: TextStyle(
              fontFamily: 'RoundedMplusMedium',
              fontSize: 16.0,
              fontWeight: FontWeight.w500,
              color: Color.fromRGBO(63, 113, 226, 1))),
      GestureDetector(
        child: Text.rich(TextSpan(
            text: '13. ',
            style: TextStyle(
                fontFamily: 'RoundedMplusMedium',
                fontSize: 16.0,
                fontWeight: FontWeight.w500,
                color: Color.fromRGBO(27, 39, 45, 0.75)),
            children: <TextSpan>[
              TextSpan(
                  text: Strings.judges01_1 + "\n",
                  style: TextStyle(
                      decoration: TextDecoration.underline,
                      fontFamily: 'RoundedMplusMedium',
                      fontSize: 16.0,
                      fontWeight: FontWeight.w500,
                      color: Color.fromRGBO(27, 39, 45, 0.75)))
            ])),
        onTap: () {
          jumpTo(126);
        },
      ),
      GestureDetector(
        child: Text.rich(TextSpan(
            text: '14. ',
            style: TextStyle(
                fontFamily: 'RoundedMplusMedium',
                fontSize: 16.0,
                fontWeight: FontWeight.w500,
                color: Color.fromRGBO(27, 39, 45, 0.75)),
            children: <TextSpan>[
              TextSpan(
                  text: Strings.judges03_1 + "\n",
                  style: TextStyle(
                      decoration: TextDecoration.underline,
                      fontFamily: 'RoundedMplusMedium',
                      fontSize: 16.0,
                      fontWeight: FontWeight.w500,
                      color: Color.fromRGBO(27, 39, 45, 0.75)))
            ])),
        onTap: () {
          jumpTo(128);
        },
      ),
      GestureDetector(
        child: Text.rich(TextSpan(
            text: '15. ',
            style: TextStyle(
                fontFamily: 'RoundedMplusMedium',
                fontSize: 16.0,
                fontWeight: FontWeight.w500,
                color: Color.fromRGBO(27, 39, 45, 0.75)),
            children: <TextSpan>[
              TextSpan(
                  text: Strings.judges05_1 + "\n",
                  style: TextStyle(
                      decoration: TextDecoration.underline,
                      fontFamily: 'RoundedMplusMedium',
                      fontSize: 16.0,
                      fontWeight: FontWeight.w500,
                      color: Color.fromRGBO(27, 39, 45, 0.75)))
            ])),
        onTap: () {
          jumpTo(130);
        },
      ),
      GestureDetector(
        child: Text.rich(TextSpan(
            text: '16. ',
            style: TextStyle(
                fontFamily: 'RoundedMplusMedium',
                fontSize: 16.0,
                fontWeight: FontWeight.w500,
                color: Color.fromRGBO(27, 39, 45, 0.75)),
            children: <TextSpan>[
              TextSpan(
                  text: Strings.judges07_1 + "\n",
                  style: TextStyle(
                      decoration: TextDecoration.underline,
                      fontFamily: 'RoundedMplusMedium',
                      fontSize: 16.0,
                      fontWeight: FontWeight.w500,
                      color: Color.fromRGBO(27, 39, 45, 0.75)))
            ])),
        onTap: () {
          jumpTo(132);
        },
      ),
      GestureDetector(
        child: Text.rich(TextSpan(
            text: '17. ',
            style: TextStyle(
                fontFamily: 'RoundedMplusMedium',
                fontSize: 16.0,
                fontWeight: FontWeight.w500,
                color: Color.fromRGBO(27, 39, 45, 0.75)),
            children: <TextSpan>[
              TextSpan(
                  text: Strings.judges09_1 + "\n",
                  style: TextStyle(
                      decoration: TextDecoration.underline,
                      fontFamily: 'RoundedMplusMedium',
                      fontSize: 16.0,
                      fontWeight: FontWeight.w500,
                      color: Color.fromRGBO(27, 39, 45, 0.75)))
            ])),
        onTap: () {
          jumpTo(134);
        },
      ),
      GestureDetector(
        child: Text.rich(TextSpan(
            text: '18. ',
            style: TextStyle(
                fontFamily: 'RoundedMplusMedium',
                fontSize: 16.0,
                fontWeight: FontWeight.w500,
                color: Color.fromRGBO(27, 39, 45, 0.75)),
            children: <TextSpan>[
              TextSpan(
                  text: Strings.judges11_1 + "\n",
                  style: TextStyle(
                      decoration: TextDecoration.underline,
                      fontFamily: 'RoundedMplusMedium',
                      fontSize: 16.0,
                      fontWeight: FontWeight.w500,
                      color: Color.fromRGBO(27, 39, 45, 0.75)))
            ])),
        onTap: () {
          jumpTo(136);
        },
      ),
      Text(Strings.formation + "\n",
          style: TextStyle(
              fontFamily: 'RoundedMplusMedium',
              fontSize: 16.0,
              fontWeight: FontWeight.w500,
              color: Color.fromRGBO(63, 113, 226, 1))),
      GestureDetector(
        child: Text.rich(TextSpan(
            text: '19. ',
            style: TextStyle(
                fontFamily: 'RoundedMplusMedium',
                fontSize: 16.0,
                fontWeight: FontWeight.w500,
                color: Color.fromRGBO(27, 39, 45, 0.75)),
            children: <TextSpan>[
              TextSpan(
                  text: Strings.formation02_1 + "\n",
                  style: TextStyle(
                      decoration: TextDecoration.underline,
                      fontFamily: 'RoundedMplusMedium',
                      fontSize: 16.0,
                      fontWeight: FontWeight.w500,
                      color: Color.fromRGBO(27, 39, 45, 0.75)))
            ])),
        onTap: () {
          jumpTo(140);
        },
      ),
      GestureDetector(
        child: Text.rich(TextSpan(
            text: '20. ',
            style: TextStyle(
                fontFamily: 'RoundedMplusMedium',
                fontSize: 16.0,
                fontWeight: FontWeight.w500,
                color: Color.fromRGBO(27, 39, 45, 0.75)),
            children: <TextSpan>[
              TextSpan(
                  text: Strings.formation04_1 + "\n",
                  style: TextStyle(
                      decoration: TextDecoration.underline,
                      fontFamily: 'RoundedMplusMedium',
                      fontSize: 16.0,
                      fontWeight: FontWeight.w500,
                      color: Color.fromRGBO(27, 39, 45, 0.75)))
            ])),
        onTap: () {
          jumpTo(142);
        },
      ),
      Text(Strings.startDistTiming + "\n",
          style: TextStyle(
              fontFamily: 'RoundedMplusMedium',
              fontSize: 16.0,
              fontWeight: FontWeight.w500,
              color: Color.fromRGBO(63, 113, 226, 1))),
      GestureDetector(
        child: Text.rich(TextSpan(
            text: '21. ',
            style: TextStyle(
                fontFamily: 'RoundedMplusMedium',
                fontSize: 16.0,
                fontWeight: FontWeight.w500,
                color: Color.fromRGBO(27, 39, 45, 0.75)),
            children: <TextSpan>[
              TextSpan(
                  text: Strings.openWater051 + "\n",
                  style: TextStyle(
                      decoration: TextDecoration.underline,
                      fontFamily: 'RoundedMplusMedium',
                      fontSize: 16.0,
                      fontWeight: FontWeight.w500,
                      color: Color.fromRGBO(27, 39, 45, 0.75)))
            ])),
        onTap: () {
          jumpTo(146);
        },
      ),
      GestureDetector(
        child: Text.rich(TextSpan(
            text: '22. ',
            style: TextStyle(
                fontFamily: 'RoundedMplusMedium',
                fontSize: 16.0,
                fontWeight: FontWeight.w500,
                color: Color.fromRGBO(27, 39, 45, 0.75)),
            children: <TextSpan>[
              TextSpan(
                  text: Strings.startDistTiming031 + "\n",
                  style: TextStyle(
                      decoration: TextDecoration.underline,
                      fontFamily: 'RoundedMplusMedium',
                      fontSize: 16.0,
                      fontWeight: FontWeight.w500,
                      color: Color.fromRGBO(27, 39, 45, 0.75)))
            ])),
        onTap: () {
          jumpTo(148);
        },
      ),
      GestureDetector(
        child: Text.rich(TextSpan(
            text: '23. ',
            style: TextStyle(
                fontFamily: 'RoundedMplusMedium',
                fontSize: 16.0,
                fontWeight: FontWeight.w500,
                color: Color.fromRGBO(27, 39, 45, 0.75)),
            children: <TextSpan>[
              TextSpan(
                  text: Strings.startDistTiming051 + "\n",
                  style: TextStyle(
                      decoration: TextDecoration.underline,
                      fontFamily: 'RoundedMplusMedium',
                      fontSize: 16.0,
                      fontWeight: FontWeight.w500,
                      color: Color.fromRGBO(27, 39, 45, 0.75)))
            ])),
        onTap: () {
          jumpTo(150);
        },
      ),
      Text(Strings.styles + "\n",
          style: TextStyle(
              fontFamily: 'RoundedMplusMedium',
              fontSize: 16.0,
              fontWeight: FontWeight.w500,
              color: Color.fromRGBO(63, 113, 226, 1))),
      GestureDetector(
        child: Text.rich(TextSpan(
            text: '24. ',
            style: TextStyle(
                fontFamily: 'RoundedMplusMedium',
                fontSize: 16.0,
                fontWeight: FontWeight.w500,
                color: Color.fromRGBO(27, 39, 45, 0.75)),
            children: <TextSpan>[
              TextSpan(
                  text: Strings.styles011 + "\n",
                  style: TextStyle(
                      decoration: TextDecoration.underline,
                      fontFamily: 'RoundedMplusMedium',
                      fontSize: 16.0,
                      fontWeight: FontWeight.w500,
                      color: Color.fromRGBO(27, 39, 45, 0.75)))
            ])),
        onTap: () {
          jumpTo(154);
        },
      ),
      GestureDetector(
        child: Text.rich(TextSpan(
            text: '25. ',
            style: TextStyle(
                fontFamily: 'RoundedMplusMedium',
                fontSize: 16.0,
                fontWeight: FontWeight.w500,
                color: Color.fromRGBO(27, 39, 45, 0.75)),
            children: <TextSpan>[
              TextSpan(
                  text: Strings.styles031 + "\n",
                  style: TextStyle(
                      decoration: TextDecoration.underline,
                      fontFamily: 'RoundedMplusMedium',
                      fontSize: 16.0,
                      fontWeight: FontWeight.w500,
                      color: Color.fromRGBO(27, 39, 45, 0.75)))
            ])),
        onTap: () {
          jumpTo(156);
        },
      ),
      GestureDetector(
        child: Text.rich(TextSpan(
            text: '26. ',
            style: TextStyle(
                fontFamily: 'RoundedMplusMedium',
                fontSize: 16.0,
                fontWeight: FontWeight.w500,
                color: Color.fromRGBO(27, 39, 45, 0.75)),
            children: <TextSpan>[
              TextSpan(
                  text: Strings.styles051 + "\n",
                  style: TextStyle(
                      decoration: TextDecoration.underline,
                      fontFamily: 'RoundedMplusMedium',
                      fontSize: 16.0,
                      fontWeight: FontWeight.w500,
                      color: Color.fromRGBO(27, 39, 45, 0.75)))
            ])),
        onTap: () {
          jumpTo(158);
        },
      ),
      GestureDetector(
        child: Text.rich(TextSpan(
            text: '27. ',
            style: TextStyle(
                fontFamily: 'RoundedMplusMedium',
                fontSize: 16.0,
                fontWeight: FontWeight.w500,
                color: Color.fromRGBO(27, 39, 45, 0.75)),
            children: <TextSpan>[
              TextSpan(
                  text: Strings.styles071 + "\n",
                  style: TextStyle(
                      decoration: TextDecoration.underline,
                      fontFamily: 'RoundedMplusMedium',
                      fontSize: 16.0,
                      fontWeight: FontWeight.w500,
                      color: Color.fromRGBO(27, 39, 45, 0.75)))
            ])),
        onTap: () {
          jumpTo(160);
        },
      ),
      GestureDetector(
        child: Text.rich(TextSpan(
            text: '28. ',
            style: TextStyle(
                fontFamily: 'RoundedMplusMedium',
                fontSize: 16.0,
                fontWeight: FontWeight.w500,
                color: Color.fromRGBO(27, 39, 45, 0.75)),
            children: <TextSpan>[
              TextSpan(
                  text: Strings.styles091 + "\n",
                  style: TextStyle(
                      decoration: TextDecoration.underline,
                      fontFamily: 'RoundedMplusMedium',
                      fontSize: 16.0,
                      fontWeight: FontWeight.w500,
                      color: Color.fromRGBO(27, 39, 45, 0.75)))
            ])),
        onTap: () {
          jumpTo(162);
        },
      ),
      Text(Strings.requirements + "\n",
          style: TextStyle(
              fontFamily: 'RoundedMplusMedium',
              fontSize: 16.0,
              fontWeight: FontWeight.w500,
              color: Color.fromRGBO(63, 113, 226, 1))),
      GestureDetector(
        child: Text.rich(TextSpan(
            text: '29. ',
            style: TextStyle(
                fontFamily: 'RoundedMplusMedium',
                fontSize: 16.0,
                fontWeight: FontWeight.w500,
                color: Color.fromRGBO(27, 39, 45, 0.75)),
            children: <TextSpan>[
              TextSpan(
                  text: Strings.requirements01_1 + "\n",
                  style: TextStyle(
                      decoration: TextDecoration.underline,
                      fontFamily: 'RoundedMplusMedium',
                      fontSize: 16.0,
                      fontWeight: FontWeight.w500,
                      color: Color.fromRGBO(27, 39, 45, 0.75)))
            ])),
        onTap: () {
          jumpTo(166);
        },
      ),
      GestureDetector(
        child: Text.rich(TextSpan(
            text: '30. ',
            style: TextStyle(
                fontFamily: 'RoundedMplusMedium',
                fontSize: 16.0,
                fontWeight: FontWeight.w500,
                color: Color.fromRGBO(27, 39, 45, 0.75)),
            children: <TextSpan>[
              TextSpan(
                  text: Strings.requirements03_1 + "\n",
                  style: TextStyle(
                      decoration: TextDecoration.underline,
                      fontFamily: 'RoundedMplusMedium',
                      fontSize: 16.0,
                      fontWeight: FontWeight.w500,
                      color: Color.fromRGBO(27, 39, 45, 0.75)))
            ])),
        onTap: () {
          jumpTo(168);
        },
      ),
      GestureDetector(
        child: Text.rich(TextSpan(
            text: '31. ',
            style: TextStyle(
                fontFamily: 'RoundedMplusMedium',
                fontSize: 16.0,
                fontWeight: FontWeight.w500,
                color: Color.fromRGBO(27, 39, 45, 0.75)),
            children: <TextSpan>[
              TextSpan(
                  text: Strings.requirements05_1 + "\n",
                  style: TextStyle(
                      decoration: TextDecoration.underline,
                      fontFamily: 'RoundedMplusMedium',
                      fontSize: 16.0,
                      fontWeight: FontWeight.w500,
                      color: Color.fromRGBO(27, 39, 45, 0.75)))
            ])),
        onTap: () {
          jumpTo(170);
        },
      ),
      Text(Strings.openWater + "\n",
          style: TextStyle(
              fontFamily: 'RoundedMplusMedium',
              fontSize: 16.0,
              fontWeight: FontWeight.w500,
              color: Color.fromRGBO(63, 113, 226, 1))),
      GestureDetector(
        child: Text.rich(TextSpan(
            text: '32. ',
            style: TextStyle(
                fontFamily: 'RoundedMplusMedium',
                fontSize: 16.0,
                fontWeight: FontWeight.w500,
                color: Color.fromRGBO(27, 39, 45, 0.75)),
            children: <TextSpan>[
              TextSpan(
                  text: Strings.openWater011 + "\n",
                  style: TextStyle(
                      decoration: TextDecoration.underline,
                      fontFamily: 'RoundedMplusMedium',
                      fontSize: 16.0,
                      fontWeight: FontWeight.w500,
                      color: Color.fromRGBO(27, 39, 45, 0.75)))
            ])),
        onTap: () {
          jumpTo(174);
        },
      ),
      GestureDetector(
        child: Text.rich(TextSpan(
            text: '33. ',
            style: TextStyle(
                fontFamily: 'RoundedMplusMedium',
                fontSize: 16.0,
                fontWeight: FontWeight.w500,
                color: Color.fromRGBO(27, 39, 45, 0.75)),
            children: <TextSpan>[
              TextSpan(
                  text: Strings.judges + "\n",
                  style: TextStyle(
                      decoration: TextDecoration.underline,
                      fontFamily: 'RoundedMplusMedium',
                      fontSize: 16.0,
                      fontWeight: FontWeight.w500,
                      color: Color.fromRGBO(27, 39, 45, 0.75)))
            ])),
        onTap: () {
          jumpTo(176);
        },
      ),
      GestureDetector(
        child: Text.rich(TextSpan(
            text: '34. ',
            style: TextStyle(
                fontFamily: 'RoundedMplusMedium',
                fontSize: 16.0,
                fontWeight: FontWeight.w500,
                color: Color.fromRGBO(27, 39, 45, 0.75)),
            children: <TextSpan>[
              TextSpan(
                  text: Strings.openWater051 + "\n",
                  style: TextStyle(
                      decoration: TextDecoration.underline,
                      fontFamily: 'RoundedMplusMedium',
                      fontSize: 16.0,
                      fontWeight: FontWeight.w500,
                      color: Color.fromRGBO(27, 39, 45, 0.75)))
            ])),
        onTap: () {
          jumpTo(178);
        },
      ),
      GestureDetector(
        child: Text.rich(TextSpan(
            text: '35. ',
            style: TextStyle(
                fontFamily: 'RoundedMplusMedium',
                fontSize: 16.0,
                fontWeight: FontWeight.w500,
                color: Color.fromRGBO(27, 39, 45, 0.75)),
            children: <TextSpan>[
              TextSpan(
                  text: Strings.openWater071 + "\n",
                  style: TextStyle(
                      decoration: TextDecoration.underline,
                      fontFamily: 'RoundedMplusMedium',
                      fontSize: 16.0,
                      fontWeight: FontWeight.w500,
                      color: Color.fromRGBO(27, 39, 45, 0.75)))
            ])),
        onTap: () {
          jumpTo(180);
        },
      ),
      GestureDetector(
        child: Text.rich(TextSpan(
            text: '36. ',
            style: TextStyle(
                fontFamily: 'RoundedMplusMedium',
                fontSize: 16.0,
                fontWeight: FontWeight.w500,
                color: Color.fromRGBO(27, 39, 45, 0.75)),
            children: <TextSpan>[
              TextSpan(
                  text: Strings.openWater091 + "\n\n",
                  style: TextStyle(
                      decoration: TextDecoration.underline,
                      fontFamily: 'RoundedMplusMedium',
                      fontSize: 16.0,
                      fontWeight: FontWeight.w500,
                      color: Color.fromRGBO(27, 39, 45, 0.75)))
            ])),
        onTap: () {
          jumpTo(182);
        },
      ),
      //Generalities
      Text(
        Strings.generalities001,
        style: TextStyle(
            fontFamily: 'RoundedMplusMedium',
            fontSize: 16.0,
            fontWeight: FontWeight.w500,
            color: Color.fromRGBO(27, 39, 45, 0.75)),
      ),
      Text(Strings.generalities1,
          style: TextStyle(
            fontFamily: 'RoundedMplus',
            fontSize: 14.0,
            color: Color.fromRGBO(27, 39, 45, 0.75),
          )),
      Text(Strings.generalities2,
          style: TextStyle(
              fontFamily: 'RoundedMplusMedium',
              fontSize: 16.0,
              fontWeight: FontWeight.w500,
              color: Color.fromRGBO(27, 39, 45, 0.75))),
      Text(Strings.generalities3,
          style: TextStyle(
              fontFamily: 'RoundedMplus',
              fontSize: 14.0,
              color: Color.fromRGBO(27, 39, 45, 0.75))),
      Divider(),
      Row(
        children: <Widget>[
          Expanded(child: Container()),
          Expanded(
            child: Padding(
                padding: EdgeInsets.only(left: 4),
                child: Text(
                  Strings.generalities002,
                  style: TextStyle(
                      fontFamily: 'RoundedMplusMedium',
                      fontSize: 12.0,
                      fontWeight: FontWeight.w500,
                      color: Color.fromRGBO(27, 39, 45, 0.75)),
                )),
          )
        ],
      ),
      Divider(),
      Row(
        children: <Widget>[
          Expanded(
            child: Padding(
                padding: EdgeInsets.only(right: 4),
                child: Text(
                  Strings.freestyle,
                  style: TextStyle(
                      fontFamily: 'RoundedMplusMedium',
                      fontSize: 12.0,
                      fontWeight: FontWeight.w500,
                      color: Color.fromRGBO(27, 39, 45, 0.75)),
                )),
          ),
          Expanded(
            child: Padding(
                padding: EdgeInsets.only(left: 4),
                child: Text(
                  Strings.generalities003,
                  style: TextStyle(
                      fontFamily: 'RoundedMplus',
                      fontSize: 12.0,
                      color: Color.fromRGBO(27, 39, 45, 0.75)),
                )),
          ),
        ],
      ),
      Divider(),
      Row(
        children: <Widget>[
          Expanded(
            child: Padding(
                padding: EdgeInsets.only(right: 4),
                child: Text(
                  Strings.onBack,
                  style: TextStyle(
                      fontFamily: 'RoundedMplusMedium',
                      fontSize: 12.0,
                      fontWeight: FontWeight.w500,
                      color: Color.fromRGBO(27, 39, 45, 0.75)),
                )),
          ),
          Expanded(
            child: Padding(
                padding: EdgeInsets.only(left: 4),
                child: Text(
                  Strings.generalities004,
                  style: TextStyle(
                      fontFamily: 'RoundedMplus',
                      fontSize: 12.0,
                      color: Color.fromRGBO(27, 39, 45, 0.75)),
                )),
          ),
        ],
      ),
      Divider(),
      Row(
        children: <Widget>[
          Expanded(
            child: Padding(
                padding: EdgeInsets.only(right: 4),
                child: Text(
                  Strings.breaststroke,
                  style: TextStyle(
                      fontFamily: 'RoundedMplusMedium',
                      fontSize: 12.0,
                      fontWeight: FontWeight.w500,
                      color: Color.fromRGBO(27, 39, 45, 0.75)),
                )),
          ),
          Expanded(
            child: Padding(
                padding: EdgeInsets.only(left: 4),
                child: Text(
                  Strings.generalities004,
                  style: TextStyle(
                      fontFamily: 'RoundedMplus',
                      fontSize: 12.0,
                      color: Color.fromRGBO(27, 39, 45, 0.75)),
                )),
          ),
        ],
      ),
      Divider(),
      Row(
        children: <Widget>[
          Expanded(
            child: Padding(
                padding: EdgeInsets.only(right: 4),
                child: Text(
                  Strings.butterfly,
                  style: TextStyle(
                      fontFamily: 'RoundedMplusMedium',
                      fontSize: 12.0,
                      fontWeight: FontWeight.w500,
                      color: Color.fromRGBO(27, 39, 45, 0.75)),
                )),
          ),
          Expanded(
            child: Padding(
                padding: EdgeInsets.only(left: 4),
                child: Text(
                  Strings.generalities004,
                  style: TextStyle(
                      fontFamily: 'RoundedMplus',
                      fontSize: 12.0,
                      color: Color.fromRGBO(27, 39, 45, 0.75)),
                )),
          ),
        ],
      ),
      Divider(),
      Row(
        children: <Widget>[
          Expanded(
            child: Padding(
                padding: EdgeInsets.only(right: 4),
                child: Text(
                  Strings.complexSwim,
                  style: TextStyle(
                      fontFamily: 'RoundedMplusMedium',
                      fontSize: 12.0,
                      fontWeight: FontWeight.w500,
                      color: Color.fromRGBO(27, 39, 45, 0.75)),
                )),
          ),
          Expanded(
            child: Padding(
                padding: EdgeInsets.only(left: 4),
                child: Text(
                  Strings.generalities005,
                  style: TextStyle(
                      fontFamily: 'RoundedMplus',
                      fontSize: 12.0,
                      color: Color.fromRGBO(27, 39, 45, 0.75)),
                )),
          ),
        ],
      ),
      Divider(),
      Text(Strings.generalities4,
          style: TextStyle(
              fontFamily: 'RoundedMplusMedium',
              fontSize: 12.0,
              fontWeight: FontWeight.w500,
              color: Color.fromRGBO(27, 39, 45, 0.75))),
      Divider(),
      Row(
        children: <Widget>[
          Expanded(
            child: Padding(
                padding: EdgeInsets.only(right: 4),
                child: Text(
                  Strings.freestyle,
                  style: TextStyle(
                      fontFamily: 'RoundedMplusMedium',
                      fontSize: 12.0,
                      fontWeight: FontWeight.w500,
                      color: Color.fromRGBO(27, 39, 45, 0.75)),
                )),
          ),
          Expanded(
            child: Padding(
                padding: EdgeInsets.only(left: 4),
                child: Text(
                  Strings.generalities006,
                  style: TextStyle(
                      fontFamily: 'RoundedMplus',
                      fontSize: 12.0,
                      color: Color.fromRGBO(27, 39, 45, 0.75)),
                )),
          ),
        ],
      ),
      Divider(),
      Row(
        children: <Widget>[
          Expanded(
            child: Padding(
                padding: EdgeInsets.only(right: 4),
                child: Text(
                  Strings.combined,
                  style: TextStyle(
                      fontFamily: 'RoundedMplusMedium',
                      fontSize: 12.0,
                      fontWeight: FontWeight.w500,
                      color: Color.fromRGBO(27, 39, 45, 0.75)),
                )),
          ),
          Expanded(
            child: Padding(
                padding: EdgeInsets.only(left: 4),
                child: Text(
                  Strings.generalities007,
                  style: TextStyle(
                      fontFamily: 'RoundedMplus',
                      fontSize: 12.0,
                      color: Color.fromRGBO(27, 39, 45, 0.75)),
                )),
          ),
        ],
      ),
      Divider(),
      Row(
        children: <Widget>[
          Expanded(
            child: Padding(
                padding: EdgeInsets.only(right: 4),
                child: Text(
                  Strings.mixedFreestyle,
                  style: TextStyle(
                      fontFamily: 'RoundedMplusMedium',
                      fontSize: 12.0,
                      fontWeight: FontWeight.w500,
                      color: Color.fromRGBO(27, 39, 45, 0.75)),
                )),
          ),
          Expanded(
            child: Padding(
                padding: EdgeInsets.only(left: 4),
                child: Text(
                  Strings.generalities008,
                  style: TextStyle(
                      fontFamily: 'RoundedMplus',
                      fontSize: 12.0,
                      color: Color.fromRGBO(27, 39, 45, 0.75)),
                )),
          ),
        ],
      ),
      Divider(),
      Row(
        children: <Widget>[
          Expanded(
            child: Padding(
                padding: EdgeInsets.only(right: 4),
                child: Text(
                  Strings.mixedCombined,
                  style: TextStyle(
                      fontFamily: 'RoundedMplusMedium',
                      fontSize: 12.0,
                      fontWeight: FontWeight.w500,
                      color: Color.fromRGBO(27, 39, 45, 0.75)),
                )),
          ),
          Expanded(
            child: Padding(
                padding: EdgeInsets.only(left: 4),
                child: Text(
                  Strings.generalities008,
                  style: TextStyle(
                      fontFamily: 'RoundedMplus',
                      fontSize: 12.0,
                      color: Color.fromRGBO(27, 39, 45, 0.75)),
                )),
          ),
        ],
      ),
      Divider(),
      Text(Strings.generalities5,
          style: TextStyle(
              fontFamily: 'RoundedMplus',
              fontSize: 14.0,
              color: Color.fromRGBO(27, 39, 45, 0.75))),
      Divider(),
      Row(
        children: <Widget>[
          Expanded(child: Container()),
          Expanded(
            child: Padding(
                padding: EdgeInsets.only(left: 4),
                child: Text(
                  Strings.generalities002,
                  style: TextStyle(
                      fontFamily: 'RoundedMplusMedium',
                      fontSize: 12.0,
                      fontWeight: FontWeight.w500,
                      color: Color.fromRGBO(27, 39, 45, 0.75)),
                )),
          )
        ],
      ),
      Divider(),
      Row(
        children: <Widget>[
          Expanded(
            child: Padding(
                padding: EdgeInsets.only(right: 4),
                child: Text(
                  Strings.freestyle,
                  style: TextStyle(
                      fontFamily: 'RoundedMplusMedium',
                      fontSize: 12.0,
                      fontWeight: FontWeight.w500,
                      color: Color.fromRGBO(27, 39, 45, 0.75)),
                )),
          ),
          Expanded(
            child: Padding(
                padding: EdgeInsets.only(left: 4),
                child: Text(
                  Strings.generalities003,
                  style: TextStyle(
                      fontFamily: 'RoundedMplus',
                      fontSize: 12.0,
                      color: Color.fromRGBO(27, 39, 45, 0.75)),
                )),
          ),
        ],
      ),
      Divider(),
      Row(
        children: <Widget>[
          Expanded(
            child: Padding(
                padding: EdgeInsets.only(right: 4),
                child: Text(
                  Strings.onBack,
                  style: TextStyle(
                      fontFamily: 'RoundedMplusMedium',
                      fontSize: 12.0,
                      fontWeight: FontWeight.w500,
                      color: Color.fromRGBO(27, 39, 45, 0.75)),
                )),
          ),
          Expanded(
            child: Padding(
                padding: EdgeInsets.only(left: 4),
                child: Text(
                  Strings.generalities004,
                  style: TextStyle(
                      fontFamily: 'RoundedMplus',
                      fontSize: 12.0,
                      color: Color.fromRGBO(27, 39, 45, 0.75)),
                )),
          ),
        ],
      ),
      Divider(),
      Row(
        children: <Widget>[
          Expanded(
            child: Padding(
                padding: EdgeInsets.only(right: 4),
                child: Text(
                  Strings.breaststroke,
                  style: TextStyle(
                      fontFamily: 'RoundedMplusMedium',
                      fontSize: 12.0,
                      fontWeight: FontWeight.w500,
                      color: Color.fromRGBO(27, 39, 45, 0.75)),
                )),
          ),
          Expanded(
            child: Padding(
                padding: EdgeInsets.only(left: 4),
                child: Text(
                  Strings.generalities004,
                  style: TextStyle(
                      fontFamily: 'RoundedMplus',
                      fontSize: 12.0,
                      color: Color.fromRGBO(27, 39, 45, 0.75)),
                )),
          ),
        ],
      ),
      Divider(),
      Row(
        children: <Widget>[
          Expanded(
            child: Padding(
                padding: EdgeInsets.only(right: 4),
                child: Text(
                  Strings.butterfly,
                  style: TextStyle(
                      fontFamily: 'RoundedMplusMedium',
                      fontSize: 12.0,
                      fontWeight: FontWeight.w500,
                      color: Color.fromRGBO(27, 39, 45, 0.75)),
                )),
          ),
          Expanded(
            child: Padding(
                padding: EdgeInsets.only(left: 4),
                child: Text(
                  Strings.generalities004,
                  style: TextStyle(
                      fontFamily: 'RoundedMplus',
                      fontSize: 12.0,
                      color: Color.fromRGBO(27, 39, 45, 0.75)),
                )),
          ),
        ],
      ),
      Divider(),
      Row(
        children: <Widget>[
          Expanded(
            child: Padding(
                padding: EdgeInsets.only(right: 4),
                child: Text(
                  Strings.complexSwim,
                  style: TextStyle(
                      fontFamily: 'RoundedMplusMedium',
                      fontSize: 12.0,
                      fontWeight: FontWeight.w500,
                      color: Color.fromRGBO(27, 39, 45, 0.75)),
                )),
          ),
          Expanded(
            child: Padding(
                padding: EdgeInsets.only(left: 4),
                child: Text(
                  Strings.generalities009,
                  style: TextStyle(
                      fontFamily: 'RoundedMplus',
                      fontSize: 12.0,
                      color: Color.fromRGBO(27, 39, 45, 0.75)),
                )),
          ),
        ],
      ),
      Divider(),
      Text(Strings.generalities4,
          style: TextStyle(
              fontFamily: 'RoundedMplusMedium',
              fontSize: 12.0,
              fontWeight: FontWeight.w500,
              color: Color.fromRGBO(27, 39, 45, 0.75))),
      Divider(),
      Row(
        children: <Widget>[
          Expanded(
            child: Padding(
                padding: EdgeInsets.only(right: 4),
                child: Text(
                  Strings.freestyle,
                  style: TextStyle(
                      fontFamily: 'RoundedMplusMedium',
                      fontSize: 12.0,
                      fontWeight: FontWeight.w500,
                      color: Color.fromRGBO(27, 39, 45, 0.75)),
                )),
          ),
          Expanded(
            child: Padding(
                padding: EdgeInsets.only(left: 4),
                child: Text(
                  Strings.generalities010,
                  style: TextStyle(
                      fontFamily: 'RoundedMplus',
                      fontSize: 12.0,
                      color: Color.fromRGBO(27, 39, 45, 0.75)),
                )),
          ),
        ],
      ),
      Divider(),
      Row(
        children: <Widget>[
          Expanded(
            child: Padding(
                padding: EdgeInsets.only(right: 4),
                child: Text(
                  Strings.combined,
                  style: TextStyle(
                      fontFamily: 'RoundedMplusMedium',
                      fontSize: 12.0,
                      fontWeight: FontWeight.w500,
                      color: Color.fromRGBO(27, 39, 45, 0.75)),
                )),
          ),
          Expanded(
            child: Padding(
                padding: EdgeInsets.only(left: 4),
                child: Text(
                  Strings.generalities011,
                  style: TextStyle(
                      fontFamily: 'RoundedMplus',
                      fontSize: 12.0,
                      color: Color.fromRGBO(27, 39, 45, 0.75)),
                )),
          ),
        ],
      ),
      Divider(),
      Row(
        children: <Widget>[
          Expanded(
            child: Padding(
                padding: EdgeInsets.only(right: 4),
                child: Text(
                  Strings.mixedFreestyle,
                  style: TextStyle(
                      fontFamily: 'RoundedMplusMedium',
                      fontSize: 12.0,
                      fontWeight: FontWeight.w500,
                      color: Color.fromRGBO(27, 39, 45, 0.75)),
                )),
          ),
          Expanded(
            child: Padding(
                padding: EdgeInsets.only(left: 4),
                child: Text(
                  Strings.generalities011,
                  style: TextStyle(
                      fontFamily: 'RoundedMplus',
                      fontSize: 12.0,
                      color: Color.fromRGBO(27, 39, 45, 0.75)),
                )),
          ),
        ],
      ),
      Divider(),
      Row(
        children: <Widget>[
          Expanded(
            child: Padding(
                padding: EdgeInsets.only(right: 4),
                child: Text(
                  Strings.mixedCombined,
                  style: TextStyle(
                      fontFamily: 'RoundedMplusMedium',
                      fontSize: 12.0,
                      fontWeight: FontWeight.w500,
                      color: Color.fromRGBO(27, 39, 45, 0.75)),
                )),
          ),
          Expanded(
            child: Padding(
                padding: EdgeInsets.only(left: 4),
                child: Text(
                  Strings.generalities011,
                  style: TextStyle(
                      fontFamily: 'RoundedMplus',
                      fontSize: 12.0,
                      color: Color.fromRGBO(27, 39, 45, 0.75)),
                )),
          ),
        ],
      ),
      Divider(),
      Text(Strings.generalities6,
          style: TextStyle(
              fontFamily: 'RoundedMplus',
              fontSize: 14.0,
              color: Color.fromRGBO(27, 39, 45, 0.75))),
      Text(Strings.generalities7,
          style: TextStyle(
              fontFamily: 'RoundedMplusMedium',
              fontSize: 16.0,
              fontWeight: FontWeight.w500,
              color: Color.fromRGBO(27, 39, 45, 0.75))),
      Text(Strings.generalities8,
          style: TextStyle(
              fontFamily: 'RoundedMplus',
              fontSize: 14.0,
              color: Color.fromRGBO(27, 39, 45, 0.75))),
      Text(Strings.generalities9,
          style: TextStyle(
              fontFamily: 'RoundedMplusMedium',
              fontSize: 16.0,
              fontWeight: FontWeight.w500,
              color: Color.fromRGBO(27, 39, 45, 0.75))),
      Text(Strings.generalities10,
          style: TextStyle(
              fontFamily: 'RoundedMplus',
              fontSize: 14.0,
              color: Color.fromRGBO(27, 39, 45, 0.75))),
      Text(Strings.generalities11,
          style: TextStyle(
              fontFamily: 'RoundedMplusMedium',
              fontSize: 16.0,
              fontWeight: FontWeight.w500,
              color: Color.fromRGBO(27, 39, 45, 0.75))),
      Text(Strings.generalities12,
          style: TextStyle(
              fontFamily: 'RoundedMplus',
              fontSize: 14.0,
              color: Color.fromRGBO(27, 39, 45, 0.75))),
      Text(Strings.generalities13,
          style: TextStyle(
              fontFamily: 'RoundedMplusMedium',
              fontSize: 16.0,
              fontWeight: FontWeight.w500,
              color: Color.fromRGBO(27, 39, 45, 0.75))),
      Text(Strings.generalities14,
          style: TextStyle(
              fontFamily: 'RoundedMplus',
              fontSize: 14.0,
              color: Color.fromRGBO(27, 39, 45, 0.75))),
      Divider(),

      //participants
      Text(Strings.participants + "\n",
          style: TextStyle(
              fontFamily: 'RoundedMplusMedium',
              fontSize: 16.0,
              fontWeight: FontWeight.w500,
              color: Color.fromRGBO(63, 113, 226, 1))),
      Text(
        Strings.participants01,
        style: TextStyle(
            fontFamily: 'RoundedMplusMedium',
            fontSize: 16.0,
            fontWeight: FontWeight.w500,
            color: Color.fromRGBO(27, 39, 45, 0.75)),
      ),
      Text(Strings.participants02,
          style: TextStyle(
              fontFamily: 'RoundedMplus',
              fontSize: 14.0,
              color: Color.fromRGBO(27, 39, 45, 0.75))),
      Text(
        Strings.participants03,
        style: TextStyle(
            fontFamily: 'RoundedMplusMedium',
            fontSize: 16.0,
            fontWeight: FontWeight.w500,
            color: Color.fromRGBO(27, 39, 45, 0.75)),
      ),
      Text(Strings.participants04,
          style: TextStyle(
              fontFamily: 'RoundedMplus',
              fontSize: 14.0,
              color: Color.fromRGBO(27, 39, 45, 0.75))),
      Text(
        Strings.participants05,
        style: TextStyle(
            fontFamily: 'RoundedMplusMedium',
            fontSize: 16.0,
            fontWeight: FontWeight.w500,
            color: Color.fromRGBO(27, 39, 45, 0.75)),
      ),
      Text(Strings.participants06,
          style: TextStyle(
              fontFamily: 'RoundedMplus',
              fontSize: 14.0,
              color: Color.fromRGBO(27, 39, 45, 0.75))),
      Text(
        Strings.participants07,
        style: TextStyle(
            fontFamily: 'RoundedMplusMedium',
            fontSize: 16.0,
            fontWeight: FontWeight.w500,
            color: Color.fromRGBO(27, 39, 45, 0.75)),
      ),
      Text(Strings.participants08,
          style: TextStyle(
              fontFamily: 'RoundedMplus',
              fontSize: 14.0,
              color: Color.fromRGBO(27, 39, 45, 0.75))),
      Text(
        Strings.participants09,
        style: TextStyle(
            fontFamily: 'RoundedMplusMedium',
            fontSize: 16.0,
            fontWeight: FontWeight.w500,
            color: Color.fromRGBO(27, 39, 45, 0.75)),
      ),
      Text(Strings.participants10,
          style: TextStyle(
              fontFamily: 'RoundedMplus',
              fontSize: 14.0,
              color: Color.fromRGBO(27, 39, 45, 0.75))),
      Divider(),

      //management
      Text(Strings.management + "\n",
          style: TextStyle(
              fontFamily: 'RoundedMplusMedium',
              fontSize: 16.0,
              fontWeight: FontWeight.w500,
              color: Color.fromRGBO(63, 113, 226, 1))),
      Text(Strings.management01,
          style: TextStyle(
              fontFamily: 'RoundedMplusMedium',
              fontSize: 16.0,
              fontWeight: FontWeight.w500,
              color: Color.fromRGBO(27, 39, 45, 0.75))),
      Text(Strings.management02,
          style: TextStyle(
              fontFamily: 'RoundedMplus',
              fontSize: 14.0,
              color: Color.fromRGBO(27, 39, 45, 0.75))),
      Text(Strings.management03,
          style: TextStyle(
              fontFamily: 'RoundedMplusMedium',
              fontSize: 16.0,
              fontWeight: FontWeight.w500,
              color: Color.fromRGBO(27, 39, 45, 0.75))),
      Text(Strings.management04,
          style: TextStyle(
              fontFamily: 'RoundedMplus',
              fontSize: 14.0,
              color: Color.fromRGBO(27, 39, 45, 0.75))),
      Divider(),

      //judges
      Text(Strings.judges + "\n",
          style: TextStyle(
              fontFamily: 'RoundedMplusMedium',
              fontSize: 16.0,
              fontWeight: FontWeight.w500,
              color: Color.fromRGBO(63, 113, 226, 1))),
      Text(Strings.judges01,
          style: TextStyle(
              fontFamily: 'RoundedMplusMedium',
              fontSize: 16.0,
              fontWeight: FontWeight.w500,
              color: Color.fromRGBO(27, 39, 45, 0.75))),
      Text(Strings.judges02,
          style: TextStyle(
              fontFamily: 'RoundedMplus',
              fontSize: 14.0,
              color: Color.fromRGBO(27, 39, 45, 0.75))),
      Text(Strings.judges03,
          style: TextStyle(
              fontFamily: 'RoundedMplusMedium',
              fontSize: 16.0,
              fontWeight: FontWeight.w500,
              color: Color.fromRGBO(27, 39, 45, 0.75))),
      Text(Strings.judges04,
          style: TextStyle(
              fontFamily: 'RoundedMplus',
              fontSize: 14.0,
              color: Color.fromRGBO(27, 39, 45, 0.75))),
      Text(Strings.judges05,
          style: TextStyle(
              fontFamily: 'RoundedMplusMedium',
              fontSize: 16.0,
              fontWeight: FontWeight.w500,
              color: Color.fromRGBO(27, 39, 45, 0.75))),
      Text(Strings.judges06,
          style: TextStyle(
              fontFamily: 'RoundedMplus',
              fontSize: 14.0,
              color: Color.fromRGBO(27, 39, 45, 0.75))),
      Text(Strings.judges07,
          style: TextStyle(
              fontFamily: 'RoundedMplusMedium',
              fontSize: 16.0,
              fontWeight: FontWeight.w500,
              color: Color.fromRGBO(27, 39, 45, 0.75))),
      Text(Strings.judges08,
          style: TextStyle(
              fontFamily: 'RoundedMplus',
              fontSize: 14.0,
              color: Color.fromRGBO(27, 39, 45, 0.75))),
      Text(Strings.judges09,
          style: TextStyle(
              fontFamily: 'RoundedMplusMedium',
              fontSize: 16.0,
              fontWeight: FontWeight.w500,
              color: Color.fromRGBO(27, 39, 45, 0.75))),
      Text(Strings.judges10,
          style: TextStyle(
              fontFamily: 'RoundedMplus',
              fontSize: 14.0,
              color: Color.fromRGBO(27, 39, 45, 0.75))),
      Text(Strings.judges11,
          style: TextStyle(
              fontFamily: 'RoundedMplusMedium',
              fontSize: 16.0,
              fontWeight: FontWeight.w500,
              color: Color.fromRGBO(27, 39, 45, 0.75))),
      Text(Strings.judges12,
          style: TextStyle(
              fontFamily: 'RoundedMplus',
              fontSize: 14.0,
              color: Color.fromRGBO(27, 39, 45, 0.75))),
      Divider(),

      //formation
      Text(Strings.formation + "\n",
          style: TextStyle(
              fontFamily: 'RoundedMplusMedium',
              fontSize: 16.0,
              fontWeight: FontWeight.w500,
              color: Color.fromRGBO(63, 113, 226, 1))),
      Text(Strings.formation02,
          style: TextStyle(
              fontFamily: 'RoundedMplusMedium',
              fontSize: 16.0,
              fontWeight: FontWeight.w500,
              color: Color.fromRGBO(27, 39, 45, 0.75))),
      Text(Strings.formation03,
          style: TextStyle(
              fontFamily: 'RoundedMplus',
              fontSize: 14.0,
              color: Color.fromRGBO(27, 39, 45, 0.75))),
      Text(Strings.formation04,
          style: TextStyle(
              fontFamily: 'RoundedMplusMedium',
              fontSize: 16.0,
              fontWeight: FontWeight.w500,
              color: Color.fromRGBO(27, 39, 45, 0.75))),
      Text(Strings.formation05,
          style: TextStyle(
              fontFamily: 'RoundedMplus',
              fontSize: 14.0,
              color: Color.fromRGBO(27, 39, 45, 0.75))),
      Divider(),

      //start, distance, chrono
      Text(Strings.startDistTiming + "\n",
          style: TextStyle(
              fontFamily: 'RoundedMplusMedium',
              fontSize: 16.0,
              fontWeight: FontWeight.w500,
              color: Color.fromRGBO(63, 113, 226, 1))),
      Text(Strings.startDistTiming01,
          style: TextStyle(
              fontFamily: 'RoundedMplusMedium',
              fontSize: 16.0,
              fontWeight: FontWeight.w500,
              color: Color.fromRGBO(27, 39, 45, 0.75))),
      Text(Strings.startDistTiming02,
          style: TextStyle(
              fontFamily: 'RoundedMplus',
              fontSize: 14.0,
              color: Color.fromRGBO(27, 39, 45, 0.75))),
      Text(Strings.startDistTiming03,
          style: TextStyle(
              fontFamily: 'RoundedMplusMedium',
              fontSize: 16.0,
              fontWeight: FontWeight.w500,
              color: Color.fromRGBO(27, 39, 45, 0.75))),
      Text(Strings.startDistTiming04,
          style: TextStyle(
              fontFamily: 'RoundedMplus',
              fontSize: 14.0,
              color: Color.fromRGBO(27, 39, 45, 0.75))),
      Text(Strings.startDistTiming05,
          style: TextStyle(
              fontFamily: 'RoundedMplusMedium',
              fontSize: 16.0,
              fontWeight: FontWeight.w500,
              color: Color.fromRGBO(27, 39, 45, 0.75))),
      Text(Strings.startDistTiming06,
          style: TextStyle(
              fontFamily: 'RoundedMplus',
              fontSize: 14.0,
              color: Color.fromRGBO(27, 39, 45, 0.75))),
      Divider(),

      //styles
      Text(Strings.styles + "\n",
          style: TextStyle(
              fontFamily: 'RoundedMplusMedium',
              fontSize: 16.0,
              fontWeight: FontWeight.w500,
              color: Color.fromRGBO(63, 113, 226, 1))),
      Text(Strings.styles01,
          style: TextStyle(
              fontFamily: 'RoundedMplusMedium',
              fontSize: 16.0,
              fontWeight: FontWeight.w500,
              color: Color.fromRGBO(27, 39, 45, 0.75))),
      Text(Strings.styles02,
          style: TextStyle(
              fontFamily: 'RoundedMplus',
              fontSize: 14.0,
              color: Color.fromRGBO(27, 39, 45, 0.75))),
      Text(Strings.styles03,
          style: TextStyle(
              fontFamily: 'RoundedMplusMedium',
              fontSize: 16.0,
              fontWeight: FontWeight.w500,
              color: Color.fromRGBO(27, 39, 45, 0.75))),
      Text(Strings.styles04,
          style: TextStyle(
              fontFamily: 'RoundedMplus',
              fontSize: 14.0,
              color: Color.fromRGBO(27, 39, 45, 0.75))),
      Text(Strings.styles05,
          style: TextStyle(
              fontFamily: 'RoundedMplusMedium',
              fontSize: 16.0,
              fontWeight: FontWeight.w500,
              color: Color.fromRGBO(27, 39, 45, 0.75))),
      Text(Strings.styles06,
          style: TextStyle(
              fontFamily: 'RoundedMplus',
              fontSize: 14.0,
              color: Color.fromRGBO(27, 39, 45, 0.75))),
      Text(Strings.styles07,
          style: TextStyle(
              fontFamily: 'RoundedMplusMedium',
              fontSize: 16.0,
              fontWeight: FontWeight.w500,
              color: Color.fromRGBO(27, 39, 45, 0.75))),
      Text(Strings.styles08,
          style: TextStyle(
              fontFamily: 'RoundedMplus',
              fontSize: 14.0,
              color: Color.fromRGBO(27, 39, 45, 0.75))),
      Text(Strings.styles09,
          style: TextStyle(
              fontFamily: 'RoundedMplusMedium',
              fontSize: 16.0,
              fontWeight: FontWeight.w500,
              color: Color.fromRGBO(27, 39, 45, 0.75))),
      Text(Strings.styles10,
          style: TextStyle(
              fontFamily: 'RoundedMplus',
              fontSize: 14.0,
              color: Color.fromRGBO(27, 39, 45, 0.75))),
      Divider(),

      //requirements
      Text(Strings.requirements + "\n",
          style: TextStyle(
              fontFamily: 'RoundedMplusMedium',
              fontSize: 16.0,
              fontWeight: FontWeight.w500,
              color: Color.fromRGBO(63, 113, 226, 1))),
      Text(Strings.requirements01,
          style: TextStyle(
              fontFamily: 'RoundedMplusMedium',
              fontSize: 16.0,
              fontWeight: FontWeight.w500,
              color: Color.fromRGBO(27, 39, 45, 0.75))),
      Text(Strings.requirements02,
          style: TextStyle(
              fontFamily: 'RoundedMplus',
              fontSize: 14.0,
              color: Color.fromRGBO(27, 39, 45, 0.75))),
      Text(Strings.requirements03,
          style: TextStyle(
              fontFamily: 'RoundedMplusMedium',
              fontSize: 16.0,
              fontWeight: FontWeight.w500,
              color: Color.fromRGBO(27, 39, 45, 0.75))),
      Text(Strings.requirements04,
          style: TextStyle(
              fontFamily: 'RoundedMplus',
              fontSize: 14.0,
              color: Color.fromRGBO(27, 39, 45, 0.75))),
      Text(Strings.requirements05,
          style: TextStyle(
              fontFamily: 'RoundedMplusMedium',
              fontSize: 16.0,
              fontWeight: FontWeight.w500,
              color: Color.fromRGBO(27, 39, 45, 0.75))),
      Text(Strings.requirements06,
          style: TextStyle(
              fontFamily: 'RoundedMplus',
              fontSize: 14.0,
              color: Color.fromRGBO(27, 39, 45, 0.75))),
      Divider(),

      //openwater
      Text(Strings.openWater + "\n",
          style: TextStyle(
              fontFamily: 'RoundedMplusMedium',
              fontSize: 16.0,
              fontWeight: FontWeight.w500,
              color: Color.fromRGBO(63, 113, 226, 1))),
      Text(Strings.openWater01,
          style: TextStyle(
              fontFamily: 'RoundedMplusMedium',
              fontSize: 16.0,
              fontWeight: FontWeight.w500,
              color: Color.fromRGBO(27, 39, 45, 0.75))),
      Text(Strings.openWater02,
          style: TextStyle(
              fontFamily: 'RoundedMplus',
              fontSize: 14.0,
              color: Color.fromRGBO(27, 39, 45, 0.75))),
      Text(Strings.openWater03,
          style: TextStyle(
              fontFamily: 'RoundedMplusMedium',
              fontSize: 16.0,
              fontWeight: FontWeight.w500,
              color: Color.fromRGBO(27, 39, 45, 0.75))),
      Text(Strings.openWater04,
          style: TextStyle(
              fontFamily: 'RoundedMplus',
              fontSize: 14.0,
              color: Color.fromRGBO(27, 39, 45, 0.75))),
      Text(Strings.openWater05,
          style: TextStyle(
              fontFamily: 'RoundedMplusMedium',
              fontSize: 16.0,
              fontWeight: FontWeight.w500,
              color: Color.fromRGBO(27, 39, 45, 0.75))),
      Text(Strings.openWater06,
          style: TextStyle(
              fontFamily: 'RoundedMplus',
              fontSize: 14.0,
              color: Color.fromRGBO(27, 39, 45, 0.75))),
      Text(Strings.openWater07,
          style: TextStyle(
              fontFamily: 'RoundedMplusMedium',
              fontSize: 16.0,
              fontWeight: FontWeight.w500,
              color: Color.fromRGBO(27, 39, 45, 0.75))),
      Text(Strings.openWater08,
          style: TextStyle(
              fontFamily: 'RoundedMplus',
              fontSize: 14.0,
              color: Color.fromRGBO(27, 39, 45, 0.75))),
      Text(Strings.openWater09,
          style: TextStyle(
              fontFamily: 'RoundedMplusMedium',
              fontSize: 16.0,
              fontWeight: FontWeight.w500,
              color: Color.fromRGBO(27, 39, 45, 0.75))),
      Text(Strings.openWater10,
          style: TextStyle(
              fontFamily: 'RoundedMplus',
              fontSize: 14.0,
              color: Color.fromRGBO(27, 39, 45, 0.75))),
      FlatButton(
        onPressed: () {
          jumpTo(1);
        },
        child: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            SvgPicture.asset('images/scroll_up.svg'),
            Text(Strings.scrollUp,
                style: TextStyle(
                    fontFamily: 'RoundedMplusMedium',
                    fontSize: 14.0,
                    fontWeight: FontWeight.w500,
                    color: Color.fromRGBO(63, 113, 226, 1))),
          ],
        ),
      ),

      SizedBox(
        height: 1000,
        child: FlatButton(
          onPressed: () {
            jumpTo(1);
          },
          child: Icon(
            Icons.arrow_upward,
            color: Color.fromRGBO(63, 113, 226, 1),
          ),
        ),
      ),
      Divider(),
    ];

    return WillPopScope(
      onWillPop: () {
        AppNavigation.toMainPage(context);
      },
      child: Scaffold(
        backgroundColor: Colors.white,
        drawer: NavigationDrawer(),
        appBar: AppBar(
          backgroundColor: Colors.white,
          elevation: 0.0,
          centerTitle: true,
          title: Text(Strings.rulesTitle,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontWeight: FontWeight.w500,
                  fontSize: 16,
                  color: Color.fromRGBO(27, 39, 45, 0.75))),
          leading: Builder(
              builder: (context) => FlatButton(
                  onPressed: () {
                    Scaffold.of(context).openDrawer();
                  },
                  child: SvgPicture.asset('images/hamburger.svg'))),
        ),
        body: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Expanded(
              child: NotificationListener<ScrollUpdateNotification>(
                onNotification: (notification) {
                  getVisible();
                  return true;
                },
                child: RectGetter(
                    key: listViewKey,
                    child: DraggableScrollbar.rrect(
                        child: ListView.builder(
                          padding: EdgeInsets.only(left: 24, right: 24),
                          controller: _ctl,
                          itemCount: listRulesScreen.length,
                          itemBuilder: (BuildContext context, int index) {
                            print('build : $index');
                            _keys[index] = RectGetter.createGlobalKey();
                            return RectGetter(
                              key: _keys[index],
                              child: listRulesScreen[index],
                            );
                          },
                        ),
                        controller: _ctl)),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
