import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:swim_teacher/presenter/custom_route.dart';
import 'package:swim_teacher/screens/main_screen/main_drawer.dart';

class SplashScreen extends StatefulWidget {
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  double _height = 50;
  double _width = 50;
  var colorTop = Color.fromRGBO(63, 113, 226, 0.001);
  var colorBotton = Color.fromRGBO(127, 233, 242, 0.001);

  bool _first = true;

  startTime() async {
    var _duration = Duration(seconds: 5);
    return Timer(_duration, navigationPage);
  }

  void navigationPage() {
    Navigator.push(context, CustomRoute(builder: (context) => MainDrawer()));
  }

  @override
  void initState() {
    super.initState();
    startTime();
  }

  @override
  Widget build(BuildContext context) {
    Timer(Duration(milliseconds: 1), () {
      setState(() {
        _first = false;
      });
    });

    if (!_first) {
      setState(() {
        _width = 250;
        _height = 250;
        colorTop = Color.fromRGBO(127, 233, 242, 1);
        colorBotton = Color.fromRGBO(63, 113, 226, 1);
      });
    }
    return Scaffold(
      body: AnimatedContainer(
        duration: Duration(milliseconds: 4900),
        decoration: BoxDecoration(
          gradient: LinearGradient(
            colors: [
              colorTop, //Color.fromRGBO(127, 233, 242, 1),
              colorBotton //Color.fromRGBO(63, 113, 226, 1)
            ],
            begin: Alignment(0.0, -0.8),
            end: Alignment.bottomCenter,
          ),
        ),
        child: Center(
            child: AnimatedContainer(
          curve: Curves.easeInCirc,
          duration: Duration(milliseconds: 2500),
          width: _width,
          height: _height,
          child: SvgPicture.asset(
            'images/logo_white.svg',
            height: _height,
          ),
        )),
      ),
    );
  }
}
