import 'package:flutter/material.dart';
import 'package:swim_teacher/model/standarts/first_sc_data.dart';
import 'package:swim_teacher/model/strings.dart';
import 'package:swim_teacher/presenter/custom_route.dart';
import 'package:swim_teacher/screens/standarts_screen/standarts.dart';

class FirstSc extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          backgroundColor: Colors.white,
          elevation: 0.0,
          centerTitle: true,
          title: Hero(
            tag: "firstScHero",
            child: Material(
              color: Colors.transparent,
              child: Text(Strings.firstSc,
                  style: TextStyle(
                      fontFamily: 'RoundedMplusMedium',
                      fontSize: 16.0,
                      fontWeight: FontWeight.w500,
                      color: Color.fromRGBO(27, 39, 45, 0.75))),
            ),
          ),
          leading: Builder(
              builder: (context) => FlatButton(
                  onPressed: () {
                    Navigator.push(context,
                        CustomRoute(builder: (context) => Standarts()));
                  },
                  child: Icon(
                    Icons.arrow_back,
                    color: Color.fromRGBO(63, 113, 226, 1),
                  ))),
        ),
        body: Padding(
          padding: EdgeInsets.only(left: 24, right: 24),
          child: ListView.separated(
            itemCount: FirstScData.firstScList.length,
            separatorBuilder: (BuildContext context, int index) => Divider(),
            itemBuilder: (BuildContext context, int index) =>
                FirstScData.firstScList[index],
          ),
        ));
  }
}
