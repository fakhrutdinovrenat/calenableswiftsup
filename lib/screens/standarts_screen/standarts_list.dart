import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:swim_teacher/model/strings.dart';
import 'package:swim_teacher/presenter/custom_route.dart';
import 'package:url_launcher/url_launcher.dart';

import 'package:swim_teacher/screens/standarts_screen/standart_tables/first_sc.dart';
import 'package:swim_teacher/screens/standarts_screen/standart_tables/first_ysc.dart';
import 'package:swim_teacher/screens/standarts_screen/standart_tables/second_sc.dart';
import 'package:swim_teacher/screens/standarts_screen/standart_tables/second_ysc.dart';
import 'package:swim_teacher/screens/standarts_screen/standart_tables/standarts_cms.dart';
import 'package:swim_teacher/screens/standarts_screen/standart_tables/standarts_ms.dart';
import 'package:swim_teacher/screens/standarts_screen/standart_tables/standarts_msmc.dart';
import 'package:swim_teacher/screens/standarts_screen/standart_tables/third_sc.dart';
import 'package:swim_teacher/screens/standarts_screen/standart_tables/third_ysc.dart';

class StandartList extends StatefulWidget {
  @override
  State createState() => StandartListState();
}

class StandartListState extends State<StandartList> {
  _launchURL() async {
    const url = 'http://minsport.gov.ru/2017/doc/Plavanie131117_evsk2021.xlsx';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: EdgeInsets.all(24),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Column(
              children: <Widget>[
                FlatButton(
                  onPressed: () {
                    _launchURL();
                  },
                  child: Column(
                    children: <Widget>[
                      Text(
                        Strings.sPage,
                        style: TextStyle(
                            fontFamily: 'RoundedMplusMedium',
                            fontSize: 14.0,
                            fontWeight: FontWeight.w500,
                            color: Color.fromRGBO(63, 113, 226, 1)),
                        textAlign: TextAlign.center,
                      ),
                      Row(
                        mainAxisSize: MainAxisSize.min,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          SvgPicture.asset('images/save.svg'),
                          Padding(
                              padding: EdgeInsets.only(left: 8),
                              child: Text(Strings.download,
                                  style: TextStyle(
                                      fontFamily: 'RoundedMplus',
                                      fontSize: 14.0,
                                      fontWeight: FontWeight.w500,
                                      color: Color.fromRGBO(63, 113, 226, 1)))),
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
            Text(Strings.msmcHeadOfTable,
                style: TextStyle(
                    fontFamily: 'RoundedMplusMedium',
                    fontSize: 12.0,
                    color: Color.fromRGBO(27, 39, 45, 0.75))),
            Column(
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Expanded(
                        child: Padding(
                            padding:
                                EdgeInsets.only(right: 4, top: 4, bottom: 4),
                            child: Container(
                              decoration: BoxDecoration(
                                  color: Color.fromRGBO(148, 202, 245, 0.3),
                                  borderRadius: BorderRadius.circular(22.0)),
                              child: FlatButton(
                                shape: StadiumBorder(),
                                onPressed: () {
                                  Navigator.push(
                                      context,
                                      CustomRoute(
                                          builder: (context) =>
                                              StandartsMsmc()));
                                },
                                child: Hero(
                                  tag: 'msmcHero',
                                  child: Material(
                                    color: Colors.transparent,
                                    child: Text(Strings.msmc,
                                        style: TextStyle(
                                            fontFamily: 'RoundedMplusMedium',
                                            fontSize: 14.0,
                                            fontWeight: FontWeight.w500,
                                            color: Color.fromRGBO(
                                                27, 39, 45, 0.75))),
                                  ),
                                ),
                              ),
                            ))),
                    Expanded(
                        child: Padding(
                            padding:
                                EdgeInsets.only(left: 4, top: 4, bottom: 4),
                            child: Container(
                              decoration: BoxDecoration(
                                  color: Color.fromRGBO(148, 202, 245, 0.3),
                                  borderRadius: BorderRadius.circular(22.0)),
                              child: FlatButton(
                                shape: StadiumBorder(),
                                onPressed: () {
                                  Navigator.push(
                                      context,
                                      CustomRoute(
                                          builder: (context) => StandartsMs()));
                                },
                                child: Hero(
                                  tag: 'msHero',
                                  child: Material(
                                      color: Colors.transparent,
                                      child: Text(Strings.standartsMs,
                                          style: TextStyle(
                                              fontFamily: 'RoundedMplusMedium',
                                              fontSize: 14.0,
                                              fontWeight: FontWeight.w500,
                                              color: Color.fromRGBO(
                                                  27, 39, 45, 0.75)))),
                                ),
                              ),
                            ))),
                  ],
                ),
                Row(
                  children: <Widget>[
                    Expanded(
                        child: Padding(
                            padding:
                                EdgeInsets.only(right: 4, top: 4, bottom: 4),
                            child: Container(
                              decoration: BoxDecoration(
                                  color: Color.fromRGBO(148, 202, 245, 0.3),
                                  borderRadius: BorderRadius.circular(22.0)),
                              child: FlatButton(
                                shape: StadiumBorder(),
                                onPressed: () {
                                  Navigator.push(
                                      context,
                                      CustomRoute(
                                          builder: (context) =>
                                              StandartsCms()));
                                },
                                child: Hero(
                                  tag: "cmsHero",
                                  child: Material(
                                    color: Colors.transparent,
                                    child: Text(Strings.standartsCms,
                                        style: TextStyle(
                                            fontFamily: 'RoundedMplusMedium',
                                            fontSize: 14.0,
                                            fontWeight: FontWeight.w500,
                                            color: Color.fromRGBO(
                                                27, 39, 45, 0.75))),
                                  ),
                                ),
                              ),
                            ))),
                    Expanded(
                        child: Padding(
                            padding:
                                EdgeInsets.only(left: 4, top: 4, bottom: 4),
                            child: Container(
                              decoration: BoxDecoration(
                                  color: Color.fromRGBO(148, 202, 245, 0.3),
                                  borderRadius: BorderRadius.circular(22.0)),
                              child: FlatButton(
                                shape: StadiumBorder(),
                                onPressed: () {
                                  Navigator.push(
                                      context,
                                      CustomRoute(
                                          builder: (context) => FirstSc()));
                                },
                                child: Hero(
                                  tag: "firstScHero",
                                  child: Material(
                                    color: Colors.transparent,
                                    child: Text(
                                      Strings.firstSc,
                                      style: TextStyle(
                                          fontFamily: 'RoundedMplusMedium',
                                          fontSize: 14.0,
                                          fontWeight: FontWeight.w500,
                                          color:
                                              Color.fromRGBO(27, 39, 45, 0.75)),
                                      textAlign: TextAlign.center,
                                    ),
                                  ),
                                ),
                              ),
                            ))),
                  ],
                ),
                Row(
                  children: <Widget>[
                    Expanded(
                        child: Padding(
                            padding:
                                EdgeInsets.only(right: 4, top: 4, bottom: 4),
                            child: Container(
                              decoration: BoxDecoration(
                                  color: Color.fromRGBO(148, 202, 245, 0.3),
                                  borderRadius: BorderRadius.circular(22.0)),
                              child: FlatButton(
                                shape: StadiumBorder(),
                                onPressed: () {
                                  Navigator.push(
                                      context,
                                      CustomRoute(
                                          builder: (context) => SecondSc()));
                                },
                                child: Hero(
                                  tag: "secondScHero",
                                  child: Material(
                                    color: Colors.transparent,
                                    child: Text(
                                      Strings.secondSc,
                                      style: TextStyle(
                                          fontFamily: 'RoundedMplusMedium',
                                          fontSize: 14.0,
                                          fontWeight: FontWeight.w500,
                                          color:
                                              Color.fromRGBO(27, 39, 45, 0.75)),
                                      textAlign: TextAlign.center,
                                    ),
                                  ),
                                ),
                              ),
                            ))),
                    Expanded(
                        child: Padding(
                            padding:
                                EdgeInsets.only(left: 4, top: 4, bottom: 4),
                            child: Container(
                              decoration: BoxDecoration(
                                  color: Color.fromRGBO(148, 202, 245, 0.3),
                                  borderRadius: BorderRadius.circular(22.0)),
                              child: FlatButton(
                                shape: StadiumBorder(),
                                onPressed: () {
                                  Navigator.push(
                                      context,
                                      CustomRoute(
                                          builder: (context) => ThirdSc()));
                                },
                                child: Hero(
                                  tag: "thirdScHero",
                                  child: Material(
                                    color: Colors.transparent,
                                    child: Text(
                                      Strings.thirdSc,
                                      style: TextStyle(
                                          fontFamily: 'RoundedMplusMedium',
                                          fontSize: 14.0,
                                          fontWeight: FontWeight.w500,
                                          color:
                                              Color.fromRGBO(27, 39, 45, 0.75)),
                                      textAlign: TextAlign.center,
                                    ),
                                  ),
                                ),
                              ),
                            ))),
                  ],
                ),
                Row(
                  children: <Widget>[
                    Expanded(
                        child: Padding(
                            padding: EdgeInsets.only(top: 4, bottom: 4),
                            child: Container(
                              decoration: BoxDecoration(
                                  color: Color.fromRGBO(148, 202, 245, 0.3),
                                  borderRadius: BorderRadius.circular(22.0)),
                              child: FlatButton(
                                shape: StadiumBorder(),
                                onPressed: () {
                                  Navigator.push(
                                      context,
                                      CustomRoute(
                                          builder: (context) => FirstYsc()));
                                },
                                child: Hero(
                                  tag: "firstYscHero",
                                  child: Material(
                                    color: Colors.transparent,
                                    child: Text(
                                      Strings.firstYsc,
                                      style: TextStyle(
                                          fontFamily: 'RoundedMplusMedium',
                                          fontSize: 14.0,
                                          fontWeight: FontWeight.w500,
                                          color:
                                              Color.fromRGBO(27, 39, 45, 0.75)),
                                      textAlign: TextAlign.center,
                                    ),
                                  ),
                                ),
                              ),
                            )))
                  ],
                ),
                Row(
                  children: <Widget>[
                    Expanded(
                        child: Padding(
                            padding: EdgeInsets.only(top: 4, bottom: 4),
                            child: Container(
                              decoration: BoxDecoration(
                                  color: Color.fromRGBO(148, 202, 245, 0.3),
                                  borderRadius: BorderRadius.circular(22.0)),
                              child: FlatButton(
                                shape: StadiumBorder(),
                                onPressed: () {
                                  Navigator.push(
                                      context,
                                      CustomRoute(
                                          builder: (context) => SecondYsc()));
                                },
                                child: Hero(
                                  tag: "secondYscHero",
                                  child: Material(
                                    color: Colors.transparent,
                                    child: Text(
                                      Strings.secondYsc,
                                      style: TextStyle(
                                          fontFamily: 'RoundedMplusMedium',
                                          fontSize: 14.0,
                                          fontWeight: FontWeight.w500,
                                          color:
                                              Color.fromRGBO(27, 39, 45, 0.75)),
                                      textAlign: TextAlign.center,
                                    ),
                                  ),
                                ),
                              ),
                            )))
                  ],
                ),
                Row(
                  children: <Widget>[
                    Expanded(
                        child: Padding(
                            padding: EdgeInsets.only(top: 4),
                            child: Container(
                              decoration: BoxDecoration(
                                  color: Color.fromRGBO(148, 202, 245, 0.3),
                                  borderRadius: BorderRadius.circular(22.0)),
                              child: FlatButton(
                                shape: StadiumBorder(),
                                onPressed: () {
                                  Navigator.push(
                                      context,
                                      CustomRoute(
                                          builder: (context) => ThirdYsc()));
                                },
                                child: Hero(
                                  tag: "thirdYscHero",
                                  child: Material(
                                      color: Colors.transparent,
                                      child: Text(
                                        Strings.thirdYsc,
                                        style: TextStyle(
                                            fontFamily: 'RoundedMplusMedium',
                                            fontSize: 14.0,
                                            fontWeight: FontWeight.w500,
                                            color: Color.fromRGBO(
                                                27, 39, 45, 0.75)),
                                        textAlign: TextAlign.center,
                                      )),
                                ),
                              ),
                            )))
                  ],
                ),
              ],
            ),
          ],
        ));
  }
}
