import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import 'package:swim_teacher/model/strings.dart';
import 'package:swim_teacher/presenter/stopwatch/stopwatch_dependencies.dart';
import 'package:swim_teacher/presenter/toastview.dart';
import 'package:swim_teacher/screens/timer_screen/screen/stopwatch_screen_portrait.dart';

class SaveResultDialog extends StatefulWidget {
  final StopwatchDependencies dependencies;

  SaveResultDialog({
    Key key,
    this.dependencies,
  }); //: super(key: key);

  @override
  SaveResultDialogState createState() => new SaveResultDialogState();
}

class SaveResultDialogState extends State<SaveResultDialog> {
  FocusNode _focusNode;
  static TextEditingController controller = TextEditingController();
  static bool validate;

  bool _changeSex;
  bool _changePool;
  static String sex; //= Strings.manResults;
  static String poolSize; // = Strings.saveDialog11;
  static double age;
  static String day = DateFormat.d().format(DateTime.now());
  static String month =
      DateFormat.MMM("ky").format(DateTime.now()).toLowerCase();

  static String style;
  var styles = <String>[
    Strings.freestyle,
    Strings.onBack,
    Strings.breaststroke,
    Strings.butterfly,
    Strings.complexSwim,
    Strings.relayFreestyle,
    Strings.relayCombined
  ];

  static String dist;
  var distances = <String>[
    Strings.distances01,
    Strings.distances02,
    Strings.distances03,
    Strings.distances04,
    Strings.distances05,
    Strings.distances06,
    Strings.distances07,
    Strings.distances08,
    Strings.distances09
  ];

  void _setValue(double value) => setState(() => age = value);

  @override
  void initState() {
    super.initState();
    setState(() {
      _changePool = true;
      _changeSex = true;
      validate = false;
      sex = Strings.manResults;
      poolSize = Strings.saveDialog11;
      controller.text = "";
      age = 50.0;
      style = Strings.freestyle;
      dist = Strings.distances01;
    });

    _focusNode = FocusNode();

    _focusNode.addListener(
        () => print('focusNode updated: hasFocus: ${_focusNode.hasFocus}'));
  }

  _setSelectedRadio(bool val) {
    setState(() {
      _changeSex = val;
      _changeSex ? sex = Strings.manResults : sex = Strings.womenResults;
    });
  }

  _setSelectedRadioPool(bool val) {
    setState(() {
      _changePool = val;
      _changePool
          ? poolSize = Strings.saveDialog11
          : poolSize = Strings.saveDialog12;
    });
  }

  @override
  Widget build(BuildContext context) {
    //FocusScope.of(context).requestFocus(_focusNode);
    return Padding(
        padding: EdgeInsets.symmetric(horizontal: 16),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            DropdownButton<String>(
              isExpanded: true,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 14.0,
                  color: Color.fromRGBO(27, 39, 45, 0.56)),
              value: dist,
              items: distances.map((String value) {
                return DropdownMenuItem<String>(
                  value: value,
                  child: Text(value),
                );
              }).toList(),
              onChanged: (value) {
                setState(() {
                  dist = value;
                });
              },
            ),
            Padding(
              padding: EdgeInsets.only(top: 12),
              child: Text(
                TimerScreenState.stopwatchIndicator,
                style: TextStyle(
                    fontFamily: 'RoundedMplus',
                    fontSize: 14.0,
                    color: Color.fromRGBO(27, 39, 45, 0.56)),
              ),
            ),
            Divider(color: Color.fromRGBO(27, 39, 45, 0.30)),
            DropdownButton<String>(
              isExpanded: true,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 14.0,
                  color: Color.fromRGBO(27, 39, 45, 0.56)),
              value: style,
              items: styles.map((String value) {
                return DropdownMenuItem<String>(
                  value: value,
                  child: Text(value),
                );
              }).toList(),
              onChanged: (value) {
                setState(() {
                  style = value;
                  Toast.show(style, context);
                });
              },
            ),
            Row(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  Strings.saveDialog08,
                  style: TextStyle(
                      fontFamily: 'RoundedMplus',
                      fontSize: 14.0,
                      color: Color.fromRGBO(27, 39, 45, 0.75)),
                ),
                Row(
                  children: <Widget>[
                    Radio(
                      value: true,
                      groupValue: _changePool,
                      onChanged: (val) {
                        _setSelectedRadioPool(val);
                      },
                    ),
                    Text(
                      Strings.saveDialog09,
                      style: TextStyle(
                          fontFamily: 'RoundedMplus',
                          fontSize: 14.0,
                          color: Color.fromRGBO(27, 39, 45, 0.75)),
                    ),
                  ],
                ),
                Row(
                  children: <Widget>[
                    Radio(
                      value: false,
                      groupValue: _changePool,
                      onChanged: (val) {
                        _setSelectedRadioPool(val);
                      },
                    ),
                    Text(
                      Strings.saveDialog10,
                      style: TextStyle(
                          fontFamily: 'RoundedMplus',
                          fontSize: 14.0,
                          color: Color.fromRGBO(27, 39, 45, 0.75)),
                    ),
                  ],
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  Strings.resultCard01,
                  style: TextStyle(
                      fontFamily: 'RoundedMplus',
                      fontSize: 14.0,
                      color: Color.fromRGBO(27, 39, 45, 0.75)),
                ),
                Row(
                  children: <Widget>[
                    Radio(
                      value: true,
                      groupValue: _changeSex,
                      onChanged: (val) {
                        _setSelectedRadio(val);
                      },
                    ),
                    Text(
                      Strings.manResults,
                      style: TextStyle(
                          fontFamily: 'RoundedMplus',
                          fontSize: 14.0,
                          color: Color.fromRGBO(27, 39, 45, 0.75)),
                    ),
                  ],
                ),
                Row(
                  children: <Widget>[
                    Radio(
                      value: false,
                      groupValue: _changeSex,
                      onChanged: (val) {
                        _setSelectedRadio(val);
                      },
                    ),
                    Text(
                      Strings.womenResults,
                      style: TextStyle(
                          fontFamily: 'RoundedMplus',
                          fontSize: 14.0,
                          color: Color.fromRGBO(27, 39, 45, 0.75)),
                    ),
                  ],
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  Strings.resultCard02,
                  style: TextStyle(
                      fontFamily: 'RoundedMplus',
                      fontSize: 14.0,
                      color: Color.fromRGBO(27, 39, 45, 0.75)),
                ),
                Text(
                  '${age.toInt()}',
                  style: TextStyle(
                      fontFamily: 'RoundedMplus',
                      fontSize: 14.0,
                      color: Color.fromRGBO(27, 39, 45, 0.75)),
                ),
              ],
            ),
            Slider(
                min: 0.0,
                max: 100.0,
                divisions: 100,
                value: age,
                onChanged: _setValue),
            TextField(
              controller: controller,
              focusNode: _focusNode,
              maxLength: 64,
              decoration: InputDecoration(
                hintText: Strings.hintName,
                counterText: "",
                hintStyle: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 14.0,
                ),
              ),
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 14.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75)),
            ),
            /**
            Padding(
              padding: EdgeInsets.only(top: 12),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    Strings.saveDialog13,
                    style: TextStyle(
                        fontFamily: 'RoundedMplus',
                        fontSize: 14.0,
                        color: Color.fromRGBO(27, 39, 45, 0.75)),
                  ),
                  IconButton(
                    icon: Icon(
                      Icons.event,
                      color: Color.fromRGBO(63, 113, 226, 1),
                    ), //SvgPicture.asset('images/date.svg'),
                    onPressed: () {
                      DatePicker.showDatePicker(context,
                          showTitleActions: true,
                          minTime: DateTime(2018, 1, 1),
                          maxTime: DateTime.now(), //DateTime(2049, 6, 7),
                          theme: DatePickerTheme(
                              backgroundColor: Color.fromRGBO(63, 113, 226, 1),
                              itemStyle: TextStyle(
                                  fontFamily: 'RoundedMplus',
                                  color: Colors.white),
                              doneStyle: TextStyle(
                                  fontFamily: 'RoundedMplus',
                                  color: Colors.white,
                                  fontSize: 16)), onChanged: (date) {
                        print('change $date');
                      }, onConfirm: (date) {
                        print('confirm $date');
                        initializeDateFormatting("ky", null);
                        day = DateFormat.d().format(date);
                        month = DateFormat.MMM("ky").format(date).toLowerCase();
                      }, currentTime: DateTime.now(), locale: LocaleType.ru);
                    },
                  )
                ],
              ),
            ),
            */
            //Divider(color: Color.fromRGBO(27, 39, 45, 0.30))
          ],
        ));
  }
}