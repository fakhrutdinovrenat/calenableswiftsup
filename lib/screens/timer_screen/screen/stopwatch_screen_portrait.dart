import 'dart:async';
import 'package:intl/date_symbol_data_local.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:swim_teacher/model/auth_status.dart';
import 'package:swim_teacher/model/results_model.dart';
import 'package:swim_teacher/model/strings.dart';
import 'package:swim_teacher/presenter/authentication.dart';
import 'package:swim_teacher/presenter/custom_route.dart';
import 'package:swim_teacher/presenter/stopwatch/stopwatch_dependencies.dart';
import 'package:swim_teacher/presenter/toastview.dart';
import 'package:swim_teacher/screens/not_signed_in_screen/not_signed_in.dart';
import 'package:swim_teacher/screens/results_screen/results.dart';
import 'package:swim_teacher/screens/timer_screen/screen/save_result_dialog.dart';
import 'package:swim_teacher/screens/timer_screen/screen/timer_clock.dart';

class TimerScreen extends StatefulWidget {
  final StopwatchDependencies dependencies;
  final BaseAuth auth;

  TimerScreen({Key key, this.dependencies, this.auth}) : super(key: key);

  TimerScreenState createState() => TimerScreenState();
}

class TimerScreenState extends State<TimerScreen> {
  AuthStatus authStatus = AuthStatus.notSignedIn;

  ResultsModel item;
  DatabaseReference _reference;

  Icon buttonIcon;

  bool _pushToStart = true;

  Timer timer;

  static String stopwatchIndicator;

  updateTime(Timer timer) {
    if (widget.dependencies.stopwatch.isRunning) {
      setState(() {});
    } else {
      timer.cancel();
    }
  }

  void _getReference() async {
    try {
      String _userUid = await widget.auth.getUid();
      final FirebaseDatabase database = FirebaseDatabase.instance;
      _reference = database.reference().child(_userUid).child(Strings.resultsReference);
    } catch (e) {
      print(e);
    }
  }

  @override
  void initState() {
    if (widget.dependencies.stopwatch.isRunning) {
      timer = new Timer.periodic(new Duration(milliseconds: 20), updateTime);
      buttonIcon = Icon(Icons.pause);
    } else {
      buttonIcon = Icon(Icons.play_arrow);
    }
    super.initState();
    item = ResultsModel("", "", "", "", "", "", "", "", "", "");
    _getReference();

    widget.auth.currentName().then((userId) {
      setState(() {
        authStatus =
            userId != null ? AuthStatus.signedIn : AuthStatus.notSignedIn;
      });
    });
  }

  void handleSubmit() {
    item.swimSex = SaveResultDialogState.sex;
    item.swimAge = SaveResultDialogState.age.toInt().toString();
    item.swimStyle = SaveResultDialogState.style +
        '\n(' +
        SaveResultDialogState.poolSize +
        ')';
    item.swimDist = SaveResultDialogState.dist;
    item.swimTime = widget.dependencies.transformMilliSecondsToString(
        widget.dependencies.stopwatch.elapsedMilliseconds);
    item.swimName = SaveResultDialogState.controller.text;
    DateTime date = DateTime.now();
    initializeDateFormatting("ky", null);
    item.swimDate = date.toString();
    item.swimDay = SaveResultDialogState.day; //DateFormat.d().format(date);
    item.swimMonth =
        SaveResultDialogState.month; //DateFormat.MMM("ky").format(date);

    item.key = _reference.push().key;

    _reference.child(item.key).set(item.toJson()).then((_) {
      Toast.show(Strings.resultSaved, context);
    });
  }

  @override
  void dispose() {
    if (timer.isActive) {
      timer.cancel();
      timer = null;
    }
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              height: 250.0,
              width: 250.0,
              child: TimerClock(widget.dependencies),
            ),
            Padding(
              padding: EdgeInsets.only(left: 32.0, right: 32.0, bottom: 8),
              child: Row(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Container(
                    child: FloatingActionButton(
                      heroTag: "btnLap",
                      child: ClipOval(
                          child: Container(
                              decoration: BoxDecoration(
                                  gradient: LinearGradient(
                                    colors: [
                                      Color.fromRGBO(86, 155, 232, 1),
                                      Color.fromRGBO(64, 114, 226, 1)
                                    ],
                                    begin: Alignment.bottomRight,
                                    end: Alignment.topLeft,
                                  ),
                                  borderRadius: BorderRadius.circular(22.0)),
                              child: Center(child: Text(Strings.timerLap)))),
                      elevation: 0.0,
                      backgroundColor: Colors.transparent,
                      onPressed: () {
                        _addLap();
                      },
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 16),
                    child: FloatingActionButton(
                      heroTag: "btnStartStop",
                      child: ClipOval(
                          child: Container(
                              decoration: BoxDecoration(
                                  gradient: LinearGradient(
                                    colors: [
                                      Color.fromRGBO(86, 155, 232, 1),
                                      Color.fromRGBO(64, 114, 226, 1)
                                    ],
                                    begin: Alignment.bottomRight,
                                    end: Alignment.topLeft,
                                  ),
                                  borderRadius: BorderRadius.circular(22.0)),
                              child: Center(child: buttonIcon))),
                      elevation: 0.0,
                      backgroundColor: Colors.transparent,
                      onPressed: () {
                        _startOrStopWatch();
                      },
                    ),
                  ),
                  Container(
                    child: FloatingActionButton(
                      heroTag: "btnRefresh",
                      child: ClipOval(
                          child: Container(
                              decoration: BoxDecoration(
                                  gradient: LinearGradient(
                                    colors: [
                                      Color.fromRGBO(86, 155, 232, 1),
                                      Color.fromRGBO(64, 114, 226, 1)
                                    ],
                                    begin: Alignment.bottomRight,
                                    end: Alignment.topLeft,
                                  ),
                                  borderRadius: BorderRadius.circular(22.0)),
                              child: Center(child: Icon(Icons.refresh)))),
                      elevation: 0.0,
                      backgroundColor: Colors.transparent,
                      onPressed: () {
                        _refreshWatch();
                      },
                    ),
                  ),
                ],
              ),
            ),
            Expanded(
              child: ListView.separated(
                  //shrinkWrap: true,
                  //reverse: true,
                  padding: EdgeInsets.symmetric(horizontal: 24),
                  itemCount: widget.dependencies.savedTimeList.length,
                  separatorBuilder: (BuildContext context, int index) =>
                      Divider(),
                  itemBuilder: (context, index) {
                    if (index == widget.dependencies.savedTimeList.length - 1) {
                      return Column(
                        children: <Widget>[
                          Row(
                            children: <Widget>[
                              Expanded(
                                child: Text(
                                  createListItemText(
                                      widget.dependencies.savedTimeList.length,
                                      index),
                                  style: TextStyle(
                                      fontFamily: 'RoundedMplus',
                                      fontSize: 16.0,
                                      color: Color.fromRGBO(27, 39, 45, 0.75)),
                                ),
                              ),
                              Expanded(
                                child: Text(
                                  '${widget.dependencies.savedTimeList.elementAt(index)}',
                                  style: TextStyle(
                                      fontFamily: 'RoundedMplus',
                                      fontSize: 16.0,
                                      color: Color.fromRGBO(27, 39, 45, 0.75)),
                                  textAlign: TextAlign.right,
                                ),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 70,
                          )
                        ],
                      );
                    } else
                      return Row(
                        children: <Widget>[
                          Expanded(
                            child: Text(
                              createListItemText(
                                  widget.dependencies.savedTimeList.length,
                                  index),
                              style: TextStyle(
                                  fontFamily: 'RoundedMplus',
                                  fontSize: 16.0,
                                  color: Color.fromRGBO(27, 39, 45, 0.75)),
                            ),
                          ),
                          Expanded(
                            child: Text(
                              '${widget.dependencies.savedTimeList.elementAt(index)}',
                              style: TextStyle(
                                  fontFamily: 'RoundedMplus',
                                  fontSize: 16.0,
                                  color: Color.fromRGBO(27, 39, 45, 0.75)),
                              textAlign: TextAlign.right,
                            ),
                          ),
                        ],
                      );
                  }),
            ),
          ],
        ),
        Container(
          alignment: Alignment.bottomCenter,
          child: Container(
            decoration: BoxDecoration(
              gradient: LinearGradient(
                colors: [Colors.white30, Colors.white],
                begin: Alignment(0.0, -0.9),
                end: Alignment.bottomCenter,
              ),
            ),
            height: 100,
            alignment: Alignment.bottomCenter,
            child: Padding(
              padding: EdgeInsets.only(left: 24, right: 24, bottom: 16),
              child: Row(
                children: <Widget>[
                  Expanded(
                      child: Padding(
                          padding: EdgeInsets.only(right: 4),
                          child: Container(
                            decoration: BoxDecoration(
                                border: Border.all(
                                    color: Color.fromRGBO(27, 39, 45, 0.1)),
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(22.0)),
                            child: FlatButton(
                              shape: StadiumBorder(),
                              onPressed: () {
                                Navigator.push(
                                    context,
                                    CustomRoute(
                                        builder: (context) =>
                                            Results(auth: Authentication())));
                              },
                              child: Row(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  SvgPicture.asset(
                                      'images/results_icon_auth.svg'),
                                  Padding(
                                    padding: EdgeInsets.only(left: 4.0),
                                    child: Text(Strings.results,
                                        style: TextStyle(
                                            fontFamily: 'RoundedMplusMedium',
                                            fontWeight: FontWeight.w500,
                                            fontSize: 14,
                                            color: Color.fromRGBO(
                                                27, 39, 45, 0.75))),
                                  ),
                                ],
                              ),
                            ),
                          ))),
                  Expanded(
                      child: Padding(
                          padding: EdgeInsets.only(left: 4),
                          child: Container(
                            decoration: BoxDecoration(
                                gradient: LinearGradient(
                                  colors: [
                                    Color.fromRGBO(86, 155, 232, 1),
                                    Color.fromRGBO(64, 114, 226, 1)
                                  ],
                                  begin: Alignment.bottomRight,
                                  end: Alignment.topLeft,
                                ),
                                borderRadius: BorderRadius.circular(22.0)),
                            child: FlatButton(
                              shape: StadiumBorder(),
                              onPressed: () {
                                if (_pushToStart) {
                                  //widget.dependencies.stopwatch.isRunning) {
                                  Toast.show(Strings.nothingToSave, context);
                                } else if (widget
                                    .dependencies.stopwatch.isRunning) {
                                  Toast.show(Strings.turnOffStopwatch, context);
                                } else {
                                  switch (authStatus) {
                                    case AuthStatus.notSignedIn:
                                      Navigator.push(
                                          context,
                                          CustomRoute(
                                              builder: (context) =>
                                                  NotSignedIn()));
                                      break;
                                    case AuthStatus.signedIn:
                                      _showDialog();
                                  }
                                }
                              },
                              child: Row(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  SvgPicture.asset('images/save_white.svg'),
                                  Padding(
                                    padding: EdgeInsets.only(left: 4.0),
                                    child: Text(Strings.saveDialog01,
                                        style: TextStyle(
                                            fontFamily: 'RoundedMplusMedium',
                                            fontWeight: FontWeight.w500,
                                            fontSize: 14,
                                            color: Colors.white)),
                                  ),
                                ],
                              ),
                            ),
                          ))),
                ],
              ),
            ),
          ),
        )
      ],
    );
  }

  _addLap() {
    if (widget.dependencies.stopwatch.isRunning) {
      widget.dependencies.savedTimeList.insert(
          0,
          widget.dependencies.transformMilliSecondsToString(
              widget.dependencies.stopwatch.elapsedMilliseconds));
    } else {
      Toast.show(Strings.turnOnStopwatch, context);
    }
  }

  _startOrStopWatch() {
    if (widget.dependencies.stopwatch.isRunning) {
      buttonIcon = Icon(Icons.play_arrow);
      widget.dependencies.stopwatch.stop();
      stopwatchIndicator = widget.dependencies.transformMilliSecondsToString(
          widget.dependencies.stopwatch.elapsedMilliseconds);
      setState(() {});
    } else {
      _pushToStart = false;
      buttonIcon = Icon(Icons.pause);
      widget.dependencies.stopwatch.start();
      timer = new Timer.periodic(new Duration(milliseconds: 20), updateTime);
      stopwatchIndicator = widget.dependencies.transformMilliSecondsToString(
          widget.dependencies.stopwatch.elapsedMilliseconds);
    }
  }

  _refreshWatch() {
    setState(() {
      buttonIcon = Icon(Icons.play_arrow);
      widget.dependencies.stopwatch.stop();
      widget.dependencies.stopwatch.reset();
      widget.dependencies.savedTimeList.clear();
      _pushToStart = true;
    });
  }

  String createListItemText(int listSize, int index) {
    index = listSize - index;
    String indexText = index.toString().padLeft(2);

    return '${Strings.timerLap} $indexText';
  }

  _showDialog() async {
    await showDialog<String>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(22.0))),
          contentPadding: EdgeInsets.all(8.0),
          content: SingleChildScrollView(
            child: SaveResultDialog(),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text(
                Strings.saveDialog02,
                style: TextStyle(
                    fontFamily: 'RoundedMplusMedium',
                    fontSize: 14.0,
                    fontWeight: FontWeight.w500,
                    color: Color.fromRGBO(27, 39, 45, 0.56)),
              ),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            FlatButton(
              child: Text(Strings.saveDialog01,
                  style: TextStyle(
                      fontFamily: 'RoundedMplusMedium',
                      fontSize: 14.0,
                      fontWeight: FontWeight.w500,
                      color: Color.fromRGBO(63, 113, 226, 1))),
              onPressed: () {
                if (SaveResultDialogState.controller.text.trim().isEmpty) {
                  Toast.show(Strings.writeName, context);
                } else if (SaveResultDialogState.controller.text.trim().length <
                    3) {
                  Toast.show(Strings.shortName, context);
                } else {
                  Navigator.of(context).pop();
                  handleSubmit();
                }
              },
            ),
          ],
        );
      },
    );
  }
}
