import 'package:flutter/material.dart';
import 'package:swim_teacher/screens/timer_screen/screen/clock_painter.dart';
import 'package:swim_teacher/presenter/stopwatch/current_time.dart';
import 'package:swim_teacher/presenter/stopwatch/stopwatch_dependencies.dart';

class TimerClock extends StatefulWidget {
  final StopwatchDependencies dependencies;

  TimerClock(this.dependencies, {Key key}) : super(key: key);

  TimerClockState createState() => TimerClockState();
}

class TimerClockState extends State<TimerClock> {
  CurrentTime currentTime;

  Paint paint;

  @override
  void initState() {
    paint = new Paint();
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    currentTime = widget.dependencies.transformMilliSecondsToTime(
        widget.dependencies.stopwatch.elapsedMilliseconds);

    return CustomPaint(
      painter: ClockPainter(
          lineColor: Colors.lightBlueAccent,
          completeColor: Colors.blueAccent,
          hundreds: currentTime.hundreds,
          seconds: currentTime.seconds,
          minutes: currentTime.minutes,
          hours: currentTime.hours,
          width: 4.0,
          linePaint: paint),
      child: Container(
        alignment: Alignment.center,
        padding: const EdgeInsets.all(8.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              currentTime.hours.toString().padLeft(2, '0'),
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 36.0,
                  color: Color.fromRGBO(63, 113, 226, 1)),
            ),
            Text(
              '${currentTime.minutes.toString().padLeft(2, '0')} : ${currentTime.seconds.toString().padLeft(2, '0')}',
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 48.0,
                  color: Color.fromRGBO(63, 113, 226, 1)),
            ),
            Text(
              currentTime.hundreds.toString().padLeft(2, '0'),
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 24.0,
                  color: Color.fromRGBO(63, 113, 226, 1)),
            )
          ],
        ),
      ),
    );
  }
}
