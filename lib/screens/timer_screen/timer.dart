import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:swim_teacher/model/strings.dart';
import 'package:swim_teacher/presenter/authentication.dart';
import 'package:swim_teacher/presenter/navigation.dart';
import 'package:swim_teacher/presenter/stopwatch/stopwatch_dependencies.dart';
import 'package:swim_teacher/screens/main_screen/navigation_drawer.dart';
import 'package:swim_teacher/screens/timer_screen/screen/stopwatch_screen_portrait.dart';

class TimerScr extends StatefulWidget {
  State createState() => _TimerState();
}

class _TimerState extends State<TimerScr> {
  final StopwatchDependencies dependencies = new StopwatchDependencies();
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey();

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        AppNavigation.toMainPage(context);
      },
      child: Scaffold(
          backgroundColor: Colors.white,
          drawer: NavigationDrawer(),
          key: _scaffoldKey, // New Line
          appBar: AppBar(
              backgroundColor: Colors.white,
              elevation: 0.0,
              centerTitle: true,
              title: Text(
                Strings.timer,
                style: TextStyle(
                    fontFamily: 'RoundedMplusMedium',
                    fontWeight: FontWeight.w500,
                    fontSize: 16,
                    color: Color.fromRGBO(27, 39, 45, 0.75)),
              ),
              leading: FlatButton(
                  onPressed: () {
                    _scaffoldKey.currentState.openDrawer();
                  },
                  child: SvgPicture.asset('images/hamburger.svg'))),
          body: Container(
              alignment: Alignment.center,
              padding: EdgeInsets.all(0.0),
              child: TimerScreen(
                  dependencies: dependencies, auth: Authentication()))),
    );
  }
}
