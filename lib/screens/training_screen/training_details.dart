import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:intl/intl.dart';
import 'package:share/share.dart';
import 'package:swim_teacher/model/distance_model.dart';
import 'package:swim_teacher/model/strings.dart';
import 'package:swim_teacher/presenter/authentication.dart';
import 'package:swim_teacher/presenter/custom_route.dart';
import 'package:swim_teacher/presenter/navigation.dart';
import 'package:swim_teacher/presenter/toastview.dart';
import 'package:swim_teacher/screens/main_screen/navigation_drawer.dart';
import 'package:swim_teacher/screens/training_screen/training_date.dart';
import 'package:swim_teacher/screens/training_screen/training_screen.dart';

class TrainingDetails extends StatefulWidget {
  TrainingDetails({Key key, this.auth, this.editBool}) : super(key: key);
  final BaseAuth auth;
  final bool editBool;

  @override
  _TrainingDetailsState createState() => _TrainingDetailsState();
}

class _TrainingDetailsState extends State<TrainingDetails> {
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey();
  static List<DistanceModel> _exerciseList = List();
  DatabaseReference _reference;
  DatabaseReference _referenceExercises;

  static DateTime _date = DateTime.parse(TrainingDate.date);
  static String _dateOfTraining =
      '${DateFormat.d().format(_date)}.${DateFormat.M().format(_date).length == 1 ? "0" + DateFormat.M().format(_date) : DateFormat.M().format(_date)}.${DateFormat.y().format(_date)}';
  String _shareData = Strings.trainingName +
      ": " +
      TrainingDate.title +
      '\n' +
      Strings.saveDialog13 +
      ": " +
      _dateOfTraining +
      '\n' +
      '${TrainingDate.description.isEmpty ? "" : Strings.description + ": " + TrainingDate.description + '\n'}';

  bool _editOff;
  bool isKeyboardShowing;

  TextEditingController _nameController = TextEditingController();
  TextEditingController _descriptionController = TextEditingController();

  @override
  void initState() {
    super.initState();
    _getReference();
    widget.editBool ? _editOff = true : _editOff = false;
    _nameController.text = TrainingDate.title;
    _descriptionController.text = TrainingDate.description;
  }

  Future<void> _getReference() async {
    try {
      String _userUid = await widget.auth.getUid();
      final FirebaseDatabase database = FirebaseDatabase.instance;
      _reference = database
          .reference()
          .child(_userUid)
          .child(Strings.trainings)
          .child(TrainingDate.key);
      _referenceExercises = database
          .reference()
          .child(_userUid)
          .child(Strings.trainings)
          .child(TrainingDate.key)
          .child(Strings.exercises);
      _referenceExercises.onChildAdded.listen(_onEntryAddedExercises);
    } catch (e) {
      print(e);
    }
  }

  _onEntryAddedExercises(Event event) {
    setState(() {
      _exerciseList.add(DistanceModel.fromSnapshot(event.snapshot));
    });
  }

  _shareTraining() {
    List _exerciseStrings = List();
    for (int i = 0; i < _exerciseList.length; i++) {
      _exerciseStrings.add(
          '${_exerciseList[i].repetitions == 1 ? "" : _exerciseList[i].repetitions.toString() + "x "}' +
              //_exerciseList[i].length +
              " " +
              _exerciseList[i].style +
              "\n" +
              '${_exerciseList[i].rest == 0 ? "" : "Отдых " + _exerciseList[i].rest.toString() + " секунд"}');
    }
    Share.share(_shareData + "\n" + _exerciseStrings.join('\n\n'));
  }

  _goToTrainingList() {
    Navigator.push(
        context,
        CustomRoute(
            builder: (context) => TrainingScreen(auth: Authentication())));
    _exerciseList.clear();
  }

  Future<void> _updateData() async {
    _reference.update({
      'title': _nameController.text,
      'description': _descriptionController.text,
      'date': _date.toString()
    });
  }

  @override
  Widget build(BuildContext context) {
    isKeyboardShowing = MediaQuery.of(context).viewInsets.vertical > 0;

    return WillPopScope(
      onWillPop: () {
        AppNavigation.toMainPage(context);
      },
      child: Scaffold(
          resizeToAvoidBottomPadding: true,
          backgroundColor: Colors.white,
          drawer: NavigationDrawer(),
          key: _scaffoldKey,
          appBar: AppBar(
              backgroundColor: Colors.white,
              elevation: 0.0,
              centerTitle: true,
              title: Text(
                Strings.coaching,
                style: TextStyle(
                    fontFamily: 'RoundedMplusMedium',
                    fontWeight: FontWeight.w500,
                    fontSize: 16,
                    color: Color.fromRGBO(27, 39, 45, 0.75)),
              ),
              actions: <Widget>[
                _editOff
                    ? Padding(
                        padding: EdgeInsets.symmetric(horizontal: 12),
                        child: SvgPicture.asset('images/edit_settings.svg'),
                      )
                    : SizedBox.shrink()
              ],
              leading: FlatButton(
                  onPressed: () {
                    _goToTrainingList();
                  },
                  child: SvgPicture.asset('images/arrow_back.svg'))),
          body: GestureDetector(
            onTap: () {
              FocusScope.of(context).requestFocus(FocusNode());
            },
            child: Padding(
              padding: EdgeInsets.only(left: 24, right: 24, bottom: 24),
              child: Stack(
                children: <Widget>[
                  ListView(
                    children: <Widget>[
                      TextField(
                        controller: _nameController,
                        maxLength: 24,
                        enabled: _editOff,
                        decoration: InputDecoration(
                          contentPadding: EdgeInsets.only(left: 32, bottom: 8),
                          hintText: Strings.trainingName,
                          counterText: "",
                          enabledBorder: UnderlineInputBorder(
                            borderSide: BorderSide(
                                width: 0.5,
                                color: Color.fromRGBO(27, 39, 45, 0.1)),
                          ),
                          disabledBorder: UnderlineInputBorder(
                            borderSide: BorderSide(
                                width: 0.5,
                                color: Color.fromRGBO(27, 39, 45, 0.1)),
                          ),
                          hintStyle: TextStyle(
                              fontFamily: 'RoundedMplusMedium',
                              fontWeight: FontWeight.w500,
                              fontSize: 14,
                              color: Color.fromRGBO(63, 113, 226, 0.3)),
                        ),
                        style: TextStyle(
                            fontFamily: 'RoundedMplusMedium',
                            fontWeight: FontWeight.w500,
                            fontSize: 20,
                            color: Color.fromRGBO(63, 113, 226, 1)),
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(vertical: 8),
                        child: GestureDetector(
                          onTap: () {
                            if (_editOff) {
                              _datePickerShow();
                            }
                          },
                          child: Row(
                            children: <Widget>[
                              Padding(
                                padding: EdgeInsets.only(right: 8),
                                child: Icon(
                                  Icons.event,
                                  color: Color.fromRGBO(63, 113, 226, 1),
                                ),
                              ),
                              Expanded(
                                child: Text(
                                  _dateOfTraining,
                                  style: TextStyle(
                                      fontFamily: 'RoundedMplus',
                                      fontSize: 14.0,
                                      color: Color.fromRGBO(27, 39, 45, 0.75)),
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                      Divider(color: Color.fromRGBO(27, 39, 45, 0.1)),
                      IntrinsicHeight(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Align(
                              alignment: Alignment.topLeft,
                              child: Padding(
                                  padding:
                                  EdgeInsets.only(left: 4, right: 12, top: 8),
                                  child:
                                  SvgPicture.asset('images/description.svg')),
                            ),
                            Expanded(
                              child: TextField(
                                controller: _descriptionController,
                                maxLength: 120,
                                maxLines: null,
                                enabled: _editOff,
                                decoration: InputDecoration(
                                  hintText: Strings.description,
                                  counterText: "",
                                  border: InputBorder.none,
                                  hintStyle: TextStyle(
                                      fontFamily: 'RoundedMplusMedium',
                                      fontWeight: FontWeight.w500,
                                      fontSize: 14,
                                      color: Color.fromRGBO(63, 113, 226, 0.3)),
                                ),
                                style: TextStyle(
                                    fontFamily: 'RoundedMplus',
                                    fontSize: 14.0,
                                    color: Color.fromRGBO(27, 39, 45, 0.75)),
                              ),
                            )
                          ],
                        ),
                      ),
                      Divider(color: Color.fromRGBO(27, 39, 45, 0.1)),
                      ListView.builder(
                          shrinkWrap: true,
                          physics: NeverScrollableScrollPhysics(),
                          itemCount: _exerciseList.length,
                          itemBuilder: (context, index) {
                            //_focusRepetitions.add(FocusNode());
                            //_repetitionsControllers.add(TextEditingController());
                            //_focusInventory.add(FocusNode());
                            //_inventoryControllers.add(TextEditingController());
                            //_inventoryControllers[index].text =
                            //    _exerciseList[index].inventory;
                            //_focusRest.add(FocusNode());
                            //_restControllers.add(TextEditingController());
                            //_focusStyle.add(FocusNode());
                            //_styleControllers.add(TextEditingController());
                            //_styleControllers[index].text =
                            //    _exerciseList[index].style;
                            return IntrinsicHeight(
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: <Widget>[
                                  SizedBox(
                                      width: 32,
                                      child: _exerciseList[index].repetitions <= 1
                                          ? SizedBox.shrink()
                                          : Align(
                                        alignment: Alignment.topLeft,
                                        child: Text(
                                          _exerciseList[index]
                                              .repetitions
                                              .toString() +
                                              'x',
                                          style: TextStyle(
                                              fontFamily:
                                              'RoundedMplusMedium',
                                              fontWeight: FontWeight.w500,
                                              fontSize: 16,
                                              color: Color.fromRGBO(
                                                  63, 113, 226, 1)),
                                        ),
                                      )),
                                  Expanded(
                                    child: Column(
                                      crossAxisAlignment:
                                      CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Padding(
                                          padding:
                                          EdgeInsets.only(top: 2, bottom: 8),
                                          child: Text(
                                            _exerciseList[index].style,
                                            style: TextStyle(
                                                fontFamily: 'RoundedMplus',
                                                fontSize: 14,
                                                color: Color.fromRGBO(
                                                    27, 39, 45, 0.75)),
                                          ),
                                        ),
                                        /*_editOff
                                          ? Padding(
                                              padding:
                                                  EdgeInsets.only(bottom: 8),
                                              child: TextField(
                                                controller:
                                                    _inventoryControllers[
                                                        index],
                                                focusNode:
                                                    _focusInventory[index],
                                                maxLength: 400,
                                                maxLines: null,
                                                //enabled: _editOff,
                                                decoration: InputDecoration(
                                                    contentPadding:
                                                        EdgeInsets.all(1),
                                                    counterText: "",
                                                    hintText:
                                                        _exerciseList[index]
                                                            .inventory,
                                                    hintStyle: TextStyle(
                                                        fontFamily:
                                                            'RoundedMplusMedium',
                                                        fontWeight:
                                                            FontWeight.w500,
                                                        fontSize: 14,
                                                        color: Color.fromRGBO(
                                                            63, 113, 226, 1)),
                                                    border: InputBorder.none),
                                                style: TextStyle(
                                                    fontFamily:
                                                        'RoundedMplusMedium',
                                                    fontWeight: FontWeight.w500,
                                                    fontSize: 14,
                                                    color: Color.fromRGBO(
                                                        63, 113, 226, 1)),
                                                onChanged: (_) {
                                                  _exerciseList[index]
                                                          .inventory =
                                                      _inventoryControllers[
                                                              index]
                                                          .text;
                                                },
                                              ),
                                            )
                                          : Padding(
                                              padding:
                                                  EdgeInsets.only(bottom: 8),
                                              child: _exerciseList[index]
                                                      .inventory
                                                      .isEmpty
                                                  ? Text(
                                                      Strings.withoutInventary,
                                                      style: TextStyle(
                                                          fontFamily:
                                                              'RoundedMplus',
                                                          fontSize: 14,
                                                          color: Color.fromRGBO(
                                                              27,
                                                              39,
                                                              45,
                                                              0.56)),
                                                    )
                                                  : Text(
                                                      _exerciseList[index]
                                                          .inventory,
                                                      style: TextStyle(
                                                          fontFamily:
                                                              'RoundedMplusMedium',
                                                          fontWeight:
                                                              FontWeight.w500,
                                                          fontSize: 14,
                                                          color: Color.fromRGBO(
                                                              63, 113, 226, 1)),
                                                    ),
                                            ),*/
                                        Padding(
                                          padding: EdgeInsets.only(bottom: 8),
                                          child: _exerciseList[index].rest == 0
                                              ? Text(
                                            Strings.withoutRest,
                                            style: TextStyle(
                                                fontFamily: 'RoundedMplus',
                                                fontSize: 14,
                                                color: Color.fromRGBO(
                                                    27, 39, 45, 0.56)),
                                          )
                                              : Text(
                                            Strings.rest +
                                                _exerciseList[index]
                                                    .rest
                                                    .toString() +
                                                Strings.sec,
                                            style: TextStyle(
                                                fontFamily:
                                                'RoundedMplusMedium',
                                                fontWeight: FontWeight.w500,
                                                fontSize: 14,
                                                color: Color.fromRGBO(
                                                    63, 113, 226, 1)),
                                          ),
                                        ),
                                        Divider(
                                            color:
                                            Color.fromRGBO(27, 39, 45, 0.1)),
                                      ],
                                    ),
                                  )
                                ],
                              ),
                            );
                          }),
                    ],
                  ),
                  Container(
                      alignment: Alignment.bottomCenter,
                      child: Visibility(
                        visible: !isKeyboardShowing,
                        child: Container(
                          decoration: BoxDecoration(
                            gradient: LinearGradient(
                              colors: [Colors.white30, Colors.white],
                              begin: Alignment(0.0, -0.9),
                              end: Alignment.bottomCenter,
                            ),
                          ),
                          height: 56,
                          alignment: Alignment.bottomCenter,
                          child: Row(
                            children: <Widget>[
                              Expanded(
                                  child: Padding(
                                      padding: EdgeInsets.only(
                                          right: 4, top: 4, bottom: 4),
                                      child: Container(
                                        decoration: BoxDecoration(
                                            color: Colors.white,
                                            border: Border.all(
                                                color: Color.fromRGBO(
                                                    27, 39, 45, 0.1)),
                                            borderRadius:
                                            BorderRadius.circular(22.0)),
                                        child: FlatButton(
                                          shape: StadiumBorder(),
                                          onPressed: () {
                                            if (!_editOff) {
                                              setState(() {
                                                _editOff = true;
                                              });
                                            } else
                                              setState(() {
                                                _editOff = false;
                                                _nameController.text =
                                                    TrainingDate.title;
                                                _descriptionController.text =
                                                    TrainingDate.description;
                                                _date = DateTime.parse(
                                                    TrainingDate.date);
                                                _dateOfTraining =
                                                '${DateFormat.d().format(_date)}.${DateFormat.M().format(_date).length == 1 ? "0" + DateFormat.M().format(_date) : DateFormat.M().format(_date)}.${DateFormat.y().format(_date)}';
                                                //_repetitionsControllers.clear();
                                              });
                                          },
                                          child: AutoSizeText(
                                              _editOff
                                                  ? Strings.cancelButton
                                                  : Strings.edit,
                                              maxLines: 1,
                                              style: TextStyle(
                                                  fontFamily:
                                                  'RoundedMplusMedium',
                                                  fontSize: 14.0,
                                                  fontWeight: FontWeight.w500,
                                                  color: Color.fromRGBO(
                                                      27, 39, 45, 0.75))),
                                        ),
                                      ))),
                              Expanded(
                                  child: Padding(
                                      padding: EdgeInsets.only(
                                          left: 4, top: 4, bottom: 4),
                                      child: Container(
                                        decoration: BoxDecoration(
                                            gradient: LinearGradient(
                                              colors: [
                                                Color.fromRGBO(86, 155, 232, 1),
                                                Color.fromRGBO(64, 114, 226, 1)
                                              ],
                                              begin: Alignment.bottomRight,
                                              end: Alignment.topLeft,
                                            ),
                                            borderRadius:
                                            BorderRadius.circular(22.0)),
                                        child: FlatButton(
                                          shape: StadiumBorder(),
                                          onPressed: () {
                                            if (_editOff) {
                                              if (_nameController.text.isEmpty) {
                                                Toast.show(
                                                    Strings.writeNameOfTraining,
                                                    context);
                                              } else
                                                _updateData();
                                              setState(() {
                                                _editOff = false;
                                              });
                                            } else
                                              _shareTraining();
                                          },
                                          child: Text(
                                            _editOff
                                                ? Strings.saveDialog01
                                                : Strings.share,
                                            style: TextStyle(
                                                fontFamily: 'RoundedMplusMedium',
                                                fontSize: 14.0,
                                                fontWeight: FontWeight.w500,
                                                color: Colors.white),
                                            textAlign: TextAlign.center,
                                          ),
                                        ),
                                      ))),
                            ],
                          ),
                        ),
                      )),
                ],
              ),
            ),
          ),
      ),
    );
  }

  _datePickerShow() {
    DatePicker.showDatePicker(context,
        showTitleActions: true,
        minTime: DateTime.now().subtract(Duration(days: 31)),
        maxTime: DateTime.now().add(Duration(days: 366)),
        theme: DatePickerTheme(
            backgroundColor: Color.fromRGBO(63, 113, 226, 1),
            itemStyle: TextStyle(
                fontFamily: 'RoundedMplus', fontSize: 16, color: Colors.white),
            doneStyle: TextStyle(
                fontFamily: 'RoundedMplus',
                color: Colors.white,
                fontSize: 16)), onChanged: (date) {
      print('change $date');
    }, onConfirm: (date) {
      _dateOfTraining = date.toString();
      print('confirm $date');
      initializeDateFormatting("ky", null);
      setState(() {
        _date = date;
        _dateOfTraining =
            '${DateFormat.d().format(_date)}.${DateFormat.M().format(_date).length == 1 ? "0" + DateFormat.M().format(_date) : DateFormat.M().format(_date)}.${DateFormat.y().format(_date)}';
        /*_trainingDate = DateFormat.d().format(date).toString() +
                "." +
                DateFormat.M().format(date).toString() +
                "." +
                DateFormat.y().format(date).toString();*/
      });
    }, currentTime: DateTime.now(), locale: LocaleType.ru);
  }
}
