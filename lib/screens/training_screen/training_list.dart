import 'dart:async';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:intl/intl.dart';
import 'package:swim_teacher/model/distance_model.dart';
import 'package:swim_teacher/model/strings.dart';
import 'package:swim_teacher/model/training_model.dart';
import 'package:swim_teacher/presenter/authentication.dart';
import 'package:swim_teacher/presenter/custom_route.dart';
import 'package:swim_teacher/screens/training_screen/training_date.dart';
import 'package:swim_teacher/screens/training_screen/training_details.dart';

class TrainingList extends StatefulWidget {
  TrainingList({Key key, this.auth}) : super(key: key);
  final BaseAuth auth;

  @override
  _TrainingListState createState() => _TrainingListState();
}

class _TrainingListState extends State<TrainingList> {
  List<TrainingModel> _trainingList = List();
  List<DistanceModel> _exerciseList = List();

  DatabaseReference _reference;
  DatabaseReference _referenceExercises;

  final SlidableController _slidableController = SlidableController();
  bool _preload = true;

  @override
  void initState() {
    super.initState();
    _trainingList.clear();
    _exerciseList.clear();
    _getReference();

    Timer(Duration(milliseconds: 2900), () {
      setState(() {
        _preload = false;
      });
    });
  }

  @override
  void setState(fn) {
    if (mounted) {
      super.setState(fn);
    }
  }

  Future<void> _getReference() async {
    try {
      String _userUid = await widget.auth.getUid();
      final FirebaseDatabase database = FirebaseDatabase.instance;
      _reference =
          database.reference().child(_userUid).child(Strings.trainings);
      _reference.onChildAdded.listen(_onEntryAdded);
    } catch (e) {
      print(e);
    }
  }

  _onEntryAdded(Event event) {
    setState(() {
      _trainingList.add(TrainingModel.fromSnapshot(event.snapshot));
    });
  }

  Future<void> _deleteTraining(
      BuildContext context, TrainingModel training, int position) async {
    await _reference.child(training.key).remove().then((_) {
      setState(() {
        _trainingList.removeAt(position);
      });
    });
  }

  Future<void> _getReferenceExercises(
      BuildContext context, TrainingModel training) async {
    try {
      String _userUid = await widget.auth.getUid();
      final FirebaseDatabase database = FirebaseDatabase.instance;
      _referenceExercises = database
          .reference()
          .child(_userUid)
          .child(Strings.trainings)
          .child(training.key)
          .child(Strings.exercises);
      _referenceExercises.onChildAdded.listen(_onEntryAddedExercises);
    } catch (e) {
      print(e);
    }
  }

  _onEntryAddedExercises(Event event) {
    _exerciseList.add(DistanceModel.fromSnapshot(event.snapshot));
  }

  Future<void> _setTraining(BuildContext context, TrainingModel model) async {
    _reference.child(model.key).set(model.toJson());
  }

  void _setExercise() {
    for (int i = 0; i < _exerciseList.length; i++) {
      _referenceExercises
          .child((i + 1).toString())
          .set(_exerciseList[i].toJson());
    }
  }

  void _toDetails(BuildContext context, int index, bool edit) {
    TrainingDate.title = _trainingList[index].title;
    TrainingDate.date = _trainingList[index].date;
    TrainingDate.description = _trainingList[index].description;
    TrainingDate.key = _trainingList[index].key;
    Navigator.push(
        context,
        CustomRoute(
            builder: (context) =>
                TrainingDetails(auth: Authentication(), editBool: edit)));
  }

  @override
  Widget build(BuildContext context) {
    if (_trainingList.isNotEmpty) {
      return _trainingScreen();
    } else if (_trainingList.isEmpty && _preload) {
      return Container(
        color: Colors.white,
        child: Visibility(
          visible: _preload,
          child: Center(
            child: SpinKitChasingDots(
                color: Color.fromRGBO(63, 113, 226, 1), size: 100),
          ),
        ),
      );
    } else if (_trainingList.isEmpty && !_preload) {
      return _noData();
    }
  }

  Widget _trainingScreen() {
    _trainingList.sort((a, b) => b.date.compareTo(a.date));
    return ListView.separated(
      itemCount: _trainingList.length,
      separatorBuilder: (BuildContext context, int index) => Divider(),
      itemBuilder: (context, index) {
        DateTime _date = DateTime.parse(_trainingList[index].date);
        if (index == 0) {
          return Slidable(
            closeOnScroll: true,
            controller: _slidableController,
            delegate: SlidableScrollDelegate(),
            secondaryActions: <Widget>[
              GestureDetector(
                onTap: () {
                  _toDetails(context, index, true);
                },
                child: _editButton(),
              ),
              GestureDetector(
                onTap: () {
                  TrainingModel _deletedModel = _trainingList[index];
                  _getReferenceExercises(context, _trainingList[index])
                      .then((_) {
                    _deleteTraining(context, _trainingList[index], index);
                    print(_exerciseList);
                  });

                  Scaffold.of(context)
                      .showSnackBar(SnackBar(
                        content: Text(
                          Strings.resultsRemove,
                          style: TextStyle(
                              fontFamily: 'RoundedMplus',
                              fontSize: 14.0,
                              color: Colors.white),
                        ),
                        action: SnackBarAction(
                            label: Strings.saveDialog02,
                            textColor: Color.fromRGBO(223, 239, 252, 1),
                            onPressed: () {
                              _setTraining(context, _deletedModel).then((_) {
                                _setExercise();
                              });
                              print(_exerciseList);
                            }),
                      ))
                      .closed
                      .then((SnackBarClosedReason reason) {
                    _exerciseList.clear();
                  });

                  _slidableController.activeState.close();
                },
                child: _deleteButton(),
              ),
            ],
            child: GestureDetector(
              onTap: () {
                _toDetails(context, index, false);
              },
              child: Container(
                decoration: BoxDecoration(
                    border: Border(
                        top: BorderSide(
                            width: 1, color: Color.fromRGBO(27, 39, 45, 0.1)))),
                padding: EdgeInsets.symmetric(vertical: 16),
                child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Column(
                          mainAxisSize: MainAxisSize.max,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text('${_trainingList[index].title}',
                                style: TextStyle(
                                    fontFamily: 'RoundedMplusMedium',
                                    fontSize: 16.0,
                                    fontWeight: FontWeight.w500,
                                    color: Color.fromRGBO(63, 113, 226, 1))),
                            Text(
                                '${DateFormat.d().format(_date)}.${DateFormat.M().format(_date).length == 1 ? "0" + DateFormat.M().format(_date) : DateFormat.M().format(_date)}.${DateFormat.y().format(_date)}',
                                style: TextStyle(
                                    fontFamily: 'RoundedMplus',
                                    fontSize: 12.0,
                                    color: Color.fromRGBO(27, 39, 45, 0.56)))
                          ],
                        ),
                        Icon(
                          Icons.arrow_forward_ios,
                          color: Color.fromRGBO(27, 39, 45, 0.3),
                          size: 20,
                        )
                      ],
                    )),
              ),
            ),
          );
        } else if (index == _trainingList.length - 1) {
          return Slidable(
            closeOnScroll: true,
            controller: _slidableController,
            delegate: SlidableScrollDelegate(),
            secondaryActions: <Widget>[
              GestureDetector(
                onTap: () {
                  _toDetails(context, index, true);
                },
                child: _editButton(),
              ),
              GestureDetector(
                onTap: () {
                  TrainingModel _deletedModel = _trainingList[index];
                  _getReferenceExercises(context, _trainingList[index])
                      .then((_) {
                    _deleteTraining(context, _trainingList[index], index);
                    print(_exerciseList);
                  });

                  Scaffold.of(context)
                      .showSnackBar(SnackBar(
                        content: Text(
                          Strings.resultsRemove,
                          style: TextStyle(
                              fontFamily: 'RoundedMplus',
                              fontSize: 14.0,
                              color: Colors.white),
                        ),
                        action: SnackBarAction(
                            label: Strings.saveDialog02,
                            textColor: Color.fromRGBO(223, 239, 252, 1),
                            onPressed: () {
                              _setTraining(context, _deletedModel).then((_) {
                                _setExercise();
                              });
                              print(_exerciseList);
                            }),
                      ))
                      .closed
                      .then((SnackBarClosedReason reason) {
                    _exerciseList.clear();
                  });

                  _slidableController.activeState.close();
                },
                child: _deleteButton(),
              ),
            ],
            child: GestureDetector(
              onTap: () {
                _toDetails(context, index, false);
              },
              child: Container(
                decoration: BoxDecoration(
                    border: Border(
                        bottom: BorderSide(
                            width: 1, color: Color.fromRGBO(27, 39, 45, 0.1)))),
                padding: EdgeInsets.symmetric(vertical: 16),
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 24),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Column(
                        mainAxisSize: MainAxisSize.max,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text('${_trainingList[index].title}',
                              style: TextStyle(
                                  fontFamily: 'RoundedMplusMedium',
                                  fontSize: 16.0,
                                  fontWeight: FontWeight.w500,
                                  color: Color.fromRGBO(63, 113, 226, 1))),
                          Text(
                              '${DateFormat.d().format(_date)}.${DateFormat.M().format(_date).length == 1 ? "0" + DateFormat.M().format(_date) : DateFormat.M().format(_date)}.${DateFormat.y().format(_date)}',
                              style: TextStyle(
                                  fontFamily: 'RoundedMplus',
                                  fontSize: 12.0,
                                  color: Color.fromRGBO(27, 39, 45, 0.56)))
                        ],
                      ),
                      Icon(
                        Icons.arrow_forward_ios,
                        color: Color.fromRGBO(27, 39, 45, 0.3),
                        size: 20,
                      )
                    ],
                  ),
                ),
              ),
            ),
          );
        } else
          return Slidable(
            closeOnScroll: true,
            controller: _slidableController,
            delegate: SlidableScrollDelegate(),
            secondaryActions: <Widget>[
              GestureDetector(
                onTap: () {
                  _toDetails(context, index, true);
                },
                child: _editButton(),
              ),
              GestureDetector(
                onTap: () {
                  TrainingModel _deletedModel = _trainingList[index];
                  _getReferenceExercises(context, _trainingList[index])
                      .then((_) {
                    _deleteTraining(context, _trainingList[index], index);
                    print(_exerciseList);
                  });

                  Scaffold.of(context)
                      .showSnackBar(SnackBar(
                        content: Text(
                          Strings.resultsRemove,
                          style: TextStyle(
                              fontFamily: 'RoundedMplus',
                              fontSize: 14.0,
                              color: Colors.white),
                        ),
                        action: SnackBarAction(
                            label: Strings.saveDialog02,
                            textColor: Color.fromRGBO(223, 239, 252, 1),
                            onPressed: () {
                              _setTraining(context, _deletedModel).then((_) {
                                _setExercise();
                              });
                              print(_exerciseList);
                            }),
                      ))
                      .closed
                      .then((SnackBarClosedReason reason) {
                    _exerciseList.clear();
                  });

                  _slidableController.activeState.close();
                },
                child: _deleteButton(),
              ),
            ],
            child: GestureDetector(
              onTap: () {
                _toDetails(context, index, false);
              },
              child: Container(
                padding: EdgeInsets.symmetric(vertical: 16),
                color: Colors.transparent,
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 24),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Column(
                        mainAxisSize: MainAxisSize.max,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text('${_trainingList[index].title}',
                              style: TextStyle(
                                  fontFamily: 'RoundedMplusMedium',
                                  fontSize: 16.0,
                                  fontWeight: FontWeight.w500,
                                  color: Color.fromRGBO(63, 113, 226, 1))),
                          Text(
                              '${DateFormat.d().format(_date)}.${DateFormat.M().format(_date).length == 1 ? "0" + DateFormat.M().format(_date) : DateFormat.M().format(_date)}.${DateFormat.y().format(_date)}',
                              style: TextStyle(
                                  fontFamily: 'RoundedMplus',
                                  fontSize: 12.0,
                                  color: Color.fromRGBO(27, 39, 45, 0.56)))
                        ],
                      ),
                      Icon(
                        Icons.arrow_forward_ios,
                        color: Color.fromRGBO(27, 39, 45, 0.3),
                        size: 20,
                      )
                    ],
                  ),
                ),
              ),
            ),
          );
      },
    );
  }

  Widget _noData() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        SvgPicture.asset('images/swimmer_nodata.svg'),
        Text(
          Strings.noTrainings,
          style: TextStyle(
              fontFamily: 'RoundedMplus',
              fontSize: 16.0,
              color: Color.fromRGBO(27, 39, 45, 0.5)),
          textAlign: TextAlign.center,
        ),
      ],
    );
  }

  Widget _deleteButton() {
    return Container(
      height: 80,
      color: Color.fromRGBO(238, 104, 104, 1),
      child: OverflowBox(
        minHeight: 0,
        minWidth: 0,
        child: SvgPicture.asset("images/basket.svg"),
      ),
    );
  }

  Widget _editButton() {
    return Container(
        height: 80,
        color: Color.fromRGBO(63, 113, 226, 0.8),
        child: OverflowBox(
          minHeight: 0,
          minWidth: 0,
          child: SvgPicture.asset("images/edit_settings_white.svg"),
        ));
  }
}
