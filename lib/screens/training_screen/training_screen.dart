import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:flutter_svg/svg.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:intl/intl.dart';
import 'package:swim_teacher/model/distance_model.dart';
import 'package:swim_teacher/model/strings.dart';
import 'package:swim_teacher/model/training_model.dart';
import 'package:swim_teacher/presenter/authentication.dart';
import 'package:swim_teacher/presenter/navigation.dart';
import 'package:swim_teacher/presenter/toastview.dart';
import 'package:swim_teacher/screens/main_screen/navigation_drawer.dart';
import 'package:swim_teacher/screens/training_screen/training_list.dart';
import 'package:add_to_calendar/add_to_calendar.dart';

class TrainingScreen extends StatefulWidget {
  TrainingScreen({Key key, this.auth}) : super(key: key);
  final BaseAuth auth;

  @override
  _TrainingScreenState createState() => _TrainingScreenState();
}

class _TrainingScreenState extends State<TrainingScreen>
    with SingleTickerProviderStateMixin {
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey();
  TrainingModel _trainingModel;
  DatabaseReference _reference;

  AnimationController animationController;
  PageController
      _pageController; //контроллер перехода от тренировок к созданию тренировок
  PageController
      _pageCreateTrainingController; //контроллер перехода страничек внутри создания тренировок
  FocusNode _focusNodeName = FocusNode();
  FocusNode _focusNodeDescription = FocusNode();
  TextEditingController _editingNameController =
      TextEditingController(); //контроллер названия
  TextEditingController _editingDescriptionController =
      TextEditingController(); //контроллер описания

  bool _plusIcon = true;
  String _title;
  int maxLength = 120;
  String _trainingDate = DateFormat.d().format(DateTime.now()).toString() +
      "." +
      '${DateFormat.M().format(DateTime.now()).toString().length == 1 ? "0" + DateFormat.M().format(DateTime.now()).toString() : DateFormat.M().format(DateTime.now()).toString()}' +
      "." +
      DateFormat.y().format(DateTime.now()).toString();

  int _distCount = 0;
  String _half = "";
  int _breakstrokeChoice = 0;
  int _dolfineChoice = 0;
  int _krollChoice = 0;
  int _onBackChoice = 0;

  bool _board = false;
  bool _kolobashka = false;
  bool _flippers = false;
  bool _paddle = false;
  bool _brake = false;
  bool _pipe = false;
  bool _pipes = false;

  int _restSecondsCount = 0;
  int _repititions = 1;

  ///Здесь переменные для сохранения в БД

  String _dateOfTraining;
  String _description;
  List<DistanceModel> _exerciseList = List<DistanceModel>();
  Set<String> _styleSet = Set<String>();
  Set<String> _inventorySet = Set<String>(); //методом join соединим данные
  Set<String> _distStyleSet = Set();

  @override
  void initState() {
    super.initState();
    animationController = AnimationController(
      vsync: this,
      duration: Duration(seconds: 1),
    );
    _trainingModel = TrainingModel("", "", "", "");
    _getReference();
    _title = Strings.coaching;
    _pageController = PageController();
    _pageCreateTrainingController = PageController();
    _editingNameController.text = Strings.trainingTitle;

    ///переменные для бд
    _dateOfTraining = DateTime.now().toString();
    _description = "";
  }

  @override
  void setState(fn) {
    if (mounted) {
      super.setState(fn);
    }
  }

  void _getReference() async {
    try {
      String _userUid = await widget.auth.getUid();
      final FirebaseDatabase database = FirebaseDatabase.instance;
      _reference =
          database.reference().child(_userUid).child(Strings.trainings);
    } catch (e) {
      print(e);
    }
  }

  void _handleSubmit() {
    _trainingModel.title = _editingNameController.text; //_titleOfTraining;
    _trainingModel.date = _dateOfTraining;
    _trainingModel.description = _description;
    _trainingModel.key = _reference.push().key;
    _reference.child(_trainingModel.key).set(_trainingModel.toJson()).then((_) {
      _editingNameController.text = Strings.trainingTitle;
      //_titleOfTraining = "";
      _dateOfTraining = DateTime.now().toString();
      _description = "";
      for (int i = 0; i < _exerciseList.length; i++) {
        _reference
            .child(_trainingModel.key)
            .child(Strings.exercises)
            .child((i + 1).toString())
            .set(_exerciseList.elementAt(i).toJson())
            .then((_) {
          _exerciseList.clear();
        });
      }
    });
  }

  Future<void> _toCreateTraining() async {
    animationController.animateTo(1.0, curve: Curves.easeInOut);
    _plusIcon = false;
    _pageController.animateToPage(1,
        duration: Duration(milliseconds: 500), curve: Curves.linear);
    setState(() {
      _title = Strings.createTraining;
    });
  }

  Future<void> _toTrainingList() async {
    animationController.animateTo(-1.0);
    _pageController.animateToPage(0,
        duration: Duration(milliseconds: 500), curve: Curves.linear);
    _pageCreateTrainingController.jumpToPage(0);
    _clearData();
    _plusIcon = true;
    setState(() {
      _title = Strings.coaching;
      _editingNameController.text = Strings.trainingTitle;
      _editingDescriptionController.text = "";
      _trainingDate = DateFormat.d().format(DateTime.now()).toString() +
          "." +
          '${DateFormat.M().format(DateTime.now()).toString().length == 1 ? "0" + DateFormat.M().format(DateTime.now()).toString() : DateFormat.M().format(DateTime.now()).toString()}' +
          "." +
          DateFormat.y().format(DateTime.now()).toString();
    });
  }

  Future<void> _clearData() async {
    setState(() {
      _distCount = 0;
      _half = "";
      _breakstrokeChoice = 0;
      _dolfineChoice = 0;
      _krollChoice = 0;
      _onBackChoice = 0;
      _restSecondsCount = 0;
      _repititions = 1;
      _board = false;
      _kolobashka = false;
      _flippers = false;
      _paddle = false;
      _brake = false;
      _pipe = false;
      _pipes = false;
      _inventorySet.clear();
      _distStyleSet.clear();
      _styleSet.clear();
    });
  }

  Future<void> _addDataToExerciseList() async {
    _exerciseList.add(DistanceModel(
        _distStyleSet.join("\n"),
        //_distCount.toString() + _half + " - " +
        //_styleSet.join('/ '),//_styleOfTraining,
        _inventorySet.isEmpty
            ? ""
            : _inventorySet.join(", ")[0].toUpperCase() +
                _inventorySet.join(", ").substring(1).toLowerCase(),
        _restSecondsCount,
        _repititions));
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        AppNavigation.toMainPage(context);
      },
      child: Scaffold(
        resizeToAvoidBottomPadding: false,
        backgroundColor: Colors.white,
        drawer: NavigationDrawer(),
        key: _scaffoldKey,
        appBar: AppBar(
            backgroundColor: Colors.white,
            elevation: 0.0,
            centerTitle: true,
            title: AnimatedSwitcher(
              duration: Duration(milliseconds: 300),
              child: Text(
                '$_title',
                style: TextStyle(
                    fontFamily: 'RoundedMplusMedium',
                    fontWeight: FontWeight.w500,
                    fontSize: 16,
                    color: Color.fromRGBO(27, 39, 45, 0.75)),
                key: ValueKey(_title),
              ),
              transitionBuilder: (Widget child, Animation<double> animation) {
                return FadeTransition(child: child, opacity: animation);
              },
            ),
            actions: <Widget>[
              IconButton(
                icon: AnimatedBuilder(
                  animation: animationController,
                  builder: (BuildContext context, Widget _widget) {
                    return Transform.rotate(
                        angle: animationController.value * 5.5,
                        child: SvgPicture.asset('images/plus.svg'));
                  },
                ),
                onPressed: () {
                  if (_plusIcon) {
                    _toCreateTraining();
                  } else {
                    _showDialog();
                  }
                },
              )
            ],
            leading: FlatButton(
                onPressed: () {
                  _scaffoldKey.currentState.openDrawer();
                },
                child: SvgPicture.asset('images/hamburger.svg'))),
        body: Padding(
            padding: EdgeInsets.only(bottom: 24),
            child: PageView(
              controller: _pageController,
              physics: NeverScrollableScrollPhysics(),
              children: <Widget>[
                TrainingList(auth: Authentication()),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 24),
                  child: PageView(
                    controller: _pageCreateTrainingController,
                    physics: NeverScrollableScrollPhysics(),
                    children: <Widget>[
                      _trainingHead(), //0
                      _distChoiceWidget(), //1
                      _styleChoiceWidget(), //2
                      _inventoryChoiceWidget(), //3
                      _newDistWidget(), //4
                      _restSeconds(), //5
                      _repetitionCount(), //6
                      _addRestExercise(), //7
                    ],
                  ),
                ),
              ],
            )),
      ),
    );
  }

  Widget _trainingHead() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      mainAxisSize: MainAxisSize.max,
      children: <Widget>[
        Column(
          children: <Widget>[
            TextField(
              controller: _editingNameController,
              focusNode: _focusNodeName,
              maxLength: 24,
              decoration: InputDecoration(
                contentPadding: EdgeInsets.only(left: 32, bottom: 8),
                hintText: Strings.trainingName,
                counterText: "",
                hintStyle: TextStyle(
                    fontFamily: 'RoundedMplusMedium',
                    fontWeight: FontWeight.w500,
                    fontSize: 14,
                    color: Color.fromRGBO(63, 113, 226, 0.3)),
                enabledBorder: UnderlineInputBorder(
                  borderSide: BorderSide(
                      color: Color.fromRGBO(27, 39, 45, 0.1),
                      width: 0.3,
                      style: BorderStyle.solid),
                ),
              ),
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontWeight: FontWeight.w500,
                  fontSize: 20,
                  color: Color.fromRGBO(63, 113, 226, 1)),
              onChanged: (_) {
                //_titleOfTraining = _editingNameController.text;
              },
            ),
            FlatButton(
              padding: EdgeInsets.all(0),
              onPressed: () {
                _datePickerShow();
              },
              child: Row(
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.only(right: 8),
                    child: Icon(
                      Icons.event,
                      color: Color.fromRGBO(63, 113, 226, 1),
                    ),
                  ),
                  Expanded(
                    child: Text(
                      _trainingDate,
                      style: TextStyle(
                          fontFamily: 'RoundedMplus',
                          fontSize: 14.0,
                          color: Color.fromRGBO(27, 39, 45, 0.75)),
                    ),
                  )
                ],
              ),
            ),
            Divider(color: Color.fromRGBO(27, 39, 45, 0.1)),
            IntrinsicHeight(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Align(
                    alignment: Alignment.topLeft,
                    child: Padding(
                        padding: EdgeInsets.only(left: 4, right: 12, top: 4),
                        child: SvgPicture.asset('images/description.svg')),
                  ),
                  Expanded(
                    child: TextField(
                      controller: _editingDescriptionController,
                      focusNode: _focusNodeDescription, //здесь другой фокусноде
                      maxLength: maxLength,
                      maxLines: null,
                      decoration: InputDecoration(
                        contentPadding: EdgeInsets.only(bottom: 8),
                        hintText: Strings.description,
                        counterText: "",
                        hintStyle: TextStyle(
                          fontFamily: 'RoundedMplus',
                          fontSize: 14.0,
                          color: Color.fromRGBO(27, 39, 45, 0.1),
                        ),
                        enabledBorder: UnderlineInputBorder(
                          borderSide: BorderSide(style: BorderStyle.none),
                        ),
                      ),
                      style: TextStyle(
                          fontFamily: 'RoundedMplus',
                          fontSize: 14.0,
                          color: Color.fromRGBO(27, 39, 45, 0.75)),
                      onChanged: (_) {
                        _description = _editingDescriptionController.text;
                      },
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
        Row(
          children: <Widget>[
            Expanded(
                child: Padding(
                    padding: EdgeInsets.only(right: 4, top: 4, bottom: 4),
                    child: Container(
                      decoration: BoxDecoration(
                          border: Border.all(
                              color: Color.fromRGBO(27, 39, 45, 0.1)),
                          borderRadius: BorderRadius.circular(22.0)),
                      child: FlatButton(
                        shape: StadiumBorder(),
                        onPressed: () {
                          _showDialog();
                        },
                        child: Text(Strings.cancelButton,
                            style: TextStyle(
                                fontFamily: 'RoundedMplusMedium',
                                fontSize: 14.0,
                                fontWeight: FontWeight.w500,
                                color: Color.fromRGBO(27, 39, 45, 0.75))),
                      ),
                    ))),
            Expanded(
                child: Padding(
                    padding: EdgeInsets.only(left: 4, top: 4, bottom: 4),
                    child: Container(
                      decoration: BoxDecoration(
                          gradient: LinearGradient(
                            colors: [
                              Color.fromRGBO(86, 155, 232, 1),
                              Color.fromRGBO(64, 114, 226, 1)
                            ],
                            begin: Alignment.bottomRight,
                            end: Alignment.topLeft,
                          ),
                          borderRadius: BorderRadius.circular(22.0)),
                      child: FlatButton(
                        shape: StadiumBorder(),
                        onPressed: () {
                          if (_editingNameController.text.isEmpty) {
                            Toast.show(Strings.writeNameOfTraining, context);
                          } else {
                            _pageCreateTrainingController.animateToPage(1,
                                duration: Duration(milliseconds: 500),
                                curve: Curves.linear);
                          }
                        },
                        child: Text(
                          Strings.nextStep,
                          style: TextStyle(
                              fontFamily: 'RoundedMplusMedium',
                              fontSize: 14.0,
                              fontWeight: FontWeight.w500,
                              color: Colors.white),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ))),
          ],
        ),
      ],
    );
  }

  Widget _distChoiceWidget() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      mainAxisSize: MainAxisSize.max,
      children: <Widget>[
        Column(
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(top: 68),
              child: Text(_distCount.toString() + _half,
                  style: TextStyle(
                      fontFamily: 'RoundedMplus',
                      fontSize: 60.0,
                      height: 1.33,
                      color: Color.fromRGBO(63, 113, 226, 1))),
            ),
            Text(Strings.distCount,
                style: TextStyle(
                    fontFamily: 'RoundedMplusMedium',
                    fontSize: 20.0,
                    fontWeight: FontWeight.w500,
                    color: Color.fromRGBO(63, 113, 226, 1))),
          ],
        ),
        Column(
          children: <Widget>[
            Row(
              children: <Widget>[
                Expanded(
                    child: Padding(
                        padding: EdgeInsets.only(right: 5, bottom: 5),
                        child: Container(
                          decoration: BoxDecoration(
                              border: Border.all(
                                  color: Color.fromRGBO(27, 39, 45, 0.1)),
                              borderRadius: BorderRadius.circular(22.0)),
                          child: FlatButton(
                            shape: StadiumBorder(),
                            onPressed: () {
                              setState(() {
                                _distCount = 12;
                                _half = ",5";
                              });
                            },
                            child: Text(Strings.distCount1,
                                style: TextStyle(
                                    fontFamily: 'RoundedMplus',
                                    fontSize: 16.0,
                                    color: Color.fromRGBO(27, 39, 45, 1))),
                          ),
                        ))),
                Expanded(
                    child: Padding(
                        padding: EdgeInsets.only(right: 5, left: 5, bottom: 5),
                        child: Container(
                          decoration: BoxDecoration(
                              border: Border.all(
                                  color: Color.fromRGBO(27, 39, 45, 0.1)),
                              borderRadius: BorderRadius.circular(22.0)),
                          child: FlatButton(
                            shape: StadiumBorder(),
                            onPressed: () {
                              setState(() {
                                _distCount = 25;
                                _half = "";
                              });
                            },
                            child: Text(Strings.distCount2,
                                style: TextStyle(
                                    fontFamily: 'RoundedMplus',
                                    fontSize: 16.0,
                                    color: Color.fromRGBO(27, 39, 45, 1))),
                          ),
                        ))),
                Expanded(
                    child: Padding(
                        padding: EdgeInsets.only(left: 5, bottom: 5),
                        child: Container(
                          decoration: BoxDecoration(
                              border: Border.all(
                                  color: Color.fromRGBO(27, 39, 45, 0.1)),
                              borderRadius: BorderRadius.circular(22.0)),
                          child: FlatButton(
                            shape: StadiumBorder(),
                            onPressed: () {
                              setState(() {
                                _distCount = 50;
                                _half = "";
                              });
                            },
                            child: Text(Strings.distCount3,
                                style: TextStyle(
                                    fontFamily: 'RoundedMplus',
                                    fontSize: 16.0,
                                    color: Color.fromRGBO(27, 39, 45, 1))),
                          ),
                        ))),
              ],
            ),
            Row(
              children: <Widget>[
                Expanded(
                    child: Padding(
                        padding: EdgeInsets.only(right: 5, top: 5, bottom: 10),
                        child: Container(
                          decoration: BoxDecoration(
                              border: Border.all(
                                  color: Color.fromRGBO(27, 39, 45, 0.1)),
                              borderRadius: BorderRadius.circular(22.0)),
                          child: FlatButton(
                            shape: StadiumBorder(),
                            onPressed: () {
                              setState(() {
                                _distCount = 100;
                                _half = "";
                              });
                            },
                            child: Text(Strings.distCount4,
                                style: TextStyle(
                                    fontFamily: 'RoundedMplus',
                                    fontSize: 16.0,
                                    color: Color.fromRGBO(27, 39, 45, 1))),
                          ),
                        ))),
                Expanded(
                    child: Padding(
                        padding: EdgeInsets.only(
                            right: 5, left: 5, top: 5, bottom: 10),
                        child: Container(
                          decoration: BoxDecoration(
                              border: Border.all(
                                  color: Color.fromRGBO(27, 39, 45, 0.1)),
                              borderRadius: BorderRadius.circular(22.0)),
                          child: FlatButton(
                            shape: StadiumBorder(),
                            onPressed: () {
                              setState(() {
                                _distCount = 200;
                                _half = "";
                              });
                            },
                            child: Text(Strings.distCount5,
                                style: TextStyle(
                                    fontFamily: 'RoundedMplus',
                                    fontSize: 16.0,
                                    color: Color.fromRGBO(27, 39, 45, 1))),
                          ),
                        ))),
                Expanded(
                    child: Padding(
                        padding: EdgeInsets.only(left: 5, top: 5, bottom: 10),
                        child: Container(
                          decoration: BoxDecoration(
                              border: Border.all(
                                  color: Color.fromRGBO(27, 39, 45, 0.1)),
                              borderRadius: BorderRadius.circular(22.0)),
                          child: FlatButton(
                            shape: StadiumBorder(),
                            onPressed: () {
                              setState(() {
                                _distCount = 400;
                                _half = "";
                              });
                            },
                            child: Text(Strings.distCount6,
                                style: TextStyle(
                                    fontFamily: 'RoundedMplus',
                                    fontSize: 16.0,
                                    color: Color.fromRGBO(27, 39, 45, 1))),
                          ),
                        ))),
              ],
            ),
            Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                RawMaterialButton(
                  onPressed: () {
                    setState(() {
                      if (_distCount > 24) {
                        _distCount = _distCount - 25;
                      }
                    });
                  },
                  child: Icon(
                    Icons.remove,
                    color: Color.fromRGBO(63, 113, 226, 1),
                    size: 15.0,
                  ),
                  shape: CircleBorder(
                      side: BorderSide(color: Color.fromRGBO(27, 39, 45, 0.1))),
                  elevation: 0.0,
                  padding: const EdgeInsets.all(10.0),
                ),
                Container(
                  height: 50,
                  width: 50,
                  decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      border:
                          Border.all(color: Color.fromRGBO(27, 39, 45, 0.1))),
                  child: Center(
                    child: Text(
                      Strings.distCount2,
                      style: TextStyle(
                          fontFamily: 'RoundedMplus',
                          fontSize: 16.0,
                          color: Color.fromRGBO(63, 113, 226, 1)),
                    ),
                  ),
                ),
                RawMaterialButton(
                  onPressed: () {
                    setState(() {
                      _distCount = _distCount + 25;
                    });
                  },
                  child: new Icon(
                    Icons.add,
                    color: Color.fromRGBO(63, 113, 226, 1),
                    size: 15.0,
                  ),
                  shape: CircleBorder(
                      side: BorderSide(color: Color.fromRGBO(27, 39, 45, 0.1))),
                  elevation: 0.0,
                  padding: const EdgeInsets.all(10.0),
                ),
              ],
              //круглые кнопки
            )
          ],
        ),
        Row(
          children: <Widget>[
            Expanded(
                child: Padding(
                    padding: EdgeInsets.only(right: 4, top: 4, bottom: 4),
                    child: Container(
                      decoration: BoxDecoration(
                          border: Border.all(
                              color: Color.fromRGBO(27, 39, 45, 0.1)),
                          borderRadius: BorderRadius.circular(22.0)),
                      child: FlatButton(
                        shape: StadiumBorder(),
                        onPressed: () {
                          _pageCreateTrainingController.animateToPage(0,
                              duration: Duration(milliseconds: 500),
                              curve: Curves.linear);
                        },
                        child: Text(Strings.back,
                            style: TextStyle(
                                fontFamily: 'RoundedMplusMedium',
                                fontSize: 14.0,
                                fontWeight: FontWeight.w500,
                                color: Color.fromRGBO(27, 39, 45, 0.75))),
                      ),
                    ))),
            Expanded(
                child: Padding(
                    padding: EdgeInsets.only(left: 4, top: 4, bottom: 4),
                    child: Container(
                      decoration: BoxDecoration(
                          gradient: LinearGradient(
                            colors: [
                              Color.fromRGBO(86, 155, 232, 1),
                              Color.fromRGBO(64, 114, 226, 1)
                            ],
                            begin: Alignment.bottomRight,
                            end: Alignment.topLeft,
                          ),
                          borderRadius: BorderRadius.circular(22.0)),
                      child: FlatButton(
                        shape: StadiumBorder(),
                        onPressed: () {
                          if (_distCount != 0) {
                            _pageCreateTrainingController.animateToPage(2,
                                duration: Duration(milliseconds: 500),
                                curve: Curves.linear);
                          } else
                            Toast.show(Strings.checkDist, context);

                          /**/
                        },
                        child: Text(
                          Strings.nextStep,
                          style: TextStyle(
                              fontFamily: 'RoundedMplusMedium',
                              fontSize: 14.0,
                              fontWeight: FontWeight.w500,
                              color: Colors.white),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ))),
          ],
        ),
      ],
    );
  }

  Widget _styleChoiceWidget() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      mainAxisSize: MainAxisSize.max,
      children: <Widget>[
        Text(
          Strings.filterStyle,
          style: TextStyle(
              fontFamily: 'RoundedMplusMedium',
              fontSize: 20.0,
              fontWeight: FontWeight.w500,
              color: Color.fromRGBO(63, 113, 226, 1)),
        ),
        Column(
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  Strings.breaststroke,
                  style: TextStyle(
                      fontFamily: 'RoundedMplus',
                      fontSize: 14.0,
                      color: Color.fromRGBO(27, 39, 45, 0.75)),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    GestureDetector(
                      onTap: () {
                        if (_breakstrokeChoice == 1) {
                          setState(() {
                            _breakstrokeChoice = 0;
                          });
                          _styleSet
                              .remove(Strings.breaststroke + " " + Strings.p);
                        } else {
                          setState(() {
                            _breakstrokeChoice = 1;
                          });
                          _styleSet.add(Strings.breaststroke + " " + Strings.p);
                          _styleSet
                              .remove(Strings.breaststroke + " " + Strings.r);
                          _styleSet
                              .remove(Strings.breaststroke + " " + Strings.n);
                        }
                      },
                      child: Container(
                        height: 36,
                        width: 36,
                        decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            border: Border.all(
                                color: _breakstrokeChoice == 1
                                    ? Color.fromRGBO(63, 113, 226, 1)
                                    : Color.fromRGBO(27, 39, 45, 0.1))),
                        child: Center(
                          child: Text(
                            Strings.p,
                            style: TextStyle(
                                fontFamily: 'RoundedMplus',
                                fontSize: 14.0,
                                fontWeight: _breakstrokeChoice == 1
                                    ? FontWeight.bold
                                    : null,
                                color: _breakstrokeChoice == 1
                                    ? Color.fromRGBO(63, 113, 226, 1)
                                    : Color.fromRGBO(27, 39, 45, 1)),
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 8),
                      child: GestureDetector(
                        onTap: () {
                          if (_breakstrokeChoice == 2) {
                            setState(() {
                              _breakstrokeChoice = 0;
                            });
                            _styleSet
                                .remove(Strings.breaststroke + " " + Strings.r);
                          } else {
                            setState(() {
                              _breakstrokeChoice = 2;
                            });
                            _styleSet
                                .add(Strings.breaststroke + " " + Strings.r);
                            _styleSet
                                .remove(Strings.breaststroke + " " + Strings.p);
                            _styleSet
                                .remove(Strings.breaststroke + " " + Strings.n);
                          }
                        },
                        child: Container(
                          height: 36,
                          width: 36,
                          decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              border: Border.all(
                                  color: _breakstrokeChoice == 2
                                      ? Color.fromRGBO(63, 113, 226, 1)
                                      : Color.fromRGBO(27, 39, 45, 0.1))),
                          child: Center(
                            child: Text(
                              Strings.r,
                              style: TextStyle(
                                  fontFamily: 'RoundedMplus',
                                  fontSize: 14.0,
                                  fontWeight: _breakstrokeChoice == 2
                                      ? FontWeight.bold
                                      : null,
                                  color: _breakstrokeChoice == 2
                                      ? Color.fromRGBO(63, 113, 226, 1)
                                      : Color.fromRGBO(27, 39, 45, 1)),
                            ),
                          ),
                        ),
                      ),
                    ),
                    GestureDetector(
                      onTap: () {
                        if (_breakstrokeChoice == 3) {
                          setState(() {
                            _breakstrokeChoice = 0;
                          });
                          _styleSet
                              .remove(Strings.breaststroke + " " + Strings.n);
                        } else {
                          setState(() {
                            _breakstrokeChoice = 3;
                          });
                          _styleSet.add(Strings.breaststroke + " " + Strings.n);
                          _styleSet
                              .remove(Strings.breaststroke + " " + Strings.p);
                          _styleSet
                              .remove(Strings.breaststroke + " " + Strings.r);
                        }
                      },
                      child: Container(
                        height: 36,
                        width: 36,
                        decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            border: Border.all(
                                color: _breakstrokeChoice == 3
                                    ? Color.fromRGBO(63, 113, 226, 1)
                                    : Color.fromRGBO(27, 39, 45, 0.1))),
                        child: Center(
                          child: Text(
                            Strings.n,
                            style: TextStyle(
                                fontFamily: 'RoundedMplus',
                                fontSize: 14.0,
                                fontWeight: _breakstrokeChoice == 3
                                    ? FontWeight.bold
                                    : null,
                                color: _breakstrokeChoice == 3
                                    ? Color.fromRGBO(63, 113, 226, 1)
                                    : Color.fromRGBO(27, 39, 45, 1)),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
            Divider(color: Color.fromRGBO(27, 39, 45, 0.1)),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  Strings.dolphin,
                  style: TextStyle(
                      fontFamily: 'RoundedMplus',
                      fontSize: 14.0,
                      color: Color.fromRGBO(27, 39, 45, 0.75)),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    GestureDetector(
                      onTap: () {
                        if (_dolfineChoice == 1) {
                          setState(() {
                            _dolfineChoice = 0;
                          });
                          _styleSet.remove(Strings.dolphin + " " + Strings.p);
                        } else {
                          setState(() {
                            _dolfineChoice = 1;
                          });
                          _styleSet.add(Strings.dolphin + " " + Strings.p);
                          _styleSet.remove(Strings.dolphin + " " + Strings.r);
                          _styleSet.remove(Strings.dolphin + " " + Strings.n);
                        }
                      },
                      child: Container(
                        height: 36,
                        width: 36,
                        decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            border: Border.all(
                                color: _dolfineChoice == 1
                                    ? Color.fromRGBO(63, 113, 226, 1)
                                    : Color.fromRGBO(27, 39, 45, 0.1))),
                        child: Center(
                          child: Text(
                            Strings.p,
                            style: TextStyle(
                                fontFamily: 'RoundedMplus',
                                fontSize: 14.0,
                                fontWeight: _dolfineChoice == 1
                                    ? FontWeight.bold
                                    : null,
                                color: _dolfineChoice == 1
                                    ? Color.fromRGBO(63, 113, 226, 1)
                                    : Color.fromRGBO(27, 39, 45, 1)),
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 8),
                      child: GestureDetector(
                        onTap: () {
                          if (_dolfineChoice == 2) {
                            setState(() {
                              _dolfineChoice = 0;
                            });
                            _styleSet.remove(Strings.dolphin + " " + Strings.r);
                          } else {
                            setState(() {
                              _dolfineChoice = 2;
                            });
                            _styleSet.add(Strings.dolphin + " " + Strings.r);
                            _styleSet.remove(Strings.dolphin + " " + Strings.p);
                            _styleSet.remove(Strings.dolphin + " " + Strings.n);
                          }
                        },
                        child: Container(
                          height: 36,
                          width: 36,
                          decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              border: Border.all(
                                  color: _dolfineChoice == 2
                                      ? Color.fromRGBO(63, 113, 226, 1)
                                      : Color.fromRGBO(27, 39, 45, 0.1))),
                          child: Center(
                            child: Text(
                              Strings.r,
                              style: TextStyle(
                                  fontFamily: 'RoundedMplus',
                                  fontSize: 14.0,
                                  fontWeight: _dolfineChoice == 2
                                      ? FontWeight.bold
                                      : null,
                                  color: _dolfineChoice == 2
                                      ? Color.fromRGBO(63, 113, 226, 1)
                                      : Color.fromRGBO(27, 39, 45, 1)),
                            ),
                          ),
                        ),
                      ),
                    ),
                    GestureDetector(
                      onTap: () {
                        if (_dolfineChoice == 3) {
                          setState(() {
                            _dolfineChoice = 0;
                          });
                          _styleSet.remove(Strings.dolphin + " " + Strings.n);
                        } else {
                          setState(() {
                            _dolfineChoice = 3;
                          });
                          _styleSet.add(Strings.dolphin + " " + Strings.n);
                          _styleSet.remove(Strings.dolphin + " " + Strings.p);
                          _styleSet.remove(Strings.dolphin + " " + Strings.r);
                        }
                      },
                      child: Container(
                        height: 36,
                        width: 36,
                        decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            border: Border.all(
                                color: _dolfineChoice == 3
                                    ? Color.fromRGBO(63, 113, 226, 1)
                                    : Color.fromRGBO(27, 39, 45, 0.1))),
                        child: Center(
                          child: Text(
                            Strings.n,
                            style: TextStyle(
                                fontFamily: 'RoundedMplus',
                                fontSize: 14.0,
                                fontWeight: _dolfineChoice == 3
                                    ? FontWeight.bold
                                    : null,
                                color: _dolfineChoice == 3
                                    ? Color.fromRGBO(63, 113, 226, 1)
                                    : Color.fromRGBO(27, 39, 45, 1)),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
            Divider(color: Color.fromRGBO(27, 39, 45, 0.1)),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  Strings.kroll,
                  style: TextStyle(
                      fontFamily: 'RoundedMplus',
                      fontSize: 14.0,
                      color: Color.fromRGBO(27, 39, 45, 0.75)),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    GestureDetector(
                      onTap: () {
                        if (_krollChoice == 1) {
                          setState(() {
                            _krollChoice = 0;
                          });
                          _styleSet.remove(Strings.kroll + " " + Strings.p);
                        } else {
                          setState(() {
                            _krollChoice = 1;
                          });
                          _styleSet.add(Strings.kroll + " " + Strings.p);
                          _styleSet.remove(Strings.kroll + " " + Strings.r);
                          _styleSet.remove(Strings.kroll + " " + Strings.n);
                        }
                      },
                      child: Container(
                        height: 36,
                        width: 36,
                        decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            border: Border.all(
                                color: _krollChoice == 1
                                    ? Color.fromRGBO(63, 113, 226, 1)
                                    : Color.fromRGBO(27, 39, 45, 0.1))),
                        child: Center(
                          child: Text(
                            Strings.p,
                            style: TextStyle(
                                fontFamily: 'RoundedMplus',
                                fontSize: 14.0,
                                fontWeight:
                                    _krollChoice == 1 ? FontWeight.bold : null,
                                color: _krollChoice == 1
                                    ? Color.fromRGBO(63, 113, 226, 1)
                                    : Color.fromRGBO(27, 39, 45, 1)),
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 8),
                      child: GestureDetector(
                        onTap: () {
                          if (_krollChoice == 2) {
                            setState(() {
                              _krollChoice = 0;
                            });
                            _styleSet.remove(Strings.kroll + " " + Strings.r);
                          } else {
                            setState(() {
                              _krollChoice = 2;
                            });
                            _styleSet.add(Strings.kroll + " " + Strings.r);
                            _styleSet.remove(Strings.kroll + " " + Strings.p);
                            _styleSet.remove(Strings.kroll + " " + Strings.n);
                          }
                        },
                        child: Container(
                          height: 36,
                          width: 36,
                          decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              border: Border.all(
                                  color: _krollChoice == 2
                                      ? Color.fromRGBO(63, 113, 226, 1)
                                      : Color.fromRGBO(27, 39, 45, 0.1))),
                          child: Center(
                            child: Text(
                              Strings.r,
                              style: TextStyle(
                                  fontFamily: 'RoundedMplus',
                                  fontSize: 14.0,
                                  fontWeight: _krollChoice == 2
                                      ? FontWeight.bold
                                      : null,
                                  color: _krollChoice == 2
                                      ? Color.fromRGBO(63, 113, 226, 1)
                                      : Color.fromRGBO(27, 39, 45, 1)),
                            ),
                          ),
                        ),
                      ),
                    ),
                    GestureDetector(
                      onTap: () {
                        if (_krollChoice == 3) {
                          setState(() {
                            _krollChoice = 0;
                          });
                          _styleSet.remove(Strings.kroll + " " + Strings.n);
                        } else {
                          setState(() {
                            _krollChoice = 3;
                          });
                          _styleSet.add(Strings.kroll + " " + Strings.n);
                          _styleSet.remove(Strings.kroll + " " + Strings.p);
                          _styleSet.remove(Strings.kroll + " " + Strings.r);
                        }
                      },
                      child: Container(
                        height: 36,
                        width: 36,
                        decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            border: Border.all(
                                color: _krollChoice == 3
                                    ? Color.fromRGBO(63, 113, 226, 1)
                                    : Color.fromRGBO(27, 39, 45, 0.1))),
                        child: Center(
                          child: Text(
                            Strings.n,
                            style: TextStyle(
                                fontFamily: 'RoundedMplus',
                                fontSize: 14.0,
                                fontWeight:
                                    _krollChoice == 3 ? FontWeight.bold : null,
                                color: _krollChoice == 3
                                    ? Color.fromRGBO(63, 113, 226, 1)
                                    : Color.fromRGBO(27, 39, 45, 1)),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
            Divider(color: Color.fromRGBO(27, 39, 45, 0.1)),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  Strings.onBack,
                  style: TextStyle(
                      fontFamily: 'RoundedMplus',
                      fontSize: 14.0,
                      color: Color.fromRGBO(27, 39, 45, 0.75)),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    GestureDetector(
                      onTap: () {
                        if (_onBackChoice == 1) {
                          setState(() {
                            _onBackChoice = 0;
                          });
                          _styleSet.remove(Strings.onBack + " " + Strings.p);
                        } else {
                          setState(() {
                            _onBackChoice = 1;
                          });
                          _styleSet.add(Strings.onBack + " " + Strings.p);
                          _styleSet.remove(Strings.onBack + " " + Strings.r);
                          _styleSet.remove(Strings.onBack + " " + Strings.n);
                        }
                      },
                      child: Container(
                        height: 36,
                        width: 36,
                        decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            border: Border.all(
                                color: _onBackChoice == 1
                                    ? Color.fromRGBO(63, 113, 226, 1)
                                    : Color.fromRGBO(27, 39, 45, 0.1))),
                        child: Center(
                          child: Text(
                            Strings.p,
                            style: TextStyle(
                                fontFamily: 'RoundedMplus',
                                fontSize: 14.0,
                                fontWeight:
                                    _onBackChoice == 1 ? FontWeight.bold : null,
                                color: _onBackChoice == 1
                                    ? Color.fromRGBO(63, 113, 226, 1)
                                    : Color.fromRGBO(27, 39, 45, 1)),
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 8),
                      child: GestureDetector(
                        onTap: () {
                          if (_onBackChoice == 2) {
                            setState(() {
                              _onBackChoice = 0;
                            });
                            _styleSet.remove(Strings.onBack + " " + Strings.r);
                          } else {
                            setState(() {
                              _onBackChoice = 2;
                            });
                            _styleSet.add(Strings.onBack + " " + Strings.r);
                            _styleSet.remove(Strings.onBack + " " + Strings.p);
                            _styleSet.remove(Strings.onBack + " " + Strings.n);
                          }
                        },
                        child: Container(
                          height: 36,
                          width: 36,
                          decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              border: Border.all(
                                  color: _onBackChoice == 2
                                      ? Color.fromRGBO(63, 113, 226, 1)
                                      : Color.fromRGBO(27, 39, 45, 0.1))),
                          child: Center(
                            child: Text(
                              Strings.r,
                              style: TextStyle(
                                  fontFamily: 'RoundedMplus',
                                  fontSize: 14.0,
                                  fontWeight: _onBackChoice == 2
                                      ? FontWeight.bold
                                      : null,
                                  color: _onBackChoice == 2
                                      ? Color.fromRGBO(63, 113, 226, 1)
                                      : Color.fromRGBO(27, 39, 45, 1)),
                            ),
                          ),
                        ),
                      ),
                    ),
                    GestureDetector(
                      onTap: () {
                        if (_onBackChoice == 3) {
                          setState(() {
                            _onBackChoice = 0;
                          });
                          _styleSet.remove(Strings.onBack + " " + Strings.n);
                        } else {
                          setState(() {
                            _onBackChoice = 3;
                          });
                          _styleSet.add(Strings.onBack + " " + Strings.n);
                          _styleSet.remove(Strings.onBack + " " + Strings.p);
                          _styleSet.remove(Strings.onBack + " " + Strings.r);
                        }
                      },
                      child: Container(
                        height: 36,
                        width: 36,
                        decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            border: Border.all(
                                color: _onBackChoice == 3
                                    ? Color.fromRGBO(63, 113, 226, 1)
                                    : Color.fromRGBO(27, 39, 45, 0.1))),
                        child: Center(
                          child: Text(
                            Strings.n,
                            style: TextStyle(
                                fontFamily: 'RoundedMplus',
                                fontSize: 14.0,
                                fontWeight:
                                    _onBackChoice == 3 ? FontWeight.bold : null,
                                color: _onBackChoice == 3
                                    ? Color.fromRGBO(63, 113, 226, 1)
                                    : Color.fromRGBO(27, 39, 45, 1)),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ],
        ),
        Column(
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(bottom: 8),
              child: Row(
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.only(right: 8),
                    child: Container(
                      height: 24,
                      width: 24,
                      decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          border: Border.all(
                              color: Color.fromRGBO(27, 39, 45, 0.1))),
                      child: Center(
                        child: Text(
                          Strings.p,
                          style: TextStyle(
                              fontFamily: 'RoundedMplus',
                              fontSize: 14.0,
                              color: Color.fromRGBO(27, 39, 45, 0.56)),
                        ),
                      ),
                    ),
                  ),
                  Text(
                    Strings.fullTraining,
                    style: TextStyle(
                        fontFamily: 'RoundedMplus',
                        fontSize: 14.0,
                        color: Color.fromRGBO(27, 39, 45, 0.56)),
                  )
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.only(bottom: 8),
              child: Row(
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.only(right: 8),
                    child: Container(
                      height: 24,
                      width: 24,
                      decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          border: Border.all(
                              color: Color.fromRGBO(27, 39, 45, 0.1))),
                      child: Center(
                        child: Text(
                          Strings.r,
                          style: TextStyle(
                              fontFamily: 'RoundedMplus',
                              fontSize: 14.0,
                              color: Color.fromRGBO(27, 39, 45, 0.56)),
                        ),
                      ),
                    ),
                  ),
                  Text(
                    Strings.armTraining,
                    style: TextStyle(
                        fontFamily: 'RoundedMplus',
                        fontSize: 14.0,
                        color: Color.fromRGBO(27, 39, 45, 0.56)),
                  )
                ],
              ),
            ),
            Row(
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.only(right: 8),
                  child: Container(
                    height: 24,
                    width: 24,
                    decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        border:
                            Border.all(color: Color.fromRGBO(27, 39, 45, 0.1))),
                    child: Center(
                      child: Text(
                        Strings.n,
                        style: TextStyle(
                            fontFamily: 'RoundedMplus',
                            fontSize: 14.0,
                            color: Color.fromRGBO(27, 39, 45, 0.56)),
                      ),
                    ),
                  ),
                ),
                Text(
                  Strings.legTraining,
                  style: TextStyle(
                      fontFamily: 'RoundedMplus',
                      fontSize: 14.0,
                      color: Color.fromRGBO(27, 39, 45, 0.56)),
                )
              ],
            )
          ],
        ),
        Row(
          children: <Widget>[
            Expanded(
                child: Padding(
                    padding: EdgeInsets.only(right: 4, top: 4, bottom: 4),
                    child: Container(
                      decoration: BoxDecoration(
                          border: Border.all(
                              color: Color.fromRGBO(27, 39, 45, 0.1)),
                          borderRadius: BorderRadius.circular(22.0)),
                      child: FlatButton(
                        shape: StadiumBorder(),
                        onPressed: () {
                          _pageCreateTrainingController.animateToPage(1,
                              duration: Duration(milliseconds: 500),
                              curve: Curves.linear);
                        },
                        child: Text(Strings.back,
                            style: TextStyle(
                                fontFamily: 'RoundedMplusMedium',
                                fontSize: 14.0,
                                fontWeight: FontWeight.w500,
                                color: Color.fromRGBO(27, 39, 45, 0.75))),
                      ),
                    ))),
            Expanded(
                child: Padding(
                    padding: EdgeInsets.only(left: 4, top: 4, bottom: 4),
                    child: Container(
                      decoration: BoxDecoration(
                          gradient: LinearGradient(
                            colors: [
                              Color.fromRGBO(86, 155, 232, 1),
                              Color.fromRGBO(64, 114, 226, 1)
                            ],
                            begin: Alignment.bottomRight,
                            end: Alignment.topLeft,
                          ),
                          borderRadius: BorderRadius.circular(22.0)),
                      child: FlatButton(
                        shape: StadiumBorder(),
                        onPressed: () {
                          if (_styleSet.isEmpty) {
                            Toast.show(Strings.styleValidation, context);
                          } else
                            _pageCreateTrainingController.animateToPage(3,
                                duration: Duration(milliseconds: 500),
                                curve: Curves.linear);
                        },
                        child: Text(
                          Strings.nextStep,
                          style: TextStyle(
                              fontFamily: 'RoundedMplusMedium',
                              fontSize: 14.0,
                              fontWeight: FontWeight.w500,
                              color: Colors.white),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ))),
          ],
        ),
      ],
    );
  }

  Widget _inventoryChoiceWidget() {
    return Stack(
      children: <Widget>[
        ListView(
          children: <Widget>[
            Text(
              Strings.inventory,
              textAlign: TextAlign.center,
              style: TextStyle(
                  fontFamily: 'RoundedMplusMedium',
                  fontSize: 20.0,
                  fontWeight: FontWeight.w500,
                  color: Color.fromRGBO(63, 113, 226, 1)),
            ),
            FlatButton(
              onPressed: () {
                setState(() {
                  if (_board) {
                    _board = false;
                    _inventorySet.remove(Strings.board);
                    print(_inventorySet);
                  } else {
                    _board = true;
                    _inventorySet.add(Strings.board);
                    print(_inventorySet);
                  }
                });
              },
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(Strings.board,
                      style: TextStyle(
                          fontFamily: 'RoundedMplus',
                          fontSize: 14.0,
                          color: Color.fromRGBO(27, 39, 45, 0.75))),
                  Visibility(
                    visible: _board,
                    child: Icon(Icons.check,
                        color: Color.fromRGBO(63, 113, 226, 1)),
                  )
                ],
              ),
            ),
            Divider(color: Color.fromRGBO(27, 39, 45, 0.1)),
            FlatButton(
              onPressed: () {
                setState(() {
                  if (_kolobashka) {
                    _kolobashka = false;
                    _inventorySet.remove(Strings.kolobashka);
                  } else {
                    _kolobashka = true;
                    _inventorySet.add(Strings.kolobashka);
                  }
                });
              },
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(Strings.kolobashka,
                      style: TextStyle(
                          fontFamily: 'RoundedMplus',
                          fontSize: 14.0,
                          color: Color.fromRGBO(27, 39, 45, 0.75))),
                  Visibility(
                    visible: _kolobashka,
                    child: Icon(Icons.check,
                        color: Color.fromRGBO(63, 113, 226, 1)),
                  )
                ],
              ),
            ),
            Divider(color: Color.fromRGBO(27, 39, 45, 0.1)),
            FlatButton(
              onPressed: () {
                setState(() {
                  if (_flippers) {
                    _flippers = false;
                    _inventorySet.remove(Strings.flippers);
                  } else {
                    _flippers = true;
                    _inventorySet.add(Strings.flippers);
                  }
                });
              },
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(Strings.flippers,
                      style: TextStyle(
                          fontFamily: 'RoundedMplus',
                          fontSize: 14.0,
                          color: Color.fromRGBO(27, 39, 45, 0.75))),
                  Visibility(
                    visible: _flippers,
                    child: Icon(Icons.check,
                        color: Color.fromRGBO(63, 113, 226, 1)),
                  )
                ],
              ),
            ),
            Divider(color: Color.fromRGBO(27, 39, 45, 0.1)),
            FlatButton(
              onPressed: () {
                setState(() {
                  if (_paddle) {
                    _paddle = false;
                    _inventorySet.remove(Strings.paddle);
                  } else {
                    _paddle = true;
                    _inventorySet.add(Strings.paddle);
                  }
                });
              },
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(Strings.paddle,
                      style: TextStyle(
                          fontFamily: 'RoundedMplus',
                          fontSize: 14.0,
                          color: Color.fromRGBO(27, 39, 45, 0.75))),
                  Visibility(
                    visible: _paddle,
                    child: Icon(Icons.check,
                        color: Color.fromRGBO(63, 113, 226, 1)),
                  )
                ],
              ),
            ),
            Divider(color: Color.fromRGBO(27, 39, 45, 0.1)),
            FlatButton(
              onPressed: () {
                setState(() {
                  if (_brake) {
                    _brake = false;
                    _inventorySet.remove(Strings.brake);
                  } else {
                    _brake = true;
                    _inventorySet.add(Strings.brake);
                  }
                });
              },
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(Strings.brake,
                      style: TextStyle(
                          fontFamily: 'RoundedMplus',
                          fontSize: 14.0,
                          color: Color.fromRGBO(27, 39, 45, 0.75))),
                  Visibility(
                    visible: _brake,
                    child: Icon(Icons.check,
                        color: Color.fromRGBO(63, 113, 226, 1)),
                  )
                ],
              ),
            ),
            Divider(color: Color.fromRGBO(27, 39, 45, 0.1)),
            FlatButton(
              onPressed: () {
                setState(() {
                  if (_pipe) {
                    _pipe = false;
                    _inventorySet.remove(Strings.pipe);
                  } else {
                    _pipe = true;
                    _inventorySet.add(Strings.pipe);
                  }
                });
              },
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(Strings.pipe,
                      style: TextStyle(
                          fontFamily: 'RoundedMplus',
                          fontSize: 14.0,
                          color: Color.fromRGBO(27, 39, 45, 0.75))),
                  Visibility(
                    visible: _pipe,
                    child: Icon(Icons.check,
                        color: Color.fromRGBO(63, 113, 226, 1)),
                  )
                ],
              ),
            ),
            Divider(color: Color.fromRGBO(27, 39, 45, 0.1)),
            FlatButton(
              onPressed: () {
                setState(() {
                  if (_pipes) {
                    _pipes = false;
                    _inventorySet.remove(Strings.pipes);
                  } else {
                    _pipes = true;
                    _inventorySet.add(Strings.pipes);
                  }
                });
              },
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(Strings.pipes,
                      style: TextStyle(
                          fontFamily: 'RoundedMplus',
                          fontSize: 14.0,
                          color: Color.fromRGBO(27, 39, 45, 0.75))),
                  Visibility(
                    visible: _pipes,
                    child: Icon(Icons.check,
                        color: Color.fromRGBO(63, 113, 226, 1)),
                  )
                ],
              ),
            ),
            SizedBox(
              height: 60,
            )
          ],
        ),
        Container(
          alignment: Alignment.bottomCenter,
          child: Container(
            decoration: BoxDecoration(
              gradient: LinearGradient(
                colors: [Colors.white30, Colors.white],
                begin: Alignment(0.0, -0.9),
                end: Alignment.bottomCenter,
              ),
            ),
            height: 56,
            alignment: Alignment.bottomCenter,
            child: Row(
              children: <Widget>[
                Expanded(
                    child: Padding(
                        padding: EdgeInsets.only(right: 4, top: 4, bottom: 4),
                        child: Container(
                          decoration: BoxDecoration(
                              color: Colors.white,
                              border: Border.all(
                                  color: Color.fromRGBO(27, 39, 45, 0.1)),
                              borderRadius: BorderRadius.circular(22.0)),
                          child: FlatButton(
                            shape: StadiumBorder(),
                            onPressed: () {
                              _pageCreateTrainingController.animateToPage(2,
                                  duration: Duration(milliseconds: 500),
                                  curve: Curves.linear);
                            },
                            child: Text(Strings.back,
                                style: TextStyle(
                                    fontFamily: 'RoundedMplusMedium',
                                    fontSize: 14.0,
                                    fontWeight: FontWeight.w500,
                                    color: Color.fromRGBO(27, 39, 45, 0.75))),
                          ),
                        ))),
                Expanded(
                    child: Padding(
                        padding: EdgeInsets.only(left: 4, top: 4, bottom: 4),
                        child: Container(
                          decoration: BoxDecoration(
                              gradient: LinearGradient(
                                colors: [
                                  Color.fromRGBO(86, 155, 232, 1),
                                  Color.fromRGBO(64, 114, 226, 1)
                                ],
                                begin: Alignment.bottomRight,
                                end: Alignment.topLeft,
                              ),
                              borderRadius: BorderRadius.circular(22.0)),
                          child: FlatButton(
                            shape: StadiumBorder(),
                            onPressed: () {
                              _pageCreateTrainingController.animateToPage(4,
                                  duration: Duration(milliseconds: 500),
                                  curve: Curves.linear);
                            },
                            child: Text(
                              Strings.nextStep,
                              style: TextStyle(
                                  fontFamily: 'RoundedMplusMedium',
                                  fontSize: 14.0,
                                  fontWeight: FontWeight.w500,
                                  color: Colors.white),
                              textAlign: TextAlign.center,
                            ),
                          ),
                        ))),
              ],
            ),
          ),
        ),
      ],
    );
  }

  Widget _newDistWidget() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      mainAxisSize: MainAxisSize.max,
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(top: 36),
          child: Text(Strings.addRestExercise,
              style: TextStyle(
                  fontFamily: 'RoundedMplus',
                  fontSize: 14.0,
                  color: Color.fromRGBO(27, 39, 45, 0.75))),
        ),
        SvgPicture.asset('images/swimmer_training.svg'), //поменять картинку
        Column(
          children: <Widget>[
            Row(
              children: <Widget>[
                Expanded(
                  child: Padding(
                      padding: EdgeInsets.only(right: 4, top: 4, bottom: 4),
                      child: Container(
                        decoration: BoxDecoration(
                            border: Border.all(
                                color: Color.fromRGBO(27, 39, 45, 0.1)),
                            borderRadius: BorderRadius.circular(22.0)),
                        child: FlatButton(
                          shape: StadiumBorder(),
                          onPressed: () {
                            _pageCreateTrainingController.animateToPage(5,
                                duration: Duration(milliseconds: 500),
                                curve: Curves.linear);
                            _distStyleSet.add(_distCount.toString() +
                                _half +
                                " " +
                                _styleSet.join(', ') +
                                '${_inventorySet.isEmpty ? "" : "\n" + _inventorySet.join(", ")[0].toUpperCase() + _inventorySet.join(", ").substring(1).toLowerCase()}');
                          },
                          child: Text(Strings.rest,
                              style: TextStyle(
                                  fontFamily: 'RoundedMplusMedium',
                                  fontSize: 20.0,
                                  fontWeight: FontWeight.w500,
                                  color: Color.fromRGBO(63, 113, 226, 1))),
                        ),
                      )),
                )
              ],
            ),
            Row(
              children: <Widget>[
                Expanded(
                  child: Padding(
                      padding: EdgeInsets.only(right: 4, top: 4, bottom: 4),
                      child: Container(
                        decoration: BoxDecoration(
                            border: Border.all(
                                color: Color.fromRGBO(27, 39, 45, 0.1)),
                            borderRadius: BorderRadius.circular(22.0)),
                        child: FlatButton(
                          shape: StadiumBorder(),
                          onPressed: () {
                            _pageCreateTrainingController.animateToPage(1,
                                duration: Duration(milliseconds: 500),
                                curve: Curves.linear);
                            _distStyleSet.add(_distCount.toString() +
                                _half +
                                " " +
                                _styleSet.join(', ') +
                                '${_inventorySet.isEmpty ? "" : "\n" + _inventorySet.join(", ")[0].toUpperCase() + _inventorySet.join(", ").substring(1).toLowerCase()}');
                            setState(() {
                              _distCount = 0;
                              _half = "";
                              _board = false;
                              _kolobashka = false;
                              _flippers = false;
                              _paddle = false;
                              _brake = false;
                              _pipe = false;
                              _pipes = false;
                              _inventorySet.clear();

                              _breakstrokeChoice = 0;
                              _dolfineChoice = 0;
                              _krollChoice = 0;
                              _onBackChoice = 0;
                              _styleSet.clear();
                            });
                            //_clearData();
                          },
                          child: Text(Strings.exercise,
                              style: TextStyle(
                                  fontFamily: 'RoundedMplusMedium',
                                  fontSize: 20.0,
                                  fontWeight: FontWeight.w500,
                                  color: Color.fromRGBO(63, 113, 226, 1))),
                        ),
                      )),
                )
              ],
            )
          ],
        ),
        Row(
          children: <Widget>[
            Expanded(
                child: Padding(
                    padding: EdgeInsets.only(right: 4, top: 4, bottom: 4),
                    child: Container(
                      decoration: BoxDecoration(
                          border: Border.all(
                              color: Color.fromRGBO(27, 39, 45, 0.1)),
                          borderRadius: BorderRadius.circular(22.0)),
                      child: FlatButton(
                        shape: StadiumBorder(),
                        onPressed: () {
                          _pageCreateTrainingController.animateToPage(3,
                              duration: Duration(milliseconds: 500),
                              curve: Curves.linear);
                        },
                        child: Text(Strings.back,
                            style: TextStyle(
                                fontFamily: 'RoundedMplusMedium',
                                fontSize: 14.0,
                                fontWeight: FontWeight.w500,
                                color: Color.fromRGBO(27, 39, 45, 0.75))),
                      ),
                    ))),
            Expanded(
                child: Padding(
                    padding: EdgeInsets.only(left: 4, top: 4, bottom: 4),
                    child: Container(
                      decoration: BoxDecoration(
                          gradient: LinearGradient(
                            colors: [
                              Color.fromRGBO(86, 155, 232, 1),
                              Color.fromRGBO(64, 114, 226, 1)
                            ],
                            begin: Alignment.bottomRight,
                            end: Alignment.topLeft,
                          ),
                          borderRadius: BorderRadius.circular(22.0)),
                      child: FlatButton(
                        shape: StadiumBorder(),
                        onPressed: () {
                          _pageCreateTrainingController.animateToPage(6,
                              duration: Duration(milliseconds: 500),
                              curve: Curves.linear);
                          _distStyleSet.add(_distCount.toString() +
                              _half +
                              " " +
                              _styleSet.join(', ') +
                              '${_inventorySet.isEmpty ? "" : "\n" + _inventorySet.join(", ")[0].toUpperCase() + _inventorySet.join(", ").substring(1).toLowerCase()}');
                        },
                        child: Text(
                          Strings.skip,
                          style: TextStyle(
                              fontFamily: 'RoundedMplusMedium',
                              fontSize: 14.0,
                              fontWeight: FontWeight.w500,
                              color: Colors.white),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ))),
          ],
        ),
      ],
    );
  }

  Widget _restSeconds() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      mainAxisSize: MainAxisSize.max,
      children: <Widget>[
        Column(
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(top: 68),
              child: Text(_restSecondsCount.toString(),
                  style: TextStyle(
                      fontFamily: 'RoundedMplus',
                      fontSize: 60.0,
                      height: 1.33,
                      color: Color.fromRGBO(63, 113, 226, 1))),
            ),
            Text(Strings.restSec,
                style: TextStyle(
                    fontFamily: 'RoundedMplusMedium',
                    fontSize: 20.0,
                    //height: 1.33,
                    fontWeight: FontWeight.w500,
                    color: Color.fromRGBO(63, 113, 226, 1))),
          ],
        ),
        Column(
          children: <Widget>[
            Row(
              children: <Widget>[
                Expanded(
                    child: Padding(
                        padding: EdgeInsets.only(right: 5, bottom: 5),
                        child: Container(
                          decoration: BoxDecoration(
                              border: Border.all(
                                  color: Color.fromRGBO(27, 39, 45, 0.1)),
                              borderRadius: BorderRadius.circular(22.0)),
                          child: FlatButton(
                            shape: StadiumBorder(),
                            onPressed: () {
                              setState(() {
                                _restSecondsCount = 10;
                              });
                            },
                            child: Text(Strings.distCount13,
                                style: TextStyle(
                                    fontFamily: 'RoundedMplus',
                                    fontSize: 16.0,
                                    color: Color.fromRGBO(27, 39, 45, 1))),
                          ),
                        ))),
                Expanded(
                    child: Padding(
                        padding: EdgeInsets.only(right: 5, left: 5, bottom: 5),
                        child: Container(
                          decoration: BoxDecoration(
                              border: Border.all(
                                  color: Color.fromRGBO(27, 39, 45, 0.1)),
                              borderRadius: BorderRadius.circular(22.0)),
                          child: FlatButton(
                            shape: StadiumBorder(),
                            onPressed: () {
                              setState(() {
                                _restSecondsCount = 15;
                              });
                            },
                            child: Text(Strings.distCount7,
                                style: TextStyle(
                                    fontFamily: 'RoundedMplus',
                                    fontSize: 16.0,
                                    color: Color.fromRGBO(27, 39, 45, 1))),
                          ),
                        ))),
                Expanded(
                    child: Padding(
                        padding: EdgeInsets.only(left: 5, bottom: 5),
                        child: Container(
                          decoration: BoxDecoration(
                              border: Border.all(
                                  color: Color.fromRGBO(27, 39, 45, 0.1)),
                              borderRadius: BorderRadius.circular(22.0)),
                          child: FlatButton(
                            shape: StadiumBorder(),
                            onPressed: () {
                              setState(() {
                                _restSecondsCount = 20;
                              });
                            },
                            child: Text(Strings.distCount8,
                                style: TextStyle(
                                    fontFamily: 'RoundedMplus',
                                    fontSize: 16.0,
                                    color: Color.fromRGBO(27, 39, 45, 1))),
                          ),
                        ))),
              ],
            ),
            Row(
              children: <Widget>[
                Expanded(
                    child: Padding(
                        padding: EdgeInsets.only(right: 5, top: 5, bottom: 10),
                        child: Container(
                          decoration: BoxDecoration(
                              border: Border.all(
                                  color: Color.fromRGBO(27, 39, 45, 0.1)),
                              borderRadius: BorderRadius.circular(22.0)),
                          child: FlatButton(
                            shape: StadiumBorder(),
                            onPressed: () {
                              setState(() {
                                _restSecondsCount = 30;
                              });
                            },
                            child: Text(Strings.distCount9,
                                style: TextStyle(
                                    fontFamily: 'RoundedMplus',
                                    fontSize: 16.0,
                                    color: Color.fromRGBO(27, 39, 45, 1))),
                          ),
                        ))),
                Expanded(
                    child: Padding(
                        padding: EdgeInsets.only(
                            right: 5, left: 5, top: 5, bottom: 10),
                        child: Container(
                          decoration: BoxDecoration(
                              border: Border.all(
                                  color: Color.fromRGBO(27, 39, 45, 0.1)),
                              borderRadius: BorderRadius.circular(22.0)),
                          child: FlatButton(
                            shape: StadiumBorder(),
                            onPressed: () {
                              setState(() {
                                _restSecondsCount = 60;
                              });
                            },
                            child: Text(Strings.distCount10,
                                style: TextStyle(
                                    fontFamily: 'RoundedMplus',
                                    fontSize: 16.0,
                                    color: Color.fromRGBO(27, 39, 45, 1))),
                          ),
                        ))),
                Expanded(
                    child: Padding(
                        padding: EdgeInsets.only(left: 5, top: 5, bottom: 10),
                        child: Container(
                          decoration: BoxDecoration(
                              border: Border.all(
                                  color: Color.fromRGBO(27, 39, 45, 0.1)),
                              borderRadius: BorderRadius.circular(22.0)),
                          child: FlatButton(
                            shape: StadiumBorder(),
                            onPressed: () {
                              setState(() {
                                _restSecondsCount = 120;
                              });
                            },
                            child: Text(Strings.distCount11,
                                style: TextStyle(
                                    fontFamily: 'RoundedMplus',
                                    fontSize: 16.0,
                                    color: Color.fromRGBO(27, 39, 45, 1))),
                          ),
                        ))),
              ],
            ),
            Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                RawMaterialButton(
                  onPressed: () {
                    setState(() {
                      if (_restSecondsCount > 0) {
                        _restSecondsCount = _restSecondsCount - 5;
                      }
                    });
                  },
                  child: Icon(
                    Icons.remove,
                    color: Color.fromRGBO(63, 113, 226, 1),
                    size: 15.0,
                  ),
                  shape: CircleBorder(
                      side: BorderSide(color: Color.fromRGBO(27, 39, 45, 0.1))),
                  elevation: 0.0,
                  padding: const EdgeInsets.all(10.0),
                ),
                Container(
                  height: 50,
                  width: 50,
                  decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      border:
                          Border.all(color: Color.fromRGBO(27, 39, 45, 0.1))),
                  child: Center(
                    child: Text(
                      Strings.distCount12,
                      style: TextStyle(
                          fontFamily: 'RoundedMplus',
                          fontSize: 16.0,
                          color: Color.fromRGBO(63, 113, 226, 1)),
                    ),
                  ),
                ),
                RawMaterialButton(
                  onPressed: () {
                    setState(() {
                      _restSecondsCount = _restSecondsCount + 5;
                    });
                  },
                  child: new Icon(
                    Icons.add,
                    color: Color.fromRGBO(63, 113, 226, 1),
                    size: 15.0,
                  ),
                  shape: CircleBorder(
                      side: BorderSide(color: Color.fromRGBO(27, 39, 45, 0.1))),
                  elevation: 0.0,
                  padding: const EdgeInsets.all(10.0),
                ),
              ],
              //круглые кнопки
            )
          ],
        ),
        Row(
          children: <Widget>[
            Expanded(
                child: Padding(
                    padding: EdgeInsets.only(right: 4, top: 4, bottom: 4),
                    child: Container(
                      decoration: BoxDecoration(
                          border: Border.all(
                              color: Color.fromRGBO(27, 39, 45, 0.1)),
                          borderRadius: BorderRadius.circular(22.0)),
                      child: FlatButton(
                        shape: StadiumBorder(),
                        onPressed: () {
                          _pageCreateTrainingController.animateToPage(4,
                              duration: Duration(milliseconds: 500),
                              curve: Curves.linear);
                        },
                        child: Text(Strings.back,
                            style: TextStyle(
                                fontFamily: 'RoundedMplusMedium',
                                fontSize: 14.0,
                                fontWeight: FontWeight.w500,
                                color: Color.fromRGBO(27, 39, 45, 0.75))),
                      ),
                    ))),
            Expanded(
                child: Padding(
                    padding: EdgeInsets.only(left: 4, top: 4, bottom: 4),
                    child: Container(
                      decoration: BoxDecoration(
                          gradient: LinearGradient(
                            colors: [
                              Color.fromRGBO(86, 155, 232, 1),
                              Color.fromRGBO(64, 114, 226, 1)
                            ],
                            begin: Alignment.bottomRight,
                            end: Alignment.topLeft,
                          ),
                          borderRadius: BorderRadius.circular(22.0)),
                      child: FlatButton(
                        shape: StadiumBorder(),
                        onPressed: () {
                          _pageCreateTrainingController.animateToPage(6,
                              duration: Duration(milliseconds: 500),
                              curve: Curves.linear);
                        },
                        child: Text(
                          Strings.nextStep,
                          style: TextStyle(
                              fontFamily: 'RoundedMplusMedium',
                              fontSize: 14.0,
                              fontWeight: FontWeight.w500,
                              color: Colors.white),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ))),
          ],
        ),
      ],
    );
  }

  Widget _repetitionCount() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      mainAxisSize: MainAxisSize.max,
      children: <Widget>[
        Column(
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(top: 68),
              child: Text(_repititions.toString(),
                  style: TextStyle(
                      fontFamily: 'RoundedMplus',
                      fontSize: 60.0,
                      height: 1.33,
                      color: Color.fromRGBO(63, 113, 226, 1))),
            ),
            Text(Strings.repetitionCount,
                style: TextStyle(
                    fontFamily: 'RoundedMplusMedium',
                    fontSize: 20.0,
                    //height: 1.33,
                    fontWeight: FontWeight.w500,
                    color: Color.fromRGBO(63, 113, 226, 1))),
          ],
        ),
        Column(
          children: <Widget>[
            Row(
              children: <Widget>[
                Expanded(
                    child: Padding(
                        padding: EdgeInsets.only(right: 5, bottom: 5),
                        child: Container(
                          decoration: BoxDecoration(
                              border: Border.all(
                                  color: Color.fromRGBO(27, 39, 45, 0.1)),
                              borderRadius: BorderRadius.circular(22.0)),
                          child: FlatButton(
                            shape: StadiumBorder(),
                            onPressed: () {
                              setState(() {
                                _repititions = 1;
                              });
                            },
                            child: Text(Strings.repetition1,
                                style: TextStyle(
                                    fontFamily: 'RoundedMplus',
                                    fontSize: 16.0,
                                    color: Color.fromRGBO(27, 39, 45, 1))),
                          ),
                        ))),
                Expanded(
                    child: Padding(
                        padding: EdgeInsets.only(right: 5, left: 5, bottom: 5),
                        child: Container(
                          decoration: BoxDecoration(
                              border: Border.all(
                                  color: Color.fromRGBO(27, 39, 45, 0.1)),
                              borderRadius: BorderRadius.circular(22.0)),
                          child: FlatButton(
                            shape: StadiumBorder(),
                            onPressed: () {
                              setState(() {
                                _repititions = 2;
                              });
                            },
                            child: Text(Strings.repetition2,
                                style: TextStyle(
                                    fontFamily: 'RoundedMplus',
                                    fontSize: 16.0,
                                    color: Color.fromRGBO(27, 39, 45, 1))),
                          ),
                        ))),
                Expanded(
                    child: Padding(
                        padding: EdgeInsets.only(left: 5, bottom: 5),
                        child: Container(
                          decoration: BoxDecoration(
                              border: Border.all(
                                  color: Color.fromRGBO(27, 39, 45, 0.1)),
                              borderRadius: BorderRadius.circular(22.0)),
                          child: FlatButton(
                            shape: StadiumBorder(),
                            onPressed: () {
                              setState(() {
                                _repititions = 3;
                              });
                            },
                            child: Text(Strings.repetition3,
                                style: TextStyle(
                                    fontFamily: 'RoundedMplus',
                                    fontSize: 16.0,
                                    color: Color.fromRGBO(27, 39, 45, 1))),
                          ),
                        ))),
              ],
            ),
            Row(
              children: <Widget>[
                Expanded(
                    child: Padding(
                        padding: EdgeInsets.only(right: 5, top: 5, bottom: 10),
                        child: Container(
                          decoration: BoxDecoration(
                              border: Border.all(
                                  color: Color.fromRGBO(27, 39, 45, 0.1)),
                              borderRadius: BorderRadius.circular(22.0)),
                          child: FlatButton(
                            shape: StadiumBorder(),
                            onPressed: () {
                              setState(() {
                                _repititions = 4;
                              });
                            },
                            child: Text(Strings.repetition4,
                                style: TextStyle(
                                    fontFamily: 'RoundedMplus',
                                    fontSize: 16.0,
                                    color: Color.fromRGBO(27, 39, 45, 1))),
                          ),
                        ))),
                Expanded(
                    child: Padding(
                        padding: EdgeInsets.only(
                            right: 5, left: 5, top: 5, bottom: 10),
                        child: Container(
                          decoration: BoxDecoration(
                              border: Border.all(
                                  color: Color.fromRGBO(27, 39, 45, 0.1)),
                              borderRadius: BorderRadius.circular(22.0)),
                          child: FlatButton(
                            shape: StadiumBorder(),
                            onPressed: () {
                              setState(() {
                                _repititions = 5;
                              });
                            },
                            child: Text(Strings.repetition5,
                                style: TextStyle(
                                    fontFamily: 'RoundedMplus',
                                    fontSize: 16.0,
                                    color: Color.fromRGBO(27, 39, 45, 1))),
                          ),
                        ))),
                Expanded(
                    child: Padding(
                        padding: EdgeInsets.only(left: 5, top: 5, bottom: 10),
                        child: Container(
                          decoration: BoxDecoration(
                              border: Border.all(
                                  color: Color.fromRGBO(27, 39, 45, 0.1)),
                              borderRadius: BorderRadius.circular(22.0)),
                          child: FlatButton(
                            shape: StadiumBorder(),
                            onPressed: () {
                              setState(() {
                                _repititions = 6;
                              });
                            },
                            child: Text(Strings.repetition6,
                                style: TextStyle(
                                    fontFamily: 'RoundedMplus',
                                    fontSize: 16.0,
                                    color: Color.fromRGBO(27, 39, 45, 1))),
                          ),
                        ))),
              ],
            ),
            Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                RawMaterialButton(
                  onPressed: () {
                    setState(() {
                      if (_repititions > 1) {
                        _repititions--;
                      }
                    });
                  },
                  child: Icon(
                    Icons.remove,
                    color: Color.fromRGBO(63, 113, 226, 1),
                    size: 15.0,
                  ),
                  shape: CircleBorder(
                      side: BorderSide(color: Color.fromRGBO(27, 39, 45, 0.1))),
                  elevation: 0.0,
                  padding: const EdgeInsets.all(10.0),
                ),
                Container(
                  height: 50,
                  width: 50,
                  decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      border:
                          Border.all(color: Color.fromRGBO(27, 39, 45, 0.1))),
                  child: Center(
                    child: Text(
                      Strings.repetition1,
                      style: TextStyle(
                          fontFamily: 'RoundedMplus',
                          fontSize: 16.0,
                          color: Color.fromRGBO(63, 113, 226, 1)),
                    ),
                  ),
                ),
                RawMaterialButton(
                  onPressed: () {
                    setState(() {
                      if (_repititions == 99) {
                        _repititions = 99;
                      } else
                        _repititions++;
                    });
                  },
                  child: new Icon(
                    Icons.add,
                    color: Color.fromRGBO(63, 113, 226, 1),
                    size: 15.0,
                  ),
                  shape: CircleBorder(
                      side: BorderSide(color: Color.fromRGBO(27, 39, 45, 0.1))),
                  elevation: 0.0,
                  padding: const EdgeInsets.all(10.0),
                ),
              ],
            )
          ],
        ),
        Row(
          children: <Widget>[
            Expanded(
                child: Padding(
                    padding: EdgeInsets.only(right: 4, top: 4, bottom: 4),
                    child: Container(
                      decoration: BoxDecoration(
                          border: Border.all(
                              color: Color.fromRGBO(27, 39, 45, 0.1)),
                          borderRadius: BorderRadius.circular(22.0)),
                      child: FlatButton(
                        shape: StadiumBorder(),
                        onPressed: () {
                          _pageCreateTrainingController.animateToPage(5,
                              duration: Duration(milliseconds: 500),
                              curve: Curves.linear);
                        },
                        child: Text(Strings.back,
                            style: TextStyle(
                                fontFamily: 'RoundedMplusMedium',
                                fontSize: 14.0,
                                fontWeight: FontWeight.w500,
                                color: Color.fromRGBO(27, 39, 45, 0.75))),
                      ),
                    ))),
            Expanded(
                child: Padding(
                    padding: EdgeInsets.only(left: 4, top: 4, bottom: 4),
                    child: Container(
                      decoration: BoxDecoration(
                          gradient: LinearGradient(
                            colors: [
                              Color.fromRGBO(86, 155, 232, 1),
                              Color.fromRGBO(64, 114, 226, 1)
                            ],
                            begin: Alignment.bottomRight,
                            end: Alignment.topLeft,
                          ),
                          borderRadius: BorderRadius.circular(22.0)),
                      child: FlatButton(
                        shape: StadiumBorder(),
                        onPressed: () {
                          _pageCreateTrainingController.animateToPage(7,
                              duration: Duration(milliseconds: 500),
                              curve: Curves.linear);
                        },
                        child: Text(
                          Strings.nextStep,
                          style: TextStyle(
                              fontFamily: 'RoundedMplusMedium',
                              fontSize: 14.0,
                              fontWeight: FontWeight.w500,
                              color: Colors.white),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ))),
          ],
        ),
      ],
    );
  }

  Widget _addRestExercise() {
    /*Event event = Event(
      title: _editingNameController.text,
      description: _description,
      location: Strings.appTitle,
      startDate: DateTime.parse(_dateOfTraining),
      endDate: DateTime.parse(_dateOfTraining).add(Duration(hours: 20)),
      allDay: false,
    );*/

    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      mainAxisSize: MainAxisSize.max,
      children: <Widget>[
        SizedBox.shrink(),
        SvgPicture.asset('images/swimmer_training.svg'),
        Column(
          children: <Widget>[
            Container(
              height: 50,
              width: 50,
              decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  border: Border.all(color: Color.fromRGBO(27, 39, 45, 0.1))),
              child: Center(
                child: IconButton(
                    icon:
                        Icon(Icons.add, color: Color.fromRGBO(63, 113, 226, 1)),
                    onPressed: () {
                      _addDataToExerciseList();
                      _pageCreateTrainingController.animateToPage(1,
                          duration: Duration(milliseconds: 500),
                          curve: Curves.linear);
                      print(_exerciseList);
                      print(_inventorySet);
                      _clearData();
                    }),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 24),
              child: Text(
                Strings.addRestOrExercise,
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontFamily: 'RoundedMplus',
                    fontSize: 14.0,
                    color: Color.fromRGBO(27, 39, 45, 0.75)),
              ),
            ),
          ],
        ),
        Row(
          children: <Widget>[
            Expanded(
                child: Padding(
                    padding: EdgeInsets.only(right: 4, top: 4, bottom: 4),
                    child: Container(
                      decoration: BoxDecoration(
                          border: Border.all(
                              color: Color.fromRGBO(27, 39, 45, 0.1)),
                          borderRadius: BorderRadius.circular(22.0)),
                      child: FlatButton(
                        shape: StadiumBorder(),
                        onPressed: () {
                          _pageCreateTrainingController.animateToPage(6,
                              duration: Duration(milliseconds: 500),
                              curve: Curves.linear);
                        },
                        child: Text(Strings.back,
                            style: TextStyle(
                                fontFamily: 'RoundedMplusMedium',
                                fontSize: 14.0,
                                fontWeight: FontWeight.w500,
                                color: Color.fromRGBO(27, 39, 45, 0.75))),
                      ),
                    ))),
            Expanded(
                child: Padding(
                    padding: EdgeInsets.only(left: 4, top: 4, bottom: 4),
                    child: Container(
                      decoration: BoxDecoration(
                          gradient: LinearGradient(
                            colors: [
                              Color.fromRGBO(86, 155, 232, 1),
                              Color.fromRGBO(64, 114, 226, 1)
                            ],
                            begin: Alignment.bottomRight,
                            end: Alignment.topLeft,
                          ),
                          borderRadius: BorderRadius.circular(22.0)),
                      child: FlatButton(
                        shape: StadiumBorder(),
                        onPressed: () async {
                          //Add2Calendar.addEvent2Cal(event);
                          AddToCalendar.addToCalendar(
                            title: _editingNameController.text,
                            description: _description,
                            location: Strings.appTitle,
                            startTime: DateTime.parse(DateFormat("yyyyMMdd")
                                .format(DateTime.parse(
                                    _dateOfTraining))), //DateTime.parse(_dateOfTraining),
                            endTime: null,
                            isAllDay: true,
                          );

                          _addDataToExerciseList();
                          _handleSubmit();
                          _toTrainingList();
                          _clearData();
                          _inventorySet.clear();
                          print(_exerciseList);
                        },
                        child: Text(
                          Strings.close,
                          style: TextStyle(
                              fontFamily: 'RoundedMplusMedium',
                              fontSize: 14.0,
                              fontWeight: FontWeight.w500,
                              color: Colors.white),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ))),
          ],
        ),
      ],
    );
  }

  _showDialog() async {
    await showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(22.0))),
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Text(Strings.cancelCreateTraining,
                    style: TextStyle(
                        fontFamily: 'RoundedMplus',
                        fontSize: 14,
                        color: Color.fromRGBO(27, 39, 45, 1)),
                    textAlign: TextAlign.center),
                Padding(
                  padding: EdgeInsets.only(top: 24),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      IconButton(
                        icon: Icon(Icons.close,
                            color: Color.fromRGBO(238, 104, 104, 1)),
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                      ),
                      IconButton(
                        icon: Icon(
                          Icons.check,
                          color: Color.fromRGBO(63, 113, 226, 1),
                        ),
                        onPressed: () {
                          Navigator.of(context).pop();
                          _toTrainingList();
                        },
                      ),
                    ],
                  ),
                ),
              ],
            ),
          );
        });
  }

  _datePickerShow() {
    DatePicker.showDatePicker(context,
        showTitleActions: true,
        minTime: DateTime.now(),
        maxTime: DateTime.now().add(Duration(days: 366)),
        //DateTime(2049, 6, 7),
        theme: DatePickerTheme(
            backgroundColor: Color.fromRGBO(63, 113, 226, 1),
            itemStyle: TextStyle(
                fontFamily: 'RoundedMplus', fontSize: 16, color: Colors.white),
            doneStyle: TextStyle(
                fontFamily: 'RoundedMplus',
                color: Colors.white,
                fontSize: 16)), onChanged: (date) {
      print('change $date');
    }, onConfirm: (date) {
      _dateOfTraining = date.toString();
      print('confirm $date');
      initializeDateFormatting("ky", null);
      setState(() {
        _trainingDate = DateFormat.d().format(date).toString() +
            "." +
            '${DateFormat.M().format(date).toString().length == 1 ? "0" + DateFormat.M().format(date).toString() : DateFormat.M().format(date).toString()}' +
            "." +
            DateFormat.y().format(date).toString();
      });
    }, currentTime: DateTime.now(), locale: LocaleType.ru);
  }
}
