import 'package:flutter/material.dart';
import 'package:swim_teacher/model/auth_status.dart';
import 'package:swim_teacher/presenter/authentication.dart';
import 'package:swim_teacher/screens/not_signed_in_screen/not_signed_in.dart';
import 'package:swim_teacher/screens/training_screen/training_screen.dart';

class Trainings extends StatefulWidget {
  Trainings({Key key, this.auth}) : super(key: key);
  final BaseAuth auth;

  @override
  _TrainingsState createState() => _TrainingsState();
}

class _TrainingsState extends State<Trainings> {
  AuthStatus authStatus = AuthStatus.notSignedIn;

  @override
  void initState() {
    super.initState();
    widget.auth.currentName().then((userId) {
      setState(() {
        authStatus =
        userId != null ? AuthStatus.signedIn : AuthStatus.notSignedIn;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    switch (authStatus) {
      case AuthStatus.notSignedIn:
        return NotSignedIn();
      case AuthStatus.signedIn:
        return TrainingScreen(auth: Authentication());
    }
  }
}